APP_NAME = AirQuality

CONFIG += qt warn_on cascades10

LIBS += -lbbsystem -lbbdata -lbbplatform -lQtLocationSubset -lbbcascadesmaps -lbbdevice

include(config.pri)
