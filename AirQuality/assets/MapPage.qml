import bb.cascades 1.2
import bb.cascades.maps 1.0

Page {
    id: mapPage
    Container {
        id: mapViewContainer
        layout: DockLayout {
        }
        background: Color.Gray
        MapView {
            id: map
            horizontalAlignment: HorizontalAlignment.Center
            verticalAlignment: VerticalAlignment.Center
            altitude: 780000.0
            latitude: 50.0479
            longitude: 15.4383
        }
    }

    actions: []

    onCreationCompleted: {
        map.setCaptionGoButtonVisible(false);

        dataHelper.dataModelUpdated.connect(mapPage.onDataModelUpdated);
        console.log("MapPage connected to dataHelper.dataModelUpdated signal");
    }

    function onDataModelUpdated() {
        console.log("map page data model updated");
        fillMapWithPins();
    }
    function fillMapWithPins() {
        if (appDataModel != null) {
            console.log("map page putPins called");
            geoHelper.putPins(map2, appDataModel);
        } else {
            console.log("map page data model is null");
        }
    }

    property GroupDataModel dataModel
}
