import bb.cascades 1.2

Sheet {
    id: settingsSheet
    Page {
        titleBar: TitleBar {
            kind: TitleBarKind.FreeForm
            kindProperties: FreeFormTitleBarKindProperties {
                Container {
                    //background: Color.create("#ff00aae1")
                    leftPadding: 20
                    rightPadding: 20
                    layout: StackLayout {
                        orientation: LayoutOrientation.LeftToRight
                    }
                    Container {
                        layoutProperties: StackLayoutProperties {
                            spaceQuota: 1
                        }
                        verticalAlignment: VerticalAlignment.Center
                        Label {
                            text: qsTr("Air Quality") + " " + qsTr("Settings") + Retranslate.onLanguageChanged
                            textStyle {
                                //color: Color.White
                                fontSize: FontSize.Large
                            }
                        }
                    }
                    Container {
                        verticalAlignment: VerticalAlignment.Center
                        ActivityIndicator {
                            id: loadingActivityIndicator
                            running: dataHelper.updatingData
                            accessibility.name: "Update data activity indicator"
                        }
                    }
                }
            }
        }

        content: ScrollView {
            Container {
                leftPadding: 15.0
                rightPadding: 15.0
                Container {
                    layout: StackLayout {
                        orientation: LayoutOrientation.LeftToRight
                    }
                    Container {
                        topPadding: 30.0
                        layoutProperties: StackLayoutProperties {
                            spaceQuota: 1
                        }
                        layout: DockLayout {

                        }
                        horizontalAlignment: HorizontalAlignment.Fill
                        Label {
                            text: qsTr("Notifications")
                            verticalAlignment: VerticalAlignment.Center
                        }
                        ToggleButton {
                            id: notificationsToggle
                            checked: dataHelper.notifications
                            onCheckedChanged: {
                                dataHelper.notifications = notificationsToggle.checked
                            }
                            horizontalAlignment: HorizontalAlignment.Right
                            accessibility.name: "Notification toggle"
                            //enabled: false
                        }
                    }
                }
                Container {
                    Label {
                        text: qsTr("Notify when the air quality is poor.")
                        textStyle.fontSize: FontSize.Small
                    }
                }
                Divider {
                    accessibility.name: "Divider"
                }
            }
            accessibility.name: "Settings shhet content"
        }
        actions: [
            ActionItem {
                title: qsTr("Done") + Retranslate.onLanguageChanged
                ActionBar.placement: ActionBarPlacement.OnBar
                imageSource: "asset:///images/icons/ic_done.png"
                onTriggered: {
                    settingsSheet.close();
                }
            }
        ]
    }
}