import bb.cascades 1.0

Sheet {
    id: aboutSheetContent
    Page {
        content: ScrollView {
            Container {
                leftPadding: 10.0
                rightPadding: 10.0
                bottomPadding: 10.0
                //logo
                Container {
                    layout: StackLayout {
                        orientation: LayoutOrientation.LeftToRight
                    }
                    Container {
                        topPadding: 40.0
                        layoutProperties: StackLayoutProperties {
                            spaceQuota: 1
                        }
                        ImageView {
                            horizontalAlignment: HorizontalAlignment.Center
                            imageSource: "asset:///images/about_logo.png"
                        }
                    }
                }
                //title
                Container {
                    layout: StackLayout {
                        orientation: LayoutOrientation.LeftToRight
                    }
                    Container {
                        topPadding: 40.0
                        layoutProperties: StackLayoutProperties {
                            spaceQuota: 1
                        }
                        Label {
                            horizontalAlignment: HorizontalAlignment.Center
                            text: qsTr("Air Quality") + Retranslate.onLanguageChanged
                            textStyle.fontSizeValue: 140.0
                            textStyle {
                                fontSize: FontSize.PercentageValue
                                fontWeight: FontWeight.W500
                            }
                        }
                    }
                }
                //version
                Container {
                    layout: StackLayout {
                        orientation: LayoutOrientation.LeftToRight
                    }
                    Container {
                        topPadding: 10.0
                        layoutProperties: StackLayoutProperties {
                            spaceQuota: 1
                        }
                        Label {
                            horizontalAlignment: HorizontalAlignment.Center
                            text: qsTr("version") + " 2.0.2" + Retranslate.onLanguageChanged
                        }
                    }
                }
                Container {
                    topPadding: 40.0
                    horizontalAlignment: HorizontalAlignment.Center
                    Label {
                        text: qsTr("Data provided by") + " <a href=\"http://www.chmi.cz\">Český hydrometeorologický ústav</a>" + Retranslate.onLanguageChanged
                        multiline: true
                        textFormat: TextFormat.Html
                        textStyle.textAlign: TextAlign.Center
                    }
                }
                Container {
                    layout: StackLayout {
                        orientation: LayoutOrientation.LeftToRight
                    }
                    Container {
                        topPadding: 40.0
                        layoutProperties: StackLayoutProperties {
                            spaceQuota: 1
                        }
                        Label {
                            horizontalAlignment: HorizontalAlignment.Center
                            text: qsTr("build by") + " <a mimetype=\"application/x-bb-appworld\" href=\"appworld://vendorpage/63768\">Martin Polovinčák</a>" + Retranslate.onLanguageChanged
                            multiline: true
                            textFormat: TextFormat.Html
                        }
                    }
                }

                Container {
                    layout: StackLayout {
                        orientation: LayoutOrientation.LeftToRight
                    }
                    Container {
                        topPadding: 40.0
                        layoutProperties: StackLayoutProperties {
                            spaceQuota: 1
                        }
                        Label {
                            horizontalAlignment: HorizontalAlignment.Center
                            text: "<a href=\"http://www.polovincak.eu/privacy_policy.html\">" + qsTr("Privacy policy") + "</a>" + Retranslate.onLanguageChanged
                            multiline: true
                            textFormat: TextFormat.Html
                        }
                    }
                }
            }
        }
        actions: [
            ActionItem {
                title: qsTr("Done") + Retranslate.onLanguageChanged
                ActionBar.placement: ActionBarPlacement.OnBar
                imageSource: "asset:///images/icons/ic_done.png"
                onTriggered: {
                    aboutSheetContent.close();
                }
            }
        ]
    }
}
