import bb.cascades 1.2

NavigationPane {
    id: navigationPane
    Page {
        id: listpage
        
        titleBar: TitleBar {
            kind: TitleBarKind.FreeForm
            kindProperties: FreeFormTitleBarKindProperties {
                Container {
                    //background: Color.create("#ff00aae1")
                    leftPadding: 20
                    rightPadding: 20
                    layout: StackLayout {
                        orientation: LayoutOrientation.LeftToRight
                    }
                    Container {
                        layoutProperties: StackLayoutProperties {
                            spaceQuota: 1
                        }
                        verticalAlignment: VerticalAlignment.Center
                        Label {
                            text: qsTr("Automated Stations List") + Retranslate.onLanguageChanged
                            textStyle {
                                //color: Color.White
                                fontSize: FontSize.Large
                            }
                        }
                    }
                    Container {
                        verticalAlignment: VerticalAlignment.Center
                        ActivityIndicator {
                            id: loadingActivityIndicator
                            running: dataHelper.updatingData
                            accessibility.name: "Update data activity indicator"
                        }
                    }
                }
            }
        }
        
        content: Container {
            id: listPageContainer
            Container {
                ListView {
                    id: stationsList
                    dataModel: appDataModel
                    listItemComponents: [
                        ListItemComponent {
                            type: "header"
                            content: Container {
                                Container {
                                    layout: StackLayout {
                                        orientation: LayoutOrientation.LeftToRight
                                    }
                                    Container {
                                        layoutProperties: StackLayoutProperties {
                                            spaceQuota: 1
                                        }
                                        leftPadding: 10.0
                                        topPadding: 5.0
                                        bottomPadding: 5.0
                                        Label {
                                            text: {
                                                if (ListItemData == "-1") {
                                                    return qsTr("Data not complete") + Retranslate.onLanguageChanged;
                                                } else if (ListItemData == "0") {
                                                    return qsTr("Not available") + Retranslate.onLanguageChanged;
                                                } else if (ListItemData == "1") {
                                                    return qsTr("Very good") + Retranslate.onLanguageChanged;
                                                } else if (ListItemData == "2") {
                                                    return qsTr("Good") + Retranslate.onLanguageChanged;
                                                } else if (ListItemData == "3") {
                                                    return qsTr("Fair") + Retranslate.onLanguageChanged;
                                                } else if (ListItemData == "4") {
                                                    return qsTr("Suitable") + Retranslate.onLanguageChanged;
                                                } else if (ListItemData == "5") {
                                                    return qsTr("Poor") + Retranslate.onLanguageChanged;
                                                } else if (ListItemData == "6") {
                                                    return qsTr("Very Poor") + Retranslate.onLanguageChanged;
                                                } else {
                                                    return ListItemData;
                                                }
                                            }
                                            textStyle {
                                                fontSize: FontSize.XSmall
                                                fontWeight: FontWeight.W500
                                            }
                                        }
                                    }
                                }
                                Container {
                                    layout: StackLayout {
                                        orientation: LayoutOrientation.LeftToRight
                                    }
                                    Container {
                                        layoutProperties: StackLayoutProperties {
                                            spaceQuota: 1
                                        }
                                        minHeight: 3.0
                                        preferredHeight: 3.0
                                        //background: Color.create("#ff3c78b1")
                                        background: Color.create("#ff0088d1")
                                    }
                                }
                            }
                        },
                        ListItemComponent {
                            type: "item"
                            content: Container {
                                id: listItemComponentContainer
                                visible: ListItemData.visible
                                Container {
                                    id: rowContentContainer
                                    layout: StackLayout {
                                        orientation: LayoutOrientation.LeftToRight
                                    }
                                    topPadding: 15.0
                                    bottomPadding: 15.0
                                    Container {
                                        leftPadding: 25.0
                                        rightPadding: 25.0
                                        verticalAlignment: VerticalAlignment.Center
                                        Container {
                                            leftPadding: 3.0
                                            rightPadding: 0.0
                                            id: aqIndicatorContainer
                                            layout: DockLayout {

                                            }
                                            ImageView {
                                                imageSource:  {
                                                    if (ListItemData.hourlyIndex == 1) {
                                                        return "asset:///images/circle_blue.png";
                                                    } else if (ListItemData.hourlyIndex == 2) {
                                                        return "asset:///images/circle_green.png";
                                                    } else if (ListItemData.hourlyIndex == 3) {
                                                        return "asset:///images/circle_yellow.png";
                                                    } else if (ListItemData.hourlyIndex == 4) {
                                                        return "asset:///images/circle_orange.png";
                                                    } else if (ListItemData.hourlyIndex == 5) {
                                                        return "asset:///images/circle_red1.png";
                                                    } else if (ListItemData.hourlyIndex == 6) {
                                                        return "asset:///images/circle_red2.png";
                                                    } else {
                                                        return "asset:///images/circle_gray.png";
                                                    }
                                                }
                                                accessibility.name: "Hourly index icon"
                                            }
                                        }
                                    }
                                    Container {
                                        id: aqItemContainer
                                        layoutProperties: StackLayoutProperties {
                                            spaceQuota: 1
                                        }
                                        //background: Color.Green
                                        Container {
                                            Label {
                                                text: ListItemData.name
                                                multiline: true
                                                textStyle {
                                                    fontSize: FontSize.PercentageValue
                                                    fontSizeValue: 120.0
                                                    fontWeight: FontWeight.W400
                                                }
                                            }
                                        }
                                        Container {
                                            Label {
                                                text: qsTr("Distance ") + ListItemData.distance + " km"
                                                textStyle {
                                                    fontSize: FontSize.PercentageValue
                                                    fontSizeValue: 70.0
                                                }
                                            }
                                        }
                                    }
                                }
                                Container {
                                    id: dividerContentContainer
                                    layout: StackLayout {
                                        orientation: LayoutOrientation.LeftToRight
                                    }
                                    Container {
                                        layoutProperties: StackLayoutProperties {
                                            spaceQuota: 1
                                        }
                                        minHeight: 2.0
                                        background: Color.create("#ffe9e9e9")
                                    }
                                }
                            }
                        }
                    ]
                    onTriggered: {
                        if (indexPath.length > 1 || searchField.text != "") {
                            var detailPageContent = detailPage.createObject();
                            detailPageContent.dataModel = dataModel;
                            detailPageContent.dataModelIndexPath = indexPath;
                            detailPageContent.deviceOrientation = deviceOrientation;
                            navigationPane.push(detailPageContent);
                        }
                    }
                    accessibility.name: "Automated measurement stations list"
                }
            }
        }

        actions: [
            ActionItem {
                title: qsTr("Update Data") + Retranslate.onLanguageChanged
                imageSource: "asset:///images/icons/ic_reload.png"
                onTriggered: {
                    dataHelper.updateSourceFile();
                }
            },
            ActionItem {
                title: {
                    if (listType == 1) {
                        return qsTr("Quality Sort");
                    } else if (listType == 2) {
                        return qsTr("Letter Sort");
                    } else {
                        console.log("Listtype is " + listType);
                    }
                }
                imageSource: "asset:///images/icons/ic_view_details.png"
                onTriggered: {
                    if ((listType + 1) > 2) {
                        listType = 1;
                    } else {
                        listType = listType + 1;
                    }
                    console.log("listType is " + listType);
                    if (listType == 2) {
                        dataModel.setSortingKeys([ "hourlyIndex", "name" ]);
                        //dataModel.sortedAscending = false;
                    } else if (listType == 1) {
                        dataModel.setSortingKeys("name");
                        //dataModel.sortedAscending = true;
                    }
                }
            }
        ]

        attachedObjects: [
            ComponentDefinition {
                id: detailPage
                source: "ListDetailPage.qml"
            },
            OrientationHandler {
                id: handler
                onOrientationChanged: {
                    console.log("ListPage orientation Changed to: " + orientation);
                    deviceOrientation = orientation;
                }
            }
        ]
    }

    onListTypeChanged: {
        dataHelper.listType = listType
    }

    onCreationCompleted: {
        if (listType == 2) {
            dataModel.setSortingKeys([ "hourlyIndex", "name" ]);
            //dataModel.sortedAscending = false;
        } else if (listType == 1) {
            dataModel.setSortingKeys("name");
            //dataModel.sortedAscending = true;
        }
    }

    function themeStyleToString(style) {
        switch (style) {
            case VisualStyle.Bright:
                return "Bright";
            case VisualStyle.Dark:
                return "Dark";
        }
        return "Unknown";
    }

    property GroupDataModel dataModel
    property int deviceOrientation
    property int listType: dataHelper.listType

}
