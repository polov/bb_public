import bb.cascades 1.3
import bb.cascades.maps 1.3

NavigationPane {
    id: navigationPane
    Page {
        id: detailPage

        titleBar: TitleBar {
            kind: TitleBarKind.FreeForm
            kindProperties: FreeFormTitleBarKindProperties {
                Container {
                    //background: Color.create("#ff00aae1")
                    leftPadding: 20
                    rightPadding: 20
                    layout: StackLayout {
                        orientation: LayoutOrientation.LeftToRight
                    }
                    Container {
                        layoutProperties: StackLayoutProperties {
                            spaceQuota: 1
                        }
                        verticalAlignment: VerticalAlignment.Center
                        Label {
                            text: qsTr("Air Quality") + Retranslate.onLanguageChanged
                            textStyle {
                                //color: Color.White
                                fontSize: FontSize.Large
                            }
                        }
                    }
                    Container {
                        verticalAlignment: VerticalAlignment.Center
                        ActivityIndicator {
                            id: loadingActivityIndicator
                            running: dataHelper.updatingData
                            accessibility.name: "Update data activity indicator"
                        }
                    }
                }
            }
        }

        content: ScrollView {
            scrollViewProperties.overScrollEffectMode: OverScrollEffectMode.None
            content: Container {
                //location
                Container {
                    layout: StackLayout {
                        orientation: LayoutOrientation.LeftToRight
                    }

                    topPadding: 30
                    bottomPadding: 30

                    Container {
                        minWidth: 70
                        verticalAlignment: VerticalAlignment.Center
                        horizontalAlignment: HorizontalAlignment.Center
                        //background: Color.Black
                        ImageView {
                            imageSource: "asset:///images/icons/ic_location_detail_page.png"
                            accessibility.name: "Location icon"
                        }
                    }
                    Container {
                        layoutProperties: StackLayoutProperties {
                            spaceQuota: 1
                        }
                        Container {
                            layout: StackLayout {
                                orientation: LayoutOrientation.LeftToRight
                            }
                            Container {
                                Container {
                                    Label {
                                        id: nameLabel
                                        text: ""
                                        multiline: true
                                        textStyle {
                                            //color: Color.create("#ff000000")
                                            fontSize: FontSize.XLarge
                                        }
                                    }
                                }
                                Container {
                                    layout: StackLayout {
                                        orientation: LayoutOrientation.LeftToRight
                                    }
                                    Container {
                                        Label {
                                            id: areaLabel
                                            text: ""
                                            multiline: true
                                            textStyle {
                                                //color: Color.create("#ff000000")
                                                fontSize: FontSize.Small
                                            }
                                        }
                                    }
                                    Container {
                                        Label {
                                            text: ", "
                                            multiline: true
                                            textStyle {
                                                //color: Color.create("#ff000000")
                                                fontSize: FontSize.Small
                                            }
                                        }
                                    }
                                    Container {
                                        Label {
                                            id: distanceLabel
                                            text: ""
                                            multiline: true
                                            textStyle {
                                                //color: Color.create("#ff000000")
                                                fontSize: FontSize.Small
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                //quality index
                Container {
                    id: hourlyIndexContainer

                    layout: StackLayout {
                        orientation: LayoutOrientation.LeftToRight
                    }

                    //background: Color.create("#ff00c9ff")
                    background: Color.create("#ffffd000")

                    topPadding: 30
                    bottomPadding: 30

                    Container {
                        verticalAlignment: VerticalAlignment.Center
                        horizontalAlignment: HorizontalAlignment.Center
                        //background: Color.Black
                        ImageView {
                            imageSource: "asset:///images/leaf.png"
                            accessibility.name: "Hourly index icon"
                        }
                    }
                    Container {
                        layoutProperties: StackLayoutProperties {
                            spaceQuota: 1
                        }
                        Container {
                            layout: StackLayout {
                                orientation: LayoutOrientation.LeftToRight
                            }
                            Container {
                                Container {
                                    Label {
                                        id: hourlyIndexWordLabel
                                        text: qsTr("Not available") + Retranslate.onLanguageChanged
                                        multiline: true
                                        textStyle {
                                            color: Color.White
                                            fontSize: FontSize.XLarge
                                        }
                                    }
                                }
                                Container {
                                    Label {
                                        text: qsTr("air quality") + Retranslate.onLanguageChanged
                                        multiline: true
                                        textStyle {
                                            color: Color.White
                                            fontSize: FontSize.Small
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                //values
                Container {
                    layout: StackLayout {
                        orientation: LayoutOrientation.LeftToRight
                    }
                    background: Color.create("#ffb9b7bf")

                    Container {
                        layoutProperties: StackLayoutProperties {
                            spaceQuota: 1
                        }
                        rightMargin: 1.0

                        //pm10
                        Container {
                            id: pm101Container
                            layout: StackLayout {
                                orientation: LayoutOrientation.LeftToRight
                            }
                            background: {
                                detailPage.getBackgroundColor();
                            }
                            topPadding: 25.0
                            bottomPadding: 30.0
                            rightPadding: 15.0
                            leftPadding: 15.0

                            Container {
                                verticalAlignment: VerticalAlignment.Center
                                horizontalAlignment: HorizontalAlignment.Center
                                Container {
                                    rightPadding: 15.0
                                    ImageView {
                                        id: pm101Image
                                        accessibility.name: "Hourly index icon"
                                        scalingMethod: ScalingMethod.AspectFit
                                    }

                                }
                            }

                            Container {
                                verticalAlignment: VerticalAlignment.Center
                                horizontalAlignment: HorizontalAlignment.Center
                                Container {
                                    layout: StackLayout {
                                        orientation: LayoutOrientation.LeftToRight
                                    }
                                    Container {
                                        bottomPadding: 10.0
                                        Label {
                                            text: "PM"
                                            textStyle {
                                                //color: Color.create("#ff000000")
                                                fontSize: FontSize.Small
                                            }
                                        }
                                    }
                                    Container {
                                        verticalAlignment: VerticalAlignment.Bottom
                                        Label {
                                            text: "10"
                                            textStyle {
                                                //color: Color.create("#ff000000")
                                                fontSize: FontSize.XSmall
                                            }
                                        }
                                    }
                                }

                            }
                            Container {
                                verticalAlignment: VerticalAlignment.Center
                                horizontalAlignment: HorizontalAlignment.Center
                                layoutProperties: StackLayoutProperties {
                                    spaceQuota: 1
                                }
                                leftPadding: ui.du(1.0)
                                Label {
                                    text: qsTr("Particulate matter") + Retranslate.onLanguageChanged
                                    textStyle {
                                        //color: Color.create("#ff000000")
                                        fontSize: FontSize.XSmall
                                    }
                                }
                                Label {
                                    visible: false
                                    id: pm101ValueLabel
                                    accessibility.name: ""
                                }
                            }
                            gestureHandlers: TapHandler {
                                onTapped: {
                                    var valueDetailPageContent = valueDetailPage.createObject();
                                    valueDetailPageContent.name = nameLabel.text;
                                    valueDetailPageContent.area = areaLabel.text;
                                    valueDetailPageContent.distance = distanceLabel.text;
                                    valueDetailPageContent.codeName = qsTr("particulate matter") + Retranslate.onLanguageChanged;
                                    valueDetailPageContent.codeLetters = "PM";
                                    valueDetailPageContent.codeSub = "10";
                                    valueDetailPageContent.valueId = 1; // must be set before value
                                    valueDetailPageContent.value = pm101ValueLabel.text;
                                    valueDetailPageContent.deviceOrientation = deviceOrientation;
                                    navigationPane.push(valueDetailPageContent);
                                }
                            }
                        }
                    }

                    Container {
                        layoutProperties: StackLayoutProperties {
                            spaceQuota: 1
                        }

                        //pm1024
                        Container {
                            id: pm1024Container
                            layout: StackLayout {
                                orientation: LayoutOrientation.LeftToRight
                            }
                            background: {
                                detailPage.getBackgroundColor();
                            }
                            topPadding: 25.0
                            bottomPadding: 30.0
                            rightPadding: 15.0
                            leftPadding: 15.0

                            Container {
                                verticalAlignment: VerticalAlignment.Center
                                horizontalAlignment: HorizontalAlignment.Center
                                Container {
                                    rightPadding: 15.0
                                    ImageView {
                                        id: pm1024Image
                                        accessibility.name: "Hourly index icon"
                                        scalingMethod: ScalingMethod.AspectFit
                                    }

                                }
                            }

                            Container {
                                verticalAlignment: VerticalAlignment.Center
                                horizontalAlignment: HorizontalAlignment.Center
                                Container {
                                    layout: StackLayout {
                                        orientation: LayoutOrientation.LeftToRight
                                    }
                                    Container {
                                        bottomPadding: 10.0
                                        Label {
                                            text: "PM"
                                            textStyle {
                                                //color: Color.create("#ff000000")
                                                fontSize: FontSize.Small
                                            }
                                        }
                                    }
                                    Container {
                                        verticalAlignment: VerticalAlignment.Bottom
                                        Label {
                                            text: "10"
                                            textStyle {
                                                //color: Color.create("#ff000000")
                                                fontSize: FontSize.XSmall
                                            }
                                        }
                                    }
                                    Container {
                                        //bottomPadding: 10.0
                                        verticalAlignment: VerticalAlignment.Center
                                        horizontalAlignment: HorizontalAlignment.Center
                                        Label {
                                            text: "/24h"
                                            textStyle {
                                                //color: Color.create("#ff000000")
                                                fontSize: FontSize.XSmall
                                            }
                                        }
                                    }
                                }
                            }
                            Container {
                                layoutProperties: StackLayoutProperties {
                                    spaceQuota: 1
                                }
                                leftPadding: ui.du(1.0)
                                verticalAlignment: VerticalAlignment.Center
                                horizontalAlignment: HorizontalAlignment.Center
                                Label {
                                    text: qsTr("Particulate matter") + Retranslate.onLanguageChanged
                                    textStyle {
                                        //color: Color.create("#ff000000")
                                        fontSize: FontSize.XSmall
                                    }
                                }
                                Label {
                                    visible: false
                                    id: pm1024ValueLabel
                                    accessibility.name: ""
                                }
                            }
                            gestureHandlers: TapHandler {
                                onTapped: {
                                    var valueDetailPageContent = valueDetailPage.createObject();
                                    valueDetailPageContent.name = nameLabel.text;
                                    valueDetailPageContent.area = areaLabel.text;
                                    valueDetailPageContent.distance = distanceLabel.text;
                                    valueDetailPageContent.codeName = qsTr("particulate matter/24h") + Retranslate.onLanguageChanged;
                                    valueDetailPageContent.codeLetters = "PM";
                                    valueDetailPageContent.codeSub = "10";
                                    valueDetailPageContent.valueId = 2; // must be set before value
                                    valueDetailPageContent.value = pm1024ValueLabel.text;
                                    valueDetailPageContent.deviceOrientation = deviceOrientation;
                                    navigationPane.push(valueDetailPageContent);
                                }
                            }
                        }
                    }
                }
                // divider
                Container {
                    layout: StackLayout {
                        orientation: LayoutOrientation.LeftToRight
                    }
                    Container {
                        layoutProperties: StackLayoutProperties {
                            spaceQuota: 1
                        }
                        minHeight: 1
                        background: Color.create("#ffb9b7bf")
                    }
                }

                Container {
                    layout: StackLayout {
                        orientation: LayoutOrientation.LeftToRight
                    }
                    background: Color.create("#ffb9b7bf")

                    Container {
                        layoutProperties: StackLayoutProperties {
                            spaceQuota: 1
                        }
                        rightMargin: 1.0

                        //o3
                        Container {
                            id: o3Container
                            layout: StackLayout {
                                orientation: LayoutOrientation.LeftToRight
                            }
                            background: {
                                detailPage.getBackgroundColor();
                            }
                            topPadding: 25.0
                            bottomPadding: 30.0
                            rightPadding: 15.0
                            leftPadding: 15.0

                            Container {
                                verticalAlignment: VerticalAlignment.Center
                                horizontalAlignment: HorizontalAlignment.Center
                                Container {
                                    rightPadding: 15.0
                                    ImageView {
                                        id: o3Image
                                        accessibility.name: "Hourly index icon"
                                        scalingMethod: ScalingMethod.AspectFit
                                    }

                                }
                            }

                            Container {
                                verticalAlignment: VerticalAlignment.Center
                                horizontalAlignment: HorizontalAlignment.Center
                                Container {
                                    layout: StackLayout {
                                        orientation: LayoutOrientation.LeftToRight
                                    }
                                    Container {
                                        bottomPadding: 10.0
                                        Label {
                                            text: "O"
                                            textStyle {
                                                //color: Color.create("#ff000000")
                                                fontSize: FontSize.Small
                                            }
                                        }
                                    }
                                    Container {
                                        verticalAlignment: VerticalAlignment.Bottom
                                        Label {
                                            text: "3"
                                            textStyle {
                                                //color: Color.create("#ff000000")
                                                fontSize: FontSize.XSmall
                                            }
                                        }
                                    }
                                }
                            }
                            Container {
                                verticalAlignment: VerticalAlignment.Center
                                horizontalAlignment: HorizontalAlignment.Center
                                leftPadding: ui.du(1.0)
                                layoutProperties: StackLayoutProperties {
                                    spaceQuota: 1
                                }
                                Label {
                                    text: qsTr("Ozone") + Retranslate.onLanguageChanged
                                    textStyle {
                                        //color: Color.create("#ff000000")
                                        fontSize: FontSize.XSmall
                                    }
                                }
                                Label {
                                    visible: false
                                    id: o3ValueLabel
                                    accessibility.name: ""
                                }
                            }
                            gestureHandlers: TapHandler {
                                onTapped: {
                                    var valueDetailPageContent = valueDetailPage.createObject();
                                    valueDetailPageContent.name = nameLabel.text;
                                    valueDetailPageContent.area = areaLabel.text;
                                    valueDetailPageContent.distance = distanceLabel.text;
                                    valueDetailPageContent.codeName = qsTr("ozone") + Retranslate.onLanguageChanged;
                                    valueDetailPageContent.codeLetters = "O";
                                    valueDetailPageContent.codeSub = "3";
                                    valueDetailPageContent.valueId = 3; // must be set before value
                                    valueDetailPageContent.value = o3ValueLabel.text;
                                    valueDetailPageContent.deviceOrientation = deviceOrientation;
                                    navigationPane.push(valueDetailPageContent);
                                }
                            }
                        }

                    }

                    Container {
                        layoutProperties: StackLayoutProperties {
                            spaceQuota: 1
                        }

                        //no2
                        Container {
                            layout: StackLayout {
                                orientation: LayoutOrientation.LeftToRight
                            }
                            background: {
                                detailPage.getBackgroundColor();
                            }
                            topPadding: 25.0
                            bottomPadding: 30.0
                            rightPadding: 15.0
                            leftPadding: 15.0

                            Container {
                                verticalAlignment: VerticalAlignment.Center
                                horizontalAlignment: HorizontalAlignment.Center
                                Container {
                                    rightPadding: 15.0
                                    ImageView {
                                        id: no2Image
                                        accessibility.name: "Hourly index icon"
                                        scalingMethod: ScalingMethod.AspectFit
                                    }

                                }
                            }

                            Container {
                                verticalAlignment: VerticalAlignment.Center
                                horizontalAlignment: HorizontalAlignment.Center
                                Container {
                                    layout: StackLayout {
                                        orientation: LayoutOrientation.LeftToRight
                                    }
                                    Container {
                                        bottomPadding: 10.0
                                        Label {
                                            text: "NO"
                                            textStyle {
                                                //color: Color.create("#ff000000")
                                                fontSize: FontSize.Default
                                            }
                                        }
                                    }
                                    Container {
                                        verticalAlignment: VerticalAlignment.Bottom
                                        Label {
                                            text: "2"
                                            textStyle {
                                                //color: Color.create("#ff000000")
                                                fontSize: FontSize.Small
                                            }
                                        }
                                    }
                                    Container {
                                        bottomPadding: 10.0
                                        Label {
                                            text: ""
                                            textStyle {
                                                //color: Color.create("#ff000000")
                                                fontSize: FontSize.Default
                                            }
                                        }
                                    }
                                }
                            }
                            Container {
                                verticalAlignment: VerticalAlignment.Center
                                horizontalAlignment: HorizontalAlignment.Center
                                leftPadding: ui.du(1.0)
                                layoutProperties: StackLayoutProperties {
                                    spaceQuota: 1
                                }
                                Label {
                                    text: qsTr("Nitrogen dioxide") + Retranslate.onLanguageChanged
                                    textStyle {
                                        //color: Color.create("#ff000000")
                                        fontSize: FontSize.XSmall
                                        //fontWeight: FontWeight.W300
                                    }
                                }
                                Label {
                                    visible: false
                                    id: no2ValueLabel
                                    accessibility.name: ""
                                }
                            }
                            gestureHandlers: TapHandler {
                                onTapped: {
                                    var valueDetailPageContent = valueDetailPage.createObject();
                                    valueDetailPageContent.name = nameLabel.text;
                                    valueDetailPageContent.area = areaLabel.text;
                                    valueDetailPageContent.distance = distanceLabel.text;
                                    valueDetailPageContent.codeName = qsTr("nitrogen dioxide") + Retranslate.onLanguageChanged;
                                    valueDetailPageContent.codeLetters = "NO";
                                    valueDetailPageContent.codeSub = "2";
                                    valueDetailPageContent.valueId = 4; // must be set before value
                                    valueDetailPageContent.value = no2ValueLabel.text;
                                    valueDetailPageContent.deviceOrientation = deviceOrientation;
                                    navigationPane.push(valueDetailPageContent);
                                }
                            }
                        }
                    }
                }

                // divider
                Container {
                    layout: StackLayout {
                        orientation: LayoutOrientation.LeftToRight
                    }
                    Container {
                        layoutProperties: StackLayoutProperties {
                            spaceQuota: 1
                        }
                        minHeight: 1
                        background: Color.create("#ffb9b7bf")
                    }
                }

                Container {
                    layout: StackLayout {
                        orientation: LayoutOrientation.LeftToRight
                    }
                    background: Color.create("#ffb9b7bf")

                    Container {
                        layoutProperties: StackLayoutProperties {
                            spaceQuota: 1
                        }
                        rightMargin: 1.0

                        //so2
                        Container {
                            id: so2Container
                            layout: StackLayout {
                                orientation: LayoutOrientation.LeftToRight
                            }
                            background: {
                                detailPage.getBackgroundColor();
                            }
                            topPadding: 25.0
                            bottomPadding: 30.0
                            rightPadding: 15.0
                            leftPadding: 15.0

                            Container {
                                verticalAlignment: VerticalAlignment.Center
                                horizontalAlignment: HorizontalAlignment.Center
                                Container {
                                    rightPadding: 15.0
                                    ImageView {
                                        id: so2Image
                                        accessibility.name: "Hourly index icon"
                                        scalingMethod: ScalingMethod.AspectFit
                                    }

                                }
                            }

                            Container {
                                verticalAlignment: VerticalAlignment.Center
                                horizontalAlignment: HorizontalAlignment.Center
                                Container {
                                    layout: StackLayout {
                                        orientation: LayoutOrientation.LeftToRight
                                    }
                                    Container {
                                        bottomPadding: 10.0
                                        Label {
                                            text: "SO"
                                            textStyle {
                                                //color: Color.create("#ff000000")
                                                fontSize: FontSize.Small
                                            }
                                        }
                                    }
                                    Container {
                                        verticalAlignment: VerticalAlignment.Bottom
                                        Label {
                                            text: "2"
                                            textStyle {
                                                //color: Color.create("#ff000000")
                                                fontSize: FontSize.XSmall
                                            }
                                        }
                                    }
                                }
                            }
                            Container {
                                verticalAlignment: VerticalAlignment.Center
                                horizontalAlignment: HorizontalAlignment.Center
                                leftPadding: ui.du(1.0)
                                layoutProperties: StackLayoutProperties {
                                    spaceQuota: 1
                                }
                                Label {
                                    text: qsTr("Sulfur dioxide") + Retranslate.onLanguageChanged
                                    textStyle {
                                        //color: Color.create("#ff000000")
                                        fontSize: FontSize.XSmall
                                    }
                                }
                                Label {
                                    visible: false
                                    id: so2ValueLabel
                                    accessibility.name: ""
                                }
                            }
                            gestureHandlers: TapHandler {
                                onTapped: {
                                    var valueDetailPageContent = valueDetailPage.createObject();
                                    valueDetailPageContent.name = nameLabel.text;
                                    valueDetailPageContent.area = areaLabel.text;
                                    valueDetailPageContent.distance = distanceLabel.text;
                                    valueDetailPageContent.codeName = qsTr("sulfur dioxide") + Retranslate.onLanguageChanged;
                                    valueDetailPageContent.codeLetters = "SO";
                                    valueDetailPageContent.codeSub = "2";
                                    valueDetailPageContent.valueId = 5; // must be set before value
                                    valueDetailPageContent.value = so2ValueLabel.text;
                                    valueDetailPageContent.deviceOrientation = deviceOrientation;
                                    navigationPane.push(valueDetailPageContent);
                                }
                            }
                        }

                    }

                    Container {
                        layoutProperties: StackLayoutProperties {
                            spaceQuota: 1
                        }

                        //co
                        Container {
                            id: coContainer
                            layout: StackLayout {
                                orientation: LayoutOrientation.LeftToRight
                            }
                            background: {
                                detailPage.getBackgroundColor();
                            }
                            topPadding: 25.0
                            bottomPadding: 30.0
                            rightPadding: 15.0
                            leftPadding: 15.0

                            Container {
                                verticalAlignment: VerticalAlignment.Center
                                horizontalAlignment: HorizontalAlignment.Center
                                Container {
                                    rightPadding: 15.0
                                    ImageView {
                                        id: coImage
                                        accessibility.name: "Hourly index icon"
                                        scalingMethod: ScalingMethod.AspectFit
                                    }

                                }
                            }

                            Container {
                                verticalAlignment: VerticalAlignment.Center
                                horizontalAlignment: HorizontalAlignment.Center
                                Container {
                                    layout: StackLayout {
                                        orientation: LayoutOrientation.LeftToRight
                                    }
                                    Container {
                                        bottomPadding: 10.0
                                        Label {
                                            text: "CO"
                                            textStyle {
                                                //color: Color.create("#ff000000")
                                                fontSize: FontSize.Small
                                            }
                                        }
                                    }
                                    Container {
                                        verticalAlignment: VerticalAlignment.Bottom
                                        Label {
                                            text: ""
                                            textStyle {
                                                //color: Color.create("#ff000000")
                                                fontSize: FontSize.XSmall
                                            }
                                        }
                                    }
                                }
                            }
                            Container {
                                verticalAlignment: VerticalAlignment.Center
                                horizontalAlignment: HorizontalAlignment.Center
                                leftPadding: ui.du(1.0)
                                layoutProperties: StackLayoutProperties {
                                    spaceQuota: 1
                                }
                                Label {
                                    text: qsTr("Carbon monoxide") + Retranslate.onLanguageChanged
                                    textStyle {
                                        //color: Color.create("#ff000000")
                                        fontSize: FontSize.XSmall
                                        //fontWeight: FontWeight.W300
                                    }
                                }
                                Label {
                                    visible: false
                                    id: coValueLabel
                                    accessibility.name: ""
                                }
                            }
                            gestureHandlers: TapHandler {
                                onTapped: {
                                    var valueDetailPageContent = valueDetailPage.createObject();
                                    valueDetailPageContent.name = nameLabel.text;
                                    valueDetailPageContent.area = areaLabel.text;
                                    valueDetailPageContent.distance = distanceLabel.text;
                                    valueDetailPageContent.codeName = qsTr("carbon monoxide") + Retranslate.onLanguageChanged;
                                    valueDetailPageContent.codeLetters = "CO";
                                    valueDetailPageContent.codeSub = "";
                                    valueDetailPageContent.valueId = 6; // must be set before value
                                    valueDetailPageContent.value = coValueLabel.text;
                                    valueDetailPageContent.deviceOrientation = deviceOrientation;
                                    navigationPane.push(valueDetailPageContent);
                                }
                            }
                        }
                    }
                }

                // divider
                Container {
                    layout: StackLayout {
                        orientation: LayoutOrientation.LeftToRight
                    }
                    Container {
                        layoutProperties: StackLayoutProperties {
                            spaceQuota: 1
                        }
                        minHeight: 1
                        background: Color.create("#ffb9b7bf")
                    }
                }

                //bottom
                Container {
                    layout: StackLayout {
                        orientation: LayoutOrientation.LeftToRight
                    }
                    topPadding: ui.du(2.0)
                    Container {
                        layoutProperties: StackLayoutProperties {
                            spaceQuota: 1
                        }
                        bottomPadding: ui.du(3.5)
                        //last update
                        Container {
                            leftPadding: 15.0
                            //topPadding: ui.du(0.25)
                            Label {
                                id: datetime_toLabel
                                text: ""
                                textStyle {
                                    //color: Color.create("#ff000000")
                                    fontSize: FontSize.XSmall
                                }
                            }
                        }
                        //legend
                        Container {
                            leftPadding: 15.0
                            Container {
                                layout: StackLayout {
                                    orientation: LayoutOrientation.LeftToRight
                                }
                                topPadding: ui.du(1.25)
                                //bottomPadding: 10.0
                                Label {
                                    text: qsTr("Air quality legend:") + Retranslate.onLanguageChanged
                                    textStyle {
                                        //color: Color.create("#ff000000")
                                        fontSize: FontSize.XSmall
                                    }
                                }
                            }
                            Container {
                                layout: StackLayout {
                                    orientation: LayoutOrientation.LeftToRight
                                }
                                topPadding: ui.du(0.25)
                                Container {
                                    layoutProperties: StackLayoutProperties {
                                        spaceQuota: 1
                                    }
                                    Container {
                                        layout: StackLayout {
                                            orientation: LayoutOrientation.LeftToRight
                                        }
                                        bottomPadding: ui.du(0.5)

                                        Container {
                                            verticalAlignment: VerticalAlignment.Bottom
                                            ImageView {
                                                imageSource: "asset:///images/circle_blue_small.png"
                                                accessibility.name: "Hourly index icon blue"
                                            }
                                        }
                                        Container {
                                            leftPadding: 5.0
                                            Label {
                                                text: qsTr("1 - Very good") + Retranslate.onLanguageChanged
                                                multiline: true
                                                textStyle {
                                                    //color: Color.create("#ff000000")
                                                    fontSize: FontSize.XSmall
                                                    textAlign: TextAlign.Center
                                                }
                                            }
                                        }
                                    }

                                    Container {
                                        layout: StackLayout {
                                            orientation: LayoutOrientation.LeftToRight
                                        }
                                        bottomPadding: ui.du(0.5)

                                        Container {
                                            verticalAlignment: VerticalAlignment.Bottom
                                            ImageView {
                                                imageSource: "asset:///images/circle_green_small.png"
                                                accessibility.name: "Hourly index icon green"
                                            }
                                        }
                                        Container {
                                            leftPadding: 5.0
                                            Label {
                                                text: qsTr("2 - Good") + Retranslate.onLanguageChanged
                                                multiline: true
                                                textStyle {
                                                    //color: Color.create("#ff000000")
                                                    fontSize: FontSize.XSmall
                                                    textAlign: TextAlign.Center
                                                }
                                            }
                                        }
                                    }

                                    Container {
                                        layout: StackLayout {
                                            orientation: LayoutOrientation.LeftToRight
                                        }
                                        bottomPadding: ui.du(0.5)

                                        Container {
                                            verticalAlignment: VerticalAlignment.Bottom
                                            ImageView {
                                                imageSource: "asset:///images/circle_yellow_small.png"
                                                accessibility.name: "Hourly index icon yellow"
                                            }
                                        }
                                        Container {
                                            leftPadding: 5.0
                                            Label {
                                                text: qsTr("3 - Fair") + Retranslate.onLanguageChanged
                                                multiline: true
                                                textStyle {
                                                    //color: Color.create("#ff000000")
                                                    fontSize: FontSize.XSmall
                                                    textAlign: TextAlign.Center
                                                }
                                            }
                                        }
                                    }
                                    Container {
                                        //bottomPadding: 15.0
                                        Container {
                                            //bottomPadding: ui.du(0.5)
                                            layout: StackLayout {
                                                orientation: LayoutOrientation.LeftToRight
                                            }
                                            Container {
                                                verticalAlignment: VerticalAlignment.Bottom
                                                ImageView {
                                                    imageSource: "asset:///images/circle_gray_small.png"
                                                    accessibility.name: "Hourly index icon gray"
                                                }
                                            }
                                            Container {
                                                leftPadding: 5.0
                                                Label {
                                                    text: qsTr("Not available") + Retranslate.onLanguageChanged
                                                    multiline: true
                                                    textStyle {
                                                        //color: Color.create("#ff000000")
                                                        fontSize: FontSize.XSmall
                                                        textAlign: TextAlign.Center
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                                Container {
                                    layoutProperties: StackLayoutProperties {
                                        spaceQuota: 1
                                    }
                                    Container {
                                        layout: StackLayout {
                                            orientation: LayoutOrientation.LeftToRight
                                        }
                                        bottomPadding: ui.du(0.5)

                                        Container {
                                            verticalAlignment: VerticalAlignment.Bottom
                                            ImageView {
                                                imageSource: "asset:///images/circle_orange_small.png"
                                                accessibility.name: "Hourly index icon orange"
                                            }
                                        }
                                        Container {
                                            leftPadding: 5.0
                                            Label {
                                                text: qsTr("4 - Suitable") + Retranslate.onLanguageChanged
                                                multiline: true
                                                textStyle {
                                                    //color: Color.create("#ff000000")
                                                    fontSize: FontSize.XSmall
                                                    textAlign: TextAlign.Center
                                                }
                                            }
                                        }
                                    }

                                    Container {
                                        layout: StackLayout {
                                            orientation: LayoutOrientation.LeftToRight
                                        }
                                        bottomPadding: ui.du(0.5)

                                        Container {
                                            verticalAlignment: VerticalAlignment.Bottom
                                            ImageView {
                                                imageSource: "asset:///images/circle_red1_small.png"
                                                accessibility.name: "Hourly index icon red first"
                                            }
                                        }
                                        Container {
                                            leftPadding: 5.0
                                            Label {
                                                text: qsTr("5 - Poor") + Retranslate.onLanguageChanged
                                                multiline: true
                                                textStyle {
                                                    //color: Color.create("#ff000000")
                                                    fontSize: FontSize.XSmall
                                                    textAlign: TextAlign.Center
                                                }
                                            }
                                        }
                                    }

                                    Container {
                                        layout: StackLayout {
                                            orientation: LayoutOrientation.LeftToRight
                                        }
                                        bottomPadding: ui.du(0.5)

                                        Container {
                                            verticalAlignment: VerticalAlignment.Bottom
                                            ImageView {
                                                imageSource: "asset:///images/circle_red2_small.png"
                                                accessibility.name: "Hourly index icon red second"
                                            }
                                        }
                                        Container {
                                            leftPadding: 5.0
                                            Label {
                                                text: qsTr("6 - Very poor") + Retranslate.onLanguageChanged
                                                multiline: true
                                                textStyle {
                                                    //color: Color.create("#ff000000")
                                                    fontSize: FontSize.XSmall
                                                    textAlign: TextAlign.Center
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    //map view
                    Container {
                        layoutProperties: StackLayoutProperties {
                            spaceQuota: 1
                        }
                        rightPadding: 25.0
                        layout: DockLayout {
                        }
                        //background: Color.Gray
                        MapView {
                            id: mapview
                            latitude: 50.0479
                            longitude: 15.4383
                            altitude: 3000.0
                        }
                    }
                }
            }
            accessibility.name: "TODO: Add property content"
        }

        actions: [
            ActionItem {
                title: qsTr("Update Data") + Retranslate.onLanguageChanged
                imageSource: "asset:///images/icons/ic_reload.png"
                onTriggered: {
                    dataHelper.updateSourceFile();
                }
            },
            ActionItem {
                title: qsTr("Update Position") + Retranslate.onLanguageChanged
                imageSource: "asset:///images/icons/ic_located.png"
                onTriggered: {
                    geoHelper.updatePosition();
                }
            },
            ActionItem {
                title: qsTr("Show on Map") + Retranslate.onLanguageChanged
                imageSource: "asset:///images/icons/ic_map.png"
                onTriggered: {
                    geoHelper.openMapWithPin(dataModel, dataModelIndexPath);
                }
            },
            InvokeActionItem {
                ActionBar.placement: ActionBarPlacement.InOverflow
                title: qsTr("Share") + Retranslate.onLanguageChanged
                query {
                    mimeType: "text/plain"
                    invokeActionId: "bb.action.SHARE"
                }
                onTriggered: {
                    data = dataHelper.encodeQString(qsTr("Air quality in ") + nameLabel.text + ": " + hourlyIndexWordLabel.text);
                }
            }
        ]

        function setData() {
            console.log("Filling data on detail page, index path is " + dataModelIndexPath);
            var selectedItem = dataModel.data(dataModelIndexPath);

            nameLabel.text = selectedItem.name;
            if (selectedItem.code.substring(0, 1) == "A") {
                areaLabel.text = "Praha";
            } else if (selectedItem.code.substring(0, 1) == "S") {
                areaLabel.text = "Středočeský kraj";
            } else if (selectedItem.code.substring(0, 1) == "C") {
                areaLabel.text = "Jihočeský kraj";
            } else if (selectedItem.code.substring(0, 1) == "P") {
                areaLabel.text = "Plzeňský kraj";
            } else if (selectedItem.code.substring(0, 1) == "K") {
                areaLabel.text = "Karlovarský kraj";
            } else if (selectedItem.code.substring(0, 1) == "U") {
                areaLabel.text = "Ústecký kraj";
            } else if (selectedItem.code.substring(0, 1) == "L") {
                areaLabel.text = "Liberecký kraj";
            } else if (selectedItem.code.substring(0, 1) == "H") {
                areaLabel.text = "Hradecký kraj";
            } else if (selectedItem.code.substring(0, 1) == "E") {
                areaLabel.text = "Pardubický kraj";
            } else if (selectedItem.code.substring(0, 1) == "J") {
                areaLabel.text = "Vysočina";
            } else if (selectedItem.code.substring(0, 1) == "B") {
                areaLabel.text = "Jihomoravský kraj";
            } else if (selectedItem.code.substring(0, 1) == "M") {
                areaLabel.text = "Olomoucký kraj";
            } else if (selectedItem.code.substring(0, 1) == "Z") {
                areaLabel.text = "Zlínský kraj";
            } else if (selectedItem.code.substring(0, 1) == "T") {
                areaLabel.text = "Moravskoslezský kraj";
            }

            distanceLabel.text = qsTr("distance ") + selectedItem.distance + " km";
            pm101ValueLabel.text = dataHelper.formatNumber(selectedItem.pm10_1) + " µg/m³";
            pm1024ValueLabel.text = dataHelper.formatNumber(selectedItem.pm10_24) + " µg/m³";
            o3ValueLabel.text = dataHelper.formatNumber(selectedItem.o3_1) + " µg/m³";
            no2ValueLabel.text = dataHelper.formatNumber(selectedItem.no2_1) + " µg/m³";
            so2ValueLabel.text = dataHelper.formatNumber(selectedItem.so2_1) + " µg/m³";
            coValueLabel.text = dataHelper.formatNumber(selectedItem.co_8) + " µg/m³";

            if (selectedItem.hourlyIndex < 1) {
                hourlyIndexWordLabel.text = qsTr("Not Available") + Retranslate.onLanguageChanged;
                hourlyIndexContainer.background = Color.create("#ff8e8d93");
            } else if (selectedItem.hourlyIndex == 1) {
                hourlyIndexWordLabel.text = qsTr("Very Good") + Retranslate.onLanguageChanged;
                hourlyIndexContainer.background = Color.create("#ff00c9ff");
            } else if (selectedItem.hourlyIndex == 2) {
                hourlyIndexWordLabel.text = qsTr("Good") + Retranslate.onLanguageChanged;
                hourlyIndexContainer.background = Color.create("#ff00e855");
            } else if (selectedItem.hourlyIndex == 3) {
                hourlyIndexWordLabel.text = qsTr("Fair") + Retranslate.onLanguageChanged;
                hourlyIndexContainer.background = Color.create("#ffffd000");
            } else if (selectedItem.hourlyIndex == 4) {
                hourlyIndexWordLabel.text = qsTr("Suitable") + Retranslate.onLanguageChanged;
                hourlyIndexContainer.background = Color.create("#ffff8e00");
            } else if (selectedItem.hourlyIndex == 5) {
                hourlyIndexWordLabel.text = qsTr("Poor") + Retranslate.onLanguageChanged;
                hourlyIndexContainer.background = Color.create("#ffff0018");
            } else if (selectedItem.hourlyIndex == 6) {
                hourlyIndexWordLabel.text = qsTr("Very Poor") + Retranslate.onLanguageChanged;
                hourlyIndexContainer.background = Color.create("#ffff004e");
            }

            pm101Image.visible = false;
            pm101Image.resetImageSource();
            if (selectedItem.pm10_1 < 0.1) {
                pm101Image.imageSource = "asset:///images/circle_gray.png";
            } else if (selectedItem.pm10_1 < 20.0) {
                pm101Image.imageSource = "asset:///images/circle_blue.png";
            } else if (selectedItem.pm10_1 < 40.0) {
                pm101Image.imageSource = "asset:///images/circle_green.png";
            } else if (selectedItem.pm10_1 < 70) {
                pm101Image.imageSource = "asset:///images/circle_yellow.png";
            } else if (selectedItem.pm10_1 < 90) {
                pm101Image.imageSource = "asset:///images/circle_orange.png";
            } else if (selectedItem.pm10_1 < 180) {
                pm101Image.imageSource = "asset:///images/circle_red1.png";
            } else if (selectedItem.pm10_1 >= 180) {
                pm101Image.imageSource = "asset:///images/circle_red2.png";
            }
            pm101Image.visible = true;

            pm1024Image.visible = false;
            pm1024Image.resetImageSource();
            if (selectedItem.pm10_24 < 0.1) {
                pm1024Image.imageSource = "asset:///images/circle_gray.png";
            } else if (selectedItem.pm10_24 < 20.0) {
                pm1024Image.imageSource = "asset:///images/circle_blue.png";
            } else if (selectedItem.pm10_24 < 40.0) {
                pm1024Image.imageSource = "asset:///images/circle_green.png";
            } else if (selectedItem.pm10_24 < 70) {
                pm1024Image.imageSource = "asset:///images/circle_yellow.png";
            } else if (selectedItem.pm10_24 < 90) {
                pm1024Image.imageSource = "asset:///images/circle_orange.png";
            } else if (selectedItem.pm10_24 < 180) {
                pm1024Image.imageSource = "asset:///images/circle_red1.png";
            } else if (selectedItem.pm10_24 >= 180) {
                pm1024Image.imageSource = "asset:///images/circle_red2.png";
            }
            pm1024Image.visible = true;

            o3Image.visible = false;
            o3Image.resetImageSource();
            if (selectedItem.o3_1 < 0.1) {
                o3Image.imageSource = "asset:///images/circle_gray.png";
            } else if (selectedItem.o3_1 < 33.0) {
                o3Image.imageSource = "asset:///images/circle_blue.png";
            } else if (selectedItem.o3_1 < 65.0) {
                o3Image.imageSource = "asset:///images/circle_green.png";
            } else if (selectedItem.o3_1 < 120.0) {
                o3Image.imageSource = "asset:///images/circle_yellow.png";
            } else if (selectedItem.o3_1 < 180.0) {
                o3Image.imageSource = "asset:///images/circle_orange.png";
            } else if (selectedItem.o3_1 < 240.0) {
                o3Image.imageSource = "asset:///images/circle_red1.png";
            } else if (selectedItem.o3_1 >= 240.0) {
                o3Image.imageSource = "asset:///images/circle_red2.png";
            }
            o3Image.visible = true;

            no2Image.visible = false;
            no2Image.resetImageSource();
            if (selectedItem.no2_1 < 0.1) {
                no2Image.imageSource = "asset:///images/circle_gray.png";
            } else if (selectedItem.no2_1 < 25.0) {
                no2Image.imageSource = "asset:///images/circle_blue.png";
            } else if (selectedItem.no2_1 < 50.0) {
                no2Image.imageSource = "asset:///images/circle_green.png";
            } else if (selectedItem.no2_1 < 100.0) {
                no2Image.imageSource = "asset:///images/circle_yellow.png";
            } else if (selectedItem.no2_1 < 200.0) {
                no2Image.imageSource = "asset:///images/circle_orange.png";
            } else if (selectedItem.no2_1 < 400.0) {
                no2Image.imageSource = "asset:///images/circle_red1.png";
            } else if (selectedItem.no2_1 >= 400.0) {
                no2Image.imageSource = "asset:///images/circle_red2.png";
            }
            no2Image.visible = true;

            so2Image.visible = false;
            so2Image.resetImageSource();
            if (selectedItem.so2_1 < 0.1) {
                so2Image.imageSource = "asset:///images/circle_gray.png";
            } else if (selectedItem.so2_1 < 25.0) {
                so2Image.imageSource = "asset:///images/circle_blue.png";
            } else if (selectedItem.so2_1 < 50.0) {
                so2Image.imageSource = "asset:///images/circle_green.png";
            } else if (selectedItem.so2_1 < 120.0) {
                so2Image.imageSource = "asset:///images/circle_yellow.png";
            } else if (selectedItem.so2_1 < 350.0) {
                so2Image.imageSource = "asset:///images/circle_orange.png";
            } else if (selectedItem.so2_1 < 500.0) {
                so2Image.imageSource = "asset:///images/circle_red1.png";
            } else if (selectedItem.so2_1 >= 500.0) {
                so2Image.imageSource = "asset:///images/circle_red2.png";
            }
            so2Image.visible = true;

            coImage.visible = false;
            coImage.resetImageSource();
            if (selectedItem.co_8 < 0.1) {
                coImage.imageSource = "asset:///images/circle_gray.png";
            } else if (selectedItem.co_8 < 1000.0) {
                coImage.imageSource = "asset:///images/circle_blue.png";
            } else if (selectedItem.co_8 < 2000.0) {
                coImage.imageSource = "asset:///images/circle_green.png";
            } else if (selectedItem.co_8 < 4000.0) {
                coImage.imageSource = "asset:///images/circle_yellow.png";
            } else if (selectedItem.co_8 < 10000.0) {
                coImage.imageSource = "asset:///images/circle_orange.png";
            } else if (selectedItem.co_8 < 30000.0) {
                coImage.imageSource = "asset:///images/circle_red1.png";
            } else if (selectedItem.co_8 >= 30000.0) {
                coImage.imageSource = "asset:///images/circle_red2.png";
            }
            coImage.visible = true;

            //save for active frame
            dataHelper.saveForActiveFrame(selectedItem.name, selectedItem.hourlyIndex);
            
            mapview.setLatitude(selectedItem.latitude);
            mapview.setLongitude(selectedItem.longtitude);
        }

        function onPositionUpdated() {
            console.log("Detail page: GeoLocationUpdated");
            dataModelIndexPath = geoHelper.findClosest(dataModel);
            if (dataModelIndexPath.length > 0) {
                setData();
            } else {
                console.log("Invalid indexPath: " + dataModelIndexPath);
            }
        }
        function onDataModelUpdated() {
            console.log("Detail page: Data model updated");
            if (dataModelIndexPath != undefined) {
                dataModelIndexPath = geoHelper.findClosest(dataModel);
                setData();
                dataHelper.handleForNotification(dataModelIndexPath, dataModel);
            } else {
                console.log("Detail page not filled, index path have not been set yet");
            }
        }
        function onDateTimeToChanged() {
            datetime_toLabel.text = qsTr("Updated ") + dataHelper.datetime_to;
        }
        function onDataUpdateStarted() {
            if (! loadingActivityIndicator.running) {
                loadingActivityIndicator.start();
                loadingActivityIndicator.visible = true;
            }
        }
        function getBackgroundColor() {
            switch (Application.themeSupport.theme.colorTheme.style) {
                case VisualStyle.Bright:
                    return Color.White;
                case VisualStyle.Dark:
                    return Color.Black;
            }
        }

        onCreationCompleted: {
            geoHelper.positionUpdated.connect(detailPage.onPositionUpdated);
            console.log("DetailPage connected to geoHelper.positionUpdated signal");

            dataHelper.dataModelUpdated.connect(detailPage.onDataModelUpdated);
            console.log("DetailPage connected to dataHelper.dataModelUpdated signal");
            dataHelper.dateTimeToChanged.connect(detailPage.onDateTimeToChanged);
            console.log("DetailPage connected to dataHelper.dateTimeToChanged signal");
        }

        attachedObjects: [
            ComponentDefinition {
                id: valueDetailPage
                source: "valueDetailPage.qml"
            },
            ComponentDefinition {
                id: mapPage
                source: "MapPage.qml"
            },
            OrientationHandler {
                id: handler
                onOrientationChanged: {
                    console.log("DetailPage orientation Changed to: " + orientation);
                    deviceOrientation = orientation;
                }
            }
        ]
    }
    property GroupDataModel dataModel
    property variant dataModelIndexPath
    property int deviceOrientation
}
