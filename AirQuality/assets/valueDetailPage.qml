import bb.cascades 1.4

Page {
    property string name
    property string area
    property string distance
    property string codeLetters
    property string codeSub
    property string codeName    
    property string value
    property int valueId
    property int deviceOrientation

    id: detailValuePage

    titleBar: TitleBar {
        kind: TitleBarKind.FreeForm
        kindProperties: FreeFormTitleBarKindProperties {
            Container {
                //background: Color.create("#ff00aae1")
                leftPadding: 20
                rightPadding: 20
                layout: StackLayout {
                    orientation: LayoutOrientation.LeftToRight
                }
                Container {
                    layoutProperties: StackLayoutProperties {
                        spaceQuota: 1
                    }
                    verticalAlignment: VerticalAlignment.Center
                    Label {
                        text: qsTr("Measurement Level") + Retranslate.onLanguageChanged
                        textStyle {
                            //color: Color.White
                            fontSize: FontSize.Large
                        }
                    }
                }
                Container {
                    verticalAlignment: VerticalAlignment.Center
                    ActivityIndicator {
                        id: loadingActivityIndicator
                        running: dataHelper.updatingData
                        accessibility.name: "Update data activity indicator"
                    }
                }
            }
        }
    }

    content: ScrollView {
        content: Container {
            //location
            Container {
                layout: StackLayout {
                    orientation: LayoutOrientation.LeftToRight
                }

                topPadding: 30
                bottomPadding: 30
                
                navigation.focusPolicy: NavigationFocusPolicy.Focusable

                Container {
                    minWidth: 70
                    verticalAlignment: VerticalAlignment.Center
                    horizontalAlignment: HorizontalAlignment.Center
                    //background: Color.Black
                    ImageView {
                        imageSource: "asset:///images/icons/ic_location_detail_page.png"
                        accessibility.name: "Location icon"
                    }
                }
                Container {
                    layoutProperties: StackLayoutProperties {
                        spaceQuota: 1
                    }
                    Container {
                        layout: StackLayout {
                            orientation: LayoutOrientation.LeftToRight
                        }
                        Container {
                            Container {
                                Label {
                                    text: name
                                    multiline: true
                                    textStyle {
                                        //color: Color.create("#ff8e8d93")
                                        fontSize: FontSize.XLarge
                                    }
                                }
                            }
                            Container {
                                Container {
                                    layout: StackLayout {
                                        orientation: LayoutOrientation.LeftToRight
                                    }
                                    Container {
                                        Label {
                                            text: area
                                            multiline: true
                                            textStyle {
                                                //color: Color.create("#ff8e8d93")
                                                fontSize: FontSize.Small
                                            }
                                        }
                                    }
                                    Container {
                                        Label {
                                            text: ", "
                                            multiline: true
                                            textStyle {
                                                //color: Color.create("#ff8e8d93")
                                                fontSize: FontSize.Small
                                            }
                                        }
                                    }
                                    Container {
                                        Label {
                                            text: distance
                                            multiline: true
                                            textStyle {
                                                //color: Color.create("#ff8e8d93")
                                                fontSize: FontSize.Small
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

            // divider
            Container {
                layout: StackLayout {
                    orientation: LayoutOrientation.LeftToRight
                }
                Container {
                    layoutProperties: StackLayoutProperties {
                        spaceQuota: 1
                    }
                    minHeight: 1
                    background: Color.create("#ff8e8d93")
                }
            }
            //---
            Container {
                topPadding: 30.0
                bottomPadding: 0.0
                leftPadding: 25.0
                rightPadding: 25.0
                Container {
                    layout: StackLayout {
                        orientation: LayoutOrientation.LeftToRight
                    }
                    Container {
                        bottomPadding: 10.0
                        Label {
                            text: codeLetters
                            textStyle {
                                //color: Color.create("#ff8e8d93")
                                fontSize: FontSize.Default
                            }
                        }
                    }
                    Container {
                        verticalAlignment: VerticalAlignment.Bottom
                        Label {
                            text: codeSub
                            textStyle {
                                //color: Color.create("#ff8e8d93")
                                fontSize: FontSize.Small
                            }
                        }
                    }
                    Container {
                        bottomPadding: 10.0
                        Label {
                            text: "  " + codeName
                            textStyle {
                                //color: Color.create("#ff8e8d93")
                                fontSize: FontSize.Default
                            }
                        }
                    }
                    Container {
                        layoutProperties: StackLayoutProperties {
                            spaceQuota: 1
                        }
                        Label {
                            text: value
                            horizontalAlignment: HorizontalAlignment.Right
                            verticalAlignment: VerticalAlignment.Center
                            textStyle {
                                //color: Color.create("#ff8e8d93")
                                fontSize: FontSize.Default
                                textAlign: TextAlign.Right
                            }
                        }
                    }
                }

            }

            //---
            Container {
                id: bar
                topPadding: 10.0
                //leftPadding: 25.0
                //rightPadding: 25.0
                preferredWidth: 0.0
                //background: Color.Black
                Container {
                    horizontalAlignment: HorizontalAlignment.Right
                    ImageView {
                        imageSource: "asset:///images/arrow_gray.png"
                        accessibility.name: "Air quality indicator"
                    }
                }
            }
            Container {
                leftPadding: 25.0
                rightPadding: 25.0
                //topPadding: 15.0
                bottomPadding: 5.0
                layout: StackLayout {
                    orientation: LayoutOrientation.LeftToRight
                }
                Container {
                    layoutProperties: StackLayoutProperties {
                        spaceQuota: 1
                    }
                    background: Color.create("#ff00c9ff")
                    preferredHeight: 20.0
                }
                Container {
                    layoutProperties: StackLayoutProperties {
                        spaceQuota: 1
                    }
                    background: Color.create("#ff00e855")
                    preferredHeight: 20.0
                }
                Container {
                    layoutProperties: StackLayoutProperties {
                        spaceQuota: 1
                    }
                    background: Color.create("#ffffd000")
                    preferredHeight: 20.0
                }
                Container {
                    layoutProperties: StackLayoutProperties {
                        spaceQuota: 1
                    }
                    background: Color.create("#ffff8e00")
                    preferredHeight: 20.0
                }
                Container {
                    layoutProperties: StackLayoutProperties {
                        spaceQuota: 1
                    }
                    background: Color.create("#ffff0018")
                    preferredHeight: 20.0
                }
                Container {
                    layoutProperties: StackLayoutProperties {
                        spaceQuota: 1
                    }
                    background: Color.create("#ffff004e")
                    preferredHeight: 20.0
                }
            }
            Container {
                leftPadding: 25.0
                rightPadding: 25.0
                layout: StackLayout {
                    orientation: LayoutOrientation.LeftToRight
                }
                bottomPadding: 30.0
                Container {
                    layoutProperties: StackLayoutProperties {
                        spaceQuota: 2
                    }
                    Label {
                        multiline: true
                        text: "0"
                        textStyle {
                            //color: Color.create("#ff8e8d93")
                            fontSize: FontSize.Small
                        }
                    }
                }
                Container {
                    layoutProperties: StackLayoutProperties {
                        spaceQuota: 2
                    }
                    Label {
                        multiline: true
                        text: "µg/m³"
                        horizontalAlignment: HorizontalAlignment.Center
                        textStyle {
                            //color: Color.create("#ff8e8d93")
                            fontSize: FontSize.Small
                        }
                    }
                }
                Container {
                    layoutProperties: StackLayoutProperties {
                        spaceQuota: 2
                    }
                    Label {
                        multiline: true
                        text: {
                            if (valueId == 1) {
                                return "nad " + 180;
                            } else if (valueId == 2) {
                                return "nad " + 180;
                            } else if (valueId == 3) {
                                return "nad " + 240;
                            } else if (valueId == 4) {
                                return "nad " + 400;
                            } else if (valueId == 5) {
                                return "nad " + 500;
                            } else if (valueId == 6) {
                                return "nad " + 30000;
                            }
                        }
                        horizontalAlignment: HorizontalAlignment.Right
                        textStyle {
                            //color: Color.create("#ff8e8d93")
                            fontSize: FontSize.Small
                        }
                    }
                }
            }

            // divider
            Container {
                layout: StackLayout {
                    orientation: LayoutOrientation.LeftToRight
                }
                Container {
                    layoutProperties: StackLayoutProperties {
                        spaceQuota: 1
                    }
                    minHeight: 1
                    background: Color.create("#ff8e8d93")
                }
            }

            Container {
                topPadding: 30.0
                bottomPadding: 25.0
                leftPadding: 25.0
                rightPadding: 25.0
                Label {
                    multiline: true
                    text: {
                        if (valueId == 1) {
                            return qsTr("Tiny pieces of solid or liquid matter associated with the Earth's atmosphere. They can adversely affect human health and also have impacts on climate and precipitation.<br /><a href=\"http://en.wikipedia.org/wiki/Particulates\">[wikipedia]</a>") + Retranslate.onLanguageChanged;
                        } else if (valueId == 2) {
                            return qsTr("Tiny pieces of solid or liquid matter associated with the Earth's atmosphere. They can adversely affect human health and also have impacts on climate and precipitation.<br /><a href=\"http://en.wikipedia.org/wiki/Particulates\">[wikipedia]</a>") + Retranslate.onLanguageChanged;
                        } else if (valueId == 3) {
                            return qsTr("Low level ozone (or tropospheric ozone) is an atmospheric pollutant. Even very low concentrations of ozone can be harmful to the upper respiratory tract and the lungs.<br /><a href=\"http://en.wikipedia.org/wiki/Ozone\">[wikipedia]</a>") + Retranslate.onLanguageChanged;
                        } else if (valueId == 4) {
                            return qsTr("Nitrogen dioxide is toxic by inhalation. Symptoms of poisoning (lung edema) tend to appear several hours after inhalation of a low but potentially fatal dose.<br /><a href=\"http://en.wikipedia.org/wiki/Nitrogen_dioxide\">[wikipedia]</a>") + Retranslate.onLanguageChanged;
                        } else if (valueId == 5) {
                            return qsTr("Sulfur dioxide is a major air pollutant and has significant impacts upon human health.<br /><a href=\"http://en.wikipedia.org/wiki/Sulfur_dioxide\">[wikipedia]</a>") + Retranslate.onLanguageChanged;
                        } else if (valueId == 6) {
                            return qsTr("A colorless, odorless, and tasteless gas that is slightly less dense than air. It is toxic to humans and animals when encountered in higher concentrations.<br /><a href=\"http://en.wikipedia.org/wiki/Carbon_monoxide\">[wikipedia]</a>") + Retranslate.onLanguageChanged;
                        }
                    }
                    textFormat: TextFormat.Html
                    textStyle {
                        //color: Color.create("#ff8e8d93")
                        fontSize: FontSize.Default
                    }
                }
            }
        }
        attachedObjects: [
            OrientationHandler {
                id: handler
                onOrientationChanged: {
                    //console.log("valueDetailPage orientation Changed to: " + orientation);
                    deviceOrientation = orientation;
                    //bar.preferredWidth = dataHelper.getBarWidth(value, valueId, deviceOrientation);
                }
            }
        ]
        accessibility.name: "Content container"
    }
    
    onValueChanged: {
        //bar.preferredWidth = dataHelper.getBarWidth(value, valueId, deviceOrientation);
        var barWidth = dataHelper.getBarWidth(value, valueId, deviceOrientation);
        if (barWidth == 0) {
            console.log("barWidth is zero, arrow not visible");
            bar.visible = false;
        } else {
            bar.visible = true;
            bar.preferredWidth = barWidth;
        }
    }
    
    onDeviceOrientationChanged: {
        console.log("valueDetailPage orientation Changed to: " + deviceOrientation);
        var barWidth = dataHelper.getBarWidth(value, valueId, deviceOrientation);
        if (barWidth == 0) {
            console.log("barWidth is zero, arrow not visible");
            bar.visible = false;
        } else {
            bar.visible = true;
            bar.preferredWidth = barWidth;
        }
    }
}
