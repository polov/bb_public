import bb.cascades 1.2
import bb.cascades.maps 1.2

TabbedPane {
    id: tabs
    showTabsOnActionBar: true
    
    Menu.definition: MenuDefinition {
        actions: [
            ActionItem {
                title: qsTr("About") + Retranslate.onLanguageChanged
                imageSource: "asset:///images/icons/ic_info.png"
                onTriggered: {
                    aboutSheet.open();
                }
            }
        ]
        settingsAction: SettingsActionItem {
            onTriggered: {
                settingsSheet.open();
            }
        }
    }
    //main tab, local situation
    Tab {
        title: qsTr("Local") + Retranslate.onLanguageChanged
        imageSource: "asset:///images/icons/ic_located.png"
        
        delegateActivationPolicy: TabDelegateActivationPolicy.ActivateImmediately
        
        delegate: Delegate {
            id: detailPageDelegate
            DetailPage {
                id: detailPage
                dataModel: appDataModel 
            }
        }
        
        onTriggered: {
            tabs.onFullscreened();
        }
    }
    //list of all stations
    Tab {
        title: qsTr("List") + Retranslate.onLanguageChanged
        imageSource: "asset:///images/icons/ic_list.png"

        delegate: Delegate {
            id: listPageDelegate
            ListPage {
                id: listPage
                dataModel: appDataModel
            }
        }
    }
    //map view on all stations
    Tab {
        title: qsTr("Map") + Retranslate.onLanguageChanged
        imageSource: "asset:///images/icons/ic_location_.png"
        /*
        delegate: Delegate {
            id: mapPageDelegate
            MapPage {
                id: mapPage
                dataModel: appDataModel
                mode: 1
            }
        }
*/        

        Page {
            id: mapPage
            Container {
                id: mapViewContainer
                layout: DockLayout {
                }
                background: Color.Gray
                MapView {
                    id: map
                    horizontalAlignment: HorizontalAlignment.Center
                    verticalAlignment: VerticalAlignment.Center
                    altitude: 780000.0
                    latitude: 50.0479
                    longitude: 15.4383
                    //accessibility.name: "Air quality map view"
                }
            }
            actions: [
                ActionItem {
                    title: qsTr("Update Data") + Retranslate.onLanguageChanged
                	imageSource: "asset:///images/icons/ic_reload.png"
                	onTriggered: {
                    	dataHelper.updateSourceFile();
                    }
                }
            ]
        }

    }

    function onSourceFileUpdated() {
        console.log("Source file updated, data model update requested");
        dataHelper.loadDataModel(appDataModel);
    }
    
    function onFullscreened() {
        geoHelper.updatePosition();
    }
    
    function onDataModelUpdated() {
        console.log("main page data model updated");
        geoHelper.putPins(map, appDataModel);
        console.log("model updated, stop indicator if running");
    }
    
    function onDataUpdateStarted() {
        console.log("data update starting, start indicator")
    }

    onCreationCompleted: {
        //connect to signals
        dataHelper.sourceFileUpdated.connect(tabs.onSourceFileUpdated);
        console.log("Main page connected to dataHelper.sourceFileUpdated signal");
        
        dataHelper.dataModelUpdated.connect(tabs.onDataModelUpdated);
        console.log("Main Page connected to dataHelper.dataModelUpdated signal");
        
        dataHelper.dataUpdateStarted.connect(tabs.onDataUpdateStarted);
        console.log("Main Page connected to dataHelper.dataUpdateStarted signal");
        
        //load data into data model
        dataHelper.loadDataModel(appDataModel);
        //load last known position
        geoHelper.updatePositionFromSettings();
        //find actual geolocation after app launch
        geoHelper.updatePosition();
        //try to download and refresh data
        dataHelper.updateSourceFile();
        
        Application.fullscreen.connect(tabs.onFullscreened);
    }

    attachedObjects: [
        GroupDataModel {
            id: appDataModel
            sortingKeys: [ "name" ]
        },
        AboutSheet {
            id: aboutSheet
        },
        SettingsSheet {
            id: settingsSheet
        }
    ]
}