import bb.cascades 1.2

Container {
    id: activeFrameContainer
    Container {
        id: headerContainer
        layout: StackLayout {
            orientation: LayoutOrientation.RightToLeft
        }
        background: Color.create("#ff00aae1")
        preferredHeight: 62.0
        maxHeight: 62.0
        leftPadding: 5.0
        rightPadding: 5.0
        /*
        Container {
            ImageView {
                id: appActiveFrameIcon
                accessibility.name: "Application icon"
            }
            preferredWidth: 25.0
        }
        */
        Container {
            layoutProperties: StackLayoutProperties {
                spaceQuota: 1
            }
            verticalAlignment: VerticalAlignment.Center
            Label {
                id: nameLabel
                objectName: "nameLabel"
                text: nameProperty
                textStyle {
                    color: Color.White
                }
            }
        }
    }
    Container {
        layoutProperties: StackLayoutProperties {
            spaceQuota: 1
        }
        layout: DockLayout {
        }
        Container {
            horizontalAlignment: HorizontalAlignment.Center
            verticalAlignment: VerticalAlignment.Center
            Container {
                layout: StackLayout {
                    orientation: LayoutOrientation.LeftToRight
                }
                bottomPadding: 20.0
                Container {
                    layoutProperties: StackLayoutProperties {
                        spaceQuota: 1
                    }
                    ImageView {
                        id: hourlyIndexImage
                        horizontalAlignment: HorizontalAlignment.Center
                        objectName: "hourlyIndexImage"
                        preferredHeight: 100.0
                        preferredWidth: 100.0
                        accessibility.name: "Hourly index image"
                    }
                }
            }
            Container {
                layout: StackLayout {
                    orientation: LayoutOrientation.LeftToRight
                }
                Container {
                    layoutProperties: StackLayoutProperties {
                        spaceQuota: 1
                    }
                    Label {
                        id: hourlyIndexLabel
                        objectName: "hourlyIndexLabel"
                        horizontalAlignment: HorizontalAlignment.Center
                        accessibility.name: ""
                    }
                }
            }
        }
    }

    onHourlyIndexPropertyChanged: {
        hourlyIndexImage.visible = false;
        hourlyIndexImage.resetImageSource();
        if (hourlyIndexProperty < 1) {
            hourlyIndexLabel.text = qsTr("Not available") + Retranslate.onLanguageChanged;
            hourlyIndexImage.imageSource = "asset:///images/circle_gray.png";
        } else if (hourlyIndexProperty == 1) {
            hourlyIndexLabel.text = hourlyIndexProperty + qsTr(" - Very good") + Retranslate.onLanguageChanged;
            hourlyIndexImage.imageSource = "asset:///images/circle_blue.png";
        } else if (hourlyIndexProperty == 2) {
            hourlyIndexLabel.text = hourlyIndexProperty + qsTr(" - Good") + Retranslate.onLanguageChanged;
            hourlyIndexImage.imageSource = "asset:///images/circle_green.png";
        } else if (hourlyIndexProperty == 3) {
            hourlyIndexLabel.text = hourlyIndexProperty + qsTr(" - Fair") + Retranslate.onLanguageChanged;
            hourlyIndexImage.imageSource = "asset:///images/circle_yellow.png";
        } else if (hourlyIndexProperty == 4) {
            hourlyIndexLabel.text = hourlyIndexProperty + qsTr(" - Suitable") + Retranslate.onLanguageChanged;
            hourlyIndexImage.imageSource = "asset:///images/circle_orange.png";
        } else if (hourlyIndexProperty == 5) {
            hourlyIndexLabel.text = hourlyIndexProperty + qsTr(" - Poor") + Retranslate.onLanguageChanged;
            hourlyIndexImage.imageSource = "asset:///images/circle_red1.png";
        } else if (hourlyIndexProperty == 6) {
            hourlyIndexLabel.text = hourlyIndexProperty + qsTr(" - Very poor") + Retranslate.onLanguageChanged;
            hourlyIndexImage.imageSource = "asset:///images/circle_red2.png";
        }
        hourlyIndexImage.visible = true;
    }

    property string nameProperty
    property string hourlyIndexProperty

}
