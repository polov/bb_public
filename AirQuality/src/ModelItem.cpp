/*
 * ModelItem.cpp
 *
 *  Created on: Feb 3, 2014
 *      Author: martin
 */

#include "ModelItem.hpp"
#include  <QDebug>

ModelItem::ModelItem() :
		QObject(), code(""), name(""), hourlyIndex(0), longtitude(""), latitude(""), co_8(
				0.0), no2_1(0.0), o3_1(0.0), pm10_1(0.0), pm10_24(0.0), so2_1(
				0.0), distance(0.0), position(), visible(true) {

}

float ModelItem::getCo8() const {
	return co_8;
}

void ModelItem::setCo8(float co8) {
	co_8 = co8;
}

const QString& ModelItem::getCode() const {
	return code;
}

void ModelItem::setCode(const QString& code) {
	this->code = code;
}

int ModelItem::getHourlyIndex() const {
	return hourlyIndex;
}

void ModelItem::setHourlyIndex(int hourlyIndex) {
	this->hourlyIndex = hourlyIndex;
}

const QString& ModelItem::getLatitude() const {
	return latitude;
}

void ModelItem::setLatitude(const QString& latitude) {
	this->latitude = latitude;
}

const QString& ModelItem::getLongtitude() const {
	return longtitude;
}

void ModelItem::setLongtitude(const QString& longtitude) {
	this->longtitude = longtitude;
}

const QString& ModelItem::getName() const {
	return name;
}

void ModelItem::setName(const QString& name) {
	this->name = name;
}

float ModelItem::getNo21() const {
	return no2_1;
}

void ModelItem::setNo21(float no21) {
	no2_1 = no21;
}

float ModelItem::getO31() const {
	return o3_1;
}

void ModelItem::setO31(float o31) {
	o3_1 = o31;
}

float ModelItem::getPm101() const {
	return pm10_1;
}

void ModelItem::setPm101(float pm101) {
	pm10_1 = pm101;
}

float ModelItem::getPm1024() const {
	return pm10_24;
}

void ModelItem::setPm1024(float pm1024) {
	pm10_24 = pm1024;
}

float ModelItem::getSo21() const {
	return so2_1;
}

void ModelItem::setSo21(float so21) {
	so2_1 = so21;
}

QGeoPositionInfo ModelItem::getPositionInfo() {
	return position;
}

float ModelItem::getDistance() const {
	return distance;
}

QString ModelItem::getDistanceString() const {
	return QString::number(distance, 'f', 1);
}

bool ModelItem::isVisible() const {
	return visible;
}

void ModelItem::setVisible(bool visible) {
	this->visible = visible;
	emit visibleChanged();
}

void ModelItem::setDistance(float distance) {
	this->distance = distance;
}

void ModelItem::setPositionInfo(const QString& longtitude, const QString& latitude) {
	QGeoCoordinate coordinate;
	bool ok;
	double lat = latitude.toDouble(&ok);
	double longt = longtitude.toDouble(&ok);
	coordinate.setLatitude(lat);
	coordinate.setLongitude(longt);
	position.setCoordinate(coordinate);
}

ModelItem::~ModelItem() {
	// TODO Auto-generated destructor stub
}


