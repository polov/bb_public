/*
 * DataHelper.cpp
 *
 *  Created on: Feb 2, 2014
 *      Author: martin
 */

#include "DataHelper.hpp"

#include <QDebug>
#include <bb/data/XmlDataAccess>
#include <QDateTime>

#include <bb/device/DisplayInfo>

#include <QSettings>

#include "ModelItem.hpp"

DataHelper::DataHelper() :
		QObject(), datetime_to(""), updatingData(false), listType(1) {

	QSettings settings("polovincak", "AirQuality");
	if (!settings.value("datetime_to").isNull()) {
		datetime_to = settings.value("datetime_to").toString();
		qDebug() << "datetime_to value loaded from settings" << datetime_to;
	} else {
		qDebug() << "No datetime_to value found in settings";
	}

	bool ok;
	if (!settings.value("listType").isNull()) {
		listType = settings.value("listType").toInt(&ok);
		qDebug() << "listType value loaded from settings" << listType;
	} else {
		qDebug() << "No listType value found in settings";

	}

	if (!settings.value("notifications").isNull()) {
		notifications = settings.value("notifications").toBool();
		qDebug() << "notifications value loaded from settings" << notifications;
	} else {
		qDebug() << "No notifications value found in settings";
	}

	notificationsHelper = new NotificationHelper();
	timer = new QTimer(this);
	connect(timer, SIGNAL(timeout()), this, SLOT(callUpdateSourceFile()));

	qDebug() << "DataHelper ready";
}

DataHelper::~DataHelper() {
	qDebug() << "DataHelper destroyed";
}

void DataHelper::loadDataModel(bb::cascades::GroupDataModel *model) {

	qDebug() << "Loading data";

	bb::data::XmlDataAccess xda;
	QVariant list = xda.load(QDir::currentPath() + "/data/AIMdata_hourly.xml",
			"/AQ_hourly_index/Data/station");
	if (xda.hasError()) {
		qDebug() << xda.error().errorMessage();
	}

	bool items = false;
	ModelItem *modelItem;
	bool ok;
	foreach(QVariant item,list.value<QVariantList>())
	{
		modelItem = new ModelItem();
		modelItem->setCode(item.toMap().value("code").toString());
		modelItem->setName(
				item.toMap().value("name").toString().replace("Pha", "Praha ")); //+ QDateTime::currentDateTime().toString("HH:mm:ss"));
		modelItem->setHourlyIndex(
				item.toMap().value("AQ_hourly_index").toMap().value("value").toInt(
						&ok));
		modelItem->setLongtitude(
				item.toMap().value("wgs84_longitude").toString());
		modelItem->setLatitude(item.toMap().value("wgs84_latitude").toString());
		modelItem->setPositionInfo(modelItem->getLongtitude(),
				modelItem->getLatitude());

		QList<QVariant> l1 = item.toMap().value("measurement").toMap().value(
				"component").toList();
		QList<QVariant> l2 = item.toMap().value("measurement").toMap().value(
				"averaged_time").toList();
		QString measurementId;
		QString measurementHours;
		bool ok;
		for (int i = 0; i < l1.size(); i++) {
			measurementId = l1.at(i).toString();
			measurementHours =
					l2.at(i).toMap().value("averaged_hours").toString();
			if (measurementId == "CO" && measurementHours == "8") {
				modelItem->setCo8(l2.at(i).toMap().value("value").toFloat(&ok));
			} else if (measurementId == "NO2" && measurementHours == "1") {
				modelItem->setNo21(
						l2.at(i).toMap().value("value").toFloat(&ok));
			} else if (measurementId == "O3" && measurementHours == "1") {
				modelItem->setO31(l2.at(i).toMap().value("value").toFloat(&ok));
			} else if (measurementId == "PM10" && measurementHours == "1") {
				modelItem->setPm101(
						l2.at(i).toMap().value("value").toFloat(&ok));
			} else if (measurementId == "PM10" && measurementHours == "24") {
				modelItem->setPm1024(
						l2.at(i).toMap().value("value").toFloat(&ok));
			} else if (measurementId == "SO2" && measurementHours == "1") {
				modelItem->setSo21(
						l2.at(i).toMap().value("value").toFloat(&ok));
			}
		}
		if (!items) {
			model->clear();
			items = true;
		}
		if (modelItem->getCo8() > 0 || modelItem->getNo21() > 0
				|| modelItem->getO31() > 0 || modelItem->getPm101() > 0
				|| modelItem->getPm1024() > 0 || modelItem->getSo21() > 0) {
			model->insert(modelItem);
		}
	}

	qDebug() << "Got data, model size:" << model->size();

	qDebug() << "Loading date/time information";

	list = xda.load(QDir::currentPath() + "/data/AIMdata_hourly.xml",
			"/AQ_hourly_index/Data/datetime_to");
	if (xda.hasError()) {
		qDebug() << xda.error().errorMessage();
	}

	qDebug() << "Date/time information" << list.toString();

	QSettings settings("polovincak", "AirQuality");
	QString dts = list.toString();
	QDateTime dt = QDateTime::fromString(dts, "yyyy-MM-dd hh:mm");
	dts = dt.toString("dd.MM.yyyy hh:mm");
	settings.setValue("datetime_to", QVariant(dts));
	setDatetimeTo(dts);
	qDebug() << "datetime_to value added to settings" << dts;

	emit dataModelUpdated();
	emit dateTimeToChanged();
	return;
}

void DataHelper::updateSourceFile() {
	NetworkCommunicationManager* nm =
			new NetworkCommunicationManager("AirQuality",
					"http://portal.chmi.cz/files/portal/docs/uoco/web_generator/AIMdata_hourly.xml");
	connect(nm,
			SIGNAL(contentUpdated(QString,NetworkCommunicationManager*,bool)),
			this,
			SLOT(onContentUpdated(QString,NetworkCommunicationManager*,bool)));
	nm->refreshData();
	updatingData = true;
	emit updatingDataChanged();
}

void DataHelper::onContentUpdated(QString id, NetworkCommunicationManager* nm,
		bool error) {
	qDebug() << "Content update finished for" << id << ", error=" << error;
	disconnect(nm,
			SIGNAL(contentUpdated(QString,NetworkCommunicationManager*,bool)),
			this,
			SLOT(onContentUpdated(QString,NetworkCommunicationManager*,bool)));
	emit sourceFileUpdated();
	updatingData = false;
	emit updatingDataChanged();
	nm->deleteLater();
}

void DataHelper::callUpdateSourceFile() {
	updateSourceFile();
}

QString DataHelper::formatNumber(const float number) const {
	return QString::number(number, 'f', 1);
}

const QString& DataHelper::getDatetimeTo() const {
	return datetime_to;
}

void DataHelper::setDatetimeTo(const QString& datetimeTo) {
	datetime_to = datetimeTo;
}

int DataHelper::getBarWidth(QString value, int valueId, int orientation) {
	qDebug() << "Compute bar width from value" << value << " of type "
			<< valueId << " orientation" << orientation;
	bool ok;
	int computedBarSize = 0;
	if ((value.length() - 6) < 0) {
		qDebug() << "Returning zero bar size, value string size is"
				<< (value.length() - 6);
		return 0;
	}
	float number = value.mid(0, value.length() - 6).toFloat(&ok);
	float val1 = 0.0;
	float val2 = 0.0;
	float val3 = 0.0;
	float val4 = 0.0;
	float val5 = 0.0;
	float val6 = 0.0;
	if (valueId == 1 || valueId == 2) {
		val1 = 0.0;
		val2 = 20.0;
		val3 = 40.0;
		val4 = 70.0;
		val5 = 90.0;
		val6 = 180.0;
	} else if (valueId == 3) {
		val1 = 0.0;
		val2 = 33.0;
		val3 = 65.0;
		val4 = 120.0;
		val5 = 180.0;
		val6 = 240.0;
	} else if (valueId == 4) {
		val1 = 0.0;
		val2 = 25.0;
		val3 = 50.0;
		val4 = 100.0;
		val5 = 200.0;
		val6 = 400.0;
	} else if (valueId == 5) {
		val1 = 0.0;
		val2 = 25.0;
		val3 = 50.0;
		val4 = 120.0;
		val5 = 350.0;
		val6 = 500.0;
	} else if (valueId == 6) {
		val1 = 0.0;
		val2 = 1000.0;
		val3 = 2000.0;
		val4 = 4000.0;
		val5 = 10000.0;
		val6 = 30000.0;
	}

	int displayWidth = 0;
	bb::device::DisplayInfo display;
	if (orientation == 0) {
		displayWidth = display.pixelSize().width() - 50;
	} else {
		displayWidth = display.pixelSize().height() - 50;
	}
	int intervalBarSize = displayWidth / 6;

	qDebug() << "Display width" << displayWidth << " interval bar size is"
			<< intervalBarSize << "max value is" << val6;

	// value in interval 6 or greater
	if (number > val6) {
		computedBarSize = displayWidth;
		qDebug() << "got number val6 computedBarSize" << number << val6
				<< computedBarSize;
	} else
	//5
	if (number > val5) {
		//computedBarSize = 5*intervalBarSize;
		computedBarSize += ((number - (float) val5) / (val6 - (float) val5))
				* intervalBarSize + 4 * intervalBarSize;
		qDebug() << "got number val5 computedBarSize" << number << val5
				<< ((number - (float) val5) / (val6 - (float) val5))
				<< computedBarSize;
	} else
	//4
	if (number > val4) {
		//computedBarSize = 4*intervalBarSize;
		computedBarSize += ((number - val4) / (val5 - val4)) * intervalBarSize
				+ 3 * intervalBarSize;
		qDebug() << "got number val4 computedBarSize" << number << val4
				<< computedBarSize;
	} else
	// 3
	if (number > val3) {
		//computedBarSize = 3*intervalBarSize;
		computedBarSize += ((number - val3) / (val4 - val3)) * intervalBarSize
				+ 2 * intervalBarSize;
		qDebug() << "got number val3 computedBarSize" << number << val3
				<< computedBarSize;
	} else
	// 2
	if (number > val2) {
		//computedBarSize = 2*intervalBarSize;
		computedBarSize += ((number - val2) / (val3 - val2)) * intervalBarSize
				+ intervalBarSize;
		qDebug() << "got number val2 val3 computedBarSize" << number << val2
				<< val3 << computedBarSize;
	} else
	// 1
	if (number > val1) {
		//computedBarSize += 1 * intervalBarSize;
		computedBarSize += (number / val2) * intervalBarSize;
		qDebug() << "got number val1 computedBarSize" << number << val1
				<< computedBarSize;
	} else {
		computedBarSize = 0;
	}

	qDebug() << "Computed bar size" << computedBarSize;

	return computedBarSize + 17 + 27; // add half of the size of the arrow
}

void DataHelper::saveForActiveFrame(QString name, QString hourlyIndex) {
	QSettings settings("polovincak", "AirQuality");
	settings.beginGroup("activeFrame");
	settings.setValue("name", QVariant(name));
	settings.setValue("hourly_index", QVariant(hourlyIndex));
	qDebug() << "Saved for active frame" << name << hourlyIndex;
}

bool DataHelper::isUpdatingData() const {
	return updatingData;
}

void DataHelper::setUpdatingData(bool updatingData) {
	this->updatingData = updatingData;
}

int DataHelper::getListType() const {
	return listType;
}

void DataHelper::setListType(int listType) {
	QSettings settings("polovincak", "AirQuality");
	settings.setValue("listType", QVariant(listType));
	qDebug() << "Saved listType" << listType;
	this->listType = listType;
}

bool DataHelper::isNotifications() const {
	return notifications;
}

void DataHelper::setNotifications(bool notifications) {
	QSettings settings("polovincak", "AirQuality");
	settings.setValue("notifications", QVariant(notifications));
	qDebug() << "Saved notifications" << notifications;
	this->notifications = notifications;
	if (notifications) {
		timer->start(3600000);
		qDebug() << "Timer started";
	} else {
		timer->stop();
		qDebug() << "Timer stopped";
	}
}

void DataHelper::handleForNotification(QVariantList indexPath,
		bb::cascades::GroupDataModel* model) {
	ModelItem *item;
	QObject *obj;
	obj = qvariant_cast<QObject *>(model->data(indexPath));
	item = qobject_cast<ModelItem *>(obj);
	if(item->getHourlyIndex() > 4) {
		//hourly index notification when index is 5 or 6
		QString title = tr("Air Quality");
		QString body = tr("Air quality in ") + item->getName();
		switch (item->getHourlyIndex()) {
			case 5:
				body.append(tr(" is ") + tr("poor") + ".");
				break;
			case 6:
				body.append(tr(" is ") + tr("very poor") + ".");
				break;
			default:
				body.append(tr(" is ok."));
				break;
		}
		notificationsHelper->createNotification(title, body);
	}
}

QByteArray DataHelper::encodeQString(const QString& text) const {
	return text.toUtf8();
}
