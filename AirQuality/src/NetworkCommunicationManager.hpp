/*
 * NetworkCommunicationManager.hpp
 *
 *  Created on: Jul 7, 2013
 *      Author: martin
 */

#include <QObject>
#include <QString>
#include <QtNetwork/QNetworkAccessManager>
#include <QtNetwork/QNetworkReply>

#ifndef NETWORKCOMMUNICATIONMANAGER_HPP_
#define NETWORKCOMMUNICATIONMANAGER_HPP_

class NetworkCommunicationManager: public QObject {
Q_OBJECT

public:
	NetworkCommunicationManager(QString id, QString url);
	~NetworkCommunicationManager();
	void refreshData();

signals:
	//emited after content update of data/*.xml files
	void contentUpdated(QString id, NetworkCommunicationManager *nm, bool error);

private slots:
	//called when http reply is pushed to application by device
	void requestFinished(QNetworkReply* reply);

private:
	// send http request to given url string
	void sendRequest(const QString url);
	// send http request to given url
	void sendRequest(const QUrl url);

	QNetworkAccessManager networkAccessManager;
	QString id;
	QString url;
	const int redirectMaxCount;
	int redirectCount;
	QUrl redirectLast;
};

#endif /* NETWORKCOMMUNICATIONMANAGER_HPP_ */
