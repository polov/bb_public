/*
 * NotificationHelper.cpp
 *
 *  Created on: Apr 5, 2014
 *      Author: martin
 */

#include "NotificationHelper.hpp"

#include <bb/platform/Notification>
#include <bb/system/InvokeRequest>

using namespace bb::platform;
using namespace bb::system;

NotificationHelper::NotificationHelper() :
		QObject() {
}

NotificationHelper::~NotificationHelper() {
}

void NotificationHelper::createNotification(QString title, QString body) {
	Notification *notification = new Notification();
	notification->setTitle(title);
	notification->setBody(body);

	InvokeRequest invokeRequest;
	invokeRequest.setTarget("com.polov.airquality.invoke.open");
	invokeRequest.setAction("bb.action.OPEN");
	invokeRequest.setMimeType("application/vnd.polov.airquality");

	notification->setInvokeRequest(invokeRequest);
	notification->notify();
}
