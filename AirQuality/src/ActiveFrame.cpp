/*
 * ActiveFrame.cpp
 *
 *  Created on: May 28, 2013
 *      Author: martin
 */
#include "ActiveFrame.hpp"

#include <QDebug>
#include <QSettings>
#include <bb/cascades/Label>
#include <bb/platform/Notification>

ActiveFrame::ActiveFrame() :
		SceneCover(this) {

	sceneCoverQml = QmlDocument::create("asset:///activeFrame.qml").parent(
			this);
	sceneCoverContainer = sceneCoverQml->createRootObject<Container>();
	setContent(sceneCoverContainer);

	QObject::connect(Application::instance(), SIGNAL(thumbnail()), this,
			SLOT(appInThumbnail()));
	QObject::connect(Application::instance(), SIGNAL(fullscreen()), this,
			SLOT(appInFullScreen()));
	QObject::connect(Application::instance(), SIGNAL(invisible()), this,
			SLOT(appInInvisible()));
}

ActiveFrame::~ActiveFrame() {

}

void ActiveFrame::appInThumbnail() {
	qDebug() << "App thumbnailed";
	QSettings settings("polovincak", "AirQuality");
	settings.beginGroup("activeFrame");
	QString name = settings.value("name", "").toString();
	QString hourlyIndex = settings.value("hourly_index", "").toString();
	qDebug() << "Fill active frame with" << name << hourlyIndex;

	sceneCoverContainer->setProperty("nameProperty", QVariant(name));
	sceneCoverContainer->setProperty("hourlyIndexProperty", QVariant(hourlyIndex));
}

void ActiveFrame::appInFullScreen() {
	qDebug() << "App fullscreened";
	bb::platform::Notification::deleteAllFromInbox();
}

void ActiveFrame::appInInvisible() {
	qDebug() << "App invisible";
}

void ActiveFrame::update() {
	qDebug() << "App active frame update called, refresh data...";
}

void ActiveFrame::appInScreenCoverUpdate() {
	qDebug() << "appInScreenCoverUpdate() fired";
}
