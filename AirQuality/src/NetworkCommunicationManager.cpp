/*
 * NetworkCommunicationManager.cpp
 *
 *  Created on: Jul 7, 2013
 *      Author: martin
 */
#include "NetworkCommunicationManager.hpp"

#include <QDebug>
#include <QFile>
#include <QDir>

NetworkCommunicationManager::NetworkCommunicationManager(QString id,
		QString url) :
		QObject(), networkAccessManager(this), id(id), url(url), redirectMaxCount(
				5), redirectCount(0), redirectLast() {

	connect(&networkAccessManager, SIGNAL(finished(QNetworkReply*)), this,
			SLOT(requestFinished(QNetworkReply*)));

	qDebug() << "NetworkCommunicationManager created, id " << id;
}

NetworkCommunicationManager::~NetworkCommunicationManager() {

	disconnect(&networkAccessManager, SIGNAL(finished(QNetworkReply*)), this,
			SLOT(requestFinished(QNetworkReply*)));

	//qDebug() << "NetworkCommunicationManager destroyed";
}

void NetworkCommunicationManager::refreshData() {
	qDebug() << "NetworkCommunicationManager refreshing data, id" << id
			<< ", url" << url;
	sendRequest(url);
}

void NetworkCommunicationManager::sendRequest(const QString url) {
	qDebug() << "NetworkCommunicationManager sending request to (QString)"
			<< url;
	QNetworkRequest request;
	request.setUrl(QUrl(url));
	//request.setRawHeader("User-Agent", "bb-phone/20120910");
	networkAccessManager.get(request);
	qDebug() << "NetworkCommunicationManager request sent to " << url;
}

void NetworkCommunicationManager::sendRequest(const QUrl url) {
	qDebug() << "NetworkCommunicationManager sending request to (URL)"
			<< url.toString();
	QNetworkRequest request;
	request.setUrl(url);
	networkAccessManager.get(request);
	qDebug() << "NetworkCommunicationManager request sent to " << url;
}

void NetworkCommunicationManager::requestFinished(QNetworkReply* reply) {
	qDebug() << "NetworkCommunicationManager id" << id
				<< ", reply to request arrived";
	//redirection check
	QVariant possibleRedirectUrl = reply->attribute(
			QNetworkRequest::RedirectionTargetAttribute);
	if (!possibleRedirectUrl.toUrl().isEmpty()) {
		redirectCount++;
		if (redirectCount > redirectMaxCount) {
			qDebug()
					<< "NetworkCommunicationManager max redirect count reached for "
					<< id << ", returning";
			emit contentUpdated(id, this, true);
			return;
		} else if (redirectLast == possibleRedirectUrl) {
			qDebug() << "NetworkCommunicationManager cyclic redirect for " << id
					<< ", returning";
			emit contentUpdated(id, this, true);
			return;
		}
		qDebug() << "NetworkCommunicationManager redirect found for id" << id
				<< ", redirecting to" << possibleRedirectUrl.toUrl();
		sendRequest(reply->url().resolved(possibleRedirectUrl.toUrl()));
		return;
	}

	QString path = QDir::currentPath() + "/data/AIMdata_hourly.xml";

	qDebug() << "NetworkCommunicationManager id" << id
			<< ", reply to request arrived, saving to file " << path;

	QFile contentFile(path);
	qDebug() << "got file object at" << path;
	if (reply->error() == QNetworkReply::NoError) {
		//if (!contentFile.open(QIODevice::ReadWrite)) {
		if (!contentFile.open(QIODevice::WriteOnly)) {
			qDebug() << "NetworkCommunicationManager id" << id
					<< ", failed to open" << path << "file for read write:"
					<< contentFile.errorString();
			emit contentUpdated(id, this, true);
			reply->deleteLater();
			return;
		}
		contentFile.write(reply->readAll());
		contentFile.flush();
		contentFile.close();
	} else {
		qDebug() << "NetworkCommunicationManager id" << id
				<< ",error reading reply" << reply->error();
		emit contentUpdated(id, this, true);
		reply->deleteLater();
		return;
	}
	qDebug() << "NetworkCommunicationManager id" << id
			<< ", processing the reply finished, id=" << id;
	emit contentUpdated(id, this, false);
	reply->deleteLater();
}
