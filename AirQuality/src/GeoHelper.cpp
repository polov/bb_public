/*
 * GeoHelper.cpp
 *
 *  Created on: Feb 4, 2014
 *      Author: martin
 */

#include "GeoHelper.hpp"

#include <QDebug>

#include <bb/platform/LocationMapInvoker>
#include <bb/platform/geo/Marker>
#include <bb/platform/geo/GeoLocation>
#include <bb/cascades/maps/DataProvider>

#include <QSettings>

using namespace bb::cascades;
using namespace bb::cascades::maps;
using namespace bb::platform;
using namespace bb::platform::geo;

GeoHelper::GeoHelper() :
        QObject()
{
    geosrc = QGeoPositionInfoSource::createDefaultSource(this);
    geosrc->setPreferredPositioningMethods(QGeoPositionInfoSource::NonSatellitePositioningMethods);
    positionUpdatedConnected = connect(geosrc, SIGNAL(positionUpdated(const QGeoPositionInfo &)),
            this, SLOT(onPositionUpdated(const QGeoPositionInfo &)));
    qDebug() << "GeoHelper initialized";
}

GeoHelper::~GeoHelper()
{
}

void GeoHelper::updatePosition()
{
    if (positionUpdatedConnected) {
        geosrc->requestUpdate();
        qDebug() << "Position update requested";
    } else {
        qDebug() << "Cannot request for location data!";
    }
}

void GeoHelper::onPositionUpdated(const QGeoPositionInfo &p)
{
    position = p;
    qDebug() << "User position updated, latitude" << p.coordinate().latitude() << "longtitude"
            << p.coordinate().longitude() << "type" << p.coordinate().type();
    emit positionUpdated();
    //save position to the settings for later use
    QSettings settings("polovincak", "AirQuality");
    settings.setValue("position/latitude", QVariant(position.coordinate().latitude()));
    settings.setValue("position/longitude", QVariant(position.coordinate().longitude()));
}

void GeoHelper::updatePositionFromSettings()
{

    QSettings settings("polovincak", "AirQuality");

    settings.beginGroup("position");

    QGeoCoordinate coordinate;

    if (settings.value("latitude").isNull() || settings.value("longitude").isNull()) {
        qDebug() << "User position have not been updated from settings, no saved values";
        coordinate.setLatitude(49.796041);
        coordinate.setLongitude(18.24718);
        position.setCoordinate(coordinate);

    } else {
        qDebug() << "Got position values in QSettings:" << settings.value("latitude").toDouble()
                << settings.value("longitude").toDouble();

        coordinate.setLatitude(settings.value("latitude").toDouble());
        coordinate.setLongitude(settings.value("longitude").toDouble());
        position.setCoordinate(coordinate);

        qDebug() << "User position updated from settings, latitude"
                << position.coordinate().latitude() << "longtitude"
                << position.coordinate().longitude() << "type" << position.coordinate().type();

    }
    emit positionUpdated();
}

QVariantList GeoHelper::findClosest(bb::cascades::GroupDataModel *model)
{
    QVariantList closestIndexPath;
    if (model->size() > 0) {
        qDebug() << "Finding closest from model";
        qreal closest = 80000000;
        ModelItem *item;
        QObject *obj;
        for (QVariantList indexPath = model->first(); !indexPath.isEmpty(); indexPath =
                model->after(indexPath)) {
            obj = qvariant_cast<QObject *>(model->data(indexPath));
            item = qobject_cast<ModelItem *>(obj);
            qreal distance = position.coordinate().distanceTo(item->getPositionInfo().coordinate());
            item->setDistance(distance / 1000);
            //if přidat AQ!=-1 & všechny ostatní aspoň jeden !=0
            if (isMeasuring(item) && distance < closest) {
                closest = distance;
                closestIndexPath = indexPath;
            }
        }
        obj = qvariant_cast<QObject *>(model->data(closestIndexPath));
        item = qobject_cast<ModelItem *>(obj);
        qDebug() << "Closest is " << item->getCode() << item->getName() << " distance "
                << item->getDistance();
        //qDebug() << "Distance " << closest;
    }
    return closestIndexPath;
}

void GeoHelper::putPin(MapView* mapView, GroupDataModel *model, QVariantList indexPath)
{
    qDebug() << "put pin on mapView";

    QObject *obj = qvariant_cast<QObject *>(model->data(indexPath));
    ModelItem *item = qobject_cast<ModelItem *>(obj);

    bool ok;

    DataProvider* provider = mapView->mapData()->provider("station-id-provider");
    if (provider != NULL) {
        qDebug() << "Provider station-id found, trying to remove";
        //mapView->mapData()->removeProvider("station-id");
        provider->clear();
    } else {
        qDebug() << "Provider station-id is not present";
        provider = new DataProvider("station-id-provider");
        mapView->mapData()->addProvider(provider);
    }

    // Create an instance of Marker to represent the pin
    Marker purpleMarker;
    purpleMarker.setIconUri("asset:///images/icon_190_0.png");
    purpleMarker.setIconSize(QSize(81, 81));

    // Offset the location coordinates so that it's near the bottom
    // and center of the icon (that is, the pin's point)
    purpleMarker.setLocationCoordinate(QPoint(40, 81));

    // Set the position on the icon so that the caption bubble's
    // tail points to the top center of the pin
    purpleMarker.setCaptionTailCoordinate(QPoint(40, 0));

    // Create the GeoLocation object and set the marker
    GeoLocation* station = new GeoLocation(item->getCode(), item->getName(),
            Point(item->getLatitude().toFloat(&ok), item->getLongtitude().toFloat(&ok)));
    station->setMarker(purpleMarker);

    // Add the location to your map
    //mapView->mapData()->remove("SBERA");
    provider->add(station);

    //mapView->mapData()->add(station);

    // When your location is within the map's viewport,
    // you will see the custom icon
    //mapView->setLocationOnVisible();

    double altitude = 3500.5;
    mapView->setAltitude(altitude);
    mapView->setLatitude(item->getLatitude().toFloat(&ok));
    mapView->setLongitude(item->getLongtitude().toFloat(&ok));

    qDebug() << "altitude:" << mapView->altitude();

    emit pinOnMapViewAdded();
}

void GeoHelper::putPins(MapView* mapView, GroupDataModel *model)
{

    qDebug() << "put pins on mapView, model size" << model->size();
    double altitude = 780000.0;
    if (mapView != NULL) {
        mapView->setAltitude(altitude);
        mapView->setLatitude(50.0479);
        mapView->setLongitude(15.4383);
    }

    qDebug() << "MapView altitude set";

    mapView->setCaptionGoButtonVisible(false);

    if ((model != NULL) && (model->size() > 0)) {

        bool ok;
        int iconSize = 81;
        int lCoordX = 40;
        int lCoordY = 81;
        int captionCoordX = 40;
        int captionCoordY = 0;

        Marker marker0;
        //marker0.setIconUri("asset:///images/icon_190_0.png");
        marker0.setIconUri("asset:///images/circle_gray_small.png");
        marker0.setIconSize(QSize(iconSize, iconSize));
        marker0.setLocationCoordinate(QPoint(lCoordX, lCoordY));
        marker0.setCaptionTailCoordinate(QPoint(captionCoordX, captionCoordY));

        Marker marker1;
        //marker1.setIconUri("asset:///images/icon_190_1.png");
        marker1.setIconUri("asset:///images/circle_blue_small.png");
        marker1.setIconSize(QSize(iconSize, iconSize));
        marker1.setLocationCoordinate(QPoint(lCoordX, lCoordY));
        marker1.setCaptionTailCoordinate(QPoint(captionCoordX, captionCoordY));

        Marker marker2;
        //marker2.setIconUri("asset:///images/icon_190_2.png");
        marker2.setIconUri("asset:///images/circle_green_small.png");
        marker2.setIconSize(QSize(iconSize, iconSize));
        marker2.setLocationCoordinate(QPoint(lCoordX, lCoordY));
        marker2.setCaptionTailCoordinate(QPoint(captionCoordX, captionCoordY));

        Marker marker3;
        //marker3.setIconUri("asset:///images/icon_190_3.png");
        marker3.setIconUri("asset:///images/circle_yellow_small.png");
        marker3.setIconSize(QSize(iconSize, iconSize));
        marker3.setLocationCoordinate(QPoint(lCoordX, lCoordY));
        marker3.setCaptionTailCoordinate(QPoint(captionCoordX, captionCoordY));

        Marker marker4;
        //marker4.setIconUri("asset:///images/icon_190_4.png");
        marker4.setIconUri("asset:///images/circle_orange_small.png");
        marker4.setIconSize(QSize(iconSize, iconSize));
        marker4.setLocationCoordinate(QPoint(lCoordX, lCoordY));
        marker4.setCaptionTailCoordinate(QPoint(captionCoordX, captionCoordY));

        Marker marker5;
        //marker5.setIconUri("asset:///images/icon_190_5.png");
        marker5.setIconUri("asset:///images/circle_red1_small.png");
        marker5.setIconSize(QSize(iconSize, iconSize));
        marker5.setLocationCoordinate(QPoint(lCoordX, lCoordY));
        marker5.setCaptionTailCoordinate(QPoint(captionCoordX, captionCoordY));

        Marker marker6;
        //marker6.setIconUri("asset:///images/icon_190_6.png");
        marker6.setIconUri("asset:///images/circle_red2_small.png");
        marker6.setIconSize(QSize(iconSize, iconSize));
        marker6.setLocationCoordinate(QPoint(lCoordX, lCoordY));
        marker6.setCaptionTailCoordinate(QPoint(captionCoordX, captionCoordY));

        qDebug() << "Markers created";

        DataProvider* provider = mapView->mapData()->provider("all-station-id-provider");
        if (provider != NULL) {
            qDebug() << "Provider all-station-id-provider found, trying to remove";
            //mapView->mapData()->removeProvider("station-id");
            provider->clear();
        } else {
            qDebug() << "Provider all-station-id-provider is not present";
            provider = new DataProvider("all-station-id-provider");
            mapView->mapData()->addProvider(provider);
        }

        qDebug() << "MapView provider set";

        GeoLocation* station;
        ModelItem *item;
        QObject *obj;
        for (QVariantList indexPath = model->first(); !indexPath.isEmpty(); indexPath =
                model->after(indexPath)) {

            obj = qvariant_cast<QObject *>(model->data(indexPath));
            item = qobject_cast<ModelItem *>(obj);

            station = new GeoLocation(item->getCode(), item->getName(),
                    Point(item->getLatitude().toFloat(&ok), item->getLongtitude().toFloat(&ok)));
            if (item->getHourlyIndex() > 0) { //do not add pins of not measuring stations
                switch (item->getHourlyIndex()) {
                    case 6:
                        station->setMarker(marker6);
                        break;
                    case 5:
                        station->setMarker(marker5);
                        break;
                    case 4:
                        station->setMarker(marker4);
                        break;
                    case 3:
                        station->setMarker(marker3);
                        break;
                    case 2:
                        station->setMarker(marker2);
                        break;
                    case 1:
                        station->setMarker(marker1);
                        break;
                    default:
                        station->setMarker(marker0);
                        break;
                }
                provider->add(station);
            }
            //qDebug() << "MapView pin set" << item->getCode();
        }
    }

    emit pinOnMapViewAdded();
}

void GeoHelper::openMapWithPin(bb::cascades::GroupDataModel *model, QVariantList indexPath)
{
    QObject *obj = qvariant_cast<QObject *>(model->data(indexPath));
    ModelItem *item = qobject_cast<ModelItem *>(obj);

    if (item == NULL) {
        qDebug() << "Error while invoking map with pin";
        return;
    }

    LocationMapInvoker mapInvoker;
    bool ok;
    mapInvoker.setLocationLatitude(item->getLatitude().toFloat(&ok));
    mapInvoker.setLocationLongitude(item->getLongtitude().toFloat(&ok));
    mapInvoker.setAltitude(3000.0);
    mapInvoker.setLocationName(item->getName().toUtf8());
    mapInvoker.setLocationDescription("");
    mapInvoker.setGeocodeLocationEnabled(false);
    mapInvoker.setCurrentLocationEnabled(false);
    mapInvoker.go();

}

bool GeoHelper::isMeasuring(ModelItem *item) const
{
    return ((item->getHourlyIndex() > 0)
            && (item->getCo8() > 0 || item->getNo21() > 0 || item->getO31() > 0
                    || item->getPm101() > 0 || item->getPm1024() > 0 || item->getSo21() > 0));
}
