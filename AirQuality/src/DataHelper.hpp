/*
 * DataHelper.hpp
 *
 *  Created on: Feb 2, 2014
 *      Author: martin
 */

#ifndef DATAHELPER_HPP_
#define DATAHELPER_HPP_

#include <QObject>
#include <bb/cascades/GroupDataModel>

#include "NetworkCommunicationManager.hpp"
#include "NotificationHelper.hpp"

class DataHelper: public QObject {
Q_OBJECT
Q_PROPERTY(QString datetime_to READ getDatetimeTo WRITE setDatetimeTo NOTIFY dateTimeToChanged)
Q_PROPERTY(bool updatingData READ isUpdatingData WRITE setUpdatingData NOTIFY updatingDataChanged)
Q_PROPERTY(int listType READ getListType WRITE setListType NOTIFY listTypeChanged)
Q_PROPERTY(bool notifications READ isNotifications WRITE setNotifications NOTIFY notificationsChanged)

public:
	DataHelper();
	virtual ~DataHelper();

	Q_INVOKABLE
	void updateSourceFile();
	Q_INVOKABLE
	void loadDataModel(bb::cascades::GroupDataModel* model);
	Q_INVOKABLE
	QString formatNumber(const float number) const;
	Q_INVOKABLE
	int getBarWidth(QString value, int valueId, int orientation);
	Q_INVOKABLE
	void saveForActiveFrame(QString name, QString hourlyIndex);
	Q_INVOKABLE
	void handleForNotification(QVariantList indexPath, bb::cascades::GroupDataModel* model);
	Q_INVOKABLE
	QByteArray encodeQString(const QString& toEncode) const;

	const QString& getDatetimeTo() const;
	void setDatetimeTo(const QString& datetimeTo);
	bool isUpdatingData() const;
	void setUpdatingData(bool updatingData);
	int getListType() const;
	void setListType(int listType);
	bool isNotifications() const;
	void setNotifications(bool notifications);

signals:
	void sourceFileUpdated();
	void dataModelUpdated();
	void dateTimeToChanged();
	void dataUpdateStarted();
	void updatingDataChanged();
	void listTypeChanged();
	void notificationsChanged();

private slots:
	void onContentUpdated(QString, NetworkCommunicationManager*, bool);
	void callUpdateSourceFile();

private:
	QString datetime_to;
	bool updatingData;
	int listType;
	bool notifications;
	QTimer *timer;
	NotificationHelper *notificationsHelper;

};

#endif /* DATAHELPER_HPP_ */
