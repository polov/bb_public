/*
 * GeoHelper.hpp
 *
 *  Created on: Feb 4, 2014
 *      Author: martin
 */

#ifndef GEOHELPER_HPP_
#define GEOHELPER_HPP_

#include <QObject>

#include <QtLocationSubset/QGeoPositionInfo>
#include <QtLocationSubset/QGeoPositionInfoSource>
#include <QtLocationSubset/QGeoSatelliteInfo>
#include <QtLocationSubset/QGeoSatelliteInfoSource>

#include <bb/cascades/maps/MapData>
#include <bb/cascades/maps/MapView>

#include <bb/cascades/GroupDataModel>

#include "ModelItem.hpp"

using namespace QtMobilitySubset;

class GeoHelper: public QObject {
Q_OBJECT

public:
	GeoHelper();
	virtual ~GeoHelper();

	Q_INVOKABLE
	void updatePosition();
	Q_INVOKABLE
	void updatePositionFromSettings();
	Q_INVOKABLE
	QVariantList findClosest(bb::cascades::GroupDataModel *model);
	Q_INVOKABLE
	void putPin(bb::cascades::maps::MapView* mapView, bb::cascades::GroupDataModel *model, QVariantList indexPath);
	Q_INVOKABLE
	void putPins(bb::cascades::maps::MapView* mapView, bb::cascades::GroupDataModel *model);
	Q_INVOKABLE
	void openMapWithPin(bb::cascades::GroupDataModel *model, QVariantList indexPath);

signals:
	void positionUpdated();
	void pinOnMapViewAdded();

private slots:
	void onPositionUpdated(const QGeoPositionInfo &);

private:
	bool isMeasuring(ModelItem *item) const;

	QGeoPositionInfoSource *geosrc;
	QGeoPositionInfo position;
	bool positionUpdatedConnected;

};

#endif /* GEOHELPER_HPP_ */
