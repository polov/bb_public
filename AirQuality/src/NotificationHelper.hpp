/*
 * NotificationHelper.hpp
 *
 *  Created on: Apr 5, 2014
 *      Author: martin
 */

#ifndef NOTIFICATIONHELPER_HPP_
#define NOTIFICATIONHELPER_HPP_

class NotificationHelper: public QObject {
Q_OBJECT

public:
	NotificationHelper();
	virtual ~NotificationHelper();

	void createNotification(QString title, QString body);
};

#endif /* NOTIFICATIONHELPER_HPP_ */
