/*
 * ModelItem.hpp
 *
 *  Created on: Feb 3, 2014
 *      Author: martin
 */

#ifndef MODELITEM_HPP_
#define MODELITEM_HPP_

#include <QObject>
#include <QtLocationSubset/QGeoPositionInfo>

using namespace QtMobilitySubset;

class ModelItem: public QObject {
Q_OBJECT
Q_PROPERTY(QString code READ getCode WRITE setCode NOTIFY codeChanged)
Q_PROPERTY(QString name READ getName WRITE setName NOTIFY nameChanged)
Q_PROPERTY(int hourlyIndex READ getHourlyIndex WRITE setHourlyIndex NOTIFY hourlyIndexChanged)
Q_PROPERTY(QString latitude READ getLatitude WRITE setLatitude NOTIFY latitudeChanged)
Q_PROPERTY(QString longtitude READ getLongtitude WRITE setLongtitude NOTIFY longtitudeChanged)
Q_PROPERTY(float co_8 READ getCo8 WRITE setCo8 NOTIFY co8Changed)
Q_PROPERTY(float no2_1 READ getNo21 WRITE setNo21 NOTIFY no21Changed)
Q_PROPERTY(float o3_1 READ getO31 WRITE setO31 NOTIFY o31Changed)
Q_PROPERTY(float pm10_1 READ getPm101 WRITE setPm101 NOTIFY pm101Changed)
Q_PROPERTY(float pm10_24 READ getPm1024 WRITE setPm1024 NOTIFY pm1024Changed)
Q_PROPERTY(float so2_1 READ getSo21 WRITE setSo21 NOTIFY so21Changed)
Q_PROPERTY(QString distance READ getDistanceString NOTIFY distanceChanged)
Q_PROPERTY(QGeoPositionInfo position READ getPositionInfo NOTIFY positionChanged)
//Q_PROPERTY(QString longtitude READ getLongtitude WRITE setLongtitude NOTIFY longtitudeChanged)
//Q_PROPERTY(QString latitude READ getLatitude WRITE setLatitude NOTIFY latitudeChanged)
Q_PROPERTY(bool visible READ isVisible WRITE setVisible NOTIFY visibleChanged)

public:
	ModelItem();
	virtual ~ModelItem();

	float getCo8() const;
	void setCo8(float co8);
	const QString& getCode() const;
	void setCode(const QString& code);
	int getHourlyIndex() const;
	void setHourlyIndex(int hourlyIndex);
	const QString& getLatitude() const;
	void setLatitude(const QString& latitude);
	const QString& getLongtitude() const;
	void setLongtitude(const QString& longtitude);
	const QString& getName() const;
	void setName(const QString& name);
	float getNo21() const;
	void setNo21(float no21);
	float getO31() const;
	void setO31(float o31);
	float getPm101() const;
	void setPm101(float pm101);
	float getPm1024() const;
	void setPm1024(float pm1024);
	float getSo21() const;
	void setSo21(float so21);
	QGeoPositionInfo getPositionInfo();
	void setPositionInfo(const QString& longtitude, const QString& latitude);
	float getDistance() const;
	QString getDistanceString() const;
	void setDistance(float distance);
	bool isVisible() const;
	void setVisible(bool visible);

signals:
	void codeChanged();
	void nameChanged();
	void hourlyIndexChanged();
	void latitudeChanged();
	void longtitudeChanged();
	void co8Changed();
	void no21Changed();
	void o31Changed();
	void pm101Changed();
	void pm1024Changed();
	void so21Changed();
	void positionChanged();
	void distanceChanged();
	void visibleChanged();

private:
	QString code;
	QString name;
	int hourlyIndex;
	QString longtitude;
	QString latitude;
	float co_8;
	float no2_1;
	float o3_1;
	float pm10_1;
	float pm10_24;
	float so2_1;
	float distance;
	QGeoPositionInfo position;
	bool visible;

	Q_DISABLE_COPY (ModelItem)

};

Q_DECLARE_METATYPE(ModelItem*)

#endif /* MODELITEM_HPP_ */
