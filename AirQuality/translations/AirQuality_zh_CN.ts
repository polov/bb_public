<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="zh_CN">
<context>
    <name>AboutSheet</name>
    <message>
        <location filename="../assets/AboutSheet.qml" line="39"/>
        <source>Air Quality</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/AboutSheet.qml" line="60"/>
        <source>version</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/AboutSheet.qml" line="68"/>
        <source>Data provided by</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/AboutSheet.qml" line="85"/>
        <source>build by</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/AboutSheet.qml" line="103"/>
        <source>Privacy policy</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/AboutSheet.qml" line="113"/>
        <source>Done</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DataHelper</name>
    <message>
        <location filename="../src/DataHelper.cpp" line="367"/>
        <source>Air Quality</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/DataHelper.cpp" line="368"/>
        <source>Air quality in </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/DataHelper.cpp" line="371"/>
        <location filename="../src/DataHelper.cpp" line="374"/>
        <source> is </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/DataHelper.cpp" line="371"/>
        <source>poor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/DataHelper.cpp" line="374"/>
        <source>very poor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/DataHelper.cpp" line="377"/>
        <source> is ok.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DetailPage</name>
    <message>
        <location filename="../assets/DetailPage.qml" line="24"/>
        <location filename="../assets/mindw120h120du/DetailPage.qml" line="25"/>
        <source>Air Quality</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/DetailPage.qml" line="162"/>
        <location filename="../assets/DetailPage.qml" line="899"/>
        <location filename="../assets/mindw120h120du/DetailPage.qml" line="163"/>
        <location filename="../assets/mindw120h120du/DetailPage.qml" line="1025"/>
        <source>Not available</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/DetailPage.qml" line="172"/>
        <location filename="../assets/mindw120h120du/DetailPage.qml" line="173"/>
        <source>air quality</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/DetailPage.qml" line="255"/>
        <location filename="../assets/DetailPage.qml" line="358"/>
        <location filename="../assets/mindw120h120du/DetailPage.qml" line="265"/>
        <location filename="../assets/mindw120h120du/DetailPage.qml" line="377"/>
        <source>Particulate matter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/DetailPage.qml" line="274"/>
        <location filename="../assets/mindw120h120du/DetailPage.qml" line="283"/>
        <source>particulate matter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/DetailPage.qml" line="377"/>
        <location filename="../assets/mindw120h120du/DetailPage.qml" line="395"/>
        <source>particulate matter/24h</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/DetailPage.qml" line="472"/>
        <location filename="../assets/mindw120h120du/DetailPage.qml" line="498"/>
        <source>Ozone</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/DetailPage.qml" line="491"/>
        <location filename="../assets/mindw120h120du/DetailPage.qml" line="516"/>
        <source>ozone</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/DetailPage.qml" line="575"/>
        <location filename="../assets/mindw120h120du/DetailPage.qml" line="608"/>
        <source>Nitrogen dioxide</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/DetailPage.qml" line="595"/>
        <location filename="../assets/mindw120h120du/DetailPage.qml" line="627"/>
        <source>nitrogen dioxide</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/DetailPage.qml" line="691"/>
        <location filename="../assets/mindw120h120du/DetailPage.qml" line="731"/>
        <source>Sulfur dioxide</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/DetailPage.qml" line="710"/>
        <location filename="../assets/mindw120h120du/DetailPage.qml" line="749"/>
        <source>sulfur dioxide</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/DetailPage.qml" line="795"/>
        <location filename="../assets/mindw120h120du/DetailPage.qml" line="832"/>
        <source>Carbon monoxide</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/DetailPage.qml" line="815"/>
        <location filename="../assets/mindw120h120du/DetailPage.qml" line="851"/>
        <source>carbon monoxide</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/DetailPage.qml" line="872"/>
        <location filename="../assets/mindw120h120du/DetailPage.qml" line="912"/>
        <source>Air quality legend:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/DetailPage.qml" line="926"/>
        <location filename="../assets/mindw120h120du/DetailPage.qml" line="944"/>
        <source>1 - Very good</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/DetailPage.qml" line="953"/>
        <location filename="../assets/mindw120h120du/DetailPage.qml" line="971"/>
        <source>2 - Good</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/DetailPage.qml" line="980"/>
        <location filename="../assets/mindw120h120du/DetailPage.qml" line="998"/>
        <source>3 - Fair</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/DetailPage.qml" line="1007"/>
        <location filename="../assets/mindw120h120du/DetailPage.qml" line="1057"/>
        <source>4 - Suitable</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/DetailPage.qml" line="1034"/>
        <location filename="../assets/mindw120h120du/DetailPage.qml" line="1084"/>
        <source>5 - Poor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/DetailPage.qml" line="1061"/>
        <location filename="../assets/mindw120h120du/DetailPage.qml" line="1111"/>
        <source>6 - Very poor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/DetailPage.qml" line="1079"/>
        <location filename="../assets/mindw120h120du/DetailPage.qml" line="1148"/>
        <source>Update Data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/DetailPage.qml" line="1086"/>
        <location filename="../assets/mindw120h120du/DetailPage.qml" line="1155"/>
        <source>Update Position</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/DetailPage.qml" line="1093"/>
        <location filename="../assets/mindw120h120du/DetailPage.qml" line="1162"/>
        <source>Show on Map</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/DetailPage.qml" line="1101"/>
        <location filename="../assets/mindw120h120du/DetailPage.qml" line="1170"/>
        <source>Share</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/DetailPage.qml" line="1107"/>
        <location filename="../assets/mindw120h120du/DetailPage.qml" line="1176"/>
        <source>Air quality in </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/DetailPage.qml" line="1147"/>
        <location filename="../assets/mindw120h120du/DetailPage.qml" line="1216"/>
        <source>distance </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/DetailPage.qml" line="1156"/>
        <location filename="../assets/mindw120h120du/DetailPage.qml" line="1225"/>
        <source>Not Available</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/DetailPage.qml" line="1159"/>
        <location filename="../assets/mindw120h120du/DetailPage.qml" line="1228"/>
        <source>Very Good</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/DetailPage.qml" line="1162"/>
        <location filename="../assets/mindw120h120du/DetailPage.qml" line="1231"/>
        <source>Good</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/DetailPage.qml" line="1165"/>
        <location filename="../assets/mindw120h120du/DetailPage.qml" line="1234"/>
        <source>Fair</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/DetailPage.qml" line="1168"/>
        <location filename="../assets/mindw120h120du/DetailPage.qml" line="1237"/>
        <source>Suitable</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/DetailPage.qml" line="1171"/>
        <location filename="../assets/mindw120h120du/DetailPage.qml" line="1240"/>
        <source>Poor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/DetailPage.qml" line="1174"/>
        <location filename="../assets/mindw120h120du/DetailPage.qml" line="1243"/>
        <source>Very Poor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/DetailPage.qml" line="1316"/>
        <location filename="../assets/mindw120h120du/DetailPage.qml" line="1388"/>
        <source>Updated </source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ListDetailPage</name>
    <message>
        <location filename="../assets/ListDetailPage.qml" line="23"/>
        <location filename="../assets/mindw120h120du/ListDetailPage.qml" line="22"/>
        <source>Air Quality</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/ListDetailPage.qml" line="163"/>
        <location filename="../assets/ListDetailPage.qml" line="913"/>
        <location filename="../assets/mindw120h120du/ListDetailPage.qml" line="160"/>
        <location filename="../assets/mindw120h120du/ListDetailPage.qml" line="892"/>
        <source>Not available</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/ListDetailPage.qml" line="173"/>
        <location filename="../assets/mindw120h120du/ListDetailPage.qml" line="170"/>
        <source>air quality</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/ListDetailPage.qml" line="258"/>
        <location filename="../assets/ListDetailPage.qml" line="363"/>
        <location filename="../assets/mindw120h120du/ListDetailPage.qml" line="252"/>
        <location filename="../assets/mindw120h120du/ListDetailPage.qml" line="354"/>
        <source>Particulate matter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/ListDetailPage.qml" line="277"/>
        <location filename="../assets/mindw120h120du/ListDetailPage.qml" line="271"/>
        <source>particulate matter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/ListDetailPage.qml" line="382"/>
        <location filename="../assets/mindw120h120du/ListDetailPage.qml" line="373"/>
        <source>particulate matter/24h</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/ListDetailPage.qml" line="479"/>
        <location filename="../assets/mindw120h120du/ListDetailPage.qml" line="467"/>
        <source>Ozone</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/ListDetailPage.qml" line="498"/>
        <location filename="../assets/mindw120h120du/ListDetailPage.qml" line="486"/>
        <source>ozone</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/ListDetailPage.qml" line="584"/>
        <location filename="../assets/mindw120h120du/ListDetailPage.qml" line="569"/>
        <source>Nitrogen dioxide</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/ListDetailPage.qml" line="604"/>
        <location filename="../assets/mindw120h120du/ListDetailPage.qml" line="589"/>
        <source>nitrogen dioxide</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/ListDetailPage.qml" line="702"/>
        <location filename="../assets/mindw120h120du/ListDetailPage.qml" line="684"/>
        <source>Sulfur dioxide</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/ListDetailPage.qml" line="721"/>
        <location filename="../assets/mindw120h120du/ListDetailPage.qml" line="703"/>
        <source>sulfur dioxide</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/ListDetailPage.qml" line="808"/>
        <location filename="../assets/mindw120h120du/ListDetailPage.qml" line="787"/>
        <source>Carbon monoxide</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/ListDetailPage.qml" line="828"/>
        <location filename="../assets/mindw120h120du/ListDetailPage.qml" line="807"/>
        <source>carbon monoxide</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/ListDetailPage.qml" line="885"/>
        <location filename="../assets/mindw120h120du/ListDetailPage.qml" line="864"/>
        <source>Air quality legend:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/ListDetailPage.qml" line="940"/>
        <location filename="../assets/mindw120h120du/ListDetailPage.qml" line="919"/>
        <source>1 - Very good</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/ListDetailPage.qml" line="967"/>
        <location filename="../assets/mindw120h120du/ListDetailPage.qml" line="946"/>
        <source>2 - Good</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/ListDetailPage.qml" line="994"/>
        <location filename="../assets/mindw120h120du/ListDetailPage.qml" line="973"/>
        <source>3 - Fair</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/ListDetailPage.qml" line="1021"/>
        <location filename="../assets/mindw120h120du/ListDetailPage.qml" line="1000"/>
        <source>4 - Suitable</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/ListDetailPage.qml" line="1048"/>
        <location filename="../assets/mindw120h120du/ListDetailPage.qml" line="1027"/>
        <source>5 - Poor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/ListDetailPage.qml" line="1075"/>
        <location filename="../assets/mindw120h120du/ListDetailPage.qml" line="1054"/>
        <source>6 - Very poor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/ListDetailPage.qml" line="1092"/>
        <location filename="../assets/mindw120h120du/ListDetailPage.qml" line="1071"/>
        <source>Show on Map</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/ListDetailPage.qml" line="1101"/>
        <location filename="../assets/mindw120h120du/ListDetailPage.qml" line="1080"/>
        <source>Share</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/ListDetailPage.qml" line="1107"/>
        <location filename="../assets/mindw120h120du/ListDetailPage.qml" line="1086"/>
        <source>Air quality in </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/ListDetailPage.qml" line="1147"/>
        <location filename="../assets/mindw120h120du/ListDetailPage.qml" line="1126"/>
        <source>distance </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/ListDetailPage.qml" line="1156"/>
        <location filename="../assets/mindw120h120du/ListDetailPage.qml" line="1135"/>
        <source>Not Available</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/ListDetailPage.qml" line="1159"/>
        <location filename="../assets/mindw120h120du/ListDetailPage.qml" line="1138"/>
        <source>Very Good</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/ListDetailPage.qml" line="1162"/>
        <location filename="../assets/mindw120h120du/ListDetailPage.qml" line="1141"/>
        <source>Good</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/ListDetailPage.qml" line="1165"/>
        <location filename="../assets/mindw120h120du/ListDetailPage.qml" line="1144"/>
        <source>Fair</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/ListDetailPage.qml" line="1168"/>
        <location filename="../assets/mindw120h120du/ListDetailPage.qml" line="1147"/>
        <source>Suitable</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/ListDetailPage.qml" line="1171"/>
        <location filename="../assets/mindw120h120du/ListDetailPage.qml" line="1150"/>
        <source>Poor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/ListDetailPage.qml" line="1174"/>
        <location filename="../assets/mindw120h120du/ListDetailPage.qml" line="1153"/>
        <source>Very Poor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/ListDetailPage.qml" line="1292"/>
        <location filename="../assets/ListDetailPage.qml" line="1299"/>
        <location filename="../assets/mindw120h120du/ListDetailPage.qml" line="1271"/>
        <location filename="../assets/mindw120h120du/ListDetailPage.qml" line="1277"/>
        <source>Updated </source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ListPage</name>
    <message>
        <location filename="../assets/ListPage.qml" line="24"/>
        <source>Automated Stations List</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/ListPage.qml" line="67"/>
        <source>Data not complete</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/ListPage.qml" line="69"/>
        <source>Not available</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/ListPage.qml" line="71"/>
        <source>Very good</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/ListPage.qml" line="73"/>
        <source>Good</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/ListPage.qml" line="75"/>
        <source>Fair</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/ListPage.qml" line="77"/>
        <source>Suitable</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/ListPage.qml" line="79"/>
        <source>Poor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/ListPage.qml" line="81"/>
        <source>Very Poor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/ListPage.qml" line="173"/>
        <source>Distance </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/ListPage.qml" line="214"/>
        <source>Update Data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/ListPage.qml" line="223"/>
        <source>Quality Sort</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/ListPage.qml" line="225"/>
        <source>Letter Sort</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SettingsSheet</name>
    <message>
        <location filename="../assets/SettingsSheet.qml" line="22"/>
        <source>Air Quality</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/SettingsSheet.qml" line="22"/>
        <source>Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/SettingsSheet.qml" line="59"/>
        <source>Notifications</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/SettingsSheet.qml" line="76"/>
        <source>Notify when the air quality is poor.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/SettingsSheet.qml" line="88"/>
        <source>Done</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>activeFrame</name>
    <message>
        <location filename="../assets/activeFrame.qml" line="90"/>
        <source>Not available</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/activeFrame.qml" line="93"/>
        <source> - Very good</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/activeFrame.qml" line="96"/>
        <source> - Good</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/activeFrame.qml" line="99"/>
        <source> - Fair</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/activeFrame.qml" line="102"/>
        <source> - Suitable</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/activeFrame.qml" line="105"/>
        <source> - Poor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/activeFrame.qml" line="108"/>
        <source> - Very poor</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>main</name>
    <message>
        <location filename="../assets/main.qml" line="11"/>
        <source>About</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/main.qml" line="26"/>
        <source>Local</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/main.qml" line="45"/>
        <source>List</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/main.qml" line="58"/>
        <source>Map</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/main.qml" line="90"/>
        <source>Update Data</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>valueDetailPage</name>
    <message>
        <location filename="../assets/valueDetailPage.qml" line="32"/>
        <source>Measurement Level</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/valueDetailPage.qml" line="361"/>
        <location filename="../assets/valueDetailPage.qml" line="363"/>
        <source>Tiny pieces of solid or liquid matter associated with the Earth&apos;s atmosphere. They can adversely affect human health and also have impacts on climate and precipitation.&lt;br /&gt;&lt;a href=&quot;http://en.wikipedia.org/wiki/Particulates&quot;&gt;[wikipedia]&lt;/a&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/valueDetailPage.qml" line="365"/>
        <source>Low level ozone (or tropospheric ozone) is an atmospheric pollutant. Even very low concentrations of ozone can be harmful to the upper respiratory tract and the lungs.&lt;br /&gt;&lt;a href=&quot;http://en.wikipedia.org/wiki/Ozone&quot;&gt;[wikipedia]&lt;/a&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/valueDetailPage.qml" line="367"/>
        <source>Nitrogen dioxide is toxic by inhalation. Symptoms of poisoning (lung edema) tend to appear several hours after inhalation of a low but potentially fatal dose.&lt;br /&gt;&lt;a href=&quot;http://en.wikipedia.org/wiki/Nitrogen_dioxide&quot;&gt;[wikipedia]&lt;/a&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/valueDetailPage.qml" line="369"/>
        <source>Sulfur dioxide is a major air pollutant and has significant impacts upon human health.&lt;br /&gt;&lt;a href=&quot;http://en.wikipedia.org/wiki/Sulfur_dioxide&quot;&gt;[wikipedia]&lt;/a&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/valueDetailPage.qml" line="371"/>
        <source>A colorless, odorless, and tasteless gas that is slightly less dense than air. It is toxic to humans and animals when encountered in higher concentrations.&lt;br /&gt;&lt;a href=&quot;http://en.wikipedia.org/wiki/Carbon_monoxide&quot;&gt;[wikipedia]&lt;/a&gt;</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
