<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0">
<context>
    <name>AboutSheet</name>
    <message>
        <source>Air Quality</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Done</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Data provided by</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Privacy policy</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>build by</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>version</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DataHelper</name>
    <message>
        <source>Air Quality</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Air quality in </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>poor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>very poor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source> is </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source> is ok.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DetailPage</name>
    <message>
        <source>Air Quality</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Not available</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>air quality</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Particulate matter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>particulate matter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>particulate matter/24h</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Ozone</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>ozone</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Nitrogen dioxide</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>nitrogen dioxide</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Sulfur dioxide</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Carbon monoxide</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>carbon monoxide</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Air quality legend:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>1 - Very good</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>2 - Good</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>3 - Fair</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>4 - Suitable</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>5 - Poor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>6 - Very poor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Update Data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Update Position</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Show on Map</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Not Available</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Very Good</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Good</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Fair</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Suitable</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Poor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Very Poor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Updated </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>distance </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>sulfur dioxide</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Share</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Air quality in </source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ListDetailPage</name>
    <message>
        <source>Not available</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>air quality</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Particulate matter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>particulate matter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>particulate matter/24h</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Ozone</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>ozone</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Nitrogen dioxide</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>nitrogen dioxide</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Sulfur dioxide</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Carbon monoxide</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>carbon monoxide</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Air quality legend:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>1 - Very good</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>2 - Good</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>3 - Fair</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>4 - Suitable</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>5 - Poor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>6 - Very poor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Show on Map</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>distance </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Not Available</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Very Good</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Good</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Fair</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Suitable</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Poor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Very Poor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Air Quality</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>sulfur dioxide</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Share</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Air quality in </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Updated </source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ListPage</name>
    <message>
        <source>Automated Stations List</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Data not complete</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Not available</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Very good</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Good</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Fair</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Suitable</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Poor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Very Poor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Distance </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Update Data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Quality Sort</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Letter Sort</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SettingsSheet</name>
    <message>
        <source>Air Quality</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Notifications</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Notify when the air quality is poor.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Done</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>activeFrame</name>
    <message>
        <source>Not available</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source> - Very good</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source> - Good</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source> - Fair</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source> - Suitable</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source> - Poor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source> - Very poor</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>main</name>
    <message>
        <source>About</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Local</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>List</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Map</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Update Data</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>valueDetailPage</name>
    <message>
        <source>Measurement Level</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Tiny pieces of solid or liquid matter associated with the Earth&apos;s atmosphere. They can adversely affect human health and also have impacts on climate and precipitation.&lt;br /&gt;&lt;a href=&quot;http://en.wikipedia.org/wiki/Particulates&quot;&gt;[wikipedia]&lt;/a&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Low level ozone (or tropospheric ozone) is an atmospheric pollutant. Even very low concentrations of ozone can be harmful to the upper respiratory tract and the lungs.&lt;br /&gt;&lt;a href=&quot;http://en.wikipedia.org/wiki/Ozone&quot;&gt;[wikipedia]&lt;/a&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Nitrogen dioxide is toxic by inhalation. Symptoms of poisoning (lung edema) tend to appear several hours after inhalation of a low but potentially fatal dose.&lt;br /&gt;&lt;a href=&quot;http://en.wikipedia.org/wiki/Nitrogen_dioxide&quot;&gt;[wikipedia]&lt;/a&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Sulfur dioxide is a major air pollutant and has significant impacts upon human health.&lt;br /&gt;&lt;a href=&quot;http://en.wikipedia.org/wiki/Sulfur_dioxide&quot;&gt;[wikipedia]&lt;/a&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>A colorless, odorless, and tasteless gas that is slightly less dense than air. It is toxic to humans and animals when encountered in higher concentrations.&lt;br /&gt;&lt;a href=&quot;http://en.wikipedia.org/wiki/Carbon_monoxide&quot;&gt;[wikipedia]&lt;/a&gt;</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
