/****************************************************************************
** Meta object code from reading C++ file 'DataHelper.hpp'
**
** Created by: The Qt Meta Object Compiler version 63 (Qt 4.8.6)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../src/DataHelper.hpp"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'DataHelper.hpp' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.6. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_DataHelper[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
      16,   14, // methods
       4,   94, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       7,       // signalCount

 // signals: signature, parameters, type, tag, flags
      12,   11,   11,   11, 0x05,
      32,   11,   11,   11, 0x05,
      51,   11,   11,   11, 0x05,
      71,   11,   11,   11, 0x05,
      91,   11,   11,   11, 0x05,
     113,   11,   11,   11, 0x05,
     131,   11,   11,   11, 0x05,

 // slots: signature, parameters, type, tag, flags
     157,  154,   11,   11, 0x08,
     217,   11,   11,   11, 0x08,

 // methods: signature, parameters, type, tag, flags
     240,   11,   11,   11, 0x02,
     265,  259,   11,   11, 0x02,
     325,  318,  310,   11, 0x02,
     375,  349,  345,   11, 0x02,
     421,  404,   11,   11, 0x02,
     473,  457,   11,   11, 0x02,
     559,  550,  539,   11, 0x02,

 // properties: name, type, flags
     582,  310, 0x0a495003,
     599,  594, 0x01495103,
     612,  345, 0x02495103,
     621,  594, 0x01495103,

 // properties: notify_signal_id
       2,
       4,
       5,
       6,

       0        // eod
};

static const char qt_meta_stringdata_DataHelper[] = {
    "DataHelper\0\0sourceFileUpdated()\0"
    "dataModelUpdated()\0dateTimeToChanged()\0"
    "dataUpdateStarted()\0updatingDataChanged()\0"
    "listTypeChanged()\0notificationsChanged()\0"
    ",,\0onContentUpdated(QString,NetworkCommunicationManager*,bool)\0"
    "callUpdateSourceFile()\0updateSourceFile()\0"
    "model\0loadDataModel(bb::cascades::GroupDataModel*)\0"
    "QString\0number\0formatNumber(float)\0"
    "int\0value,valueId,orientation\0"
    "getBarWidth(QString,int,int)\0"
    "name,hourlyIndex\0saveForActiveFrame(QString,QString)\0"
    "indexPath,model\0"
    "handleForNotification(QVariantList,bb::cascades::GroupDataModel*)\0"
    "QByteArray\0toEncode\0encodeQString(QString)\0"
    "datetime_to\0bool\0updatingData\0listType\0"
    "notifications\0"
};

void DataHelper::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        DataHelper *_t = static_cast<DataHelper *>(_o);
        switch (_id) {
        case 0: _t->sourceFileUpdated(); break;
        case 1: _t->dataModelUpdated(); break;
        case 2: _t->dateTimeToChanged(); break;
        case 3: _t->dataUpdateStarted(); break;
        case 4: _t->updatingDataChanged(); break;
        case 5: _t->listTypeChanged(); break;
        case 6: _t->notificationsChanged(); break;
        case 7: _t->onContentUpdated((*reinterpret_cast< QString(*)>(_a[1])),(*reinterpret_cast< NetworkCommunicationManager*(*)>(_a[2])),(*reinterpret_cast< bool(*)>(_a[3]))); break;
        case 8: _t->callUpdateSourceFile(); break;
        case 9: _t->updateSourceFile(); break;
        case 10: _t->loadDataModel((*reinterpret_cast< bb::cascades::GroupDataModel*(*)>(_a[1]))); break;
        case 11: { QString _r = _t->formatNumber((*reinterpret_cast< const float(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< QString*>(_a[0]) = _r; }  break;
        case 12: { int _r = _t->getBarWidth((*reinterpret_cast< QString(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])),(*reinterpret_cast< int(*)>(_a[3])));
            if (_a[0]) *reinterpret_cast< int*>(_a[0]) = _r; }  break;
        case 13: _t->saveForActiveFrame((*reinterpret_cast< QString(*)>(_a[1])),(*reinterpret_cast< QString(*)>(_a[2]))); break;
        case 14: _t->handleForNotification((*reinterpret_cast< QVariantList(*)>(_a[1])),(*reinterpret_cast< bb::cascades::GroupDataModel*(*)>(_a[2]))); break;
        case 15: { QByteArray _r = _t->encodeQString((*reinterpret_cast< const QString(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< QByteArray*>(_a[0]) = _r; }  break;
        default: ;
        }
    }
}

const QMetaObjectExtraData DataHelper::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject DataHelper::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_DataHelper,
      qt_meta_data_DataHelper, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &DataHelper::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *DataHelper::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *DataHelper::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_DataHelper))
        return static_cast<void*>(const_cast< DataHelper*>(this));
    return QObject::qt_metacast(_clname);
}

int DataHelper::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 16)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 16;
    }
#ifndef QT_NO_PROPERTIES
      else if (_c == QMetaObject::ReadProperty) {
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< QString*>(_v) = getDatetimeTo(); break;
        case 1: *reinterpret_cast< bool*>(_v) = isUpdatingData(); break;
        case 2: *reinterpret_cast< int*>(_v) = getListType(); break;
        case 3: *reinterpret_cast< bool*>(_v) = isNotifications(); break;
        }
        _id -= 4;
    } else if (_c == QMetaObject::WriteProperty) {
        void *_v = _a[0];
        switch (_id) {
        case 0: setDatetimeTo(*reinterpret_cast< QString*>(_v)); break;
        case 1: setUpdatingData(*reinterpret_cast< bool*>(_v)); break;
        case 2: setListType(*reinterpret_cast< int*>(_v)); break;
        case 3: setNotifications(*reinterpret_cast< bool*>(_v)); break;
        }
        _id -= 4;
    } else if (_c == QMetaObject::ResetProperty) {
        _id -= 4;
    } else if (_c == QMetaObject::QueryPropertyDesignable) {
        _id -= 4;
    } else if (_c == QMetaObject::QueryPropertyScriptable) {
        _id -= 4;
    } else if (_c == QMetaObject::QueryPropertyStored) {
        _id -= 4;
    } else if (_c == QMetaObject::QueryPropertyEditable) {
        _id -= 4;
    } else if (_c == QMetaObject::QueryPropertyUser) {
        _id -= 4;
    }
#endif // QT_NO_PROPERTIES
    return _id;
}

// SIGNAL 0
void DataHelper::sourceFileUpdated()
{
    QMetaObject::activate(this, &staticMetaObject, 0, 0);
}

// SIGNAL 1
void DataHelper::dataModelUpdated()
{
    QMetaObject::activate(this, &staticMetaObject, 1, 0);
}

// SIGNAL 2
void DataHelper::dateTimeToChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 2, 0);
}

// SIGNAL 3
void DataHelper::dataUpdateStarted()
{
    QMetaObject::activate(this, &staticMetaObject, 3, 0);
}

// SIGNAL 4
void DataHelper::updatingDataChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 4, 0);
}

// SIGNAL 5
void DataHelper::listTypeChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 5, 0);
}

// SIGNAL 6
void DataHelper::notificationsChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 6, 0);
}
QT_END_MOC_NAMESPACE
