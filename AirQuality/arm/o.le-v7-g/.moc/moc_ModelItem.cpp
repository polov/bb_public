/****************************************************************************
** Meta object code from reading C++ file 'ModelItem.hpp'
**
** Created by: The Qt Meta Object Compiler version 63 (Qt 4.8.6)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../src/ModelItem.hpp"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'ModelItem.hpp' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.6. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_ModelItem[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
      14,   14, // methods
      14,   84, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
      14,       // signalCount

 // signals: signature, parameters, type, tag, flags
      11,   10,   10,   10, 0x05,
      25,   10,   10,   10, 0x05,
      39,   10,   10,   10, 0x05,
      60,   10,   10,   10, 0x05,
      78,   10,   10,   10, 0x05,
      98,   10,   10,   10, 0x05,
     111,   10,   10,   10, 0x05,
     125,   10,   10,   10, 0x05,
     138,   10,   10,   10, 0x05,
     153,   10,   10,   10, 0x05,
     169,   10,   10,   10, 0x05,
     183,   10,   10,   10, 0x05,
     201,   10,   10,   10, 0x05,
     219,   10,   10,   10, 0x05,

 // properties: name, type, flags
     244,  236, 0x0a495103,
     249,  236, 0x0a495103,
     258,  254, 0x02495103,
     270,  236, 0x0a495103,
     279,  236, 0x0a495103,
     296,  290, 0x87495003,
     301,  290, 0x87495003,
     307,  290, 0x87495003,
     312,  290, 0x87495003,
     319,  290, 0x87495003,
     327,  290, 0x87495003,
     333,  236, 0x0a495001,
     359,  342, 0x00495009,
     373,  368, 0x01495103,

 // properties: notify_signal_id
       0,
       1,
       2,
       3,
       4,
       5,
       6,
       7,
       8,
       9,
      10,
      12,
      11,
      13,

       0        // eod
};

static const char qt_meta_stringdata_ModelItem[] = {
    "ModelItem\0\0codeChanged()\0nameChanged()\0"
    "hourlyIndexChanged()\0latitudeChanged()\0"
    "longtitudeChanged()\0co8Changed()\0"
    "no21Changed()\0o31Changed()\0pm101Changed()\0"
    "pm1024Changed()\0so21Changed()\0"
    "positionChanged()\0distanceChanged()\0"
    "visibleChanged()\0QString\0code\0name\0"
    "int\0hourlyIndex\0latitude\0longtitude\0"
    "float\0co_8\0no2_1\0o3_1\0pm10_1\0pm10_24\0"
    "so2_1\0distance\0QGeoPositionInfo\0"
    "position\0bool\0visible\0"
};

void ModelItem::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        ModelItem *_t = static_cast<ModelItem *>(_o);
        switch (_id) {
        case 0: _t->codeChanged(); break;
        case 1: _t->nameChanged(); break;
        case 2: _t->hourlyIndexChanged(); break;
        case 3: _t->latitudeChanged(); break;
        case 4: _t->longtitudeChanged(); break;
        case 5: _t->co8Changed(); break;
        case 6: _t->no21Changed(); break;
        case 7: _t->o31Changed(); break;
        case 8: _t->pm101Changed(); break;
        case 9: _t->pm1024Changed(); break;
        case 10: _t->so21Changed(); break;
        case 11: _t->positionChanged(); break;
        case 12: _t->distanceChanged(); break;
        case 13: _t->visibleChanged(); break;
        default: ;
        }
    }
    Q_UNUSED(_a);
}

const QMetaObjectExtraData ModelItem::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject ModelItem::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_ModelItem,
      qt_meta_data_ModelItem, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &ModelItem::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *ModelItem::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *ModelItem::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_ModelItem))
        return static_cast<void*>(const_cast< ModelItem*>(this));
    return QObject::qt_metacast(_clname);
}

int ModelItem::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 14)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 14;
    }
#ifndef QT_NO_PROPERTIES
      else if (_c == QMetaObject::ReadProperty) {
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< QString*>(_v) = getCode(); break;
        case 1: *reinterpret_cast< QString*>(_v) = getName(); break;
        case 2: *reinterpret_cast< int*>(_v) = getHourlyIndex(); break;
        case 3: *reinterpret_cast< QString*>(_v) = getLatitude(); break;
        case 4: *reinterpret_cast< QString*>(_v) = getLongtitude(); break;
        case 5: *reinterpret_cast< float*>(_v) = getCo8(); break;
        case 6: *reinterpret_cast< float*>(_v) = getNo21(); break;
        case 7: *reinterpret_cast< float*>(_v) = getO31(); break;
        case 8: *reinterpret_cast< float*>(_v) = getPm101(); break;
        case 9: *reinterpret_cast< float*>(_v) = getPm1024(); break;
        case 10: *reinterpret_cast< float*>(_v) = getSo21(); break;
        case 11: *reinterpret_cast< QString*>(_v) = getDistanceString(); break;
        case 12: *reinterpret_cast< QGeoPositionInfo*>(_v) = getPositionInfo(); break;
        case 13: *reinterpret_cast< bool*>(_v) = isVisible(); break;
        }
        _id -= 14;
    } else if (_c == QMetaObject::WriteProperty) {
        void *_v = _a[0];
        switch (_id) {
        case 0: setCode(*reinterpret_cast< QString*>(_v)); break;
        case 1: setName(*reinterpret_cast< QString*>(_v)); break;
        case 2: setHourlyIndex(*reinterpret_cast< int*>(_v)); break;
        case 3: setLatitude(*reinterpret_cast< QString*>(_v)); break;
        case 4: setLongtitude(*reinterpret_cast< QString*>(_v)); break;
        case 5: setCo8(*reinterpret_cast< float*>(_v)); break;
        case 6: setNo21(*reinterpret_cast< float*>(_v)); break;
        case 7: setO31(*reinterpret_cast< float*>(_v)); break;
        case 8: setPm101(*reinterpret_cast< float*>(_v)); break;
        case 9: setPm1024(*reinterpret_cast< float*>(_v)); break;
        case 10: setSo21(*reinterpret_cast< float*>(_v)); break;
        case 13: setVisible(*reinterpret_cast< bool*>(_v)); break;
        }
        _id -= 14;
    } else if (_c == QMetaObject::ResetProperty) {
        _id -= 14;
    } else if (_c == QMetaObject::QueryPropertyDesignable) {
        _id -= 14;
    } else if (_c == QMetaObject::QueryPropertyScriptable) {
        _id -= 14;
    } else if (_c == QMetaObject::QueryPropertyStored) {
        _id -= 14;
    } else if (_c == QMetaObject::QueryPropertyEditable) {
        _id -= 14;
    } else if (_c == QMetaObject::QueryPropertyUser) {
        _id -= 14;
    }
#endif // QT_NO_PROPERTIES
    return _id;
}

// SIGNAL 0
void ModelItem::codeChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 0, 0);
}

// SIGNAL 1
void ModelItem::nameChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 1, 0);
}

// SIGNAL 2
void ModelItem::hourlyIndexChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 2, 0);
}

// SIGNAL 3
void ModelItem::latitudeChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 3, 0);
}

// SIGNAL 4
void ModelItem::longtitudeChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 4, 0);
}

// SIGNAL 5
void ModelItem::co8Changed()
{
    QMetaObject::activate(this, &staticMetaObject, 5, 0);
}

// SIGNAL 6
void ModelItem::no21Changed()
{
    QMetaObject::activate(this, &staticMetaObject, 6, 0);
}

// SIGNAL 7
void ModelItem::o31Changed()
{
    QMetaObject::activate(this, &staticMetaObject, 7, 0);
}

// SIGNAL 8
void ModelItem::pm101Changed()
{
    QMetaObject::activate(this, &staticMetaObject, 8, 0);
}

// SIGNAL 9
void ModelItem::pm1024Changed()
{
    QMetaObject::activate(this, &staticMetaObject, 9, 0);
}

// SIGNAL 10
void ModelItem::so21Changed()
{
    QMetaObject::activate(this, &staticMetaObject, 10, 0);
}

// SIGNAL 11
void ModelItem::positionChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 11, 0);
}

// SIGNAL 12
void ModelItem::distanceChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 12, 0);
}

// SIGNAL 13
void ModelItem::visibleChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 13, 0);
}
QT_END_MOC_NAMESPACE
