/****************************************************************************
** Meta object code from reading C++ file 'GeoHelper.hpp'
**
** Created by: The Qt Meta Object Compiler version 63 (Qt 4.8.6)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../src/GeoHelper.hpp"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'GeoHelper.hpp' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.6. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_GeoHelper[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       9,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       2,       // signalCount

 // signals: signature, parameters, type, tag, flags
      11,   10,   10,   10, 0x05,
      29,   10,   10,   10, 0x05,

 // slots: signature, parameters, type, tag, flags
      49,   10,   10,   10, 0x08,

 // methods: signature, parameters, type, tag, flags
      85,   10,   10,   10, 0x02,
     102,   10,   10,   10, 0x02,
     150,  144,  131,   10, 0x02,
     217,  193,   10,   10, 0x02,
     311,  297,   10,   10, 0x02,
     395,  379,   10,   10, 0x02,

       0        // eod
};

static const char qt_meta_stringdata_GeoHelper[] = {
    "GeoHelper\0\0positionUpdated()\0"
    "pinOnMapViewAdded()\0"
    "onPositionUpdated(QGeoPositionInfo)\0"
    "updatePosition()\0updatePositionFromSettings()\0"
    "QVariantList\0model\0"
    "findClosest(bb::cascades::GroupDataModel*)\0"
    "mapView,model,indexPath\0"
    "putPin(bb::cascades::maps::MapView*,bb::cascades::GroupDataModel*,QVar"
    "iantList)\0"
    "mapView,model\0"
    "putPins(bb::cascades::maps::MapView*,bb::cascades::GroupDataModel*)\0"
    "model,indexPath\0"
    "openMapWithPin(bb::cascades::GroupDataModel*,QVariantList)\0"
};

void GeoHelper::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        GeoHelper *_t = static_cast<GeoHelper *>(_o);
        switch (_id) {
        case 0: _t->positionUpdated(); break;
        case 1: _t->pinOnMapViewAdded(); break;
        case 2: _t->onPositionUpdated((*reinterpret_cast< const QGeoPositionInfo(*)>(_a[1]))); break;
        case 3: _t->updatePosition(); break;
        case 4: _t->updatePositionFromSettings(); break;
        case 5: { QVariantList _r = _t->findClosest((*reinterpret_cast< bb::cascades::GroupDataModel*(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< QVariantList*>(_a[0]) = _r; }  break;
        case 6: _t->putPin((*reinterpret_cast< bb::cascades::maps::MapView*(*)>(_a[1])),(*reinterpret_cast< bb::cascades::GroupDataModel*(*)>(_a[2])),(*reinterpret_cast< QVariantList(*)>(_a[3]))); break;
        case 7: _t->putPins((*reinterpret_cast< bb::cascades::maps::MapView*(*)>(_a[1])),(*reinterpret_cast< bb::cascades::GroupDataModel*(*)>(_a[2]))); break;
        case 8: _t->openMapWithPin((*reinterpret_cast< bb::cascades::GroupDataModel*(*)>(_a[1])),(*reinterpret_cast< QVariantList(*)>(_a[2]))); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData GeoHelper::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject GeoHelper::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_GeoHelper,
      qt_meta_data_GeoHelper, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &GeoHelper::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *GeoHelper::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *GeoHelper::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_GeoHelper))
        return static_cast<void*>(const_cast< GeoHelper*>(this));
    return QObject::qt_metacast(_clname);
}

int GeoHelper::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 9)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 9;
    }
    return _id;
}

// SIGNAL 0
void GeoHelper::positionUpdated()
{
    QMetaObject::activate(this, &staticMetaObject, 0, 0);
}

// SIGNAL 1
void GeoHelper::pinOnMapViewAdded()
{
    QMetaObject::activate(this, &staticMetaObject, 1, 0);
}
QT_END_MOC_NAMESPACE
