import bb.cascades 1.4

Sheet {
    id: addSheet
    Page {
        titleBar: TitleBar {
            id: titlebar
            title: (itemIdE == "0") ? qsTr("New Item") + Retranslate.onLanguageChanged : qsTr("Edit item") + Retranslate.onLanguageChanged
            dismissAction: ActionItem {
                id: closeAction
                title: qsTr("Cancel") + Retranslate.onLanguageChanged
                onTriggered: {
                    console.log("add/edit close");
                    addSheet.close();
                    console.log("add/edit closed");
                }
            }
            acceptAction: ActionItem {
                id: saveAction
                title: qsTr("Save") + Retranslate.onLanguageChanged
                onTriggered: {
                    console.log("add item " + noteText.text + " " + addSheet.parentId + " " + "?" + " " + 1 + " " + listItemToggle.checked);
                    var reminderDateTime = "";
                    if (reminderToggle.checked) {
                        reminderDateTime = dataHelper.formatDateTime(reminderDateTimePicker.value);
                    }
                    if (itemIdE != "0") {
                        var itemId = itemIdE;
                        var listItemValue = (listItemToggle.checked) ? 1 : 0;
                        dataHelper.updateItem(itemId, noteText.text, addSheet.parentId, indexPathE, chckdE, listItemValue, reminderDateTime, reminderE);
                        addSheet.dataModel.updateItem(indexPath, {
                                "itemId": itemId,
                                "title": noteText.text,
                                "parentId": addSheet.parentId,
                                "indexPath": indexPathE,
                                "chckd": chckdE,
                                "listItem": listItemValue,
                                "reminder": reminderDateTime
                            });
                    } else {
                        var itemId = dataHelper.getDateTimeId();
                        var itemsCount = dataHelper.itemsCount(parentId);
                        var listItemValue = (listItemToggle.checked) ? 1 : 0;
                        dataHelper.saveItem(itemId, noteText.text, addSheet.parentId, itemsCount, 1, listItemValue, reminderDateTime);
                    }
                    addSheet.close();
                    console.log("add/edit closed");
                }
                //TODO: shortcut?
            }
        }
        Container {
            Container {
                topPadding: ui.du(2.5)
                leftPadding: ui.du(2.0)
                Label {
                    text: qsTr("Title") + Retranslate.onLanguageChanged
                    textStyle.fontSize: FontSize.Medium
                }
            }
            Container {
                topPadding: ui.du(2.5)
                bottomPadding: ui.du(2.5)
                leftPadding: ui.du(2.0)
                rightPadding: ui.du(2.0)
                TextField {
                    id: noteText
                    textStyle.fontSize: FontSize.Medium
                }
            }
            Divider {
                visible: (parentId == "0") //visible only if coming from root
                topMargin: 0.0
                bottomMargin: 0.0
            }
            Container {
                visible: (parentId == "0") //visible only if coming from root
                layout: StackLayout {
                    orientation: LayoutOrientation.LeftToRight
                }
                topPadding: ui.du(2.5)
                bottomPadding: ui.du(2.5)
                leftPadding: ui.du(2.0)
                rightPadding: ui.du(2.0)
                Container {
                    verticalAlignment: VerticalAlignment.Center
                    layoutProperties: StackLayoutProperties {
                        spaceQuota: 1
                    }
                    Label {
                        text: qsTr("List") + Retranslate.onLanguageChanged
                        textStyle.fontSize: FontSize.Medium
                    }
                }
                Container {
                    leftPadding: ui.du(2.0)
                    ToggleButton {
                        id: listItemToggle
                        checked: false
                        //on checked - warning, later delete sub-items
                    }
                }
            }
            Divider {
                topMargin: 0.0
                bottomMargin: 0.0
            }
            Container {
                layout: StackLayout {
                    orientation: LayoutOrientation.LeftToRight
                }
                topPadding: ui.du(2.5)
                bottomPadding: ui.du(2.5)
                leftPadding: ui.du(2.0)
                rightPadding: ui.du(2.0)
                Container {
                    verticalAlignment: VerticalAlignment.Center
                    layoutProperties: StackLayoutProperties {
                        spaceQuota: 1
                    }
                    Label {
                        text: qsTr("Reminder") + Retranslate.onLanguageChanged
                        textStyle.fontSize: FontSize.Medium
                    }
                }
                Container {
                    leftPadding: ui.du(2.0)
                    ToggleButton {
                        id: reminderToggle
                        checked: false
                    }
                }
            }
            Container {
                visible: reminderToggle.checked
                bottomPadding: ui.du(2.5)
                leftPadding: ui.du(2.0)
                rightPadding: ui.du(2.0)
                id: datetimePickerContainer
                DateTimePicker {
                    id: reminderDateTimePicker
                    title: qsTr("Date and Time")
                    mode: DateTimePickerMode.DateTime
                }
            }
        }
        onCreationCompleted: {
            noteText.requestFocus();
        }
    }
    onListItemEChanged: {
        listItemToggle.checked = (listItemE == 1);
    }
    onReminderEChanged: {
        console.log("reminderE changed:" + reminderE);
        reminderToggle.checked = true;
        reminderDateTimePicker.value = reminderE;
    }
    property string parentId: "0"
    property GroupDataModel dataModel
    property variant indexPath
    property string itemIdE: "0"
    property alias titleE: noteText.text
    property int indexPathE
    property int chckdE
    property int listItemE
    property string reminderE: ""
}
