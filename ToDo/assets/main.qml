/*
 * Copyright (c) 2011-2015 BlackBerry Limited.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import bb.cascades 1.4
import bb.data 1.0

NavigationPane {
    id: navigationPane

    Menu.definition: MenuDefinition {
        actions: [
            ActionItem {
                title: qsTr("About") + Retranslate.onLanguageChanged
                imageSource: "asset:///images/ic_info.png"
                onTriggered: {
                    var aboutPageContent = aboutPage.createObject();
                    navigationPane.push(aboutPageContent);
                }
                attachedObjects: [
                    ComponentDefinition {
                        id: aboutPage
                        source: "about.qml"
                    }
                ]
            },
            ActionItem {
                title: qsTr("Send Feedback") + Retranslate.onLanguageChanged
                imageSource: "asset:///images/ic_feedback.png"
                onTriggered: {
                    dataHelper.invokeEmail("", qsTr("ToDo Feedback") + Retranslate.onLanguageChanged, "");
                }
            }
        ]
        settingsAction: SettingsActionItem {
            onTriggered: {
                console.log("settings");
                var settingsPageContent = settingsPage.createObject();
                navigationPane.push(settingsPageContent);
            }
            attachedObjects: [
                ComponentDefinition {
                    id: settingsPage
                    source: "settings.qml"
                }
            ]
        }
    }

    ListPage {
    }

}
