import bb.cascades 1.4

Page {
    content: ScrollView {
        content: Container {
            leftPadding: ui.du(2.0)
            rightPadding: ui.du(2.0)
            Container {
                layout: StackLayout {
                    orientation: LayoutOrientation.LeftToRight
                }
                topPadding: ui.du(5.0)
                Container {
                    layoutProperties: StackLayoutProperties {
                        spaceQuota: 1
                    }
                    Container {
                        horizontalAlignment: HorizontalAlignment.Center
                        ImageView {
                            imageSource: "asset:///images/icon.png"
                            preferredHeight: ui.du(15.0)
                            preferredWidth: ui.du(15.0)
                        }
                    }
                    Container {
                        topMargin: ui.du(4.0)
                        horizontalAlignment: HorizontalAlignment.Center
                        Label {
                            text: "ToDo 1.0.4"
                            textStyle.fontSize: FontSize.Large
                            textStyle.fontWeight: FontWeight.W500
                        }
                    }
                    Container {
                        layout: StackLayout {
                            orientation: LayoutOrientation.LeftToRight
                        }
                        Container {
                            topPadding: ui.du(4.0)
                            layoutProperties: StackLayoutProperties {
                                spaceQuota: 1
                            }
                            Label {
                                horizontalAlignment: HorizontalAlignment.Center
                                text: qsTr("Build by") + " <a mimetype=\"application/x-bb-appworld\" href=\"appworld://vendorpage/63768\">Martin Polovinčák</a>" + Retranslate.onLanguageChanged
                                multiline: true
                                textFormat: TextFormat.Html
                            }
                        }
                    }
                    Container {
                        topPadding: ui.du(4.0)
                        horizontalAlignment: HorizontalAlignment.Center
                        layout: StackLayout {
                            orientation: LayoutOrientation.LeftToRight
                        }
                        Container {
                            Label {
                                horizontalAlignment: HorizontalAlignment.Center
                                text: "<a href=\"http://www.polovincak.eu/privacy_policy.html\">" + qsTr("Privacy policy (English)") + "</a>"
                                multiline: true
                                textFormat: TextFormat.Html
                            }
                        }
                    }
                    Container {
                        topPadding: ui.du(8.0)
                        Container {
                            Label {
                                text: qsTr("If you like the app, please do not forget to write a review.") + Retranslate.onLanguageChanged
                                multiline: true
                                textStyle.textAlign: TextAlign.Justify
                            }
                        }
                    }
                    Container {
                        topPadding: ui.du(8.0)
                        Container {
                            Label {
                                text: qsTr("THE AUTHOR DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE, INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS, IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, INDIRECT OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.") + Retranslate.onLanguageChanged
                                multiline: true
                                textStyle.textAlign: TextAlign.Justify
                            }
                        }
                    }
                }
            }
        }
    }
}
