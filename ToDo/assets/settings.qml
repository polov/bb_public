import bb.cascades 1.4
import bb.cascades.pickers 1.0

Page {
    id: settingsPage
    titleBar: TitleBar {
        id: titlebar
        title: qsTr("Settings") + Retranslate.onLanguageChanged
    }
    content: ScrollView {
        content: Container {
            leftPadding: ui.du(2.0)
            rightPadding: ui.du(2.0)
            Container {
                topPadding: ui.du(2.5)
                DropDown {
                    id: visualStyleDropDown
                    title: qsTr("Visual style") + Retranslate.onLanguageChanged
                    options: [
                        Option {
                            value: "bright"
                            text: qsTr("Bright") + Retranslate.onLanguageChanged
                            selected: (dataHelper.getVisualStyle() == "bright")
                        },
                        Option {
                            value: "dark"
                            text: qsTr("Dark") + Retranslate.onLanguageChanged
                            selected: (dataHelper.getVisualStyle() == "dark")
                        }
                    ]
                    onSelectedValueChanged: {
                        console.log("change visual style to " + selectedValue);
                        dataHelper.setVisualStyle(selectedValue);
                        if (selectedValue == "bright") {
                            Application.themeSupport.setVisualStyle(VisualStyle.Bright);
                        } else if (selectedValue == "dark") {
                            Application.themeSupport.setVisualStyle(VisualStyle.Dark);
                        }
                    }
                }
            }
            Container {
                topPadding: ui.du(2.5)
                DropDown {
                    id: fontSizeDropDown
                    title: qsTr("Font Size") + Retranslate.onLanguageChanged
                    options: [
                        Option {
                            value: "NORMAL"
                            text: qsTr("Default") + Retranslate.onLanguageChanged
                            selected: (dataHelper.getFontSize() == "NORMAL")
                        },
                        Option {
                            value: "BIG"
                            text: qsTr("Bigger") + Retranslate.onLanguageChanged
                            selected: (dataHelper.getFontSize() == "BIG")
                        }
                    ]
                    onSelectedValueChanged: {
                        console.log("change font size to " + selectedValue);
                        dataHelper.setFontSize(selectedValue);
                    }
                }
            }
            Container {
                Container {
                    topPadding: ui.du(2.5)
                    DropDown {
                        id: countTypeDropDown
                        title: qsTr("List items count") + Retranslate.onLanguageChanged
                        options: [
                            Option {
                                value: "done"
                                text: qsTr("Done") + Retranslate.onLanguageChanged
                                selected: (dataHelper.getCountType() == "done")
                            },
                            Option {
                                value: "total"
                                text: qsTr("Total") + Retranslate.onLanguageChanged
                                selected: (dataHelper.getCountType() == "total")
                            },
                            Option {
                                value: "all"
                                text: qsTr("Done/Total") + Retranslate.onLanguageChanged
                                selected: (dataHelper.getCountType() == "all")
                            },
                            Option {
                                value: "none"
                                text: qsTr("None") + Retranslate.onLanguageChanged
                                selected: (dataHelper.getCountType() == "none")
                            }
                        ]
                        onSelectedValueChanged: {
                            console.log("change countType to " + selectedValue);
                            dataHelper.setCountType(selectedValue);
                        }
                    }
                }                
            }
            /*
            Container {
                Button {
                    id: choose
                    text: qsTr("Directory") + Retranslate.onLocaleOrLanguageChanged
                    onClicked: {
                        directoryPicker.open();
                    }
                }
            }
            */
        }
        attachedObjects: [
            FilePicker {
                id: directoryPicker
                title: qsTr("Backup Directory") + Retranslate.onLanguageChanged
                mode: FilePickerMode.SaverMultiple
                onFileSelected: {
                    dataHelper.setBackupDirectory(selectedFiles[0]);
                }
            }
        ]
    }
}