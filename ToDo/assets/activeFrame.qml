import bb.cascades 1.4

Container {
    background: Color.Black
    objectName: "activeFrameContainer"
    layout: DockLayout {}
    leftPadding: ui.du(1.0)
    rightPadding: ui.du(1.0)
    Container {
        Label {
            objectName: "activeFrameLabel"
            multiline: true
            text: qsTr("TODO") + Retranslate.onLanguageChanged
            textStyle {
                color: Color.White
                fontSize: FontSize.Default
            }
        }
        horizontalAlignment: HorizontalAlignment.Fill
        verticalAlignment: VerticalAlignment.Center
    }
}
