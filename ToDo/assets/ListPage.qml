import bb.cascades 1.4
import bb.system 1.2

Page {
    id: todoPage
    actionBarVisibility: ChromeVisibility.Compact
    titleBar: TitleBar {
        visibility: (titlebarText != "")
        title: titlebarText
    }
    content: Container {
        id: container
        property int itemOrigin: -1
        ListView {
            id: sortListView
            dataModel: sortDataModel
            visible: false
            listItemComponents: [
                ListItemComponent {
                    type: ""
                    content: Container {
                        id: sortListItem
                        Container {
                            layout: StackLayout {
                                orientation: LayoutOrientation.LeftToRight
                            }
                            topPadding: ui.du(2.5)
                            bottomPadding: ui.du(2.5)
                            leftPadding: ui.du(2.0)
                            rightPadding: ui.du(2.0)
                            Label {
                                id: sortTitleLabel
                                text: ListItemData.title
                                textStyle.fontSize: {
                                    if (sortListItem.ListItem.view.getFontSize() == "BIG") {
                                        return FontSize.Large;
                                    } else {
                                        return FontSize.Default;
                                    }
                                }
                            }
                        }
                        Divider {
                            topMargin: 0.0
                            bottomMargin: 0.0
                        }
                    }
                }
            ]
            function rearrangeList() {
                rearrange = ! rearrange;
                rearrangeHandler.setActive(rearrange);
                if (! rearrange) {
                    //dataHelper.saveItems(dataModel, todoPage.parentId);
                    console.log("rearrange finished");
                    dataHelper.updateIndexPaths(sortDataModel, todoPage.parentId);
                    dataModel.clear();
                    dataModel.insertList(dataHelper.loadItems(parentId));
                    listView.visible = true;
                    sortListView.visible = false;
                }
            }
            function getFontSize() {
                return dataHelper.getFontSize();
            }
            rearrangeHandler {
                // Do not activate the handler here. It
                // will be forcefully deactivated when
                // the data model is assigned.
                onMoveStarted: {
                    console.log("onMoveStarted: " + event.startIndexPath);
                    container.itemOrigin = event.startIndexPath[0];
                }
                onMoveEnded: {
                    console.log("onMoveEnded: " + event.endIndexPath);
                }
                onMoveUpdated: {
                    // Always call denyMove() before initiating
                    // the move. Without first denying the move
                    // the list item will only move 1 spot
                    // regardless of how many spots it's
                    // dragged.
                    event.denyMove();
                    sortDataModel.move(event.fromIndexPath[0], event.toIndexPath[0]);
                    console.log("onMoveUpdated: " + event.fromIndexPath[0] + " -> " + event.toIndexPath[0]);
                }
                onMoveAborted: {
                    console.log("onMoveAborted: " + event.endIndexPath);
                    container.undoItemMove();
                }
                onActiveChanged: {
                    console.log("active changed: " + active);
                }
            }
        }
        ListView {
            id: listView
            dataModel: dataModel
            scrollRole: ScrollRole.Main
            listItemComponents: [
                ListItemComponent {
                    type: "header"
                    content: Header {
                        title: (ListItemData == "1") ? qsTr("TODO") + Retranslate.onLanguageChanged : qsTr("DONE") + Retranslate.onLanguageChanged
                    }
                },
                ListItemComponent {
                    type: "item"
                    content: Container {
                        id: listItemComponentContainer
                        background: {
                            if (listItemComponentContainer.ListItem.selected) {
                                return Color.create("#c4dbe5");
                            } else {
                                return ui.palette.background;
                            }
                        }
                        Container {
                            layout: StackLayout {
                                orientation: LayoutOrientation.LeftToRight
                            }
                            topPadding: ui.du(2.5)
                            bottomPadding: ui.du(2.5)
                            leftPadding: ui.du(2.0)
                            rightPadding: ui.du(2.0)
                            Container {
                                verticalAlignment: VerticalAlignment.Center
                                CheckBox {
                                    id: todoCheckBox
                                    checked: (ListItemData.chckd == 1) ? false : true
                                    onCheckedChanged: {
                                        if (listItemComponentContainer.ListItem.initialized) {
                                            var item = ListItemData;
                                            var chckd = (checked) ? 0 : 1;
                                            item.chckd = chckd;
                                            //console.log("itemId:" + item.itemId + "checked:" + item.chckd + " checkbox:" + checked);
                                            listItemComponentContainer.ListItem.view.dataModel.updateItem(listItemComponentContainer.ListItem.indexPath, item);
                                            listItemComponentContainer.ListItem.view.updateItem(item.itemId, item.chckd);
                                            listItemComponentContainer.ListItem.view.updateParentItem(item.parentId);
                                        }
                                    }

                                }
                            }
                            Container {
                                verticalAlignment: VerticalAlignment.Center
                                leftMargin: ui.du(2.0)
                                layoutProperties: StackLayoutProperties {
                                    spaceQuota: 1
                                }
                                Label {
                                    id: titleLabel
                                    text: ListItemData.title // + " " + listItemComponentContainer.ListItem.view.getFontSize() //+ " " + ListItemData.itemId
                                    textStyle.fontSize: {
                                        if (listItemComponentContainer.ListItem.view.getFontSize() == "BIG") {
                                            return FontSize.Large;
                                        } else {
                                            return FontSize.Default;
                                        }
                                    }
                                    multiline: true
                                    autoSize.maxLineCount: 2
                                }
                            }
                            Container {
                                visible: (ListItemData.listItem == 1)
                                verticalAlignment: VerticalAlignment.Center
                                leftMargin: ui.du(2.0)
                                Label {
                                    id: listCountLabel
                                    text: (ListItemData.listItem == 1) ? listItemComponentContainer.ListItem.view.itemsCount(ListItemData.itemId) : ""
                                    textStyle.fontSize: {
                                        if (listItemComponentContainer.ListItem.view.getFontSize() == "BIG") {
                                            return FontSize.Large;
                                        } else {
                                            return FontSize.Default;
                                        }
                                    }
                                }
                            }
                            Container {
                                visible: (ListItemData.listItem == 1)
                                verticalAlignment: VerticalAlignment.Center
                                leftMargin: ui.du(2.0)
                                ImageButton {
                                    id: openListImageB
                                    defaultImageSource: getImageSource()
                                    preferredHeight: ui.du(5.0)
                                    preferredWidth: ui.du(5.0)
                                    onClicked: {
                                        listItemComponentContainer.ListItem.view.showListPage(ListItemData);
                                    }
                                    function getImageSource() {
                                        switch (Application.themeSupport.theme.colorTheme.style) {
                                            case VisualStyle.Bright:
                                                return "asset:///images/ic_item_more_black.png";
                                            case VisualStyle.Dark:
                                                return "asset:///images/ic_item_more_white.png";
                                        }
                                    }
                                }
                            }
                        }
                        Divider {
                            topMargin: 0.0
                            bottomMargin: 0.0
                        }
                        contextActions: [
                            ActionSet {
                                title: ListItemData.title
                                ActionItem {
                                    id: checkItemAction
                                    title: (ListItemData.chckd == 1) ? qsTr("Mark as Done") + Retranslate.onLanguageChanged : qsTr("Mark as unfinished") + Retranslate.onLanguageChanged
                                    imageSource: "asset:///images/ic_done.png"
                                    onTriggered: {
                                        todoCheckBox.checked = ! todoCheckBox.checked;
                                    }
                                    shortcuts: [
                                        Shortcut {
                                            key: "f"
                                        }
                                    ]
                                }
                                ActionItem {
                                    id: updateItemAction
                                    title: qsTr("Edit item") + Retranslate.onLanguageChanged
                                    imageSource: "asset:///images/ic_edit.png"
                                    onTriggered: {
                                        listItemComponentContainer.ListItem.view.showUpdateForm(ListItemData.itemId, ListItemData.title, ListItemData.parentId, ListItemData.indexPath, ListItemData.chckd, ListItemData.listItem, listItemComponentContainer.ListItem.indexPath, ListItemData.reminder);
                                    }
                                }
                                ActionItem {
                                    id: moveItemAction
                                    title: qsTr("Move item") + Retranslate.onLanguageChanged
                                    imageSource: "asset:///images/ic_move.png"
                                    enabled: (ListItemData.parentId != 0 || ListItemData.listItem == 0)
                                    onTriggered: {
                                        listItemComponentContainer.ListItem.view.showMoveForm(ListItemData.itemId, ListItemData.title, ListItemData.parentId, ListItemData.indexPath, ListItemData.chckd, ListItemData.listItem, listItemComponentContainer.ListItem.indexPath, ListItemData.reminder);
                                    }
                                }
                                InvokeActionItem {
                                    ActionBar.placement: ActionBarPlacement.OnBar
                                    query {
                                        mimeType: "text/plain"
                                        invokeActionId: "bb.action.SHARE"
                                    }
                                    onTriggered: {
                                        data = listItemComponentContainer.ListItem.view.buildSharedContent(ListItemData.itemId);
                                        console.log("share triggered");
                                    }
                                }
                                DeleteActionItem {
                                    id: deleteItemAction
                                    onTriggered: {
                                        deleteConfirmDialog.show();
                                    }
                                }
                            }
                        ]
                        animations: [
                            ScaleTransition {
                                id: deleteAnimation
                                toY: 0
                                toX: 0
                                duration: 500
                                onEnded: {
                                    listItemComponentContainer.ListItem.view.dataModel.remove(listItemComponentContainer.ListItem.view.dataModel.data(listItemComponentContainer.ListItem.indexPath));
                                    listItemComponentContainer.ListItem.view.deleteItem(ListItemData.itemId);
                                    listItemComponentContainer.scaleX = 1.0;
                                    listItemComponentContainer.scaleY = 1.0;
                                }
                            }
                        ]
                        attachedObjects: [
                            SystemDialog {
                                id: deleteConfirmDialog
                                body: qsTr("Delete") + " " + ListItemData.title + "?" + Retranslate.onLanguageChanged
                                title: qsTr("Delete the item") + Retranslate.onLanguageChanged
                                onFinished: {
                                    if (deleteConfirmDialog.result == SystemUiResult.ConfirmButtonSelection) {
                                        deleteAnimation.play();
                                    }
                                }
                                property string identificator
                            }
                        ]
                    }
                }
            ]
            onTriggered: {
                if (indexPath.length > 1) {
                    var chosenItem = listView.dataModel.data(indexPath);
                    showListPage(chosenItem);
                }
            }
            function buildSharedContent(itemId) {
                return dataHelper.buildSharedContent(itemId);
            }
            function showListPage(chosenItem) {
                if (chosenItem.listItem == 1) {
                    console.log("list item " + chosenItem.title + " " + chosenItem.itemId + " " + chosenItem.parentId + " " + chosenItem.chckd);
                    var listPageContent = listPage.createObject();
                    listPageContent.titlebarText = chosenItem.title;
                    listPageContent.parentId = chosenItem.itemId;
                    navigationPane.push(listPageContent);
                } else {
                    console.log("child item " + chosenItem.title + " " + chosenItem.itemId + " " + chosenItem.parentId + " " + chosenItem.chckd);
                }
            }
            function showUpdateForm(itemId, title, parentId, indexPath, chckd, listItem, modelIndexPath, reminder) {
                var addSheetContent = addSheet.createObject();
                addSheetContent.parentId = todoPage.parentId;
                addSheetContent.dataModel = dataModel;
                addSheetContent.indexPath = modelIndexPath;
                addSheetContent.itemIdE = itemId;
                addSheetContent.titleE = title;
                addSheetContent.indexPathE = indexPath;
                addSheetContent.chckdE = chckd;
                addSheetContent.listItemE = listItem;
                addSheetContent.reminderE = reminder;
                addSheetContent.open();
            }
            function showMoveForm(itemId, title, parentId, indexPath, chckd, listItem, modelIndexPath, reminder) {
                var moveSheetContent = moveSheet.createObject();
                moveSheetContent.parentId = todoPage.parentId;
                moveSheetContent.dataModel = dataModel;
                moveSheetContent.indexPath = modelIndexPath;
                moveSheetContent.itemIdE = itemId;
                //moveSheetContent.titleE = title;
                moveSheetContent.indexPathE = indexPath;
                moveSheetContent.chckdE = chckd;
                moveSheetContent.listItemE = listItem;
                moveSheetContent.reminderE = reminder;
                moveSheetContent.open();
            }
            function updateItem(itemId, chckd) {
                dataHelper.updateItem(itemId, chckd);
            }
            function updateParentItem(parentId) {
                dataHelper.updateItem(parentId);
            }
            function deleteItem(itemId) {
                dataHelper.deleteItem(itemId);
                dataHelper.checkChckd(parentId);
            }
            function itemsCount(parentId) {
                console.log("itemsCount() parentId " + parentId);
                return dataHelper.itemsCount(parentId);
            }
            function getFontSize() {
                return dataHelper.getFontSize();
            }
        }
    }
    function onDataChanged(value) {
        console.log("dataChanged signal catched, value:" + value);
        if (value == "save") {
            dataModel.clear();
            dataModel.insertList(dataHelper.loadItems(parentId));
        } else if (value == "update_parent") {
            console.log("update parentId:" + parentId);
            if (parentId == "0") {
                dataModel.clear();
                dataModel.insertList(dataHelper.loadItems(parentId));
            }
        }
    }
    actions: [
        ActionItem {
            id: signatureAction
            title: rearrange ? qsTr("Done") + Retranslate.onLanguageChanged : qsTr("Add") + Retranslate.onLanguageChanged
            imageSource: rearrange ? "asset:///images/ic_done.png" : "asset:///images/ic_add.png"
            ActionBar.placement: ActionBarPlacement.Signature
            onTriggered: {
                if (rearrange) {
                    console.log("list rearrange finished");
                    sortListView.rearrangeList();
                } else {
                    var addSheetContent = addSheet.createObject();
                    addSheetContent.parentId = todoPage.parentId;
                    addSheetContent.dataModel = dataModel;
                    addSheetContent.open();
                }
            }
        },
        ActionItem {
            id: reorderAction
            title: qsTr("Reorder") + Retranslate.onLanguageChanged
            imageSource: "asset:///images/ic_sort.png"
            ActionBar.placement: ActionBarPlacement.InOverflow
            enabled: ! rearrange
            onTriggered: {
                console.log("reorder " + todoPage.parentId);
                sortDataModel.clear();
                sortDataModel.insert(0, dataHelper.createSortList(dataModel));
                listView.visible = false;
                sortListView.visible = true;
                sortListView.rearrangeList();
            }
        }
    ]
    onCreationCompleted: {
        console.log("load model after creation (" + parentId + ")");
        dataModel.insertList(dataHelper.loadItems(parentId));
        listView.dataModel = dataModel;
        dataHelper.dataChanged.connect(todoPage.onDataChanged);
        //listView.requestFocus(); - invalid uiobject parent change
    }
    onParentIdChanged: {
        console.log("load model after parentId change (" + parentId + ")");
        dataModel.clear();
        dataModel.insertList(dataHelper.loadItems(parentId));
    }
    attachedObjects: [
        GroupDataModel {
            id: dataModel
            sortingKeys: [ "chckd", "indexPath", "itemId" ]
            sortedAscending: false
        },
        ArrayDataModel {
            id: sortDataModel
        },
        ComponentDefinition {
            id: addSheet
            source: "add.qml"
        },
        ComponentDefinition {
            id: moveSheet
            source: "move.qml"
        },
        ComponentDefinition {
            id: listPage
            source: "ListPage.qml"
        },
        PageLayoutUpdateHandler {
            id: pageLayoutUpdateHandler
            onBottomOverlayHeightChanged: {
                listView.bottomPadding = bottomOverlayHeight;
                sortListView.bottomPadding = bottomOverlayHeight;
            }
        }
    ]
    property string parentId: "0"
    property bool rearrange: false
    property string titlebarText: ""
}
