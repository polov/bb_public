import bb.cascades 1.4
import bb.system 1.2

Sheet {
    id: moveSheet
    Page {
        titleBar: TitleBar {
            id: titlebar
            title: qsTr("Move Item") + Retranslate.onLanguageChanged
            dismissAction: ActionItem {
                id: closeAction
                title: qsTr("Cancel") + Retranslate.onLanguageChanged
                onTriggered: {
                    moveSheet.close();
                }
            }
        }
        content: Container {
            /*
            Container {
                layout: StackLayout {
                    orientation: LayoutOrientation.LeftToRight
                }
                Label {
                    text: "Choose the list"
                    layoutProperties: StackLayoutProperties {
                        spaceQuota: 1
                    }
                }
            }
            */
            Divider {
                topMargin: 0.0
                bottomMargin: 0.0
            }
            ListView {
                id: lists
                listItemComponents: [
                    ListItemComponent {
                        type: "header"
                        content: Container {
                            enabled: false
                        }
                    },
                    ListItemComponent {
                        type: "item"
                        content: Container {
                            id: listItemComponentContainer
                            background: {
                                if (listItemComponentContainer.ListItem.active) {
                                    return Color.create("#c4dbe5");
                                } else {
                                    return ui.palette.background;
                                }
                            }
                            Container {
                                layout: StackLayout {
                                    orientation: LayoutOrientation.LeftToRight
                                }
                                topPadding: ui.du(2.5)
                                bottomPadding: ui.du(2.5)
                                leftPadding: ui.du(2.0)
                                rightPadding: ui.du(2.0)
                                Container {
                                    verticalAlignment: VerticalAlignment.Center
                                    leftMargin: ui.du(2.0)
                                    layoutProperties: StackLayoutProperties {
                                        spaceQuota: 1
                                    }
                                    Label {
                                        id: titleLabel
                                        text: ListItemData.title
                                        textStyle.fontSize: FontSize.Large
                                        multiline: true
                                        autoSize.maxLineCount: 2
                                    }
                                }
                            }
                            Divider {
                                topMargin: 0.0
                                bottomMargin: 0.0
                            }
                        }
                    }
                ]
                onTriggered: {
                    if (indexPath.length > 1) {
                        var chosenItem = lists.dataModel.data(indexPath);
                        console.log("Move item to list " + chosenItem.title);
                        moveConfirmDialog.moveParentId = chosenItem.itemId;
                        moveConfirmDialog.body = qsTr("Move the item to") + " " + chosenItem.title + "?" + Retranslate.onLanguageChanged
                        moveConfirmDialog.show();
                    }
                }
            }
        }
        attachedObjects: [
            GroupDataModel {
                id: dataModel
                sortingKeys: ["title"]
            },
            SystemDialog {
                id: moveConfirmDialog
                title: qsTr("Move the item?") + Retranslate.onLanguageChanged
                onFinished: {
                    if (moveConfirmDialog.result == SystemUiResult.ConfirmButtonSelection) {
                        if (parentId == moveParentId) {
                            console.log("confirmed to move the item to " + moveParentId + " - moving disabled, same list");
                        } else {
                            console.log("confirmed to move the item to " + moveParentId);
                            console.log("itemId " + itemIdE);
                            console.log("parentId " + parentId);
                            dataHelper.moveItem(itemIdE, parentId, moveParentId);
                            if(parentId!="0") {
                                moveSheet.dataModel.removeAt(indexPath);   
                            }
                        }
                        moveSheet.close();
                    }
                }
                property string moveParentId
            }
        ]
        onCreationCompleted: {
            dataModel.clear();
            dataModel.insertList(dataHelper.getExistingLists());
            dataModel.insert({
                    "itemId" : "0",
                    "title" : qsTr("Main list")
            })
            lists.dataModel = dataModel;
        }
    }
    property string parentId: "0"
    property GroupDataModel dataModel
    property variant indexPath
    property string itemIdE: "0"
    //property alias titleE not used now
    property int indexPathE
    property int chckdE
    property int listItemE
    property string reminderE: ""
}