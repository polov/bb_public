/*
 * DataHelper.cpp
 *
 *  Created on: 9. 7. 2015
 *      Author: martin
 */

#include "DataHelper.h"

#include <bb/cascades/Application>
#include <bb/cascades/Theme>
#include <bb/cascades/ThemeSupport>
#include <bb/cascades/VisualStyle>

#include <bps/removablemedia.h>

#include <QDebug>

QString DataHelper::getDateTimeId()
{
    //return QString::number(QDateTime::currentMSecsSinceEpoch());
    return QDateTime::currentDateTime().toString("yyyyMMddhhmmss");
}

DataHelper::DataHelper() :
        QObject(), sda("data/data.db"), calendarHelper(), invokeManager(
                new bb::system::InvokeManager(this))
{
    //sda.execute("DROP TABLE items");
    sda.execute(
            "CREATE TABLE IF NOT EXISTS items (itemId VARCHAR, title VARCHAR, parentId VARCHAR, indexPath INTEGER, chckd INTEGER, listItem INTEGER, reminder VARCHAR, type VARCHAR, priority VARCHAR, color VARCHAR)");
    //sda.execute("INSERT INTO items (title,parentId,indexPath) VALUES ('List',0,0)");
    //sda.execute("INSERT INTO items (title,parentId,indexPath) VALUES ('Pokus',0,1)");
    //sda.execute("INSERT INTO items (title,parentId,indexPath) VALUES ('Itemize it',0,2)");

    //sda.execute("ALTER TABLE items ADD COLUMN priority VARCHAR");

    QSettings settings("polovincak", "ToDo");
    QString visualStyle = settings.value("visual.style", "bright").toString();
    if (visualStyle == "bright") {
        bb::cascades::Application::instance()->themeSupport()->setVisualStyle(
                bb::cascades::VisualStyle::Bright);
    } else if (visualStyle == "dark") {
        bb::cascades::Application::instance()->themeSupport()->setVisualStyle(
                bb::cascades::VisualStyle::Dark);
    }
}

DataHelper::~DataHelper()
{

}

QString DataHelper::getVisualStyle()
{
    QSettings settings("polovincak", "ToDo");
    return settings.value("visual.style", "bright").toString();
}

void DataHelper::setVisualStyle(const QString& value)
{
    QSettings settings("polovincak", "ToDo");
    settings.setValue("visual.style", value);
}

QString DataHelper::getCountType()
{
    QSettings settings("polovincak", "ToDo");
    return settings.value("countFieldType", "total").toString();
}

void DataHelper::setCountType(const QString& value)
{
    QSettings settings("polovincak", "ToDo");
    settings.setValue("countFieldType", value);
    emit dataChanged("update_parent");
}

QString DataHelper::getFontSize() {
    QSettings settings("polovincak", "ToDo");
    return settings.value("fontSize", "BIG").toString();
}

void DataHelper::setFontSize(const QString& value) {
    QSettings settings("polovincak", "ToDo");
    settings.setValue("fontSize", value);
    emit dataChanged("update_parent");
}

QVariantList DataHelper::loadItems(const QString& parentId)
{
    QVariantList params;
    params.append(QVariant::fromValue(parentId));
    QVariant result =
            sda.execute(
                    "SELECT itemId,title,parentId,indexPath,chckd,listItem,reminder FROM items WHERE parentId=:parentId",
                    params);
    QVariantList items = result.value<QVariantList>();
    for (int i = 0; i < items.size(); i++) {
        QVariantMap item = items.at(i).toMap();
        bool ok;
        qDebug() << item.value("itemId").toString() << item.value("title").toString()
                << item.value("indexPath").toInt(&ok) << item.value("reminder").toString();
    }
    return result.value<QVariantList>();
}

void DataHelper::saveItem(const QString& itemId, const QString& title, const QString& parentId,
        const int indexPath, const int chckd, const int listItem, const QString& reminder)
{
    QVariantList params;
    params.append(QVariant::fromValue(itemId));
    params.append(QVariant::fromValue(title));
    params.append(QVariant::fromValue(parentId));
    params.append(QVariant::fromValue(indexPath));
    params.append(QVariant::fromValue(chckd));
    params.append(QVariant::fromValue(listItem));
    params.append(QVariant::fromValue(reminder));
    sda.execute(
            "INSERT INTO items (itemId,title,parentId,indexPath,chckd,listItem,reminder) VALUES (:itemId,:title,:parentId,:indexPath,:chckd,:listItem,:reminder)",
            params);
    this->updateItem(parentId);
    qDebug() << "Saved:" << itemId << title << parentId << indexPath << chckd << reminder;
    emit dataChanged("save");
    calendarHelper.setCalendarEvent(title, reminder, "");
}

void DataHelper::updateItem(const QString& itemId, const QString& title, const QString& parentId,
        const int indexPath, const int chckd, const int listItem, const QString& reminder,
        const QString& reminderOld)
{
    QVariantList params;
    params.append(QVariant::fromValue(itemId));
    params.append(QVariant::fromValue(title));
    params.append(QVariant::fromValue(parentId));
    params.append(QVariant::fromValue(indexPath));
    params.append(QVariant::fromValue(chckd));
    params.append(QVariant::fromValue(listItem));
    params.append(QVariant::fromValue(reminder));
    params.append(QVariant::fromValue(itemId));
    sda.execute(
            "UPDATE items SET itemId=:itemId,title=:title,parentId=:parentId,indexPath=:indexPath,chckd=:chckd,listItem=:listItem,reminder=:reminder WHERE itemId=:itemId",
            params);
    qDebug() << "Updated:" << itemId << title << parentId << indexPath << chckd << reminder
            << reminderOld;
    emit dataChanged("update");
    calendarHelper.setCalendarEvent(title, reminder, reminderOld);
}

void DataHelper::updateItem(const QString& itemId, const int chckd)
{
    QVariantList params;
    params.append(QVariant::fromValue(chckd));
    params.append(QVariant::fromValue(itemId));
    sda.execute("UPDATE items SET chckd=:chckd WHERE itemId=:itemId", params);
    sda.execute("UPDATE items SET chckd=:chckd WHERE parentId=:parentId", params);
    qDebug() << "Updated checked:" << itemId << chckd;

//do not emit anything
}

void DataHelper::updateItem(const QString& parentId)
{
    QVariantList params;
    params.append(QVariant::fromValue(parentId));
    QVariant result = sda.execute(
            "SELECT count(*) as count FROM items WHERE parentId=:parentId AND chckd=1", params);
    if (sda.hasError()) {
        qDebug() << sda.error().errorMessage();
    } else {
        if (result.toList().size() > 0) {
            bool ok;
            int count = result.toList().at(0).toMap().value("count").toInt(&ok);
            qDebug() << "itemsCount() parentId" << parentId << "items" << count;
            if (count == 0) {
                qDebug() << "all chckd=1" << count;
                sda.execute("UPDATE items SET chckd=0 WHERE itemId=:parentId", params);
                emit dataChanged("update_parent");
            } else {
                qDebug() << "not all chckd=1" << count;
                sda.execute("UPDATE items SET chckd=1 WHERE itemId=:parentId", params);
                emit dataChanged("update_parent");
            }
        }
    }
}

void DataHelper::deleteItem(const QString& itemId)
{
    QVariantList params;
    params.append(QVariant::fromValue(itemId));
    params.append(QVariant::fromValue(itemId));

    QVariant result = sda.execute(
            "SELECT itemId,title,reminder FROM items WHERE itemId=:itemId OR parentId=:parentId",
            params);
    if (sda.hasError()) {
        qDebug() << sda.error().errorMessage();
    } else {
        QVariantList items = result.value<QVariantList>();
        for (int i = 0; i < items.size(); i++) {
            QString title = items.at(i).toMap().value("title").toString();
            QString reminder = items.at(i).toMap().value("reminder").toString();
            if (reminder != "") {
                calendarHelper.setCalendarEvent(title, "", reminder);
            }
        }
    }

    sda.execute("DELETE FROM items WHERE itemId=:itemId OR parentId=:parentId", params);
    //emit dataChanged("delete");
}

QVariantList DataHelper::createSortList(bb::cascades::GroupDataModel* model)
{
    QVariantList list;
    for (QVariantList indexPath = model->first(); !indexPath.isEmpty();
            indexPath = model->after(indexPath)) {
        QVariant item = model->data(indexPath);
        if (item.toMap().value("chckd") == 1) {
            list.append(model->data(indexPath));
        }
    }
    return list;
}

void DataHelper::updateIndexPaths(bb::cascades::ArrayDataModel* model, const QString& parentId)
{
    QVariantList params;
    int size = model->size();
    bool ok;
    params.clear();
    params.append(QVariant::fromValue(parentId));
    sda.execute("UPDATE items SET indexPath=0 WHERE parentId=:parentId", params);
    for (int i = 0; i < size; i++) {
        QVariantMap item = model->value(i).toMap();
        params.clear();
        params.append(QVariant::fromValue(size - i));
        params.append(QVariant::fromValue(item.value("itemId")));
        params.append(QVariant::fromValue(item.value("parentId")));
        sda.execute(
                "UPDATE items SET indexPath=:indexPath WHERE itemId=:itemId AND parentId=:parentId",
                params);
        qDebug() << "saved updated:" << item.value("itemId").toString()
                << item.value("title").toString() << item.value("parentId").toString()
                << item.value("indexPath").toInt(&ok) << i;
    }
}

QString DataHelper::itemsCount(const QString& parentId)
{
    QVariantList params;
    params.append(QVariant::fromValue(parentId));
    QVariant result = sda.execute("SELECT count(*) as count FROM items WHERE parentId=:parentId",
            params);
    params.append(QVariant::fromValue(0));
    QVariant resultToDo = sda.execute(
            "SELECT count(*) as count FROM items WHERE parentId=:parentId and chckd=:chckd",
            params);
    QSettings settings("polovincak", "ToDo");
    QString countFieldType = settings.value("countFieldType", "total").toString();
    if (sda.hasError()) {
        qDebug() << sda.error().errorMessage();
    } else {
        if (result.toList().size() > 0) {
            bool ok;
            int count = result.toList().at(0).toMap().value("count").toInt(&ok);
            qDebug() << "itemsCount() parentId" << parentId << "items" << count;
            int countToDo = resultToDo.toList().at(0).toMap().value("count").toInt(&ok);
            qDebug() << "itemsToDoCount() parentId" << parentId << "items" << count;
            if (countFieldType == "total") {
                return QString::number(count);
            } else if (countFieldType == "all") {
                return QString::number(countToDo) + "/" + QString::number(count);
            } else if (countFieldType == "done") {
                return QString::number(countToDo);
            } else if (countFieldType == "none") {
                return "";
            }
        }
    }
    return "0";
}

QString DataHelper::formatDateTime(const QDateTime datetime)
{
    return datetime.toString("yyyy-MM-ddThh:mm:ss");
}

QVariantList DataHelper::getTitlesForThumbnail()
{
    QVariant result =
            sda.execute(
                    "SELECT title,listItem,indexPath,reminder FROM items WHERE listItem=0 ORDER BY reminder,indexPath");
    return result.value<QVariantList>();
}

void DataHelper::invokeEmail(const QString& address, const QString& subject, const QString& body)
{
    QString validAddress = (address == "") ? "support@polovincak.eu" : address;
    QString validSubject = subject;
    QString validBody = body;
    qDebug() << "email invocation, receiver:" << validAddress << validSubject << validBody;
    bb::system::InvokeRequest request;
    request.setTarget("sys.pim.uib.email.hybridcomposer");
    request.setAction("bb.action.SENDEMAIL");
    request.setUri(
            QUrl(
                    "mailto:" + validAddress + "?subject=" + validSubject.replace(" ", "%20")
                            + "&body=" + validBody.replace(" ", "%20")));
    invokeManager->invoke(request);
}

QByteArray DataHelper::buildSharedContent(QString itemId)
{
    qDebug() << "build content for item " << itemId;
    QString text = "";

    QVariantList params;
    params.append(QVariant::fromValue(itemId));
    QVariant result = sda.execute("SELECT itemId,title FROM items WHERE itemId=:itemId", params);
    if (sda.hasError()) {
        qDebug() << sda.error().errorMessage();
    } else {
        QVariantList items = result.value<QVariantList>();
        for (int i = 0; i < items.size(); i++) {
            QString title = items.at(i).toMap().value("title").toString();
            text += title;
            text += "\n";
        }
    }
    result = sda.execute("SELECT itemId,title FROM items WHERE parentId=:itemId", params);
    if (sda.hasError()) {
        qDebug() << sda.error().errorMessage();
    } else {
        QVariantList items = result.value<QVariantList>();
        for (int i = 0; i < items.size(); i++) {
            QString title = items.at(i).toMap().value("title").toString();
            text += "- ";
            text += title;
            text += "\n";
        }
    }

    return text.toUtf8();
}

QVariantList DataHelper::getExistingLists()
{
    //add new item for list on main page - parent/nadrazeny?
    QVariant result = sda.execute(
            "SELECT itemId,title,listItem FROM items WHERE listItem=1 ORDER BY title ASC");
    return result.value<QVariantList>();

}

void DataHelper::moveItem(QString itemId, QString parentId, QString moveParentId)
{
    qDebug() << "move item" << itemId << "from" << parentId << "to" << moveParentId;
    QVariantList params;
    params.append(QVariant::fromValue(moveParentId));
    params.append(QVariant::fromValue(itemId));
    sda.execute("UPDATE items SET parentId=:moveParentId WHERE itemId=:itemId", params);
    if (sda.hasError()) {
        qDebug() << sda.error().errorMessage();
    }
    int parentIdChecked;
    int parentIdUnchecked;
    int moveParentIdChecked;
    int moveParentIdUnchecked;

    QVariant result;
    QVariantList resultList;

    params.clear();
    params.append(QVariant::fromValue(moveParentId));
    result = sda.execute("SELECT itemId FROM items WHERE parentId=:parentId AND chckd=1", params);
    resultList = result.value<QVariantList>();
    moveParentIdUnchecked = resultList.size();
    result = sda.execute("SELECT itemId FROM items WHERE parentId=:parentId AND chckd=0", params);
    resultList = result.value<QVariantList>();
    moveParentIdChecked = resultList.size();

    params.clear();
    params.append(QVariant::fromValue(parentId));
    result = sda.execute("SELECT itemId FROM items WHERE parentId=:parentId AND chckd=1", params);
    resultList = result.value<QVariantList>();
    parentIdUnchecked = resultList.size();
    result = sda.execute("SELECT itemId FROM items WHERE parentId=:parentId AND chckd=0", params);
    resultList = result.value<QVariantList>();
    parentIdChecked = resultList.size();

    qDebug() << parentId << parentIdChecked << parentIdUnchecked;
    qDebug() << moveParentId << moveParentIdChecked << moveParentIdUnchecked;

    if (moveParentId != "0") {
        if (moveParentIdUnchecked == 0) {
            qDebug() << moveParentId << "no unchecked items";
            if (moveParentIdChecked > 0) {
                qDebug() << moveParentId
                        << "checked items detected, chckd for this parent id set to 0";
                updateChckdItemOnly(moveParentId, 0);
            } else {
                qDebug() << moveParentId
                        << "no unchecked items detected, no checked items detected, chckd for this parent id set to 1";
                updateChckdItemOnly(moveParentId, 1);
            }
        } else {
            qDebug() << moveParentId
                    << "unchecked items detected, chckd for this parent id set to 1";
            updateChckdItemOnly(moveParentId, 1);
        }
    }

    if (parentId != "0") {
        if (parentIdUnchecked == 0) {
            qDebug() << parentId << "no unchecked items";
            if (parentIdChecked > 0) {
                qDebug() << parentId << "checked items detected, chckd for this parent id set to 0";
                updateChckdItemOnly(parentId, 0);
            } else {
                qDebug() << parentId
                        << "no unchecked items detected, no checked items detected, chckd for this parent id set to 1";
                updateChckdItemOnly(parentId, 1);
            }
        } else {
            qDebug() << parentId << "unchecked items detected, chckd for this parent id set to 1";
            updateChckdItemOnly(parentId, 1);
        }
    }

    qDebug() << "emit data changed signal";
    emit dataChanged("update");
    emit dataChanged("update_parent");
}

void DataHelper::updateChckdItemOnly(const QString& itemId, const int chckd)
{
    QVariantList params;
    params.append(QVariant::fromValue(chckd));
    params.append(QVariant::fromValue(itemId));
    sda.execute("UPDATE items SET chckd=:chckd WHERE itemId=:itemId", params);

}

void DataHelper::checkChckd(QString parentId)
{
    QVariantList params;
    QVariant result;
    QVariantList resultList;

    int parentIdChecked;
    int parentIdUnchecked;

    params.clear();
    params.append(QVariant::fromValue(parentId));
    result = sda.execute("SELECT itemId FROM items WHERE parentId=:parentId AND chckd=1", params);
    resultList = result.value<QVariantList>();
    parentIdUnchecked = resultList.size();
    result = sda.execute("SELECT itemId FROM items WHERE parentId=:parentId AND chckd=0", params);
    resultList = result.value<QVariantList>();
    parentIdChecked = resultList.size();

    if (parentId != "0") {
        if (parentIdUnchecked == 0) {
            qDebug() << parentId << "no unchecked items";
            if (parentIdChecked > 0) {
                qDebug() << parentId << "checked items detected, chckd for this parent id set to 0";
                updateChckdItemOnly(parentId, 0);
            } else {
                qDebug() << parentId
                        << "no unchecked items detected, no checked items detected, chckd for this parent id set to 1";
                updateChckdItemOnly(parentId, 1);
            }
        } else {
            qDebug() << parentId << "unchecked items detected, chckd for this parent id set to 1";
            updateChckdItemOnly(parentId, 1);
        }
    }

    qDebug() << "emit data changed signal";
    emit dataChanged("update");
    emit dataChanged("update_parent");
}

QString DataHelper::getBackupDirectory()
{
    QSettings settings("polovincak", "ToDo");
    return settings.value("backupDirectory", "").toString();
}

void DataHelper::setBackupDirectory(const QString& value)
{
    QString validPath;
    QString replace = value;
    if(replace.contains("removable/sdcard", Qt::CaseInsensitive)) {
        removablemedia_info_t *info;
        int ret = removablemedia_get_info(&info);
        const char *path;
        path = removablemedia_info_get_device_path(info);
        removablemedia_free_info(&info);
        qDebug() << "removable path" << path;
        //validPath = replace.replace("/accounts/1000/removable/sdcard", path, Qt::CaseInsensitive);
        validPath = value;
    } else {
        validPath = replace.replace("/accounts/1000", QDir::currentPath(), Qt::CaseInsensitive);
    }
    qDebug() << "valid path" << validPath;

    backup();

    QSettings settings("polovincak", "ToDo");
    settings.setValue("backupDirectory", validPath);
}

void DataHelper::backup() {
    QDir dir;
    QFile file;
    QSettings settings("polovincak", "ToDo");
    QString path = settings.value("backupDirectory", QDir::currentPath()).toString();
    qDebug() << "backup path" << path;
}
