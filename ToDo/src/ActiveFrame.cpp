/*
 * ActiveFrame.cpp
 *
 *  Created on: May 28, 2013
 *      Author: martin
 */
#include "ActiveFrame.hpp"

#include <QDebug>
#include <bb/cascades/Label>

ActiveFrame::ActiveFrame(DataHelper* dataHelper) :
        SceneCover(this), titles(), titlesPosition(0)
{
    this->dataHelper = dataHelper;
    sceneCoverQml = QmlDocument::create("asset:///activeFrame.qml").parent(this);
    sceneCoverContainer = sceneCoverQml->createRootObject<Container>();
    setContent(sceneCoverContainer);

    timer = new QTimer(this);
    connect(timer, SIGNAL(timeout()), this, SLOT(appInScreenCoverUpdate()));

    QObject::connect(Application::instance(), SIGNAL(thumbnail()), this, SLOT(appInThumbnail()));
    QObject::connect(Application::instance(), SIGNAL(fullscreen()), this, SLOT(appInFullScreen()));
    QObject::connect(Application::instance(), SIGNAL(invisible()), this, SLOT(appInInvisible()));
}

ActiveFrame::~ActiveFrame()
{

}

void ActiveFrame::appInThumbnail()
{
    qDebug() << "App thumbnailed";

    titles.clear();
    titlesPosition = 0;

    titles = dataHelper->getTitlesForThumbnail();

    timer->start(4000);
}

void ActiveFrame::appInFullScreen()
{
    qDebug() << "App fullscreened";
    timer->stop();
}

void ActiveFrame::appInInvisible()
{
    qDebug() << "App invisible";
    timer->stop();
}

void ActiveFrame::appInScreenCoverUpdate()
{
    qDebug() << "appInScreenCoverUpdate() fired";
    if (titlesPosition == titles.size()) {
        titlesPosition = 0;
    }
    Label* label2 = sceneCoverContainer->findChild<Label*>("activeFrameLabel");
    if (label2) {
        qDebug() << "Show title" << titles.at(titlesPosition).toMap().value("title").toString();
        label2->setText(titles.at(titlesPosition).toMap().value("title").toString());
    } else {
        qDebug() << "activeFrameLabel not found";
    }
    titlesPosition++;
}
