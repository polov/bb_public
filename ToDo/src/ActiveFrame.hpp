/*
 * ActiveFrame.hpp
 *
 *  Created on: May 28, 2013
 *      Author: martin
 */

#ifndef ACTIVEFRAME_HPP_
#define ACTIVEFRAME_HPP_

//#include <QObject>
#include <bb/cascades/Application>
#include <bb/cascades/QmlDocument>
#include <bb/cascades/SceneCover>
#include <bb/cascades/Container>

#include "DataHelper.h"

using namespace ::bb::cascades;

class ActiveFrame : public SceneCover {
Q_OBJECT

public:
	ActiveFrame(DataHelper* dataHelper);
	virtual ~ActiveFrame();


private slots:
	void appInThumbnail();
	void appInFullScreen();
	void appInInvisible();
	void appInScreenCoverUpdate();

private:
	Container *sceneCoverContainer;
	QmlDocument *sceneCoverQml;
	QTimer *timer;
	QVariantList titles;
	int titlesPosition;
	DataHelper* dataHelper;
};

#endif /* ACTIVEFRAME_HPP_ */
