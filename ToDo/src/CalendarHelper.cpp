/*
 * CalendarHelper.cpp
 *
 *  Created on: 19. 7. 2015
 *      Author: martin
 */

#include <src/CalendarHelper.h>

#include <QDebug>

#include <bb/pim/calendar/Result>
#include <bb/pim/calendar/CalendarEvent>
#include <bb/pim/calendar/EventSearchParameters>

#include <bb/pim/calendar/ICalendarTodo>
#include <bb/pim/calendar/ICalendarObjects>

CalendarHelper::CalendarHelper() :
        calendarService()
{
    // TODO Auto-generated constructor stub

}

CalendarHelper::~CalendarHelper()
{
    // TODO Auto-generated destructor stub
}

void CalendarHelper::setCalendarEvent(const QString& title, const QString& reminder, const QString& reminderOld)
{
    if (reminderOld != "") {
        //find if there is any reminder to remove
        qDebug() << "setCalendarEvent(): search event with subtitle prefix" << title << "time" << QDateTime::fromString(reminderOld, "yyyy-MM-ddThh:mm:ss");

        // Retrieve the event that you want to update from the database
        EventSearchParameters searchParameters;
        searchParameters.setStart(QDateTime::fromString(reminderOld, "yyyy-MM-ddThh:mm:ss").addSecs(-10));
        searchParameters.setEnd(QDateTime::fromString(reminderOld, "yyyy-MM-ddThh:mm:ss").addSecs(10));
        searchParameters.setSubjectPrefix(title);
        const QList<CalendarEvent> events = calendarService.events(searchParameters);

        foreach(CalendarEvent event, events) {
            qDebug() << "setCalendarEvent(): found event:" << event.subject() << event.startTime();
            calendarService.deleteEvent(event);
        }

        if(reminder=="") {
            return;
        }
    }
    // Retrieve the IDs for the default calendar
    QPair<AccountId, FolderId> defaultCalendar = calendarService.defaultCalendarFolder();
    Result::Type result;

    // Create the calendar event and set its IDs
    CalendarEvent newEvent;
    newEvent.setAccountId(defaultCalendar.first);
    newEvent.setFolderId(defaultCalendar.second);
    newEvent.setSubject(title);
    newEvent.setStartTime(QDateTime::fromString(reminder, "yyyy-MM-ddThh:mm:ss"));
    newEvent.setEndTime(QDateTime::fromString(reminder, "yyyy-MM-ddThh:mm:ss"));

    result = calendarService.createEvent(newEvent);
    qDebug() << "calendarEvent result" << result << newEvent.id();

}
