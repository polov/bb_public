/*
 * CalendarHelper.h
 *
 *  Created on: 19. 7. 2015
 *      Author: martin
 */

#ifndef CALENDARHELPER_H_
#define CALENDARHELPER_H_

#include <bb/pim/calendar/CalendarService>

using namespace bb::pim::calendar;

class CalendarHelper
{
public:
    CalendarHelper();
    virtual ~CalendarHelper();

    void setCalendarEvent(const QString& title, const QString& reminder, const QString& reminderOld);

private:
    CalendarService calendarService;
};

#endif /* CALENDARHELPER_H_ */
