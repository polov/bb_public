/*
 * DataHelper.h
 *
 *  Created on: 9. 7. 2015
 *      Author: martin
 */

#ifndef DATAHELPER_H_
#define DATAHELPER_H_

#include <QObject>

#include <bb/cascades/ArrayDataModel>
#include <bb/cascades/GroupDataModel>
#include <bb/data/SqlDataAccess>
#include <bb/system/InvokeManager>

#include "CalendarHelper.h"

class DataHelper: public QObject
{
    Q_OBJECT

public:
    DataHelper();
    virtual ~DataHelper();

    Q_INVOKABLE
    QString getDateTimeId();

    Q_INVOKABLE
    QVariantList loadItems(const QString& parentId);
    Q_INVOKABLE
    void saveItem(const QString& itemId, const QString& title, const QString& parentId, const int indexPath, const int chckd, const int listItem, const QString& reminder);
    Q_INVOKABLE
    void deleteItem(const QString& itemId);
    Q_INVOKABLE
    void updateItem(const QString& itemId, const QString& title, const QString& parentId, const int indexPath, const int chckd, const int listItem, const QString& reminder, const QString& reminderOld);
    Q_INVOKABLE
    void updateItem(const QString& itemId, const int chckd);
    Q_INVOKABLE
    void updateItem(const QString& parentId);

    Q_INVOKABLE
    QVariantList createSortList(bb::cascades::GroupDataModel* model);
    Q_INVOKABLE
    void updateIndexPaths(bb::cascades::ArrayDataModel* model, const QString& parentId);

    Q_INVOKABLE
    QString itemsCount(const QString& parentId);
    Q_INVOKABLE
    QString formatDateTime(const QDateTime datetime);

    Q_INVOKABLE
    QString getVisualStyle();
    Q_INVOKABLE
    void setVisualStyle(const QString& value);
    Q_INVOKABLE
    QString getCountType();
    Q_INVOKABLE
    void setCountType(const QString& value);
    Q_INVOKABLE
    QString getFontSize();
    Q_INVOKABLE
    void setFontSize(const QString& value);

    Q_INVOKABLE
    QString getBackupDirectory();
    Q_INVOKABLE
    void setBackupDirectory(const QString& value);

    QVariantList getTitlesForThumbnail();
    void updateChckdItemOnly(const QString& itemId, const int chckd);

    Q_INVOKABLE
    void invokeEmail(const QString& address, const QString& subject, const QString& body);
    Q_INVOKABLE
    QByteArray buildSharedContent(QString itemId);
    Q_INVOKABLE
    QVariantList getExistingLists();
    Q_INVOKABLE
    void moveItem(QString itemId, QString parentId, QString moveParentId);
    Q_INVOKABLE
    void checkChckd(QString parentId);

    void backup();

signals:
    void dataChanged(const QString);

private:
    bb::data::SqlDataAccess sda;
    CalendarHelper calendarHelper;
    QSettings settings;
    bb::system::InvokeManager* invokeManager;
};

#endif /* DATAHELPER_H_ */
