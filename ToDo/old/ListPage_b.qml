import bb.cascades 1.4

Page {
    id: todoPage
    actionBarVisibility: ChromeVisibility.Compact
    Container {
        id: container
        property int itemOrigin: -1
        ListView {
            id: listView
            dataModel: dataModel
            listItemComponents: [
                ListItemComponent {
                    type: ""
                    content: Container {
                        id: listItemComponentContainer
                        //topPadding: ui.du(0.5)
                        Container {
                            id: listItemComponentContentContainer
                            layout: AbsoluteLayout {
                            }
                            Container {
                                id: doneActionContainer
                                layoutProperties: AbsoluteLayoutProperties {
                                    id: doneContainerAbsProps
                                    positionX: ui.du(1.25)
                                    positionY: ui.du(1.1)
                                }
                                ImageView {
                                    preferredWidth: ui.du(10.0)
                                    preferredHeight: ui.du(10.0)
                                    imageSource: "asset:///images/ic_done.png"
                                    gestureHandlers: [
                                        TapHandler {
                                            onTapped: {
                                                absLabelContainerLayoutProps.positionX = 0;
                                                labelContainer.tempLocalX = 0;
                                                labelContainer.currLocalX = 0;
                                                listItemComponentContainer.ListItem.view.moveItemToDone(listItemComponentContainer.ListItem.indexPath);
                                            }
                                        }
                                    ]
                                }
                            }
                            Container {
                                id: deleteActionContainer
                                layoutProperties: AbsoluteLayoutProperties {
                                    id: deleteContainerAbsProps
                                    positionX: 720 - ui.du(11.25)
                                    positionY: ui.du(1.1)
                                }
                                ImageView {
                                    preferredWidth: ui.du(10.0)
                                    preferredHeight: ui.du(10.0)
                                    imageSource: "asset:///images/ic_delete.png"
                                    gestureHandlers: [
                                        TapHandler {
                                            onTapped: {
                                                absLabelContainerLayoutProps.positionX = 0;
                                                labelContainer.tempLocalX = 0;
                                                labelContainer.currLocalX = 0;
                                                listItemComponentContainer.ListItem.view.deleteItem(listItemComponentContainer.ListItem.indexPath);
                                            }
                                        }
                                    ]
                                }
                            }
                            Container {
                                id: labelContainer
                                property int tempLocalX: 0
                                property int currLocalX: 0
                                property bool currLocalXtoZero: false
                                layoutProperties: AbsoluteLayoutProperties {
                                    id: absLabelContainerLayoutProps
                                    positionX: 0
                                    positionY: 0
                                }
                                preferredWidth: 720.0
                                preferredHeight: ui.du(12.5)
                                leftPadding: ui.du(2.5)
                                rightPadding: ui.du(2.5)
                                topPadding: ui.du(2.75)
                                background: Color.LightGray
                                Label {
                                    id: titleLabel
                                    verticalAlignment: VerticalAlignment.Center
                                    text: ListItemData.title
                                    textStyle.color: Color.White
                                    textStyle.fontSize: FontSize.Large
                                }
                                gestureHandlers: [
                                    LongPressHandler {
                                        onLongPressed: {
                                            listItemComponentContainer.ListItem.view.rearrangList();
                                        }
                                    },
                                    TapHandler {
                                        onTapped: {
                                            if(listItemComponentContainer.ListItem.view.getIsLastgeValue()) {
                                                
                                            } else {
                                                listItemComponentContainer.ListItem.view.showListPage(ListItemData.itemId);   
                                            }
                                        }
                                    }
                                ]
                            }
                        }
                        onTouch: { //down,move,up,cancel
                            console.log("touch event id: " + event.touchType);
                            var rearrange = listItemComponentContainer.ListItem.view.getRearrangeValue();
                            if (event.touchType == TouchType.Move) {
                                //moving label container left or right - not do it when in rearrange mode
                                if (! rearrange) {
                                    //set what item is moving - do not enable to move more than one item
                                    if (listItemComponentContainer.ListItem.view.getMovedItemTitle() == "") {
                                        listItemComponentContainer.ListItem.view.setMovedItemTitle(ListItemData.title)
                                    }
                                    //move of the right item wanted
                                    if (listItemComponentContainer.ListItem.view.getMovedItemTitle() == ListItemData.title) {
                                        listItemComponentContainer.ListItem.view.setCanTrigger(false);
                                        var localX = event.localX;
                                        if (labelContainer.tempLocalX == 0) {
                                            labelContainer.tempLocalX = localX;
                                        }
                                        var delta = event.localX - labelContainer.tempLocalX;
                                        var newLocalX = labelContainer.currLocalX + delta;
                                        if (newLocalX < - ui.du(12.5)) {
                                            labelContainer.currLocalX = - ui.du(12.5);
                                        } else if (newLocalX > ui.du(12.5)) {
                                            labelContainer.currLocalX = ui.du(12.5);
                                        } else {
                                            labelContainer.currLocalX = newLocalX;
                                        }
                                        absLabelContainerLayoutProps.positionX = labelContainer.currLocalX;
                                        labelContainer.tempLocalX = localX;

                                        console.log("item: " + listItemComponentContainer.ListItem.view.getMovedItemTitle() + " delta:" + delta + " newLocalX:" + labelContainer.currLocalX);

                                        if (labelContainer.currLocalX > ui.du(3.0)) {
                                            labelContainer.background = Color.Green
                                        } else {
                                            labelContainer.background = Color.LightGray
                                        }
                                    }
                                }
                            }
                            if (event.touchType == TouchType.Up || event.touchType == TouchType.Cancel) {
                                //moving label container left or right ends - not do it when in rearrange mode
                                if (! rearrange) {
                                    //move of the right item wanted
                                    if (listItemComponentContainer.ListItem.view.getMovedItemTitle() == ListItemData.title) {
                                        //if moved not enough, move it back to 0
                                        if (labelContainer.currLocalX > - ui.du(10.0) && labelContainer.currLocalX < ui.du(10.0)) {
                                            console.log("move back [" + labelContainer.currLocalX + " " + ui.du(10.0) + "]");
                                            labelContainer.currLocalXtoZero = true;
                                            //listItemComponentContainer.ListItem.view.setCanTrigger(true);
                                        } else {
                                            //if it is nearly ok, move to right place automaticaly
                                            console.log("finish move [" + labelContainer.currLocalX + " " + ui.du(10.0) + "]");
                                            if (labelContainer.currLocalX > 0) {
                                                labelContainer.currLocalX = ui.du(12.5);
                                            } else if (labelContainer.currLocalX < 0) {
                                                labelContainer.currLocalX = - (ui.du(12.5));
                                            }
                                            titleLabel.text = "<html><span style='text-decoration:line-through'>" + ListItemData.title + "</span></html>";
                                            labelContainer.tempLocalX = 0.0;
                                        }
                                        if (labelContainer.currLocalXtoZero) {
                                            //move back if needed
                                            absLabelContainerLayoutProps.positionX = 0.0;
                                            labelContainer.currLocalX = 0.0;
                                            labelContainer.tempLocalX = 0.0;
                                            labelContainer.currLocalXtoZero = false;
                                            labelContainer.background = Color.LightGray
                                            titleLabel.text = ListItemData.title;
                                            if (listItemComponentContainer.ListItem.view.getMovedItemTitle() == ListItemData.title) {
                                                listItemComponentContainer.ListItem.view.setMovedItemTitle("");
                                            }
                                            //listItemComponentContainer.ListItem.view.setCanTrigger(true);
                                        } else {
                                            //mark as move back after untouch again
                                            labelContainer.currLocalXtoZero = true;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            ]
            onTriggered: {
                var chosenItem = listView.dataModel.data(indexPath);
                if(isLast) {
                    
                } else {
                    showListPage(chosenItem.itemId);   
                }
            }
            function rearrangList() {
                rearrange = ! rearrange;
                rearrangeHandler.setActive(rearrange);
                if (! rearrange) {
                    dataHelper.saveItems(dataModel, todoPage.parentId);
                }
            }
            function getRearrangeValue() {
                return rearrange;
            }
            function getIsLastgeValue() {
                return todoPage.isLast;
            }
            function getMovedItemTitle() {
                return movedItemTitle;
            }
            function setMovedItemTitle(value) {
                movedItemTitle = value;
            }
            function setCanTrigger(value) {
                todoPage.canTrigger = value;
            }
            function deleteItem(indexPath) {
                var object = dataModel.data(indexPath);
                console.log("delete: " + object.title);
                dataModel.removeAt(indexPath);
                dataHelper.saveItems(dataModel, todoPage.parentId);
            }
            function moveItemToDone(indexPath) {
                var object = dataModel.data(indexPath);
                console.log("done: " + object.title);
            }
            function showListPage(itemId) {
                var listPageContent = listPage.createObject();
                listPageContent.parentId = itemId;
                listPageContent.isLast = true;
                navigationPane.push(listPageContent);
            }
            rearrangeHandler {
                // Do not activate the handler here. It
                // will be forcefully deactivated when
                // the data model is assigned.
                onMoveStarted: {
                    console.log("onMoveStarted: " + event.startIndexPath);
                    container.itemOrigin = event.startIndexPath[0];
                }
                onMoveEnded: {
                    console.log("onMoveEnded: " + event.endIndexPath);
                }
                onMoveUpdated: {
                    // Always call denyMove() before initiating
                    // the move. Without first denying the move
                    // the list item will only move 1 spot
                    // regardless of how many spots it's
                    // dragged.
                    event.denyMove();
                    dataModel.move(event.fromIndexPath[0], event.toIndexPath[0]);
                    console.log("onMoveUpdated: " + event.fromIndexPath[0] + " -> " + event.toIndexPath[0]);
                }
                onMoveAborted: {
                    console.log("onMoveAborted: " + event.endIndexPath);
                    container.undoItemMove();
                }
                onActiveChanged: {
                    console.log("active changed: " + active);
                }
            }
        }
    }
    attachedObjects: [
        ArrayDataModel {
            id: dataModel
        },
        ComponentDefinition {
            id: addSheet
            source: "add.qml"
        },
        ComponentDefinition {
            id: listPage
            source: "ListPage.qml"
        }
    ]
    actions: [
        ActionItem {
            id: signature
            title: (rearrange) ? "Done" : "Add"
            imageSource: (rearrange) ? "asset:///images/ic_done.png" : "asset:///images/ic_add.png"
            ActionBar.placement: ActionBarPlacement.Signature
            onTriggered: {
                if (rearrange) {
                    console.log("list rearrange finished");
                    listView.rearrangList();
                    movedItemTitle = "";
                } else {
                    var addSheetContent = addSheet.createObject();
                    addSheetContent.parentId = todoPage.parentId;
                    addSheetContent.dataModel = dataModel;
                    addSheetContent.open();
                }
            }
        }
    ]
    onCreationCompleted: {
        dataModel.insert(0, dataHelper.loadItems(parentId));
    }
    onRearrangeChanged: {
        console.log("rearrange changed: " + rearrange);
    }
    onMovedItemTitleChanged: {
        console.log("movedItemTitle changed: " + rearrange);
    }
    onParentIdChanged: {
        console.log("load model after parentId changed");
        dataModel.clear();
        dataModel.insert(0, dataHelper.loadItems(parentId));
    }
    property bool rearrange: false
    property string movedItemTitle: ""
    property bool canTrigger: true
    property int parentId: 0
    property bool isLast: false
}
