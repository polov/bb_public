/*
 * Copyright (c) 2011-2015 BlackBerry Limited.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import bb.cascades 1.4

Page {
    id: todoPage
    actionBarVisibility: ChromeVisibility.Compact
    Container {
        id: container
        property int itemOrigin: -1
        ListView {
            id: listView
            dataModel: dataModel
            listItemComponents: [
                ListItemComponent {
                    type: ""
                    content: Container {
                        id: listItemComponentContainer
                        Container {
                            topPadding: ui.du(0.1)
                            layout: AbsoluteLayout {
                            }
                            Container {
                                id: doneActionContainer
                                visible: false
                                layoutProperties: AbsoluteLayoutProperties {
                                    id: doneContainerAbsProps
                                    positionX: ui.du(10.0) //0
                                    positionY: ui.du(1.75)
                                }
                                ImageButton {
                                    preferredWidth: ui.du(7.5)
                                    preferredHeight: ui.du(7.5)
                                    defaultImageSource: "asset:///images/ic_done.png"
                                    onClicked: {
                                        absLayoutProps.positionX = 0;
                                        labelContainer.prevLocalX = 0;
                                        labelContainer.currLocalX = 0;
                                        console.log("mark item: " + ListItemData.title + " as done" );
                                        listItemComponentContainer.ListItem.view.moveItemToDone(ListItem.indexPath);
                                    }
                                }
                            }
                            Container {
                                id: deleteActionContainer
                                visible: false
                                preferredWidth: ui.du(20.0)
                                layoutProperties: AbsoluteLayoutProperties {
                                    id: deleteContainerAbsProps
                                    positionX: 720-ui.du(17.0)
                                    positionY: ui.du(1.75)
                                }
                                ImageButton {
                                    preferredWidth: ui.du(7.5)
                                    preferredHeight: ui.du(7.5)
                                    defaultImageSource: "asset:///images/ic_delete.png"
                                    onClicked: {
                                        absLayoutProps.positionX = 0;
                                        labelContainer.prevLocalX = 0;
                                        labelContainer.currLocalX = 0;
                                        console.log("delete item: " + ListItemData.title);
                                        listItemComponentContainer.ListItem.view.deleteItem(ListItem.indexPath);
                                    }
                                }
                            }
                            Container {
                                id: labelContainer
                                property int prevLocalX: 0
                                property int currLocalX: 0
                                property bool toZero: false
                                property bool actionNeeded: false
                                layoutProperties: AbsoluteLayoutProperties {
                                    id: absLayoutProps
                                    positionX: 0
                                    positionY: 0
                                }
                                preferredWidth: 720.0
                                minWidth: 720.0
                                //preferredHeight: ui.du(12.0)
                                background: Color.LightGray
                                leftPadding: ui.du(2.5)
                                rightPadding: ui.du(2.5)
                                topPadding: ui.du(3.0)
                                bottomPadding: ui.du(3.0)
                                Container {
                                    Label {
                                        id: titleLabel
                                        text: ListItemData.title
                                        textStyle.color: Color.White
                                        textStyle.fontSize: FontSize.Large
                                        //textStyle.fontWeight: FontWeight.W400
                                    }
                                }
                            }
                        }
                        onTouch: {
                            console.log("touch event " + event.touchType);
                            if (event.touchType == TouchType.Move) {
                                if (! listItemComponentContainer.ListItem.view.getRearrangeValue()) {
                                    console.log(event.localX);
                                    if (labelContainer.prevLocalX == 0) {
                                        labelContainer.prevLocalX = event.localX;
                                    }
                                    var delta = event.localX - labelContainer.prevLocalX;
                                    console.log("delta: " + delta);
                                    labelContainer.currLocalX += delta;
                                    absLayoutProps.positionX = labelContainer.currLocalX;
                                    labelContainer.prevLocalX = event.localX;
                                }
                            }
                            if (event.touchType == TouchType.Up || event.touchType == TouchType.Cancel) {
                                console.log("touch type up");
                                console.log("movedItem " + listItemComponentContainer.ListItem.view.getMovedItemTitle());
                                if(listItemComponentContainer.ListItem.view.getMovedItemTitle() == "") {
                                    listItemComponentContainer.ListItem.view.setMovedItemTitle(ListItemData.title);
                                    console.log("moving item:" + listItemComponentContainer.ListItem.view.getMovedItemTitle());
                                }
                                if (! listItemComponentContainer.ListItem.view.getRearrangeValue()) {
                                    console.log("try to move item, rearrange:" + listItemComponentContainer.ListItem.view.getRearrangeValue());
                                    if(labelContainer.currLocalX<-(ui.du(10.0)) && !labelContainer.toZero && listItemComponentContainer.ListItem.view.getMovedItemTitle()==ListItemData.title) {
                                        absLayoutProps.positionX = -(ui.du(20.0));
                                        labelContainer.currLocalX = -(ui.du(20.0));
                                        labelContainer.toZero = true;
                                        labelContainer.background = Color.DarkRed
                                        deleteActionContainer.visible = true;
                                        titleLabel.text = "<html><span style='text-decoration:line-through'>" + ListItemData.title + "</span></html>";
                                    } else if(labelContainer.currLocalX>ui.du(10.0) && !labelContainer.toZero && listItemComponentContainer.ListItem.view.getMovedItemTitle()==ListItemData.title) {
                                        absLayoutProps.positionX = ui.du(20.0);
                                        labelContainer.currLocalX = ui.du(20.0);
                                        labelContainer.toZero = true;
                                        labelContainer.background = Color.Green
                                        doneActionContainer.visible = true;
                                        titleLabel.text = "<html><span style='text-decoration:line-through'>" + ListItemData.title + "</span></html>";
                                    } else {
                                        absLayoutProps.positionX = 0.0;
                                        labelContainer.currLocalX = 0.0;
                                        labelContainer.background = Color.LightGray
                                        labelContainer.toZero = false;
                                        titleLabel.text = ListItemData.title;
                                        if(listItemComponentContainer.ListItem.view.getMovedItemTitle()==ListItemData.title) {
                                            listItemComponentContainer.ListItem.view.setMovedItemTitle("");
                                        }
                                        doneActionContainer.visible = false;
                                        deleteActionContainer.visible = false;
                                    }
                                    labelContainer.prevLocalX = 0;
                                    console.log("values reset");
                                }
                            }
                        }
                        gestureHandlers: [
                            LongPressHandler {
                                onLongPressed: {
                                    listItemComponentContainer.ListItem.view.rearrangList();
                                }
                            }
                        ]
                    }
                }
            ]
            function rearrangList() {
                rearrange = ! rearrange;
                rearrangeHandler.setActive(rearrange);
            }
            function getRearrangeValue() {
                return rearrange;
            }
            function getMovedItemTitle() {
                return movedItemTitle;
            }
            function setMovedItemTitle(value) {
                movedItemTitle = value;
            }
            function deleteItem(indexPath) {
                dataModel.removeAt(indexPath);
            }
            function moveItemToDone(indexPath) {
                var object = dataModel.data(indexPath);
                dataModelDone.insert(0, object);
                dataModel.removeAt(indexPath);
            }
            rearrangeHandler {
                // Do not activate the handler here. It
                // will be forcefully deactivated when
                // the data model is assigned.
                onMoveStarted: {
                    console.log("onMoveStarted: " + event.startIndexPath);
                    container.itemOrigin = event.startIndexPath[0];
                }
                onMoveEnded: {
                    console.log("onMoveEnded: " + event.endIndexPath);
                }
                onMoveUpdated: {
                    // Always call denyMove() before initiating
                    // the move. Without first denying the move
                    // the list item will only move 1 spot
                    // regardless of how many spots it's
                    // dragged.
                    event.denyMove();
                    dataModel.move(event.fromIndexPath[0], event.toIndexPath[0]);
                    console.log("onMoveUpdated: " + event.fromIndexPath[0] + " -> " + event.toIndexPath[0]);
                }
                onMoveAborted: {
                    console.log("onMoveAborted: " + event.endIndexPath);
                    container.undoItemMove();
                }
                onActiveChanged: {
                    console.log("active changed: " + active);
                }
            }
        }
    }
    attachedObjects: [
        ArrayDataModel {
            id: dataModel
        },
        ArrayDataModel {
            id: dataModelDone
        },
        ComponentDefinition {
            id: addSheet
            source: "add.qml"
        }
    ]
    actions: [
        ActionItem {
            id: signature
            title: (rearrange) ? "Done" : "Add"
            imageSource: (rearrange) ? "asset:///images/ic_done.png" : "asset:///images/ic_add.png"
            ActionBar.placement: ActionBarPlacement.Signature
            onTriggered: {
                if (rearrange) {
                    console.log("list rearrange finished");
                    listView.rearrangList();
                    movedItemTitle = "";
                }
                else {
                    var addSheetContent = addSheet.createObject();
                    addSheetContent.open();
                }
            }
        }
    ]
    onCreationCompleted: {
        dataModel.insert(0, {
                "title": "Poznámka",
                "list": "Personal",
                "status": "new",
                "deleted": "No"
            });
        dataModel.insert(1, {
                "title": "Vyzkoušet úpravu pořadí",
                "list": "Personal",
                "status": "new",
                "deleted": "No"
            });
        dataModel.insert(2, {
                "title": "Dodělat rozdělané",
                "list": "Personal",
                "status": "new",
                "deleted": "No"
            });
    }
    onRearrangeChanged: {
        console.log("rearrange changed: " + rearrange);
    }
    property bool rearrange: false
    property string movedItemTitle: ""
}
