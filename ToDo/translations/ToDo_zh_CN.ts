<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="zh_CN">
<context>
    <name>ListPage</name>
    <message>
        <location filename="../assets/ListPage.qml" line="104"/>
        <source>TODO</source>
        <translation>备忘录</translation>
    </message>
    <message>
        <location filename="../assets/ListPage.qml" line="104"/>
        <source>DONE</source>
        <translation>完成</translation>
    </message>
    <message>
        <location filename="../assets/ListPage.qml" line="213"/>
        <source>Mark as Done</source>
        <translation>标记为完成</translation>
    </message>
    <message>
        <location filename="../assets/ListPage.qml" line="213"/>
        <source>Mark as unfinished</source>
        <translation>标记为未完成</translation>
    </message>
    <message>
        <location filename="../assets/ListPage.qml" line="226"/>
        <source>Edit item</source>
        <translation>编辑项目</translation>
    </message>
    <message>
        <location filename="../assets/ListPage.qml" line="234"/>
        <source>Move item</source>
        <translation>移动项目</translation>
    </message>
    <message>
        <location filename="../assets/ListPage.qml" line="277"/>
        <source>Delete</source>
        <translation>删除</translation>
    </message>
    <message>
        <location filename="../assets/ListPage.qml" line="278"/>
        <source>Delete the item</source>
        <translation>删除项目</translation>
    </message>
    <message>
        <location filename="../assets/ListPage.qml" line="371"/>
        <source>Done</source>
        <translation>完成</translation>
    </message>
    <message>
        <location filename="../assets/ListPage.qml" line="371"/>
        <source>Add</source>
        <translation>添加</translation>
    </message>
    <message>
        <location filename="../assets/ListPage.qml" line="388"/>
        <source>Reorder</source>
        <translation>排列</translation>
    </message>
</context>
<context>
    <name>about</name>
    <message>
        <location filename="../assets/about.qml" line="45"/>
        <source>Build by</source>
        <translation>开发者 </translation>
    </message>
    <message>
        <location filename="../assets/about.qml" line="60"/>
        <source>Privacy policy (English)</source>
        <translation>隐私政策 （英文）</translation>
    </message>
    <message>
        <location filename="../assets/about.qml" line="70"/>
        <source>If you like the app, please do not forget to write a review.</source>
        <translation>如果您喜欢该应用，请记得给我们好评。</translation>
    </message>
    <message>
        <location filename="../assets/about.qml" line="80"/>
        <source>THE AUTHOR DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE, INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS, IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, INDIRECT OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.</source>
        <translation>作者并不保证该软件,包括所有隐含保证的适销性和适应性,在任何事件作者应当承担任何特殊的、间接的或间接的损害赔偿或使用任何赔偿造成的损失,数据或利润,是否在一个合同,疏忽或其他侵权行为的行动,引起的或与使用该软件的性能。</translation>
    </message>
</context>
<context>
    <name>activeFrame</name>
    <message>
        <location filename="../assets/activeFrame.qml" line="13"/>
        <source>TODO</source>
        <translation>备忘录</translation>
    </message>
</context>
<context>
    <name>add</name>
    <message>
        <location filename="../assets/add.qml" line="8"/>
        <source>New Item</source>
        <translation>新建项目</translation>
    </message>
    <message>
        <location filename="../assets/add.qml" line="8"/>
        <source>Edit item</source>
        <translation>编辑项目</translation>
    </message>
    <message>
        <location filename="../assets/add.qml" line="11"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../assets/add.qml" line="20"/>
        <source>Save</source>
        <translation>保存</translation>
    </message>
    <message>
        <location filename="../assets/add.qml" line="57"/>
        <source>Title</source>
        <translation>标题</translation>
    </message>
    <message>
        <location filename="../assets/add.qml" line="91"/>
        <source>List</source>
        <translation>列表</translation>
    </message>
    <message>
        <location filename="../assets/add.qml" line="122"/>
        <source>Reminder</source>
        <translation>提醒</translation>
    </message>
    <message>
        <location filename="../assets/add.qml" line="142"/>
        <source>Date and Time</source>
        <translation>日期和时间</translation>
    </message>
</context>
<context>
    <name>main</name>
    <message>
        <location filename="../assets/main.qml" line="26"/>
        <source>About</source>
        <translation>关于</translation>
    </message>
    <message>
        <location filename="../assets/main.qml" line="40"/>
        <source>Send Feedback</source>
        <translation>发送反馈</translation>
    </message>
    <message>
        <location filename="../assets/main.qml" line="43"/>
        <source>ToDo Feedback</source>
        <translation>备忘录反馈</translation>
    </message>
</context>
<context>
    <name>move</name>
    <message>
        <location filename="../assets/move.qml" line="9"/>
        <source>Move Item</source>
        <translation>移动项目</translation>
    </message>
    <message>
        <location filename="../assets/move.qml" line="12"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../assets/move.qml" line="91"/>
        <source>Move the item to</source>
        <translation>移动该项目到</translation>
    </message>
    <message>
        <location filename="../assets/move.qml" line="104"/>
        <source>Move the item?</source>
        <translation>移动该项目？</translation>
    </message>
    <message>
        <location filename="../assets/move.qml" line="129"/>
        <source>Main list</source>
        <translation>主列表</translation>
    </message>
</context>
<context>
    <name>settings</name>
    <message>
        <location filename="../assets/settings.qml" line="8"/>
        <source>Settings</source>
        <translation>设置</translation>
    </message>
    <message>
        <location filename="../assets/settings.qml" line="18"/>
        <source>Visual style</source>
        <translation>视觉主题</translation>
    </message>
    <message>
        <location filename="../assets/settings.qml" line="22"/>
        <source>Bright</source>
        <translation>亮色</translation>
    </message>
    <message>
        <location filename="../assets/settings.qml" line="27"/>
        <source>Dark</source>
        <translation>暗色</translation>
    </message>
    <message>
        <location filename="../assets/settings.qml" line="46"/>
        <source>Font Size</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/settings.qml" line="50"/>
        <source>Default</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/settings.qml" line="55"/>
        <source>Bigger</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/settings.qml" line="70"/>
        <source>List items count</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/settings.qml" line="74"/>
        <source>Done</source>
        <translation type="unfinished">完成</translation>
    </message>
    <message>
        <location filename="../assets/settings.qml" line="79"/>
        <source>Total</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/settings.qml" line="84"/>
        <source>Done/Total</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/settings.qml" line="89"/>
        <source>None</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/settings.qml" line="115"/>
        <source>Backup Directory</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
