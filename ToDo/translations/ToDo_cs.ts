<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="cs_CZ">
<context>
    <name>ListPage</name>
    <message>
        <location filename="../assets/ListPage.qml" line="104"/>
        <source>TODO</source>
        <translation>UDĚLAT</translation>
    </message>
    <message>
        <location filename="../assets/ListPage.qml" line="104"/>
        <source>DONE</source>
        <translation>HOTOVO</translation>
    </message>
    <message>
        <location filename="../assets/ListPage.qml" line="213"/>
        <source>Mark as Done</source>
        <translation>Přesunout do HOTOVO</translation>
    </message>
    <message>
        <location filename="../assets/ListPage.qml" line="213"/>
        <source>Mark as unfinished</source>
        <translation>Přesunout do UDĚLAT</translation>
    </message>
    <message>
        <location filename="../assets/ListPage.qml" line="226"/>
        <source>Edit item</source>
        <translation>Upravit</translation>
    </message>
    <message>
        <location filename="../assets/ListPage.qml" line="234"/>
        <source>Move item</source>
        <translation>Přesunout</translation>
    </message>
    <message>
        <location filename="../assets/ListPage.qml" line="277"/>
        <source>Delete</source>
        <translation>Smazat</translation>
    </message>
    <message>
        <location filename="../assets/ListPage.qml" line="278"/>
        <source>Delete the item</source>
        <translation>Smazat položku</translation>
    </message>
    <message>
        <location filename="../assets/ListPage.qml" line="371"/>
        <source>Done</source>
        <translation>Hotovo</translation>
    </message>
    <message>
        <location filename="../assets/ListPage.qml" line="371"/>
        <source>Add</source>
        <translation>Přidat</translation>
    </message>
    <message>
        <location filename="../assets/ListPage.qml" line="388"/>
        <source>Reorder</source>
        <translation>Uspořádat</translation>
    </message>
</context>
<context>
    <name>about</name>
    <message>
        <location filename="../assets/about.qml" line="45"/>
        <source>Build by</source>
        <translation>Vytvořil</translation>
    </message>
    <message>
        <location filename="../assets/about.qml" line="60"/>
        <source>Privacy policy (English)</source>
        <translation>Zásady ochrany soukromí</translation>
    </message>
    <message>
        <location filename="../assets/about.qml" line="70"/>
        <source>If you like the app, please do not forget to write a review.</source>
        <translation>Pokud se vám aplikace líbí, nezapomeňte ji doporučit.</translation>
    </message>
    <message>
        <location filename="../assets/about.qml" line="80"/>
        <source>THE AUTHOR DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE, INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS, IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, INDIRECT OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.</source>
        <translation>Používání aplikace ToDo je na vlastní zodpovědnost. Autor aplikace neodpovídá za jakoukoli škodu nebo jinou újmu, která by uživateli aplikace ToDo mohla vzniknout v důsledku jejího použití.</translation>
    </message>
</context>
<context>
    <name>activeFrame</name>
    <message>
        <location filename="../assets/activeFrame.qml" line="13"/>
        <source>TODO</source>
        <translation>UDĚLAT</translation>
    </message>
</context>
<context>
    <name>add</name>
    <message>
        <location filename="../assets/add.qml" line="8"/>
        <source>New Item</source>
        <translation>Nová položka</translation>
    </message>
    <message>
        <location filename="../assets/add.qml" line="8"/>
        <source>Edit item</source>
        <translation>Upravit</translation>
    </message>
    <message>
        <location filename="../assets/add.qml" line="11"/>
        <source>Cancel</source>
        <translation>Zrušit</translation>
    </message>
    <message>
        <location filename="../assets/add.qml" line="20"/>
        <source>Save</source>
        <translation>Uložit</translation>
    </message>
    <message>
        <location filename="../assets/add.qml" line="57"/>
        <source>Title</source>
        <translation>Text</translation>
    </message>
    <message>
        <location filename="../assets/add.qml" line="91"/>
        <source>List</source>
        <translation>Seznam</translation>
    </message>
    <message>
        <location filename="../assets/add.qml" line="122"/>
        <source>Reminder</source>
        <translation>Upozornění</translation>
    </message>
    <message>
        <location filename="../assets/add.qml" line="142"/>
        <source>Date and Time</source>
        <translation>Datum a čas</translation>
    </message>
</context>
<context>
    <name>main</name>
    <message>
        <location filename="../assets/main.qml" line="26"/>
        <source>About</source>
        <translation>O aplikaci</translation>
    </message>
    <message>
        <location filename="../assets/main.qml" line="40"/>
        <source>Send Feedback</source>
        <translation>Zpětná vazba</translation>
    </message>
    <message>
        <location filename="../assets/main.qml" line="43"/>
        <source>ToDo Feedback</source>
        <translation>Zpětná vazba na aplikaci ToDo</translation>
    </message>
</context>
<context>
    <name>move</name>
    <message>
        <location filename="../assets/move.qml" line="9"/>
        <source>Move Item</source>
        <translation>Přesunout</translation>
    </message>
    <message>
        <location filename="../assets/move.qml" line="12"/>
        <source>Cancel</source>
        <translation>Zrušit</translation>
    </message>
    <message>
        <location filename="../assets/move.qml" line="91"/>
        <source>Move the item to</source>
        <translation>Přesunout položku do</translation>
    </message>
    <message>
        <location filename="../assets/move.qml" line="104"/>
        <source>Move the item?</source>
        <translation>Přesunout položku?</translation>
    </message>
    <message>
        <location filename="../assets/move.qml" line="129"/>
        <source>Main list</source>
        <translation>Hlavní seznam</translation>
    </message>
</context>
<context>
    <name>settings</name>
    <message>
        <location filename="../assets/settings.qml" line="8"/>
        <source>Settings</source>
        <translation>Nastavení</translation>
    </message>
    <message>
        <location filename="../assets/settings.qml" line="18"/>
        <source>Visual style</source>
        <translation>Téma</translation>
    </message>
    <message>
        <location filename="../assets/settings.qml" line="22"/>
        <source>Bright</source>
        <translation>Světlé</translation>
    </message>
    <message>
        <location filename="../assets/settings.qml" line="27"/>
        <source>Dark</source>
        <translation>Tmavé</translation>
    </message>
    <message>
        <location filename="../assets/settings.qml" line="46"/>
        <source>Font Size</source>
        <translation>Velikost písmen</translation>
    </message>
    <message>
        <location filename="../assets/settings.qml" line="50"/>
        <source>Default</source>
        <translation>Výchozí</translation>
    </message>
    <message>
        <location filename="../assets/settings.qml" line="55"/>
        <source>Bigger</source>
        <translation>Větší</translation>
    </message>
    <message>
        <location filename="../assets/settings.qml" line="70"/>
        <source>List items count</source>
        <translation>Počet položek seznamu</translation>
    </message>
    <message>
        <location filename="../assets/settings.qml" line="74"/>
        <source>Done</source>
        <translation>Hotovo</translation>
    </message>
    <message>
        <location filename="../assets/settings.qml" line="79"/>
        <source>Total</source>
        <translation>Celkem</translation>
    </message>
    <message>
        <location filename="../assets/settings.qml" line="84"/>
        <source>Done/Total</source>
        <translation>Hotovo/Celkem</translation>
    </message>
    <message>
        <location filename="../assets/settings.qml" line="89"/>
        <source>None</source>
        <translation>Žádný</translation>
    </message>
    <message>
        <location filename="../assets/settings.qml" line="115"/>
        <source>Backup Directory</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
