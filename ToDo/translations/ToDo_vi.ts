<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="vi_VN">
<context>
    <name>ListPage</name>
    <message>
        <source>TODO</source>
        <translation>TODO</translation>
    </message>
    <message>
        <source>DONE</source>
        <translation>Hoàn thành</translation>
    </message>
    <message>
        <source>Mark as Done</source>
        <translation>Đánh dấu đã hoàn thành</translation>
    </message>
    <message>
        <source>Mark as unfinished</source>
        <translation>Đánh dấu chưa hoàn thành</translation>
    </message>
    <message>
        <source>Edit item</source>
        <translation>Chỉnh sửa mục</translation>
    </message>
    <message>
        <source>Delete</source>
        <translation>Xoá</translation>
    </message>
    <message>
        <source>Delete the item</source>
        <translation>Xoá mục</translation>
    </message>
    <message>
        <source>Done</source>
        <translation>Hoàn thành</translation>
    </message>
    <message>
        <source>Add</source>
        <translation>Thêm</translation>
    </message>
    <message>
        <source>Reorder</source>
        <translation>Sắp xếp lại</translation>
    </message>
    <message>
        <source>Move item</source>
        <translation>Di chuyển mục</translation>
    </message>
</context>
<context>
    <name>about</name>
    <message>
        <source>Build by</source>
        <translation>Xây dựng bởi</translation>
    </message>
    <message>
        <source>Privacy policy (English)</source>
        <translation>Chính sách riêng tư (English)</translation>
    </message>
    <message>
        <source>THE AUTHOR DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE, INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS, IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, INDIRECT OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.</source>
        <translation>TÁC GIẢ TỪ CHỐI TẤT CẢ BẢO ĐẢM ĐỐI VỚI PHẦN MỀM NÀY, BAO GỒM BẢO HÀNH TẤT CẢ KHẢ NĂNG THƯƠNG MẠI VÀ PHÙ HỢP. TÁC GIẢ KHÔNG CHỊU TRÁCH NHIỆM ĐẶC BIỆT, GIÁN TIẾP CHO BẤT KỲ HẬU QUẢ HOẶC BẤT KỲ THIỆT HẠI NÀO DO MẤT DỮ LIỆU HOẶC LỢI NHUẬN, CHO DÙ TRONG MỘT HÀNH ĐỘNG CỦA HỢP ĐỒNG, DO SƠ SUẤT HAY HÀNH ĐỘNG CÓ HẠI KHÁC, PHÁT SINH HOẶC KẾT HỢP VỚI VIỆC SỬ DỤNG HOẶC HIỆU SUẤT CỦA PHẦN MỀM NÀY.</translation>
    </message>
    <message>
        <source>If you like the app, please do not forget to write a review.</source>
        <translation>Nếu bạn thích ứng dụng, đừng quên viết một nhận xét.</translation>
    </message>
</context>
<context>
    <name>activeFrame</name>
    <message>
        <source>TODO</source>
        <translation>TODO</translation>
    </message>
</context>
<context>
    <name>add</name>
    <message>
        <source></source>
        <translation></translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Huỷ bỏ</translation>
    </message>
    <message>
        <source>Title</source>
        <translation>Tiêu đề</translation>
    </message>
    <message>
        <source>New Item</source>
        <translation>Mục mới</translation>
    </message>
    <message>
        <source>Edit item</source>
        <translation>Chỉnh sửa mục</translation>
    </message>
    <message>
        <source>Save</source>
        <translation>Lưu</translation>
    </message>
    <message>
        <source>List</source>
        <translation>Danh sách</translation>
    </message>
    <message>
        <source>Reminder</source>
        <translation>Nhắc nhở</translation>
    </message>
    <message>
        <source>Date and Time</source>
        <translation>Ngày và Giờ</translation>
    </message>
</context>
<context>
    <name>main</name>
    <message>
        <source>About</source>
        <translation>Giới thiệu</translation>
    </message>
    <message>
        <source>Send Feedback</source>
        <translation>Gửi phản hồi</translation>
    </message>
    <message>
        <source>ToDo Feedback</source>
        <translation>Phản hồi về ToDo</translation>
    </message>
</context>
<context>
    <name>move</name>
    <message>
        <source>Move Item</source>
        <translation>Di chuyển mục</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Huỷ bỏ</translation>
    </message>
    <message>
        <source>Move the item to</source>
        <translation>Di chuyển mục tới</translation>
    </message>
    <message>
        <source>Move the item?</source>
        <translation>Di chuyển mục?</translation>
    </message>
    <message>
        <source>Main list</source>
        <translation>Danh sách chính</translation>
    </message>
</context>
<context>
    <name>settings</name>
    <message>
        <source>Settings</source>
        <translation>Cài đặt</translation>
    </message>
    <message>
        <source>Visual style</source>
        <translation>Giao diện</translation>
    </message>
    <message>
        <source>Bright</source>
        <translation>Sáng</translation>
    </message>
    <message>
        <source>Dark</source>
        <translation>Tối</translation>
    </message>
    <message>
        <source>List items count</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Done</source>
        <translation type="unfinished">Hoàn thành</translation>
    </message>
    <message>
        <source>Total</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Done/Total</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>None</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Backup Directory</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Font Size</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Default</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Bigger</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
