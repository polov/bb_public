<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0">
<context>
    <name>ListPage</name>
    <message>
        <source>TODO</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>DONE</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Mark as Done</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Mark as unfinished</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Edit item</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Delete</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Delete the item</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Done</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Add</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Reorder</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Move item</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>about</name>
    <message>
        <source>Build by</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Privacy policy (English)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>THE AUTHOR DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE, INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS, IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, INDIRECT OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>If you like the app, please do not forget to write a review.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>activeFrame</name>
    <message>
        <source>TODO</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>add</name>
    <message>
        <source></source>
        <translation></translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Title</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>New Item</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Edit item</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Save</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>List</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Reminder</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Date and Time</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>main</name>
    <message>
        <source>About</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Send Feedback</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>ToDo Feedback</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>move</name>
    <message>
        <source>Move Item</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Move the item to</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Move the item?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Main list</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>settings</name>
    <message>
        <source>Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Visual style</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Bright</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Dark</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>List items count</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Done</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Total</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Done/Total</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>None</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Backup Directory</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Font Size</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Default</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Bigger</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
