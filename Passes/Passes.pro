APP_NAME = Passes

CONFIG += qt warn_on cascades10

LIBS += -lbb -lbbsystem -lbbplatform -larchive -lbbdata -lbbdevice -lbbcascadesmultimedia -lQtLocationSubset

include(config.pri)
