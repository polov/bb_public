import bb.cascades 1.3

Page {
    accessibility.name: qsTr("barcode view")
    Container {
        layout: DockLayout {
            
        }
        Label {
            verticalAlignment: VerticalAlignment.Top
            horizontalAlignment: HorizontalAlignment.Center
            text: qsTr("Pinch to zoom the barcode")
            multiline: true
            textStyle.textAlign: TextAlign.Center
        }
        ScrollView {
            accessibility.name: qsTr("barcode view")
            scrollViewProperties.scrollMode: ScrollMode.Both
            scrollRole: ScrollRole.Main
            scrollViewProperties.minContentScale: 1.0
            scrollViewProperties.maxContentScale: 8.0
            scrollViewProperties.pinchToZoomEnabled: true
            scrollViewProperties.overScrollEffectMode: OverScrollEffectMode.Default

            verticalAlignment: VerticalAlignment.Center
            horizontalAlignment: HorizontalAlignment.Center

            ImageView {
                horizontalAlignment: HorizontalAlignment.Center
                id: barcodeImageView
                scalingMethod: ScalingMethod.AspectFit
            }
        }
    }
    onIdentificatorChanged: {
        var pass = controller.getPassData(identificator);
        barcodeImageView.image = uiHelper.getBarcodeImage(pass.barcodeFormat, pass.barcodeMessage);
        if (pass.barcodeFormat == "PKBarcodeFormatQR" || pass.barcodeFormat == "PKBarcodeFormatAztec") {
            barcodeImageView.preferredHeight = ui.du(32.0);
        } else {
            barcodeImageView.preferredWidth = ui.du(60.0);
        }
    }

    property string identificator
}
