import bb.cascades 1.4

Sheet {
    id: colorPickerSheet
    content: Page {
        titleBar: TitleBar {
            title: qsTr("Color") + Retranslate.onLanguageChanged
            dismissAction: ActionItem {
                title: qsTr("Cancel") + Retranslate.onLanguageChanged
                onTriggered: {
                    if(colorFor == "bg") {
                        bgColorValue = colorInitial;
                    } else if(colorFor == "fg") {
                        fgColorValue = colorInitial;
                    } else if(colorFor == "lbl") {
                        lblColorValue = colorInitial;
                    }
                    colorPickerSheet.close();
                    colorPickerSheet.destroy();
                }
            }
            acceptAction: ActionItem {
                title: qsTr("Choose") + Retranslate.onLanguageChanged
                onTriggered: {
                    colorPickerSheet.close();
                    colorPickerSheet.destroy();
                }
            }
        }
        content: Container {
            Container {
                id: chosenColorContainer
                horizontalAlignment: HorizontalAlignment.Center
                topPadding: ui.du(4.0)
                bottomPadding: ui.du(4.0)
                layout: StackLayout {
                    orientation: LayoutOrientation.LeftToRight
                }
                Container {
                    verticalAlignment: VerticalAlignment.Center
                    Label {
                        text: qsTr("Selected color") + Retranslate.onLanguageChanged
                        textStyle {
                            fontSize: FontSize.Medium
                            fontWeight: FontWeight.Bold
                        }
                    }                    
                }
                Container {
                    leftMargin: ui.du(4.0)
                    topPadding: ui.du(0.25)
                    bottomPadding: ui.du(0.25)
                    leftPadding: ui.du(0.25)
                    rightPadding: ui.du(0.25)
                    background: Color.create("#969696")
                    Container {
                        id: chosenColorPreviewContainer
                        preferredHeight: ui.du(12.0)
                        preferredWidth: ui.du(12.0)
                    }   
                }                
            }
            Container {
                id: colorListViewContainer
                ListView {
                    id: colorListView
                    dataModel: dataModel
                    layout: FlowListLayout {
                        headerMode: ListHeaderMode.None
                    }
                    listItemComponents: [
                        ListItemComponent {
                            id: listItemComponent
                            type: ""
                            content: Container {
                                background: Color.create(ListItemData)
                                preferredWidth: ui.du(12.0)
                                preferredHeight: ui.du(12.0)
                                leftMargin: ui.du(0.25)
                                bottomMargin: ui.du(0.25)
                            }
                        }
                    ]
                    onTriggered: {
                        var chosenItem = dataModel.data(indexPath);
                        colorPicked = chosenItem;
                    }
                }
            }
        }
        attachedObjects: [
            ArrayDataModel {
                id: dataModel
            }
        ]
        onCreationCompleted: {
            dataModel.insert(0, "#ffffff");
            dataModel.insert(1, "#000000");
            dataModel.insert(2, "#b04939");
            dataModel.insert(3, "#92102f");
            dataModel.insert(4, "#7b0533");
            dataModel.insert(5, "#621348");
            dataModel.insert(6, "#4c1a60");
            dataModel.insert(7, "#630d60");
            dataModel.insert(8, "#a22c8d");
            dataModel.insert(9, "#2468b0");
            dataModel.insert(10, "#117e8e");
            dataModel.insert(11, "#0a5970");
            dataModel.insert(12, "#06466d");
            dataModel.insert(13, "#033059");
            dataModel.insert(14, "#b19dc9");
            dataModel.insert(15, "#a1589f");
            dataModel.insert(16, "#199bd8");
            dataModel.insert(17, "#90d8f7");
            dataModel.insert(18, "#a6b5d2");
            dataModel.insert(19, "#aac6d2");
            dataModel.insert(20, "#90bcc0");
            dataModel.insert(21, "#5cbcc1");
            dataModel.insert(22, "#66ada1");
            dataModel.insert(23, "#9cd4c1");
            dataModel.insert(24, "#c4e1c2");
            dataModel.insert(25, "#d0e39e");
            dataModel.insert(26, "#b2d366");
            dataModel.insert(27, "#6e934b");
            dataModel.insert(28, "#118257");
            dataModel.insert(29, "#0b644e");
            dataModel.insert(30, "#fff7a0");
            dataModel.insert(31, "#fef135");
            dataModel.insert(32, "#ffd431");
            dataModel.insert(33, "#fbaf32");
            dataModel.insert(34, "#fed099");
            dataModel.insert(35, "#f9b59e");
            dataModel.insert(36, "#f49086");
            dataModel.insert(37, "#ef698f");
            dataModel.insert(38, "#b81c6a");
            dataModel.insert(39, "#eb178c");
            dataModel.insert(40, "#eb1e56");
            dataModel.insert(41, "#eb212e");
            dataModel.insert(42, "#ee594d");
            dataModel.insert(43, "#f48656");
            dataModel.insert(44, "#efa8b3");
            dataModel.insert(45, "#fcd6dd");
            dataModel.insert(46, "#fcd6d1");
            dataModel.insert(47, "#f9f1de");
            dataModel.insert(48, "#fcf8d9");
            dataModel.insert(49, "#ecd4b6");
            dataModel.insert(50, "#c8b098");
            dataModel.insert(51, "#a4b5a3");
            dataModel.insert(52, "#a2a494");
            dataModel.insert(53, "#7c7d69");
            dataModel.insert(54, "#63605b");
            dataModel.insert(55, "#4a3837");
            dataModel.insert(56, "#6d473e");
            dataModel.insert(57, "#997d60");
            dataModel.insert(58, "#231f20");
            dataModel.insert(59, "#59595b");
            dataModel.insert(60, "#95969a");
            dataModel.insert(61, "#d2d3d5");
            dataModel.insert(62, "#e5ded4");
            dataModel.insert(63, "#d3cfc9");
            dataModel.insert(64, "#938b89");
        }
    }
    onColorPickedChanged: {
        chosenColorPreviewContainer.background = Color.create(colorPicked);
        if(colorFor == "bg") {
            bgColorValue = colorPicked;
        } else if(colorFor == "fg") {
            fgColorValue = colorPicked;
        } else if(colorFor == "lbl") {
            lblColorValue = colorPicked;
        }
    }
    property string colorPicked
    property string colorFor
    property string colorInitial
}
