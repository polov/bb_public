import bb.cascades 1.3

Container {
    id: primaryField
    accessibility.name: labelText + " - " + valueText
    horizontalAlignment: (align == "left") ? HorizontalAlignment.Left : HorizontalAlignment.Right
    Container {
        horizontalAlignment: (align == "left") ? HorizontalAlignment.Left : HorizontalAlignment.Right
        visible: (labelLabel.text != "")
        accessibility.name: ""
        accessibility.description: ""
        accessibility.labelledBy: primaryField
        Label {
            id: labelLabel
            text: labelText
            textStyle {
                color: Color.create(labelColor)
                fontSize: FontSize.XSmall
                fontWeight: (fontWeight=="bold") ? FontWeight.W500 : FontWeight.Default
            }
            accessibility.name: ""
            accessibility.description: ""
            accessibility.labelledBy: primaryField
        }
    }
    Container {
        horizontalAlignment: (align == "left") ? HorizontalAlignment.Left : HorizontalAlignment.Right
        visible: (valueLabel.text != "")
        accessibility.name: ""
        accessibility.description: ""
        accessibility.labelledBy: primaryField
        Label {
            id: valueLabel
            text: valueText
            textStyle {
                color: Color.create(foregroundColor)
                fontSize: (fontSize=="large") ? FontSize.XXLarge : FontSize.XLarge
                fontWeight: (fontWeight=="bold") ? FontWeight.W500 : FontWeight.Default
            }
            accessibility.name: ""
            accessibility.description: ""
            accessibility.labelledBy: primaryField
        }
    }
    
    property string align
    property string fontSize: "large"
    property string fontWeight: "normal"
    property string labelText
    property string valueText
    property string labelColor
    property string foregroundColor

}
