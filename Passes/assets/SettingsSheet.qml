import bb.cascades 1.4

Sheet {
    id: settingsSheet
    Page {
        id: settingsPage
        titleBar: TitleBar {
            title: qsTr("Settings") + Retranslate.onLanguageChanged
        }
        content: ScrollView {
            Container {
                leftPadding: ui.du(1.5)
                rightPadding: ui.du(1.5)
                Container {
                    layout: StackLayout {
                        orientation: LayoutOrientation.LeftToRight
                    }
                    Container {
                        topPadding: ui.du(2.5)
                        bottomPadding: ui.du(1.5)
                        layoutProperties: StackLayoutProperties {
                            spaceQuota: 1
                        }
                        layout: DockLayout {

                        }
                        horizontalAlignment: HorizontalAlignment.Fill
                        Label {
                            text: qsTr("Use location services") + Retranslate.onLanguageChanged
                            textStyle.fontSize: FontSize.Medium
                            verticalAlignment: VerticalAlignment.Center
                        }
                        ToggleButton {
                            id: geolocationEnabledToggle
                            checked: geoHelper.geolocationEnabled
                            onCheckedChanged: {
                                geoHelper.geolocationEnabled = geolocationEnabledToggle.checked
                                if (! geolocationEnabledToggle.checked) {
                                    geolocationStatusContainer.visible = false;
                                }
                            }
                            horizontalAlignment: HorizontalAlignment.Right
                            accessibility.name: "Location services toggle"
                            //enabled: false
                        }
                    }
                }
                Container {
                    Label {
                        text: qsTr("Constantly check current location and send notification when you are near any location stored in one of your pass.") + Retranslate.onLanguageChanged
                        textStyle.fontSize: FontSize.Default
                        multiline: true
                    }
                }
                Container {
                    id: geolocationStatusContainer
                    visible: false
                    topPadding: ui.du(1.5)
                    bottomPadding: ui.du(1.5)
                    Container {
                        Label {
                            multiline: true
                            text: qsTr("There are problems with using location services. Please check permissions.") + Retranslate.onLanguageChanged
                        }
                    }
                    Container {
                        topMargin: ui.du(0.5)
                        topPadding: ui.du(2.0)
                        bottomPadding: ui.du(2.0)
                        navigation.focusPolicy: NavigationFocusPolicy.Focusable
                        Label {
                            textFormat: TextFormat.Html
                            text: qsTr("Application Permissions") + Retranslate.onLanguageChanged
                            textStyle.color: Color.create("#ff1872a0")
                            textStyle.fontSize: FontSize.Medium
                        }
                        gestureHandlers: [
                            TapHandler {
                                onTapped: {
                                    controller.invokePermissionsSettings();
                                }
                            }
                        ]
                    }
                    Container {
                        topPadding: ui.du(2.0)
                        bottomPadding: ui.du(2.0)
                        navigation.focusPolicy: NavigationFocusPolicy.Focusable
                        Label {
                            textFormat: TextFormat.Html
                            text: qsTr("Location Services") + Retranslate.onLanguageChanged
                            textStyle.color: Color.create("#ff1872a0")
                            textStyle.fontSize: FontSize.Medium
                        }
                        gestureHandlers: [
                            TapHandler {
                                onTapped: {
                                    controller.invokeLocationSettings();
                                }
                            }
                        ]
                    }
                }
                Divider {
                    accessibility.name: "Divider"
                }
            }
            accessibility.name: "Settings sheet content"
        }
        actions: [
            ActionItem {
                title: qsTr("Done") + Retranslate.onLanguageChanged
                ActionBar.placement: ActionBarPlacement.OnBar
                imageSource: "asset:///images/ic_done.png"
                onTriggered: {
                    settingsSheet.close();
                }
            }
        ]
        function onGeolocationStatusChanged() {
            geolocationStatusContainer.visible = (geoHelper.geolocationStatus == 0);
        }
        onCreationCompleted: {
            geoHelper.geolocationStatusChanged.connect(settingsPage.onGeolocationStatusChanged);
            console.log("settings connected to geoHelper.geolocationStatusChanged signal");
            onGeolocationStatusChanged();
            settingsPage.onGeolocationStatusChanged();
        }
    }
}