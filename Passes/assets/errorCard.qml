import bb.cascades 1.3

NavigationPane {
    Page {
        Container {
            layout: DockLayout {
            }
            Container {
                horizontalAlignment: HorizontalAlignment.Center
                verticalAlignment: VerticalAlignment.Center
                Container {
                    horizontalAlignment: HorizontalAlignment.Center
                    ImageView {
                        imageSource: "asset:///images/ic_invalid_pass.png"
                    }
                }
                Container {
                    horizontalAlignment: HorizontalAlignment.Center
                    Label {
                        text: "Reading the pass failed."
                    }                       
                }
            }
        }
    }
}
