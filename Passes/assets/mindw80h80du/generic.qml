import bb.cascades 1.3

Page {
    content: ScrollView {
        accessibility.name: qsTr("pass") + Retranslate.onLanguageChanged
        Container {
            id: contentContainer
            Container {
                id: headerContainer
                layout: StackLayout {
                    orientation: LayoutOrientation.LeftToRight
                }
                minHeight: ui.du(12.0)
                topPadding: ui.du(1.5)
                bottomPadding: ui.du(2.5)
                leftPadding: ui.du(2.5)
                rightPadding: ui.du(2.5)
                Container {
                    id: headerImageContainer
                    verticalAlignment: VerticalAlignment.Center
                    ImageView {
                        id: logoImageView
                        maxHeight: ui.du(9.5)
                        scalingMethod: ScalingMethod.AspectFit
                    }
                }
                Container {
                    id: logoTextContainer
                    layoutProperties: StackLayoutProperties {
                        spaceQuota: 1
                    }
                    verticalAlignment: VerticalAlignment.Center
                    leftPadding: ui.du(2.5)
                    Label {
                        id: logoText
                        textStyle {
                            fontSize: FontSize.Large
                            fontWeight: FontWeight.W500
                        }
                    }
                }
                Container {
                    id: headerFieldsContainer
                    verticalAlignment: VerticalAlignment.Center
                    layout: StackLayout {
                        orientation: LayoutOrientation.LeftToRight
                    }
                    HeaderField {
                        id: headerField0
                    }
                    HeaderField {
                        id: headerField1
                    }
                    HeaderField {
                        id: headerField2
                    }
                    HeaderField {
                        id: headerField3
                    }
                }
            }
            Container {
                id: primThumbnailContentContainer
                layout: StackLayout {
                    orientation: LayoutOrientation.LeftToRight
                }
                topPadding: ui.du(4.5)
                leftPadding: ui.du(2.5)
                rightPadding: ui.du(2.5)
                Container {
                    id: primContentContainer
                    layoutProperties: StackLayoutProperties {
                        spaceQuota: 1
                    }
                    Container {
                        id: primaryFieldsContainer
                        PrimaryField {
                            id: primaryField0
                            fontSize: "smaller"
                            fontWeight: "bold"
                        }
                    }
                }
                Container {
                    id: thumbnailContainer
                    verticalAlignment: VerticalAlignment.Center
                    ImageView {
                        id: thumbnailImageView
                        minWidth: ui.du(14.0)
                        preferredWidth: ui.du(14.0)
                        minHeight: ui.du(20.0)
                        scalingMethod: ScalingMethod.AspectFit
                    }
                }
            }
            Container {
                id: secondaryFieldsContainer
                layout: StackLayout {
                    orientation: LayoutOrientation.LeftToRight
                }
                topPadding: ui.du(5.0)
                leftPadding: ui.du(2.5)
                rightPadding: ui.du(2.5)
                DefaultField {
                    id: secondaryField0
                }
                DefaultField {
                    id: secondaryField1
                }
                DefaultField {
                    id: secondaryField2
                }
                DefaultField {
                    id: secondaryField3
                }
            }
            Container {
                id: auxiliaryFieldsContainer
                layout: StackLayout {
                    orientation: LayoutOrientation.LeftToRight
                }
                topPadding: ui.du(5.0)
                leftPadding: ui.du(2.5)
                rightPadding: ui.du(2.5)
                DefaultField {
                    id: auxiliaryField0
                }
                DefaultField {
                    id: auxiliaryField1
                }
                DefaultField {
                    id: auxiliaryField2
                }
                DefaultField {
                    id: auxiliaryField3
                }
            }
            Container {
                id: fillerContainer
                visible: false
            }
            Container {
                id: barcodeContainer
                background: Color.White
                topMargin: ui.du(1.5)
                bottomMargin: ui.du(1.5)
                horizontalAlignment: HorizontalAlignment.Center
                visible: false
                Container {
                    Container {
                        horizontalAlignment: HorizontalAlignment.Center
                        id: barcodeImageViewContainer
                        ImageView {
                            id: barcodeImageView
                            scalingMethod: ScalingMethod.AspectFit
                            accessibility.name: qsTr("barcode") + Retranslate.onLanguageChanged
                        }
                    }
                    Container {
                        horizontalAlignment: HorizontalAlignment.Center
                        id: barcodeAltTextContainer
                        Label {
                            id: barcodeAltText
                            accessibility.name: qsTr("barcode alternate text: ") + barcodeAltText.text + Retranslate.onLanguageChanged
                            textStyle.color: Color.Black
                        }
                    }
                    gestureHandlers: TapHandler {
                        onTapped: {
                            openBarcodePage();
                        }
                    }
                }
            }
            Container {
                minHeight: ui.du(1.0)
            }
            
            function setContainersSizeVisibility(orientation) {
                contentContainer.preferredHeight = ((orientation) ? displayInfo.pixelSize.width : displayInfo.pixelSize.height) - ui.du(12.0);
                //fillerContainer.visible = (orientation) ? false : true;
            }
            onCreationCompleted: {
                setContainersSizeVisibility(orientationHandler.orientation);
            }
            attachedObjects: [
                OrientationHandler {
                    id: orientationHandler
                    onOrientationChanged: {
                        contentContainer.setContainersSizeVisibility(orientation);
                    }
                }
            ]
        }
        attachedObjects: [
            ImagePaintDefinition {
                id: back
                repeatPattern: RepeatPattern.XY
            }
        ]
    }
    attachedObjects: [
        ComponentDefinition {
            id: backFieldsPage
            source: "backField.qml"
        },
        ComponentDefinition {
            id: barcodePage
            source: "barcodePage.qml"
        }
    ]
    actions: [
        ActionItem {
            title: qsTr("Barcode page") + Retranslate.onLanguageChanged
            ActionBar.placement: ActionBarPlacement.InOverflow
            imageSource: "asset:///images/ic_doctype_barcode.png"
            onTriggered: {
                openBarcodePage();
            }
            shortcuts: [
                Shortcut {
                    key: qsTr("p")
                }
            ]
        },
        ActionItem {
            id: contentSwitchingAction
            ActionBar.placement: ActionBarPlacement.OnBar
            title: (contentSwitch) ? qsTr("Barcode") + Retranslate.onLanguageChanged : qsTr("Content") + Retranslate.onLanguageChanged
            imageSource: "asset:///images/ic_barcode.png"
            onTriggered: {
                if (contentSwitch) {
                    contentSwitchingAction.title = qsTr("Content") + Retranslate.onLanguageChanged;
                    contentSwitchingAction.imageSource = "asset:///images/ic_content.png";
                    contentSwitch = false;
                    primThumbnailContentContainer.visible = false;
                    auxiliaryFieldsContainer.visible = false;
                    secondaryFieldsContainer.visible = false;
                    fillerContainer.visible = true;
                    barcodeContainer.visible = true;
                } else {
                    contentSwitchingAction.title = qsTr("Barcode") + Retranslate.onLanguageChanged;
                    contentSwitchingAction.imageSource = "asset:///images/ic_barcode.png";
                    contentSwitch = true;
                    primThumbnailContentContainer.visible = true;
                    auxiliaryFieldsContainer.visible = true;
                    secondaryFieldsContainer.visible = true;
                    fillerContainer.visible = false;
                    barcodeContainer.visible = false;
                }
            }
            shortcuts: [
                Shortcut {
                    key: qsTr("r")
                }
            ]
        },
        ActionItem {
            title: qsTr("Info") + Retranslate.onLanguageChanged
            ActionBar.placement: ActionBarPlacement.OnBar
            imageSource: "asset:///images/ic_info.png"
            onTriggered: {
                var backFieldsPageContent = backFieldsPage.createObject();
                backFieldsPageContent.backFieldsPageContentText = backFieldsPageContentText
                navigationPane.push(backFieldsPageContent);
            }
            shortcuts: [
                Shortcut {
                    key: qsTr("d")
                }
            ]
        },
        ActionItem {
            title: qsTr("Brightness") + Retranslate.onLanguageChanged
            imageSource: "asset:///images/ic_display.png"
            ActionBar.placement: ActionBarPlacement.InOverflow
            onTriggered: {
                controller.invokeDisplaySettings();
            }
        }
    ]
    
    function openBarcodePage() {
        var barcodePageContent = barcodePage.createObject();
        barcodePageContent.identificator = identificator;
        navigationPane.push(barcodePageContent);
    }
    
    onIdentificatorChanged: {
        var pass = controller.getPassData(identificator);
        
        contentContainer.background = uiHelper.getColor(pass.backgroundColor);
        logoText.textStyle.color = uiHelper.getColor(pass.foregroundColor);
        
        logoImageView.imageSource = "file://" + uiHelper.getImagePath(identificator, "logo");
        logoImageView.accessibility.name = pass.organizationName;
        
        logoText.text = pass.logoText;
        
        headerField0.foregroundColor = uiHelper.getColorString(pass.foregroundColor);
        headerField0.labelColor = uiHelper.getColorString(pass.labelColor);
        headerField0.labelText = uiHelper.getFieldLabel(pass.headerFields, 0);
        headerField0.valueText = uiHelper.getFieldValue(pass.headerFields, 0);
        headerField0.organizationName = pass.organizationName;
        
        headerField1.foregroundColor = uiHelper.getColorString(pass.foregroundColor);
        headerField1.labelColor = uiHelper.getColorString(pass.labelColor);
        headerField1.labelText = uiHelper.getFieldLabel(pass.headerFields, 1);
        headerField1.valueText = uiHelper.getFieldValue(pass.headerFields, 1);
        headerField1.organizationName = pass.organizationName;
        
        headerField2.foregroundColor = uiHelper.getColorString(pass.foregroundColor);
        headerField2.labelColor = uiHelper.getColorString(pass.labelColor);
        headerField2.labelText = uiHelper.getFieldLabel(pass.headerFields, 2);
        headerField2.valueText = uiHelper.getFieldValue(pass.headerFields, 2);
        headerField2.organizationName = pass.organizationName;
        
        headerField3.foregroundColor = uiHelper.getColorString(pass.foregroundColor);
        headerField3.labelColor = uiHelper.getColorString(pass.labelColor);
        headerField3.labelText = uiHelper.getFieldLabel(pass.headerFields, 3);
        headerField3.valueText = uiHelper.getFieldValue(pass.headerFields, 3);
        headerField3.organizationName = pass.organizationName;
        
        primaryField0.foregroundColor = uiHelper.getColorString(pass.foregroundColor);
        primaryField0.labelColor = uiHelper.getColorString(pass.labelColor);
        primaryField0.labelText = uiHelper.getFieldLabel(pass.primaryFields, 0);
        primaryField0.valueText = uiHelper.getFieldValue(pass.primaryFields, 0);
        primaryField0.align = "left";
        
        secondaryField0.foregroundColor = uiHelper.getColorString(pass.foregroundColor);
        secondaryField0.labelColor = uiHelper.getColorString(pass.labelColor);
        secondaryField0.labelText = uiHelper.getFieldLabel(pass.secondaryFields, 0);
        secondaryField0.valueText = uiHelper.getFieldValue(pass.secondaryFields, 0);
        
        secondaryField1.foregroundColor = uiHelper.getColorString(pass.foregroundColor);
        secondaryField1.labelColor = uiHelper.getColorString(pass.labelColor);
        secondaryField1.labelText = uiHelper.getFieldLabel(pass.secondaryFields, 1);
        secondaryField1.valueText = uiHelper.getFieldValue(pass.secondaryFields, 1);
        
        secondaryField2.foregroundColor = uiHelper.getColorString(pass.foregroundColor);
        secondaryField2.labelColor = uiHelper.getColorString(pass.labelColor);
        secondaryField2.labelText = uiHelper.getFieldLabel(pass.secondaryFields, 2);
        secondaryField2.valueText = uiHelper.getFieldValue(pass.secondaryFields, 2);
        
        secondaryField3.foregroundColor = uiHelper.getColorString(pass.foregroundColor);
        secondaryField3.labelColor = uiHelper.getColorString(pass.labelColor);
        secondaryField3.labelText = uiHelper.getFieldLabel(pass.secondaryFields, 3);
        secondaryField3.valueText = uiHelper.getFieldValue(pass.secondaryFields, 3);
        
        auxiliaryField0.foregroundColor = uiHelper.getColorString(pass.foregroundColor);
        auxiliaryField0.labelColor = uiHelper.getColorString(pass.labelColor);
        auxiliaryField0.labelText = uiHelper.getFieldLabel(pass.auxiliaryFields, 0);
        auxiliaryField0.valueText = uiHelper.getFieldValue(pass.auxiliaryFields, 0);
        
        auxiliaryField1.foregroundColor = uiHelper.getColorString(pass.foregroundColor);
        auxiliaryField1.labelColor = uiHelper.getColorString(pass.labelColor);
        auxiliaryField1.labelText = uiHelper.getFieldLabel(pass.auxiliaryFields, 1);
        auxiliaryField1.valueText = uiHelper.getFieldValue(pass.auxiliaryFields, 1);
        
        auxiliaryField2.foregroundColor = uiHelper.getColorString(pass.foregroundColor);
        auxiliaryField2.labelColor = uiHelper.getColorString(pass.labelColor);
        auxiliaryField2.labelText = uiHelper.getFieldLabel(pass.auxiliaryFields, 2);
        auxiliaryField2.valueText = uiHelper.getFieldValue(pass.auxiliaryFields, 2);
        
        auxiliaryField3.foregroundColor = uiHelper.getColorString(pass.foregroundColor);
        auxiliaryField3.labelColor = uiHelper.getColorString(pass.labelColor);
        auxiliaryField3.labelText = uiHelper.getFieldLabel(pass.auxiliaryFields, 3);
        auxiliaryField3.valueText = uiHelper.getFieldValue(pass.auxiliaryFields, 3);
        
        thumbnailImageView.imageSource = "file://" + uiHelper.getImagePath(identificator, "thumbnail");
        
        if (pass.barcodeMessage == "" || pass.barcodeMessage == null) {
            barcodeImageViewContainer.visible = false;
            contentSwitchingAction.enabled = false;
        } else {
            barcodeImageView.image = uiHelper.getBarcodeImage(pass.barcodeFormat, pass.barcodeMessage);
            if (pass.barcodeFormat == "PKBarcodeFormatQR" || pass.barcodeFormat == "PKBarcodeFormatAztec") {
                barcodeImageView.preferredHeight = ui.du(32.0);
            } else {
                barcodeImageView.preferredWidth = ui.du(60.0);
            }
        }
        if (pass.barcodeAltText == "" || pass.barcodeAltText == null) {
            barcodeAltTextContainer.visible = false;
        } else {
            barcodeAltText.text = pass.barcodeAltText;
        }
        fillerContainer.minHeight = ui.du(15.0);
        
        backFieldsPageContentText = uiHelper.getBackFieldContentHtml(pass.backFields);
        
        var backgroundImagePath = uiHelper.getImagePath(identificator, "blured_background");
        if (backgroundImagePath != "") {
            back.imageSource = "file://" + backgroundImagePath;
            contentContainer.background = back.imagePaint;
        }
    }
    
    property string identificator
    property string backFieldsPageContentText
    property bool contentSwitch: true
}
