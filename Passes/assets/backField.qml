import bb.cascades 1.3

Page {
    id: backFieldsPage
    ScrollView {
        id: backFieldsPageScrollView
        WebView {
            id: webView
            html: backFieldsPageContentText
            settings.viewport: {
                "width": "device-width",
                "initial-scale": 1.0
            }
        }
    }
    property string backFieldsPageContentText
}
