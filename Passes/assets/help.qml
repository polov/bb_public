import bb.cascades 1.4

Page {
    ScrollView {
        content: Container {
            topPadding: ui.du(2.5)
            bottomPadding: ui.du(2.5)
            leftPadding: ui.du(2.5)
            rightPadding: ui.du(2.5)
            Container {
                Label {
                    id: helpTitleContent
                    textStyle {
                        fontSize: FontSize.Large
                        fontWeight: FontWeight.W500
                    }
                    multiline: true
                }
            }
            Container {
                layout: StackLayout {
                    orientation: LayoutOrientation.LeftToRight
                }
                Container {
                    topPadding: ui.du(2.5)
                    layoutProperties: StackLayoutProperties {
                        spaceQuota: 1
                    }
                    ImageView {
                        id: helpImageView
                        horizontalAlignment: HorizontalAlignment.Center
                        preferredHeight: ui.du(40.0)
                    }
                }
            }
            Container {
                topPadding: ui.du(2.5)
                Label {
                    id: helpTextContent
                    textStyle {
                        fontSize: FontSize.Default
                    }
                    multiline: true
                    textFormat: TextFormat.Auto

                }
            }
        }
    }
    property alias title: helpTitleContent.text
    property alias imageSource: helpImageView.imageSource
    property alias imageVisible: helpImageView.visible
    property alias textContent: helpTextContent.text
}
