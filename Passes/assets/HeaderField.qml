import bb.cascades 1.3

Container {
    id: headerField
    accessibility.name: organizationName + " " + labelText + " " + valueText
    Container {
        horizontalAlignment: HorizontalAlignment.Right
        visible: (labelLabel.text != "")
        accessibility.labelledBy: headerField
        Label {
            id: labelLabel
            text: labelText
            accessibility.name: ""
            accessibility.description: ""
            accessibility.labelledBy: headerField
            textStyle {
                color: Color.create(labelColor)
                fontSize: FontSize.XSmall
            }
            onTextChanged: {
                headerField.leftPadding = ui.du(1.5)
            }
        }
    }
    Container {
        horizontalAlignment: HorizontalAlignment.Right
        visible: (valueLabel.text != "")
        accessibility.labelledBy: headerField
        Label {
            id: valueLabel
            text: valueText
            accessibility.name: ""
            accessibility.description: ""
            accessibility.labelledBy: headerField
            textStyle {
                color: Color.create(foregroundColor)
                fontSize: FontSize.Large
            }
            onTextChanged: {
                headerField.leftPadding = ui.du(1.5)
            }
        }
    }
    
    property string labelText
    property string valueText
    property string labelColor
    property string foregroundColor
    property string organizationName: ""

}
