import bb.cascades 1.3
import bb.cascades.pickers 1.0
import bb.device 1.3
import bb.displayInfo 1.0
import bb.platform 1.3
import bb.system 1.2
import com.polov 1.0

NavigationPane {
    id: navigationPane
    Menu.definition: MenuDefinition {
        actions: [
            ActionItem {
                title: qsTr("About") + Retranslate.onLanguageChanged
                imageSource: "asset:///images/ic_info.png"
                onTriggered: {
                    aboutSheet.open();
                }
            }
        ]
        settingsAction: SettingsActionItem {
            onTriggered: {
                settingsSheet.open();
            }
        }
    }
    Page {
        id: mainPage
        content: Container {
            Container {
                layout: StackLayout {
                    orientation: LayoutOrientation.LeftToRight
                }
                id: searchFieldContainer
                topPadding: ui.du(1.0)
                bottomPadding: ui.du(1.0)
                visible: false
                Container {
                    TextField {
                        id: searchField
                        text: ""
                        input.submitKey: SubmitKey.Search
                        hintText: qsTr("Search term") + Retranslate.onLanguageChanged
                        onTextChanging: {
                            filterableDataModel.filterText = searchField.text;
                            if (text == "") {
                                searchField.loseFocus();
                                searchFieldContainer.visible = false;
                            }
                        }
                    }
                }
                Container {
                    leftPadding: ui.du(0.25)
                    rightPadding: ui.du(0.25)
                    Button {
                        id: cancelButton
                        text: qsTr("Cancel") + Retranslate.onLanguageChanged
                        preferredWidth: ui.du(10.0)
                        onClicked: {
                            if (searchField.text != "") {
                                searchField.text = "";
                                filterableDataModel.filterText = searchField.text;
                            }
                            searchField.loseFocus();
                            searchFieldContainer.visible = false;
                        }
                    }
                }
            }

            Container {
                id: emptyLabel
                visible: false
                preferredHeight: mainPage.emptyLabelHigh(true)
                layout: DockLayout {

                }
                Container {
                    horizontalAlignment: HorizontalAlignment.Center
                    verticalAlignment: VerticalAlignment.Center
                    Container {
                        layout: StackLayout {
                            orientation: LayoutOrientation.LeftToRight
                        }
                        Container {
                            layoutProperties: StackLayoutProperties {
                                spaceQuota: 1
                            }
                            Label {
                                horizontalAlignment: HorizontalAlignment.Center
                                text: qsTr("There are no passes yet.") + Retranslate.onLanguageChanged
                                textStyle {
                                    fontSize: FontSize.XLarge
                                }
                            }
                        }
                    }
                    Container {
                        horizontalAlignment: HorizontalAlignment.Center
                        layout: StackLayout {
                            orientation: LayoutOrientation.LeftToRight
                        }
                        Container {
                            verticalAlignment: VerticalAlignment.Center
                            layout: GridLayout {
                                columnCount: 3
                            }
                            layoutProperties: StackLayoutProperties {
                                //spaceQuota: 1
                            }
                            Container {
                                topPadding: ui.du(3.5)
                                verticalAlignment: VerticalAlignment.Center
                                ImageView {
                                    accessibility.name: qsTr("Import pass help") + Retranslate.onLanguageChanged
                                    imageSource: "asset:///images/ic_forward.png"
                                    preferredHeight: ui.du(6.75)
                                    scalingMethod: ScalingMethod.AspectFit
                                }
                                gestureHandlers: [
                                    TapHandler {
                                        onTapped: {
                                            filepicker.open();
                                        }
                                    }
                                ]
                            }
                            Container {
                                topPadding: ui.du(3.5)
                                verticalAlignment: VerticalAlignment.Center
                                Label {
                                    horizontalAlignment: HorizontalAlignment.Center
                                    text: qsTr("You can import the pass") + Retranslate.onLanguageChanged
                                    textStyle {
                                        fontSize: FontSize.Medium
                                    }
                                }
                                gestureHandlers: [
                                    TapHandler {
                                        onTapped: {
                                            filepicker.open();
                                        }
                                    }
                                ]
                            }
                            Container {
                                topPadding: ui.du(3.5)
                                leftPadding: ui.du(2.0)
                                verticalAlignment: VerticalAlignment.Center
                                Container {
                                    background: Color.DarkGray
                                    ImageButton {
                                        accessibility.name: qsTr("Import pass help") + Retranslate.onLanguageChanged
                                        defaultImageSource: "asset:///images/ic_help.png"
                                        preferredHeight: ui.du(7.0)
                                        preferredWidth: ui.du(7.0)
                                        onClicked: {
                                            var helpPageContent = helpPage.createObject();
                                            helpPageContent.title = qsTr("Importing passes") + Retranslate.onLanguageChanged;
                                            helpPageContent.imageSource = "";
                                            helpPageContent.imageVisible = false;
                                            helpPageContent.textContent = qsTr("Passes supports import of existing files. Pass issuer usually gives link to download pkpass pass file or sends it by mail. The application works with pkpass files. PDF or other files are not supported.") + Retranslate.onLanguageChanged;
                                            navigationPane.push(helpPageContent);
                                        }
                                    }
                                }
                            }
                            Container {
                                topPadding: ui.du(3.5)
                                verticalAlignment: VerticalAlignment.Center
                                ImageView {
                                    accessibility.name: qsTr("Create pass") + Retranslate.onLanguageChanged
                                    imageSource: "asset:///images/ic_forward.png"
                                    preferredHeight: ui.du(6.5)
                                    scalingMethod: ScalingMethod.AspectFit
                                    
                                }
                                gestureHandlers: [
                                    TapHandler {
                                        onTapped: {
                                            var addPageContent = addPage.createObject();
                                            addPageContent.open();
                                        }
                                    }
                                ]
                            }
                            Container {
                                topPadding: ui.du(3.5)
                                verticalAlignment: VerticalAlignment.Center
                                Label {
                                    horizontalAlignment: HorizontalAlignment.Center
                                    text: qsTr("You can create the pass") + Retranslate.onLanguageChanged
                                    textStyle {
                                        fontSize: FontSize.Medium
                                    }
                                }
                                gestureHandlers: [
                                    TapHandler {
                                        onTapped: {
                                            var addPageContent = addPage.createObject();
                                            addPageContent.open();
                                        }
                                    }
                                ]
                            }
                            Container {
                                topPadding: ui.du(3.5)
                                leftPadding: ui.du(2.0)
                                verticalAlignment: VerticalAlignment.Center
                                Container {
                                    background: Color.DarkGray
                                    ImageButton {
                                        accessibility.name: qsTr("Create pass help") + Retranslate.onLanguageChanged
                                        defaultImageSource: "asset:///images/ic_help.png"
                                        preferredHeight: ui.du(7.0)
                                        preferredWidth: ui.du(7.0)
                                        onClicked: {
                                            var helpPageContent = helpPage.createObject();
                                            helpPageContent.title = qsTr("Create new pass") + Retranslate.onLanguageChanged;
                                            helpPageContent.imageSource = "";
                                            helpPageContent.imageVisible = false;
                                            helpPageContent.textContent = qsTr("You can create your own pass. There are two types predefined - generic pass and boarding pass.") + Retranslate.onLanguageChanged;
                                            navigationPane.push(helpPageContent);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

            ListView {
                id: passesList
                accessibility.name: qsTr("Saved passes")
                accessibility.description: qsTr("Cards are stacked. Activate the card to view the full card.")
                dataModel: filterableDataModel
                layout: (mainPage.listviewLayout == "stack") ? stackListLayout : gridListLayout
                rootIndexPath: []
                listItemComponents: [
                    ListItemComponent {
                        id: listItemComponent
                        type: "item"
                        content: Container {
                            id: listItemContainer
                            topPadding: ui.du(3.5)
                            bottomPadding: ui.du(3.5)
                            leftPadding: ui.du(2.5)
                            rightPadding: ui.du(2.5)
                            bottomMargin: ui.du(0.25)
                            opacity: listItemContainer.ListItem.active ? 0.5 : 1.0
                            property int indexInSection: ListItem.indexInSection
                            Container {
                                id: listItemRowContainer
                                layout: StackLayout {
                                    orientation: LayoutOrientation.LeftToRight
                                }
                                minHeight: ui.du(12.0)
                                //Container {
                                //    verticalAlignment: VerticalAlignment.Center
                                ImageView {
                                    id: logoImageView
                                    //imageSource: "file://" + listItemContainer.ListItem.view.getImagePath(ListItemData.identificator, "logo")
                                    imageSource: listItemContainer.ListItem.view.getImagePath(ListItemData.identificator, "logo")
                                    maxHeight: ui.du(9.5)
                                    verticalAlignment: VerticalAlignment.Center
                                    scalingMethod: ScalingMethod.AspectFit
                                    accessibility.labelledBy: listItemContainer
                                    accessibility.name: ""
                                    accessibility.description: ""
                                }
                                //}
                                Container {
                                    layoutProperties: StackLayoutProperties {
                                        spaceQuota: 1
                                    }
                                    verticalAlignment: VerticalAlignment.Center
                                    leftPadding: ui.du(2.5)
                                    Label {
                                        text: ListItemData.logoText
                                        //text: ListItemData.passType
                                        accessibility.labelledBy: ListItemData.organizationName
                                        accessibility.name: ListItemData.organizationName
                                        accessibility.description: ""
                                        //text: ListItemData.used_time
                                        textStyle {
                                            color: listItemContainer.ListItem.view.getColor(ListItemData.foregroundColor)
                                            fontSize: FontSize.Large
                                            fontWeight: FontWeight.W500
                                        }
                                    }
                                }
                                Container {
                                    verticalAlignment: VerticalAlignment.Center
                                    visible: listItemContainer.ListItem.view.getKeyvalueContainerVisibility()
                                    layout: StackLayout {
                                        orientation: LayoutOrientation.LeftToRight
                                    }
                                    HeaderField {
                                        labelText: listItemContainer.ListItem.view.getheaderLabel(ListItemData.headerFields, 0)
                                        valueText: listItemContainer.ListItem.view.getheaderValue(ListItemData.headerFields, 0)
                                        labelColor: listItemContainer.ListItem.view.getColorString(ListItemData.labelColor)
                                        foregroundColor: listItemContainer.ListItem.view.getColorString(ListItemData.foregroundColor)
                                        organizationName: ListItemData.organizationName
                                    }
                                    HeaderField {
                                        labelText: listItemContainer.ListItem.view.getheaderLabel(ListItemData.headerFields, 1)
                                        valueText: listItemContainer.ListItem.view.getheaderValue(ListItemData.headerFields, 1)
                                        labelColor: listItemContainer.ListItem.view.getColorString(ListItemData.labelColor)
                                        foregroundColor: listItemContainer.ListItem.view.getColorString(ListItemData.foregroundColor)
                                        organizationName: ListItemData.organizationName
                                    }
                                    HeaderField {
                                        labelText: listItemContainer.ListItem.view.getheaderLabel(ListItemData.headerFields, 2)
                                        valueText: listItemContainer.ListItem.view.getheaderValue(ListItemData.headerFields, 2)
                                        labelColor: listItemContainer.ListItem.view.getColorString(ListItemData.labelColor)
                                        foregroundColor: listItemContainer.ListItem.view.getColorString(ListItemData.foregroundColor)
                                        organizationName: ListItemData.organizationName
                                    }
                                    HeaderField {
                                        labelText: listItemContainer.ListItem.view.getheaderLabel(ListItemData.headerFields, 3)
                                        valueText: listItemContainer.ListItem.view.getheaderValue(ListItemData.headerFields, 3)
                                        labelColor: listItemContainer.ListItem.view.getColorString(ListItemData.labelColor)
                                        foregroundColor: listItemContainer.ListItem.view.getColorString(ListItemData.foregroundColor)
                                        organizationName: ListItemData.organizationName
                                    }
                                }
                            }
                            Container {
                                visible: ! listItemContainer.ListItem.view.getKeyvalueContainerVisibility()
                                verticalAlignment: VerticalAlignment.Bottom
                                horizontalAlignment: HorizontalAlignment.Right
                                Container {
                                    layoutProperties: StackLayoutProperties {
                                        spaceQuota: 1
                                    }
                                }
                                Container {
                                    layout: StackLayout {
                                        orientation: LayoutOrientation.LeftToRight
                                    }
                                    HeaderField {
                                        labelText: listItemContainer.ListItem.view.getheaderLabel(ListItemData.headerFields, 0)
                                        valueText: listItemContainer.ListItem.view.getheaderValue(ListItemData.headerFields, 0)
                                        labelColor: listItemContainer.ListItem.view.getColorString(ListItemData.labelColor)
                                        foregroundColor: listItemContainer.ListItem.view.getColorString(ListItemData.foregroundColor)
                                        organizationName: ListItemData.organizationName
                                    }
                                    HeaderField {
                                        labelText: listItemContainer.ListItem.view.getheaderLabel(ListItemData.headerFields, 1)
                                        valueText: listItemContainer.ListItem.view.getheaderValue(ListItemData.headerFields, 1)
                                        labelColor: listItemContainer.ListItem.view.getColorString(ListItemData.labelColor)
                                        foregroundColor: listItemContainer.ListItem.view.getColorString(ListItemData.foregroundColor)
                                        organizationName: ListItemData.organizationName
                                    }
                                    HeaderField {
                                        labelText: listItemContainer.ListItem.view.getheaderLabel(ListItemData.headerFields, 2)
                                        valueText: listItemContainer.ListItem.view.getheaderValue(ListItemData.headerFields, 2)
                                        labelColor: listItemContainer.ListItem.view.getColorString(ListItemData.labelColor)
                                        foregroundColor: listItemContainer.ListItem.view.getColorString(ListItemData.foregroundColor)
                                        organizationName: ListItemData.organizationName
                                    }
                                    HeaderField {
                                        labelText: listItemContainer.ListItem.view.getheaderLabel(ListItemData.headerFields, 3)
                                        valueText: listItemContainer.ListItem.view.getheaderValue(ListItemData.headerFields, 3)
                                        labelColor: listItemContainer.ListItem.view.getColorString(ListItemData.labelColor)
                                        foregroundColor: listItemContainer.ListItem.view.getColorString(ListItemData.foregroundColor)
                                        organizationName: ListItemData.organizationName
                                    }
                                }
                            }
                            background: {
                                var hasBackgroundImage = listItemContainer.ListItem.view.hasImage(ListItemData.identificator, "blured_background_listview");
                                if (! hasBackgroundImage) {
                                    return listItemContainer.ListItem.view.getColor(ListItemData.backgroundColor)
                                } else {
                                    imagePaintDefinition.imageSource = listItemContainer.ListItem.view.getImagePath(ListItemData.identificator, "blured_background_listview");
                                    return imagePaintDefinition.imagePaint;
                                }
                            }
                            attachedObjects: [
                                ImagePaintDefinition {
                                    id: imagePaintDefinition
                                }
                            ]
                            contextActions: [
                                ActionSet {
                                    title: ListItemData.organizationName + Retranslate.onLanguageChanged
                                    DeleteActionItem {
                                        onTriggered: {
                                            listItemContainer.ListItem.view.deleteItem(ListItemData.identificator, ListItemData.organizationName);
                                        }
                                    }
                                }
                            ]
                        }
                    }
                ]
                onTriggered: {
                    var chosenItem = passesList.dataModel.data(indexPath);
                    mainPage.pushPassPage(chosenItem);

                    //passesList.dataModel.itemMoved(indexPath, [0]);
                    //filterableDataModel.itemToBeginning(indexPath);
                }
                function getColor(appleColorNotation) {
                    return uiHelper.getColor(appleColorNotation);
                }
                function getColorString(appleColorNotation) {
                    return uiHelper.getColorString(appleColorNotation);
                }
                function hasImage(identificator, name) {
                    return uiHelper.hasImage(identificator, name);
                }
                function getImagePath(identificator, name) {
                    var path = uiHelper.getImagePath(identificator, name);
                    if (path != "") {
                        return "file://" + path;
                    }
                    return "";
                }
                function getKeyvalueContainerVisibility() {
                    if (mainPage.listviewLayout == "stack") {
                        return true;
                    } else {
                        return false;
                    }
                }
                function deleteItem(id, org) {
                    deleteConfirmDialog.identificator = id;
                    deleteConfirmDialog.body = qsTr("Delete the pass from") + " " + org + "?" + Retranslate.onLanguageChanged
                    deleteConfirmDialog.show();
                }
                function getheaderLabel(list, i) {
                    return uiHelper.getFieldLabel(list, i);
                }
                function getheaderValue(list, i) {
                    return uiHelper.getFieldValue(list, i);
                }
            }
        }
        //actionBarVisibility:ChromeVisibility.Compact
        actions: [
            ActionItem {
                id: searchAction
                title: qsTr("Search") + Retranslate.onLanguageChanged
                ActionBar.placement: ActionBarPlacement.Signature
                imageSource: "asset:///images/ic_search.png"
                onTriggered: {
                    searchFieldContainer.visible = true;
                    searchField.requestFocus();
                }
                shortcuts: [
                    Shortcut {
                        key: "s"
                    }
                ]
            },
            ActionItem {
                id: importAction
                ActionBar.placement: ActionBarPlacement.OnBar
                //ActionBar.placement: ActionBarPlacement.OnBar
                imageSource: "asset:///images/ic_import.png"
                title: qsTr("Import") + Retranslate.onLanguageChanged
                onTriggered: {
                    filepicker.open();
                }
            },
            ActionItem {
                ActionBar.placement: ActionBarPlacement.OnBar
                imageSource: "asset:///images/ic_create.png"
                title: qsTr("Create") + Retranslate.onLanguageChanged
                onTriggered: {
                    var addPageContent = addPage.createObject();
                    addPageContent.open();
                }
            },
            ActionItem {
                id: changeListLayoutAction
                ActionBar.placement: ActionBarPlacement.InOverflow
                //ActionBar.placement: ActionBarPlacement.OnBar
                title: {
                    (mainPage.listviewLayout == "stack") ? qsTr("Grid view") + Retranslate.onLanguageChanged : qsTr("List view") + Retranslate.onLanguageChanged
                }
                imageSource: (mainPage.listviewLayout == "stack") ? "asset:///images/ic_view_grid.png" : "asset:///images/ic_view_list.png"
                onTriggered: {
                    if (mainPage.listviewLayout == "stack") {
                        mainPage.listviewLayout = "grid";
                        uiHelper.listviewLayoutType = "grid";
                        changeListLayoutAction.title = qsTr("List view") + Retranslate.onLanguageChanged;
                        changeListLayoutAction.imageSource = "asset:///images/ic_view_list.png"
                        passesList.layout = gridListLayout;
                    } else {
                        mainPage.listviewLayout = "stack";
                        uiHelper.listviewLayoutType = "stack";
                        changeListLayoutAction.title = qsTr("Grid view") + Retranslate.onLanguageChanged;
                        changeListLayoutAction.imageSource = "asset:///images/ic_view_grid.png"
                        passesList.layout = stackListLayout;
                    }

                }
            },
            ActionItem {
                ActionBar.placement: ActionBarPlacement.InOverflow
                id: emailAction
                title: qsTr("Send Feedback") + Retranslate.onLanguageChanged
                imageSource: "asset:///images/ic_feedback.png"
                onTriggered: {
                    controller.invokeEmail("Passes feedback");
                }
            }
        ]
        /*
         * do not work if there are not letters keys
         * keyListeners: [
         * KeyListener {
         * onKeyPressed: {
         * if (searchFieldContainer.visible == false) {
         * searchFieldContainer.visible = true;
         * searchField.text = searchField.text + event.unicode
         * searchField.requestFocus();
         * }
         * }
         * }
         * ]
         */
        attachedObjects: [
            FilterableGroupDataModel {
                id: filterableDataModel
                grouping: ItemGrouping.None
                sortedAscending: false
                //sortingKeys: [ "organizationName" ]
                sortingKeys: [ "used_time" ]
            },
            FilePicker {
                id: filepicker
                type: FileType.Other
                onFileSelected: {
                    controller.importPassData(selectedFiles[0]);
                }
                onCanceled: {

                    controller.checkShouldReloadPasses();
                }
            },
            SystemToast {
                id: infoToast
                position: SystemUiPosition.MiddleCenter
            },
            SystemDialog {
                id: deleteConfirmDialog
                title: qsTr("Delete the pass?") + Retranslate.onLanguageChanged
                onFinished: {
                    if (deleteConfirmDialog.result == SystemUiResult.ConfirmButtonSelection) {
                        filterableDataModel.refreshOriginalItems = true;
                        controller.removePassData(deleteConfirmDialog.identificator);
                    }
                }
                property string identificator
            },
            GridListLayout {
                id: gridListLayout
                columnCount: mainPage.countColumnNumber(null)
                headerMode: ListHeaderMode.None
            },
            StackListLayout {
                id: stackListLayout
                headerMode: ListHeaderMode.None
            },
            DisplayInfo {
                id: displayInfo
            },
            OrientationHandler {
                id: orientationHandler
                onOrientationChanged: {
                    var columnCount = mainPage.countColumnNumber(orientation);
                    gridListLayout.columnCount = columnCount;
                    console.log("columns:" + gridListLayout.columnCount);
                    emptyLabel.preferredHeight = mainPage.emptyLabelHigh(orientation);
                    console.log("empty label height:" + emptyLabel.preferredHeight);
                }
            },
            ComponentDefinition {
                id: boardingPass
                source: "boardingPass.qml"
            },
            ComponentDefinition {
                id: coupon
                source: "coupon.qml"
            },
            ComponentDefinition {
                id: event
                source: "event.qml"
            },
            ComponentDefinition {
                id: eventStrip
                source: "eventStrip.qml"
            },
            ComponentDefinition {
                id: generic
                source: "generic.qml"
            },
            ComponentDefinition {
                id: genericSquare
                source: "genericSquare.qml"
            },
            ComponentDefinition {
                id: store
                source: "store.qml"
            },
            ComponentDefinition {
                id: addPage
                source: "add.qml"
            },
            ComponentDefinition {
                id: helpPage
                source: "help.qml"
            }
        ]

        function countColumnNumber(orientation) {
            if (orientation == null) {
                orientation = orientationHandler.orientation;
            }
            return ((orientation) ? displayInfo.pixelSize.height : displayInfo.pixelSize.width) / ui.du(38.0);
        }
        
        function emptyLabelHigh(orientation) {
            if (orientation == null) {
                orientation = orientationHandler.orientation;
            }
            return ((orientation) ? displayInfo.pixelSize.width : displayInfo.pixelSize.height) - ui.du(12.0)
        }

        function pushPassPage(chosenItem) {
            var pageContent;
            console.log("pass type: " + chosenItem.passType);
            if (chosenItem.passType == "boardingPass") {
                pageContent = boardingPass.createObject();
            } else if (chosenItem.passType == "coupon") {
                pageContent = coupon.createObject();
            } else if (chosenItem.passType == "eventTicket") {
                if (passesList.hasImage(chosenItem.identificator, "strip")) {
                    pageContent = eventStrip.createObject();
                } else { // if (passesList.hasImage(chosenItem.identificator, "background")) {
                    pageContent = event.createObject();
                }
            } else if (chosenItem.passType == "generic") {
                if (chosenItem.barcodeFormat == "PKBarcodeFormatQR" || chosenItem.barcodeFormat == "PKBarcodeFormatAztec") {
                    pageContent = genericSquare.createObject();
                } else {
                    pageContent = generic.createObject();
                }
            } else if (chosenItem.passType == "storeCard") {
                pageContent = store.createObject();
            }
            pageContent.identificator = chosenItem.identificator;
            navigationPane.push(pageContent);

            controller.updateLastUsedTime(chosenItem.identificator);
            //chosenItem.used_time = new Date();

            //updateDataModel();

            filterableDataModel.clear();
            filterableDataModel.insertList(controller.getRefreshedPassesData());

            searchFieldContainer.visible = false;
            searchField.text = "";
        }

        function updateDataModel() {
            filterableDataModel.clear();
            //var data = controller.getRefreshedPassesData();
            var data = controller.getRefreshedReducedPassesData();
            //filterableDataModel.insertList(controller.getRefreshedPassesData());
            //geoHelper.updateLocationsList(controller.getRefreshedPassesData());
            filterableDataModel.insertList(data);
            geoHelper.updateLocationsList(data);
            if (filterableDataModel.size() == 0) {
                console.log("list is empty");
                passesList.visible = false;
                emptyLabel.visible = true;
            } else {
                passesList.visible = true;
                emptyLabel.visible = false;
            }
        }

        function onUpdatePasses() {
            // notification about changing list outside the app turned off
            // infoToast.body = qsTr("Refreshing passes list") + Retranslate.onLanguageChanged;
            // infoToast.show();
            updateDataModel();
        }

        function onImportPassDataFinished(error, op_type, identificator) {
            if (error) {
                infoToast.body = qsTr("Can't add selected pass") + Retranslate.onLanguageChanged;
                infoToast.show();
            } else {
                filterableDataModel.refreshOriginalItems = true;
                if (op_type == "insert") {
                    infoToast.body = qsTr("Pass added successfuly") + Retranslate.onLanguageChanged;
                    updateDataModel();
                } else if (op_type == "update") {
                    infoToast.body = qsTr("Pass updated successfuly") + Retranslate.onLanguageChanged;
                    updateDataModel();
                } else {
                    infoToast.body = qsTr("Pass operation finished successfuly") + Retranslate.onLanguageChanged;
                }
                infoToast.show();
            }
            if (identificator != "") {
                var chosenItem = controller.getPassData(identificator);
                pushPassPage(chosenItem);
            }
        }

        function onImportPassDataStarted() {
            infoToast.body = qsTr("Processing pass started") + Retranslate.onLanguageChanged;
            infoToast.show();
        }

        function onRemovePassDataFinished(result) {
            updateDataModel();
            if (result) {
                infoToast.body = qsTr("Pass successfuly removed") + Retranslate.onLanguageChanged;
                infoToast.show();
            } else {
                infoToast.body = qsTr("Removing the pass failed") + Retranslate.onLanguageChanged;
                infoToast.show();
            }
        }

        function onShowInvokedPass(identificator) {
            var chosenItem = controller.getPassData(identificator);
            pushPassPage(chosenItem);
        }

        onCreationCompleted: {
            controller.importPassDataStarted.connect(mainPage.onImportPassDataStarted);
            console.log("main connected to controller.importPassDataStarted signal");
            controller.importPassDataFinished.connect(mainPage.onImportPassDataFinished);
            console.log("main connected to controller.importPassDataFinished signal");
            controller.removePassDataFinished.connect(mainPage.onRemovePassDataFinished);
            console.log("main connected to controller.removePassDataFinished signal");
            controller.updatePasses.connect(mainPage.onUpdatePasses);
            console.log("main connected to controller.updatePasses signal");
            controller.showInvokedPass.connect(mainPage.onShowInvokedPass);
            console.log("main connected to controller.showInvokedPass signal");

            gridListLayout.columnCount = mainPage.countColumnNumber(null);
            emptyLabel.preferredHeight = mainPage.emptyLabelHigh(null);

            updateDataModel();
        }

        property string listviewLayout: uiHelper.listviewLayoutType
    }

    onPopTransitionEnded: {
        page.destroy();
    }

    attachedObjects: [
        AboutSheet {
            id: aboutSheet
        },
        SettingsSheet {
            id: settingsSheet
        }
    ]

}
