import bb.cascades 1.3
import bb.cascades.multimedia 1.2
import bb.system 1.2

Sheet {
    id: scanPage
    property string barcodeFormat
    content: Page {
        titleBar: TitleBar {
            title: qsTr("Scan Code") + Retranslate.onLanguageChanged
            dismissAction: ActionItem {
                title: qsTr("Cancel") + Retranslate.onLanguageChanged
                onTriggered: {
                    scanPage.close();
                    scanPage.destroy();
                }
            }
        }
        Container {
            Camera {
                id: camera
                horizontalAlignment: HorizontalAlignment.Fill
                verticalAlignment: VerticalAlignment.Fill

                attachedObjects: [
                    BarcodeDetector {
                        id: barcodeDetector
                        formats: {
                            if (barcodeFormat == "EAN") {
                                return BarcodeFormat.Ean13 | BarcodeFormat.Ean8;
                            } else if (barcodeFormat == "QR") {
                                return BarcodeFormat.QrCode;
                            } else if (barcodeFormat == "Aztec") {
                                return BarcodeFormat.Aztec;
                            } else if (barcodeFormat == "Pdf417") {
                                return BarcodeFormat.Any;
                            } else {
                                return BarcodeFormat.Any;
                            }
                        }
                        camera: camera

                        onBarcodeDetected: {
                            console.log("scanned message:" + data);
                            console.log("scanned message format:" + format);
                            if (format == 1) {
                                //addPage.barcodeTypeOfAdded = "PKBarcodeFormatQR";
                                barcodeTypeOfAddedDropDown.selectedIndex = 0;
                                barcodeMessage.text = data;
                            } else if (format == 512) {
                                //addPage.barcodeTypeOfAdded = "PKBarcodeFormatAztec";
                                barcodeTypeOfAddedDropDown.selectedIndex = 1;
                                barcodeMessage.text = data;
                            } else if (format == 32) {
                                //addPage.barcodeTypeOfAdded = "PKBarcodeFormatEAN";
                                barcodeTypeOfAddedDropDown.selectedIndex = 2;
                                barcodeMessage.text = data;
                            } else if (format == 16 || format == 8) {
                                //addPage.barcodeTypeOfAdded = "PKBarcodeFormatEAN";
                                barcodeTypeOfAddedDropDown.selectedIndex = 2;
                                barcodeMessage.text = data;
                            } else {
                                errrToast.body = qsTr("Unable to scan the barcode") + Retranslate.onLanguageChanged
                                errrToast.show();
                            }
                            camera.stopViewfinder();
                            camera.close();
                            scanPage.close();
                        }
                    },
                    SystemToast {
                        id: errrToast
                        position: SystemUiPosition.MiddleCenter
                    }
                ]

                onCameraOpened: {
                    camera.startViewfinder();
                }
            }
        }
        onCreationCompleted: {
            camera.open();
        }
    }
}