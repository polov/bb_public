import bb.cascades 1.3
import bb.cascades.pickers 1.0

Sheet {
    id: addPage
    NavigationPane {
        id: navigationPaneAddPage
        Page {
            id: addPageContent
            titleBar: TitleBar {
                title: qsTr("Create Pass") + Retranslate.onLanguageChanged
                dismissAction: ActionItem {
                    title: qsTr("Cancel") + Retranslate.onLanguageChanged
                    onTriggered: {
                        addPage.close();
                        addPage.destroy();
                    }
                }
                acceptAction: ActionItem {
                    id: saveAction
                    title: qsTr("Create") + Retranslate.onLanguageChanged
                    onTriggered: {
                        console.log("save values");
                        controller.createPass(passTypeTypeDropDown.selectedValue, logoText.text, headerFieldLabelText.text, headerFieldValueText.text, primaryFieldLabelText.text, primaryFieldValueText.text, primaryFieldLabelText2.text, primaryFieldValueText2.text, backFieldLabelText.text, backFieldTextArea.text, barcodeTypeOfAdded, barcodeMessage.text, barcodeAltText.text, logoImageSource, bgColorValue, fgColorValue, lblColorValue, companyNameText.text);
                        addPage.close();
                        addPage.destroy();
                    }
                }
            }
            content: ScrollView {
                Container {
                    Container {
                        topPadding: ui.du(1.5)
                        leftPadding: ui.du(2.5)
                        rightPadding: ui.du(2.5)
                        DropDown {
                            id: passTypeTypeDropDown
                            title: qsTr("Pass type") + Retranslate.onLanguageChanged
                            Option {
                                text: qsTr("Boarding pass") + Retranslate.onLanguageChanged
                                value: "boardingPass"
                            }
                            Option {
                                text: qsTr("Generic pass") + Retranslate.onLanguageChanged
                                value: "generic"
                                selected: true
                            }
                            onSelectedValueChanged: {
                                console.log(selectedValue + " chosen");
                                if (selectedValue == "boardingPass") {
                                    primaryFields2.visible = true;
                                    primaryFieldLabelText.hintText = qsTr("Departure label") + Retranslate.onLanguageChanged;
                                    primaryFieldValueText.hintText = qsTr("Departure value") + Retranslate.onLanguageChanged;
                                    primaryFieldLabelText2.hintText = qsTr("Arrival label") + Retranslate.onLanguageChanged;
                                    primaryFieldValueText2.hintText = qsTr("Arrival value") + Retranslate.onLanguageChanged;
                                } else {
                                    primaryFields2.visible = false;
                                    primaryFieldLabelText.hintText = qsTr("Primary field label") + Retranslate.onLanguageChanged;
                                    primaryFieldValueText.hintText = qsTr("Primary field value") + Retranslate.onLanguageChanged;
                                }
                            }
                        }
                    }
                    Divider {
                    }
                    Container {
                        leftPadding: ui.du(2.5)
                        rightPadding: ui.du(2.5)
                        layout: StackLayout {
                            orientation: LayoutOrientation.LeftToRight
                        }
                        Container {
                            layoutProperties: StackLayoutProperties {
                                spaceQuota: 1
                            }
                            verticalAlignment: VerticalAlignment.Center
                            Label {
                                text: qsTr("Company name") + Retranslate.onLanguageChanged
                                textStyle.fontSize: FontSize.Medium
                            }
                        }
                        Container {
                            background: Color.DarkGray
                            ImageButton {
                                defaultImageSource: "asset:///images/ic_help.png"
                                preferredHeight: ui.du(7.0)
                                preferredWidth: ui.du(7.0)
                                onClicked: {
                                    var helpPageContent = helpPage.createObject();
                                    helpPageContent.title = qsTr("Company name") + Retranslate.onLanguageChanged;
                                    helpPageContent.imageSource = "";
                                    helpPageContent.imageVisible = false;
                                    helpPageContent.textContent = qsTr("Name of the company which issued the card/coupon/pass. The name will be used in search.") + Retranslate.onLanguageChanged;
                                    navigationPaneAddPage.push(helpPageContent);
                                }
                            }
                        }
                    }
                    Container {
                        topMargin: ui.du(1.0)
                        leftPadding: ui.du(2.5)
                        rightPadding: ui.du(2.5)
                        TextField {
                            id: companyNameText
                            hintText: qsTr("Company name") + Retranslate.onLanguageChanged
                        }
                    }
                    Divider {

                    }
                    Container {
                        leftPadding: ui.du(2.5)
                        rightPadding: ui.du(2.5)
                        layout: StackLayout {
                            orientation: LayoutOrientation.LeftToRight
                        }
                        Container {
                            layoutProperties: StackLayoutProperties {
                                spaceQuota: 1
                            }
                            verticalAlignment: VerticalAlignment.Center
                            Label {
                                text: qsTr("Logo and text") + Retranslate.onLanguageChanged
                                textStyle.fontSize: FontSize.Medium
                            }
                        }
                        Container {
                            background: Color.DarkGray
                            ImageButton {
                                defaultImageSource: "asset:///images/ic_help.png"
                                preferredHeight: ui.du(7.0)
                                preferredWidth: ui.du(7.0)
                                onClicked: {
                                    var helpPageContent = helpPage.createObject();
                                    helpPageContent.title = qsTr("Logo and text") + Retranslate.onLanguageChanged;
                                    helpPageContent.imageSource = "asset:///images/pass_logo_text.png";
                                    helpPageContent.imageVisible = true;
                                    helpPageContent.textContent = qsTr("Logo is the image on the top and is followed by the text. The text will be used in search.") + Retranslate.onLanguageChanged;
                                    navigationPaneAddPage.push(helpPageContent);
                                }
                            }
                        }
                    }
                    Container {
                        layout: StackLayout {
                            orientation: LayoutOrientation.LeftToRight
                        }
                        topMargin: ui.du(1.0)
                        leftPadding: ui.du(2.5)
                        rightPadding: ui.du(2.5)
                        Container {
                            ImageView {
                                id: iv
                                //imageSource: "asset:///images/ic_capture_image.png"
                                imageSource: logoImageSource
                                preferredHeight: ui.du(15.0)
                                preferredWidth: ui.du(15.0)
                                scalingMethod: ScalingMethod.AspectFit
                            }
                        }
                        Container {
                            leftPadding: ui.du(2.5)
                            verticalAlignment: VerticalAlignment.Center
                            Button {
                                text: qsTr("Change image") + Retranslate.onLanguageChanged
                                onClicked: {
                                    logoImagePicker.open();
                                }
                            }
                            attachedObjects: [
                                FilePicker {
                                    id: logoImagePicker
                                    type: FileType.Picture
                                    imageCropEnabled: true
                                    onFileSelected: {
                                        console.log("file selected: " + selectedFiles[0]);
                                        addPageContent.onImageReady(selectedFiles[0]);
                                    }
                                }
                            ]
                        }
                    }
                    Container {
                        topMargin: ui.du(1.0)
                        leftPadding: ui.du(2.5)
                        rightPadding: ui.du(2.5)
                        TextField {
                            id: logoText
                            hintText: qsTr("Text next to the logo") + Retranslate.onLanguageChanged
                        }
                    }
                    Divider {
                    }
                    Container {
                        leftPadding: ui.du(2.5)
                        rightPadding: ui.du(2.5)
                        layout: StackLayout {
                            orientation: LayoutOrientation.LeftToRight
                        }
                        Container {
                            layoutProperties: StackLayoutProperties {
                                spaceQuota: 1
                            }
                            verticalAlignment: VerticalAlignment.Center
                            Label {
                                text: qsTr("Colors") + Retranslate.onLanguageChanged
                                textStyle.fontSize: FontSize.Medium
                            }
                        }
                        Container {
                            background: Color.DarkGray
                            ImageButton {
                                defaultImageSource: "asset:///images/ic_help.png"
                                preferredHeight: ui.du(7.0)
                                preferredWidth: ui.du(7.0)
                                onClicked: {
                                    var helpPageContent = helpPage.createObject();
                                    helpPageContent.title = qsTr("Colors") + Retranslate.onLanguageChanged;
                                    helpPageContent.imageSource = "";
                                    helpPageContent.imageVisible = false;
                                    helpPageContent.textContent = qsTr("Every pass has its background color. Text color is color of field values. Label color is color of field labels that are above field values.") + Retranslate.onLanguageChanged;
                                    navigationPaneAddPage.push(helpPageContent);
                                }
                            }
                        }
                    }
                    Container {
                        topMargin: ui.du(1.0)
                        leftPadding: ui.du(2.5)
                        rightPadding: ui.du(2.5)
                        Label {
                            text: qsTr("Background Color") + Retranslate.onLanguageChanged
                            textStyle.fontSize: FontSize.Default
                        }
                    }
                    Container {
                        layout: StackLayout {
                            orientation: LayoutOrientation.LeftToRight
                        }
                        topMargin: ui.du(1.0)
                        leftPadding: ui.du(2.5)
                        rightPadding: ui.du(2.5)
                        Container {
                            background: Color.create(bgColorValue)
                            id: bgColorContainer
                            preferredHeight: ui.du(15.0)
                            preferredWidth: ui.du(15.0)
                        }
                        Container {
                            leftPadding: ui.du(2.5)
                            verticalAlignment: VerticalAlignment.Center
                            Button {
                                text: qsTr("Change color") + Retranslate.onLanguageChanged
                                onClicked: {
                                    var colorPickerContent = colorPicker.createObject();
                                    colorPickerContent.colorPicked = bgColorValue;
                                    colorPickerContent.colorInitial = bgColorValue;
                                    colorPickerContent.colorFor = "bg";
                                    colorPickerContent.open();
                                }
                            }
                        }
                    }
                    Container {
                        topMargin: ui.du(1.0)
                        leftPadding: ui.du(2.5)
                        rightPadding: ui.du(2.5)
                        Label {
                            text: qsTr("Text Color") + Retranslate.onLanguageChanged
                            textStyle.fontSize: FontSize.Default
                        }
                    }
                    Container {
                        layout: StackLayout {
                            orientation: LayoutOrientation.LeftToRight
                        }
                        topMargin: ui.du(1.0)
                        leftPadding: ui.du(2.5)
                        rightPadding: ui.du(2.5)
                        Container {
                            background: Color.create(fgColorValue)
                            id: fgColorContainer
                            preferredHeight: ui.du(15.0)
                            preferredWidth: ui.du(15.0)
                        }
                        Container {
                            leftPadding: ui.du(2.5)
                            verticalAlignment: VerticalAlignment.Center
                            Button {
                                text: qsTr("Change color") + Retranslate.onLanguageChanged
                                onClicked: {
                                    var colorPickerContent = colorPicker.createObject();
                                    colorPickerContent.colorPicked = fgColorValue;
                                    colorPickerContent.colorInitial = fgColorValue;
                                    colorPickerContent.colorFor = "fg";
                                    colorPickerContent.open();
                                }
                            }
                        }
                    }
                    Container {
                        topMargin: ui.du(1.0)
                        leftPadding: ui.du(2.5)
                        rightPadding: ui.du(2.5)
                        Label {
                            text: qsTr("Label Color") + Retranslate.onLanguageChanged
                            textStyle.fontSize: FontSize.Default
                        }
                    }
                    Container {
                        layout: StackLayout {
                            orientation: LayoutOrientation.LeftToRight
                        }
                        topMargin: ui.du(1.0)
                        leftPadding: ui.du(2.5)
                        rightPadding: ui.du(2.5)
                        Container {
                            background: Color.create(lblColorValue)
                            id: lblColorContainer
                            preferredHeight: ui.du(15.0)
                            preferredWidth: ui.du(15.0)
                        }
                        Container {
                            leftPadding: ui.du(2.5)
                            verticalAlignment: VerticalAlignment.Center
                            Button {
                                text: qsTr("Change color") + Retranslate.onLanguageChanged
                                onClicked: {
                                    var colorPickerContent = colorPicker.createObject();
                                    colorPickerContent.colorPicked = lblColorValue;
                                    colorPickerContent.colorInitial = lblColorValue;
                                    colorPickerContent.colorFor = "lbl";
                                    colorPickerContent.open();
                                }
                            }
                        }
                    }
                    Divider {
                    }
                    Container {
                        leftPadding: ui.du(2.5)
                        rightPadding: ui.du(2.5)
                        layout: StackLayout {
                            orientation: LayoutOrientation.LeftToRight
                        }
                        Container {
                            layoutProperties: StackLayoutProperties {
                                spaceQuota: 1
                            }
                            verticalAlignment: VerticalAlignment.Center
                            Label {
                                text: qsTr("Header Field") + Retranslate.onLanguageChanged
                                textStyle.fontSize: FontSize.Medium
                            }
                        }
                        Container {
                            background: Color.DarkGray
                            ImageButton {
                                defaultImageSource: "asset:///images/ic_help.png"
                                preferredHeight: ui.du(7.0)
                                preferredWidth: ui.du(7.0)
                                onClicked: {
                                    var helpPageContent = helpPage.createObject();
                                    helpPageContent.title = qsTr("Header Field") + Retranslate.onLanguageChanged;
                                    helpPageContent.imageSource = "asset:///images/pass_header.png";
                                    helpPageContent.textContent = qsTr("Header field label and text are displayed on the top of the pass and in the list of your passes. Header field label uses small letters and is above the header field value. Header fiel value uses larger letters and is under the label. You can write label \"POINTS\" and amount as the value, or in case of boarding pass \"TRACK\" and the track of the train.") + Retranslate.onLanguageChanged;
                                    navigationPaneAddPage.push(helpPageContent);
                                }
                            }
                        }
                    }
                    Container {
                        topMargin: ui.du(1.0)
                        leftPadding: ui.du(2.5)
                        rightPadding: ui.du(2.5)
                        TextField {
                            id: headerFieldLabelText
                            hintText: qsTr("Header field label") + Retranslate.onLanguageChanged
                        }
                        TextField {
                            id: headerFieldValueText
                            hintText: qsTr("Header field value") + Retranslate.onLanguageChanged
                        }
                    }
                    Divider {
                    }
                    Container {
                        leftPadding: ui.du(2.5)
                        rightPadding: ui.du(2.5)
                        layout: StackLayout {
                            orientation: LayoutOrientation.LeftToRight
                        }
                        Container {
                            layoutProperties: StackLayoutProperties {
                                spaceQuota: 1
                            }
                            verticalAlignment: VerticalAlignment.Center
                            Label {
                                text: qsTr("Primary Field") + Retranslate.onLanguageChanged
                                textStyle.fontSize: FontSize.Medium
                            }
                        }
                        Container {
                            background: Color.DarkGray
                            ImageButton {
                                defaultImageSource: "asset:///images/ic_help.png"
                                preferredHeight: ui.du(7.0)
                                preferredWidth: ui.du(7.0)
                                onClicked: {
                                    var helpPageContent = helpPage.createObject();
                                    helpPageContent.title = qsTr("Primary Field") + Retranslate.onLanguageChanged;
                                    if (primaryFields2.visible) {
                                        helpPageContent.imageSource = "asset:///images/pass_primary_2.png";
                                    } else {
                                        helpPageContent.imageSource = "asset:///images/pass_primary.png";
                                    }
                                    helpPageContent.textContent = qsTr("Primary Field usually contains important information, such as Member Id, Coupon Id, Member Name. It depends on the purpose of the pass. In case of boarding pass there are deptarture and arrival places with its codes as values. There are primary field label (use smaller letters) and bellow is primary field value (uses larger letters).") + Retranslate.onLanguageChanged;
                                    navigationPaneAddPage.push(helpPageContent);
                                }
                            }
                        }
                    }
                    Container {
                        topMargin: ui.du(1.0)
                        leftPadding: ui.du(2.5)
                        rightPadding: ui.du(2.5)
                        TextField {
                            id: primaryFieldLabelText
                            hintText: qsTr("Primary field label") + Retranslate.onLanguageChanged
                        }
                        TextField {
                            id: primaryFieldValueText
                            hintText: qsTr("Primary field value") + Retranslate.onLanguageChanged
                        }
                    }
                    Container {
                        id: primaryFields2
                        visible: false
                        topMargin: ui.du(1.0)
                        leftPadding: ui.du(2.5)
                        rightPadding: ui.du(2.5)
                        TextField {
                            id: primaryFieldLabelText2
                        }
                        TextField {
                            id: primaryFieldValueText2
                        }
                    }
                    Divider {
                    }
                    Container {
                        leftPadding: ui.du(2.5)
                        rightPadding: ui.du(2.5)
                        layout: StackLayout {
                            orientation: LayoutOrientation.LeftToRight
                        }
                        Container {
                            layoutProperties: StackLayoutProperties {
                                spaceQuota: 1
                            }
                            verticalAlignment: VerticalAlignment.Center
                            Label {
                                text: qsTr("More info") + Retranslate.onLanguageChanged
                                textStyle.fontSize: FontSize.Medium
                            }
                        }
                        Container {
                            background: Color.DarkGray
                            ImageButton {
                                defaultImageSource: "asset:///images/ic_help.png"
                                preferredHeight: ui.du(7.0)
                                preferredWidth: ui.du(7.0)
                                onClicked: {
                                    var helpPageContent = helpPage.createObject();
                                    helpPageContent.title = qsTr("More info") + Retranslate.onLanguageChanged;
                                    helpPageContent.imageSource = "";
                                    helpPageContent.imageVisible = false;
                                    helpPageContent.textContent = qsTr("Place for additional information that you want to have available together with the pass.") + Retranslate.onLanguageChanged;
                                    navigationPaneAddPage.push(helpPageContent);
                                }
                            }
                        }
                    }
                    Container {
                        topMargin: ui.du(1.0)
                        leftPadding: ui.du(2.5)
                        rightPadding: ui.du(2.5)
                        TextField {
                            id: backFieldLabelText
                            hintText: qsTr("Additional information label") + Retranslate.onLanguageChanged
                        }
                        TextArea {
                            id: backFieldTextArea
                            hintText: qsTr("Additional information") + Retranslate.onLanguageChanged
                            preferredHeight: ui.du(17.0)
                        }
                    }
                    Divider {
                    }
                    Container {
                        leftPadding: ui.du(2.5)
                        rightPadding: ui.du(2.5)
                        layout: StackLayout {
                            orientation: LayoutOrientation.LeftToRight
                        }
                        Container {
                            layoutProperties: StackLayoutProperties {
                                spaceQuota: 1
                            }
                            verticalAlignment: VerticalAlignment.Center
                            Label {
                                text: qsTr("Barcode") + Retranslate.onLanguageChanged
                                textStyle.fontSize: FontSize.Medium
                            }
                        }
                        Container {
                            background: Color.DarkGray
                            ImageButton {
                                defaultImageSource: "asset:///images/ic_help.png"
                                preferredHeight: ui.du(7.0)
                                preferredWidth: ui.du(7.0)
                                onClicked: {
                                    var helpPageContent = helpPage.createObject();
                                    helpPageContent.title = qsTr("Barcode") + Retranslate.onLanguageChanged;
                                    helpPageContent.imageSource = "asset:///images/pass_barcode.png";
                                    helpPageContent.textContent = qsTr("You can create 3 types of barcode: QR code, EAN code and Aztec code. You can manually enter message encoded in the barcode and the barcode type or you can use scan feature (e.g. scan an existing card/coupon/etc). If it is possible and if it is nowhere on the pass, it is good to write the message to the alternate text - it is a back up in the case that scanner will be unable to scan the display of your mobile phone.") + Retranslate.onLanguageChanged;
                                    navigationPaneAddPage.push(helpPageContent);
                                }
                            }
                        }
                    }
                    Container {
                        topMargin: ui.du(1.0)
                        leftPadding: ui.du(2.5)
                        rightPadding: ui.du(2.5)
                        Container {
                            DropDown {
                                id: barcodeTypeOfAddedDropDown
                                title: qsTr("Barcode type") + Retranslate.onLanguageChanged
                                Option {
                                    imageSource: "asset:///images/ic_barcode.png"
                                    text: "QR"
                                    value: "QR"
                                    selected: true
                                }
                                Option {
                                    imageSource: "asset:///images/ic_barcode_aztec.png"
                                    text: "Aztec"
                                    value: "Aztec"
                                }
                                Option {
                                    imageSource: "asset:///images/ic_barcode_ean.png"
                                    text: "EAN"
                                    value: "EAN"
                                }
                                onSelectedIndexChanged: {
                                    if (selectedIndex == 0) {
                                        barcodeTypeOfAdded = "PKBarcodeFormatQR";
                                    } else if (selectedIndex == 1) {
                                        barcodeTypeOfAdded = "PKBarcodeFormatAztec";
                                    } else if (selectedIndex == 2) {
                                        barcodeTypeOfAdded = "PKBarcodeFormatEAN";
                                    }
                                }
                            }
                        }
                        Container {
                            topMargin: ui.du(1.0)
                            layout: StackLayout {
                                orientation: LayoutOrientation.LeftToRight
                            }
                            TextField {
                                id: barcodeMessage
                                hintText: qsTr("Information in the barcode") + Retranslate.onLanguageChanged
                            }
                            Button {
                                text: qsTr("Scan Barcode") + Retranslate.onLanguageChanged
                                maxWidth: ui.du(30.0)
                                onClicked: {
                                    var scanPageContent = scanPage.createObject();
                                    //scanPageContent.barcodeFormat = barcodeTypeOfAddedDropDown.selectedValue; // try to scan anything
                                    scanPageContent.barcodeFormat = "";
                                    scanPageContent.open();
                                }
                            }
                        }
                        Container {
                            topMargin: ui.du(1.0)
                            TextField {
                                id: barcodeAltText
                                hintText: qsTr("Alternate text") + Retranslate.onLanguageChanged
                            }
                        }
                        Divider {

                        }
                    }

                }
                attachedObjects: [
                    ComponentDefinition {
                        id: scanPage
                        source: "scan.qml"
                    },
                    ComponentDefinition {
                        id: colorPicker
                        source: "colorPicker.qml"
                    },
                    ComponentDefinition {
                        id: helpPage
                        source: "help.qml"
                    }
                ]
            }
            function onImageReady(value) {
                console.log("got imageReady signal, setting the image " + "file://" + value);
                if(value=="") {
                    value = tempLogoImageSource;   
                }
                iv.imageSource = "file://" + value;
                logoImageSource = value;
            }
            onCreationCompleted: {
                controller.imageReady.connect(addPageContent.onImageReady);
                console.log("add connected to controller.imageReady signal");
            }
        }
    }
    property string logoImageSource: "asset:///images/ic_capture_image.png"
    property string tempLogoImageSource: ""
    property string barcodeTypeOfAdded: ""
    
    property string fgColorValue: "#59595b"
    property string bgColorValue: "#f9f1de"
    property string lblColorValue: "#95969a"
}