import bb.cascades 1.3

Container {
    id: primaryField
    accessibility.name: labelText + " - " + valueText
    Container {
        visible: (valueLabel.text != "")
        accessibility.name: ""
        accessibility.description: ""
        accessibility.labelledBy: primaryField
        Label {
            id: valueLabel
            text: valueText
            textStyle {
                color: Color.create(foregroundColor)
                base: SystemDefaults.TextStyles.BigText
                fontSize: FontSize.XXLarge
                fontWeight: FontWeight.W400
            }
            accessibility.name: ""
            accessibility.description: ""
            accessibility.labelledBy: primaryField
        }
    }
    Container {
        visible: (labelLabel.text != "")
        accessibility.name: ""
        accessibility.description: ""
        accessibility.labelledBy: primaryField
        Label {
            id: labelLabel
            text: labelText
            textStyle {
                color: Color.create(labelColor)
                fontSize: FontSize.Small
                fontWeight: FontWeight.W400
            }
            accessibility.name: ""
            accessibility.description: ""
            accessibility.labelledBy: primaryField
        }
    }
    
    property string labelText
    property string valueText
    property string labelColor
    property string foregroundColor

}
