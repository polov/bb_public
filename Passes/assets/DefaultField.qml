import bb.cascades 1.3

Container {
    id: defaultField
    accessibility.name: labelText + " " + valueText
    Container {
        visible: (labelLabel.text != "")
        accessibility.name: ""
        accessibility.description: ""
        accessibility.labelledBy: defaultField
        Label {
            id: labelLabel
            text: labelText
            accessibility.name: ""
            accessibility.description: ""
            accessibility.labelledBy: defaultField
            textStyle {
                color: Color.create(labelColor)
                fontSize: FontSize.XSmall
            }
            onTextChanged: {
                defaultField.rightPadding = ui.du(3.0)
                if(labelLabel.text.length > 15) {
                    defaultField.layoutProperties = stackLayoutProperties;
                }
            }
        }
    }
    Container {
        visible: (valueLabel.text != "")
        accessibility.name: ""
        accessibility.description: ""
        accessibility.labelledBy: defaultField
        Label {
            id: valueLabel
            text: valueText
            accessibility.name: ""
            accessibility.description: ""
            accessibility.labelledBy: defaultField
            textStyle {
                color: Color.create(foregroundColor)
                fontSize: FontSize.Default
            }
            onTextChanged: {
                defaultField.rightPadding = ui.du(3.0)
                if(valueLabel.text.length > 10) {
                    defaultField.layoutProperties = stackLayoutProperties;
                }
                if(labelLabel.text.length == 0) {
                    labelLabel.text = " "; 
                }
            }
        }
    }
    
    attachedObjects: [
        StackLayoutProperties {
            id: stackLayoutProperties
            spaceQuota: 1
        }
    ]
    
    property string labelText
    property string valueText
    property string labelColor
    property string foregroundColor

}
