<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="zh_CN">
<context>
    <name>AboutSheet</name>
    <message>
        <location filename="../assets/AboutSheet.qml" line="39"/>
        <source>Passes</source>
        <translation>护照本</translation>
    </message>
    <message>
        <location filename="../assets/AboutSheet.qml" line="60"/>
        <source>version</source>
        <translation>版本</translation>
    </message>
    <message>
        <location filename="../assets/AboutSheet.qml" line="76"/>
        <source>build by</source>
        <translation>开发者 </translation>
    </message>
    <message>
        <location filename="../assets/AboutSheet.qml" line="94"/>
        <source>This application can help you managing all passbook files (.pkpass files). You can receive them by mail or download from an issuer through link.</source>
        <translation>该应用程序可以帮助您管理所有存折文件 （.pkpass 文件）。您可以通过邮件接收它们，或通过链接从发行人处下载。</translation>
    </message>
    <message>
        <location filename="../assets/AboutSheet.qml" line="113"/>
        <source>Privacy policy (English)</source>
        <translation>隐私政策 （英文）</translation>
    </message>
    <message>
        <location filename="../assets/AboutSheet.qml" line="131"/>
        <source>License agreement (English)</source>
        <translation>许可协议 （英文）</translation>
    </message>
    <message>
        <location filename="../assets/AboutSheet.qml" line="228"/>
        <source>Done</source>
        <translation>完成</translation>
    </message>
</context>
<context>
    <name>SettingsSheet</name>
    <message>
        <location filename="../assets/SettingsSheet.qml" line="8"/>
        <source>Settings</source>
        <translation>设置</translation>
    </message>
    <message>
        <location filename="../assets/SettingsSheet.qml" line="29"/>
        <source>Use location services</source>
        <translation>使用定位服务</translation>
    </message>
    <message>
        <location filename="../assets/SettingsSheet.qml" line="63"/>
        <source>There are problems with using location services. Please check permissions.</source>
        <translation>使用定位服务存在问题。请检查权限。</translation>
    </message>
    <message>
        <location filename="../assets/SettingsSheet.qml" line="73"/>
        <source>Application Permissions</source>
        <translation>应用程序权限</translation>
    </message>
    <message>
        <location filename="../assets/SettingsSheet.qml" line="91"/>
        <source>Location Services</source>
        <translation>定位服务</translation>
    </message>
    <message>
        <location filename="../assets/SettingsSheet.qml" line="50"/>
        <source>Constantly check current location and send notification when you are near any location stored in one of your pass.</source>
        <translation>当您靠近存储在您的护照之中的任何位置时，不断检查当前所在位置并发送通知，。</translation>
    </message>
    <message>
        <location filename="../assets/SettingsSheet.qml" line="112"/>
        <source>Done</source>
        <translation>完成</translation>
    </message>
</context>
<context>
    <name>add</name>
    <message>
        <location filename="../assets/add.qml" line="11"/>
        <source>Create Pass</source>
        <translation>创建护照</translation>
    </message>
    <message>
        <location filename="../assets/add.qml" line="13"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../assets/add.qml" line="21"/>
        <source>Create</source>
        <translation>创建</translation>
    </message>
    <message>
        <location filename="../assets/add.qml" line="38"/>
        <source>Pass type</source>
        <translation>护照类型</translation>
    </message>
    <message>
        <location filename="../assets/add.qml" line="40"/>
        <source>Boarding pass</source>
        <translation>登机牌</translation>
    </message>
    <message>
        <location filename="../assets/add.qml" line="44"/>
        <source>Generic pass</source>
        <translation>通用护照</translation>
    </message>
    <message>
        <location filename="../assets/add.qml" line="52"/>
        <source>Departure label</source>
        <translation>离开标签</translation>
    </message>
    <message>
        <location filename="../assets/add.qml" line="53"/>
        <source>Departure value</source>
        <translation>离开数值</translation>
    </message>
    <message>
        <location filename="../assets/add.qml" line="54"/>
        <source>Arrival label</source>
        <translation>到达标签</translation>
    </message>
    <message>
        <location filename="../assets/add.qml" line="55"/>
        <source>Arrival value</source>
        <translation>到达数值</translation>
    </message>
    <message>
        <location filename="../assets/add.qml" line="58"/>
        <location filename="../assets/add.qml" line="429"/>
        <source>Primary field label</source>
        <translation>主要字段标签</translation>
    </message>
    <message>
        <location filename="../assets/add.qml" line="59"/>
        <location filename="../assets/add.qml" line="433"/>
        <source>Primary field value</source>
        <translation>主要字段数值</translation>
    </message>
    <message>
        <location filename="../assets/add.qml" line="78"/>
        <location filename="../assets/add.qml" line="90"/>
        <location filename="../assets/add.qml" line="105"/>
        <source>Company name</source>
        <translation>公司名称</translation>
    </message>
    <message>
        <location filename="../assets/add.qml" line="93"/>
        <source>Name of the company which issued the card/coupon/pass. The name will be used in search.</source>
        <translation>公司发行卡/优惠券/护照的名称。名称将用于搜索。</translation>
    </message>
    <message>
        <location filename="../assets/add.qml" line="123"/>
        <location filename="../assets/add.qml" line="135"/>
        <source>Logo and text</source>
        <translation>标志和文本</translation>
    </message>
    <message>
        <location filename="../assets/add.qml" line="138"/>
        <source>Logo is the image on the top and is followed by the text. The text will be used in search.</source>
        <translation>标志在图片顶部,紧随其后的是文本。文本将用于搜索。。</translation>
    </message>
    <message>
        <location filename="../assets/add.qml" line="165"/>
        <source>Change image</source>
        <translation>更改图片</translation>
    </message>
    <message>
        <location filename="../assets/add.qml" line="189"/>
        <source>Text next to the logo</source>
        <translation>文本在标志旁</translation>
    </message>
    <message>
        <location filename="../assets/add.qml" line="206"/>
        <location filename="../assets/add.qml" line="218"/>
        <source>Colors</source>
        <translation>颜色</translation>
    </message>
    <message>
        <location filename="../assets/add.qml" line="221"/>
        <source>Every pass has its background color. Text color is color of field values. Label color is color of field labels that are above field values.</source>
        <translation>每一本护照都有其背景颜色。文本颜色是字段值的颜色。标签颜色是字段值上面的字段标签颜色。</translation>
    </message>
    <message>
        <location filename="../assets/add.qml" line="232"/>
        <source>Background Color</source>
        <translation>背景颜色</translation>
    </message>
    <message>
        <location filename="../assets/add.qml" line="253"/>
        <location filename="../assets/add.qml" line="290"/>
        <location filename="../assets/add.qml" line="327"/>
        <source>Change color</source>
        <translation>更改颜色</translation>
    </message>
    <message>
        <location filename="../assets/add.qml" line="269"/>
        <source>Text Color</source>
        <translation>文本颜色</translation>
    </message>
    <message>
        <location filename="../assets/add.qml" line="306"/>
        <source>Label Color</source>
        <translation>标签颜色</translation>
    </message>
    <message>
        <location filename="../assets/add.qml" line="352"/>
        <location filename="../assets/add.qml" line="364"/>
        <source>Header Field</source>
        <translation>标题字段</translation>
    </message>
    <message>
        <location filename="../assets/add.qml" line="366"/>
        <source>Header field label and text are displayed on the top of the pass and in the list of your passes. Header field label uses small letters and is above the header field value. Header fiel value uses larger letters and is under the label. You can write label &quot;POINTS&quot; and amount as the value, or in case of boarding pass &quot;TRACK&quot; and the track of the train.</source>
        <translation>标题段标签和文本显示在列表的顶部,通过和你的通行证。标题段标签使用小写字母和上面标题字段值。头事业中值使用更大的字母和标签。你可以写标签“点”和数量的值,或者在登机牌的情况下“跟踪”和火车的轨道。</translation>
    </message>
    <message>
        <location filename="../assets/add.qml" line="378"/>
        <source>Header field label</source>
        <translation>标题字段标签</translation>
    </message>
    <message>
        <location filename="../assets/add.qml" line="382"/>
        <source>Header field value</source>
        <translation>标题字段数值</translation>
    </message>
    <message>
        <location filename="../assets/add.qml" line="399"/>
        <location filename="../assets/add.qml" line="411"/>
        <source>Primary Field</source>
        <translation>主要字段</translation>
    </message>
    <message>
        <location filename="../assets/add.qml" line="417"/>
        <source>Primary Field usually contains important information, such as Member Id, Coupon Id, Member Name. It depends on the purpose of the pass. In case of boarding pass there are deptarture and arrival places with its codes as values. There are primary field label (use smaller letters) and bellow is primary field value (uses larger letters).</source>
        <translation>主要字段通常包含重要的信息,比如会员Id、优惠券Id、成员的名字。这取决于传递的目的。的登机牌有deptarture和到达的地方的代码值。有主要字段标签(用小字母)和波形基本字段值(使用较大的信件)。</translation>
    </message>
    <message>
        <location filename="../assets/add.qml" line="463"/>
        <location filename="../assets/add.qml" line="475"/>
        <source>More info</source>
        <translation>更多信息</translation>
    </message>
    <message>
        <location filename="../assets/add.qml" line="478"/>
        <source>Place for additional information that you want to have available together with the pass.</source>
        <translation>您想要可以使用该护照去往的地方的附加信息。</translation>
    </message>
    <message>
        <location filename="../assets/add.qml" line="490"/>
        <source>Additional information label</source>
        <translation>附加信息标签</translation>
    </message>
    <message>
        <location filename="../assets/add.qml" line="494"/>
        <source>Additional information</source>
        <translation>附加信息</translation>
    </message>
    <message>
        <location filename="../assets/add.qml" line="512"/>
        <location filename="../assets/add.qml" line="524"/>
        <source>Barcode</source>
        <translation>条形码</translation>
    </message>
    <message>
        <location filename="../assets/add.qml" line="526"/>
        <source>You can create 3 types of barcode: QR code, EAN code and Aztec code. You can manually enter message encoded in the barcode and the barcode type or you can use scan feature (e.g. scan an existing card/coupon/etc). If it is possible and if it is nowhere on the pass, it is good to write the message to the alternate text - it is a back up in the case that scanner will be unable to scan the display of your mobile phone.</source>
        <translation>您可以创建3个类型的条形码:二维码,EAN代码和阿兹特克的代码。您可以手动输入消息编码条码和条码类型或您可以使用扫描功能(如扫描现有卡/优惠券/等等)。如果它是可能的,如果没有通过,最好写消息给替代文本,这是一个备份在扫描仪无法扫描显示你的手机。</translation>
    </message>
    <message>
        <location filename="../assets/add.qml" line="539"/>
        <source>Barcode type</source>
        <translation>条形码类型</translation>
    </message>
    <message>
        <location filename="../assets/add.qml" line="574"/>
        <source>Information in the barcode</source>
        <translation>条形码信息</translation>
    </message>
    <message>
        <location filename="../assets/add.qml" line="577"/>
        <source>Scan Barcode</source>
        <translation>扫描条形码</translation>
    </message>
    <message>
        <location filename="../assets/add.qml" line="591"/>
        <source>Alternate text</source>
        <translation>替代文本</translation>
    </message>
</context>
<context>
    <name>barcodePage</name>
    <message>
        <location filename="../assets/barcodePage.qml" line="4"/>
        <location filename="../assets/barcodePage.qml" line="17"/>
        <source>barcode view</source>
        <translation>条形码查看</translation>
    </message>
    <message>
        <location filename="../assets/barcodePage.qml" line="12"/>
        <source>Pinch to zoom the barcode</source>
        <translation>捏合缩放条形码</translation>
    </message>
</context>
<context>
    <name>boardingPass</name>
    <message>
        <location filename="../assets/mindw80h80du/boardingPass.qml" line="261"/>
        <location filename="../assets/mindw80h80du/boardingPass.qml" line="275"/>
        <source>Barcode</source>
        <translation>条形码</translation>
    </message>
    <message>
        <location filename="../assets/mindw120h120du/boardingPass.qml" line="252"/>
        <location filename="../assets/mindw76h128du/boardingPass.qml" line="252"/>
        <location filename="../assets/mindw80h80du/boardingPass.qml" line="293"/>
        <source>Info</source>
        <translation>信息</translation>
    </message>
    <message>
        <location filename="../assets/mindw80h80du/boardingPass.qml" line="261"/>
        <location filename="../assets/mindw80h80du/boardingPass.qml" line="265"/>
        <source>Content</source>
        <translation>内容</translation>
    </message>
    <message>
        <location filename="../assets/mindw120h120du/boardingPass.qml" line="6"/>
        <location filename="../assets/mindw76h128du/boardingPass.qml" line="6"/>
        <location filename="../assets/mindw80h80du/boardingPass.qml" line="6"/>
        <source>boarding pass</source>
        <translation>登机牌</translation>
    </message>
    <message>
        <location filename="../assets/mindw120h120du/boardingPass.qml" line="186"/>
        <location filename="../assets/mindw76h128du/boardingPass.qml" line="186"/>
        <location filename="../assets/mindw80h80du/boardingPass.qml" line="189"/>
        <source>barcode</source>
        <translation>条形码</translation>
    </message>
    <message>
        <location filename="../assets/mindw120h120du/boardingPass.qml" line="194"/>
        <location filename="../assets/mindw76h128du/boardingPass.qml" line="194"/>
        <location filename="../assets/mindw80h80du/boardingPass.qml" line="197"/>
        <source>barcode alternate text: </source>
        <translation>条形码替代文本： </translation>
    </message>
    <message>
        <location filename="../assets/mindw120h120du/boardingPass.qml" line="244"/>
        <location filename="../assets/mindw76h128du/boardingPass.qml" line="244"/>
        <location filename="../assets/mindw80h80du/boardingPass.qml" line="246"/>
        <source>Barcode page</source>
        <translation>条形码页面</translation>
    </message>
    <message>
        <location filename="../assets/mindw120h120du/boardingPass.qml" line="262"/>
        <location filename="../assets/mindw76h128du/boardingPass.qml" line="262"/>
        <location filename="../assets/mindw80h80du/boardingPass.qml" line="308"/>
        <source>Brightness</source>
        <translation>亮度</translation>
    </message>
    <message>
        <location filename="../assets/mindw80h80du/boardingPass.qml" line="254"/>
        <source>p</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../assets/mindw80h80du/boardingPass.qml" line="288"/>
        <source>r</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../assets/mindw80h80du/boardingPass.qml" line="303"/>
        <source>d</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>boardingPassCard</name>
    <message>
        <location filename="../assets/mindw120h120du/boardingPassCard.qml" line="263"/>
        <location filename="../assets/mindw76h128du/boardingPassCard.qml" line="263"/>
        <location filename="../assets/mindw80h80du/boardingPassCard.qml" line="304"/>
        <source>Import</source>
        <translation>导入</translation>
    </message>
    <message>
        <location filename="../assets/mindw80h80du/boardingPassCard.qml" line="271"/>
        <location filename="../assets/mindw80h80du/boardingPassCard.qml" line="285"/>
        <source>Barcode</source>
        <translation>条形码</translation>
    </message>
    <message>
        <location filename="../assets/mindw120h120du/boardingPassCard.qml" line="271"/>
        <location filename="../assets/mindw76h128du/boardingPassCard.qml" line="271"/>
        <location filename="../assets/mindw80h80du/boardingPassCard.qml" line="317"/>
        <source>Info</source>
        <translation>信息</translation>
    </message>
    <message>
        <location filename="../assets/mindw80h80du/boardingPassCard.qml" line="271"/>
        <location filename="../assets/mindw80h80du/boardingPassCard.qml" line="275"/>
        <source>Content</source>
        <translation>内容</translation>
    </message>
    <message>
        <location filename="../assets/mindw120h120du/boardingPassCard.qml" line="432"/>
        <location filename="../assets/mindw76h128du/boardingPassCard.qml" line="432"/>
        <location filename="../assets/mindw80h80du/boardingPassCard.qml" line="484"/>
        <source>Can&apos;t add selected pass</source>
        <translation>无法添加选定护照</translation>
    </message>
    <message>
        <location filename="../assets/mindw120h120du/boardingPassCard.qml" line="436"/>
        <location filename="../assets/mindw76h128du/boardingPassCard.qml" line="436"/>
        <location filename="../assets/mindw80h80du/boardingPassCard.qml" line="488"/>
        <source>Pass added successfuly</source>
        <translation>护照已成功添加</translation>
    </message>
    <message>
        <location filename="../assets/mindw120h120du/boardingPassCard.qml" line="438"/>
        <location filename="../assets/mindw76h128du/boardingPassCard.qml" line="438"/>
        <location filename="../assets/mindw80h80du/boardingPassCard.qml" line="490"/>
        <source>Pass updated successfuly</source>
        <translation>护照已成功更新</translation>
    </message>
    <message>
        <location filename="../assets/mindw120h120du/boardingPassCard.qml" line="440"/>
        <location filename="../assets/mindw76h128du/boardingPassCard.qml" line="440"/>
        <location filename="../assets/mindw80h80du/boardingPassCard.qml" line="492"/>
        <source>Pass operation finished successfuly</source>
        <translation>护照操作已成功完成</translation>
    </message>
    <message>
        <location filename="../assets/mindw120h120du/boardingPassCard.qml" line="9"/>
        <location filename="../assets/mindw76h128du/boardingPassCard.qml" line="9"/>
        <location filename="../assets/mindw80h80du/boardingPassCard.qml" line="9"/>
        <source>boarding pass</source>
        <translation>登机牌</translation>
    </message>
    <message>
        <location filename="../assets/mindw120h120du/boardingPassCard.qml" line="189"/>
        <location filename="../assets/mindw76h128du/boardingPassCard.qml" line="189"/>
        <location filename="../assets/mindw80h80du/boardingPassCard.qml" line="192"/>
        <source>barcode</source>
        <translation>条形码</translation>
    </message>
    <message>
        <location filename="../assets/mindw120h120du/boardingPassCard.qml" line="197"/>
        <location filename="../assets/mindw76h128du/boardingPassCard.qml" line="197"/>
        <location filename="../assets/mindw80h80du/boardingPassCard.qml" line="200"/>
        <source>barcode alternate text: </source>
        <translation>条形码替代文本： </translation>
    </message>
    <message>
        <location filename="../assets/mindw120h120du/boardingPassCard.qml" line="254"/>
        <location filename="../assets/mindw76h128du/boardingPassCard.qml" line="254"/>
        <location filename="../assets/mindw80h80du/boardingPassCard.qml" line="256"/>
        <source>Barcode page</source>
        <translation>条形码页面</translation>
    </message>
    <message>
        <location filename="../assets/mindw80h80du/boardingPassCard.qml" line="264"/>
        <source>p</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../assets/mindw80h80du/boardingPassCard.qml" line="298"/>
        <source>r</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../assets/mindw80h80du/boardingPassCard.qml" line="312"/>
        <source>i</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../assets/mindw80h80du/boardingPassCard.qml" line="327"/>
        <source>d</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../assets/mindw120h120du/boardingPassCard.qml" line="281"/>
        <location filename="../assets/mindw76h128du/boardingPassCard.qml" line="281"/>
        <location filename="../assets/mindw80h80du/boardingPassCard.qml" line="332"/>
        <source>Brightness</source>
        <translation>亮度</translation>
    </message>
</context>
<context>
    <name>colorPicker</name>
    <message>
        <location filename="../assets/colorPicker.qml" line="7"/>
        <source>Color</source>
        <translation>颜色</translation>
    </message>
    <message>
        <location filename="../assets/colorPicker.qml" line="9"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../assets/colorPicker.qml" line="23"/>
        <source>Choose</source>
        <translation>选择</translation>
    </message>
    <message>
        <location filename="../assets/colorPicker.qml" line="42"/>
        <source>Selected color</source>
        <translation>已选定颜色</translation>
    </message>
</context>
<context>
    <name>coupon</name>
    <message>
        <location filename="../assets/mindw80h80du/coupon.qml" line="205"/>
        <location filename="../assets/mindw80h80du/coupon.qml" line="218"/>
        <source>Barcode</source>
        <translation>条形码</translation>
    </message>
    <message>
        <location filename="../assets/mindw120h120du/coupon.qml" line="196"/>
        <location filename="../assets/mindw76h128du/coupon.qml" line="196"/>
        <location filename="../assets/mindw80h80du/coupon.qml" line="235"/>
        <source>Info</source>
        <translation>信息</translation>
    </message>
    <message>
        <location filename="../assets/mindw80h80du/coupon.qml" line="205"/>
        <location filename="../assets/mindw80h80du/coupon.qml" line="209"/>
        <source>Content</source>
        <translation>内容</translation>
    </message>
    <message>
        <location filename="../assets/mindw120h120du/coupon.qml" line="5"/>
        <location filename="../assets/mindw76h128du/coupon.qml" line="5"/>
        <location filename="../assets/mindw80h80du/coupon.qml" line="5"/>
        <source>coupon</source>
        <translation>优惠券</translation>
    </message>
    <message>
        <location filename="../assets/mindw120h120du/coupon.qml" line="136"/>
        <location filename="../assets/mindw76h128du/coupon.qml" line="136"/>
        <location filename="../assets/mindw80h80du/coupon.qml" line="138"/>
        <source>barcode</source>
        <translation>条形码</translation>
    </message>
    <message>
        <location filename="../assets/mindw120h120du/coupon.qml" line="144"/>
        <location filename="../assets/mindw76h128du/coupon.qml" line="144"/>
        <location filename="../assets/mindw80h80du/coupon.qml" line="146"/>
        <source>barcode alternate text: </source>
        <translation>条形码替代文本： </translation>
    </message>
    <message>
        <location filename="../assets/mindw120h120du/coupon.qml" line="188"/>
        <location filename="../assets/mindw76h128du/coupon.qml" line="188"/>
        <location filename="../assets/mindw80h80du/coupon.qml" line="190"/>
        <source>Barcode page</source>
        <translation>条形码页面</translation>
    </message>
    <message>
        <location filename="../assets/mindw80h80du/coupon.qml" line="198"/>
        <source>p</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../assets/mindw80h80du/coupon.qml" line="230"/>
        <source>r</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../assets/mindw80h80du/coupon.qml" line="245"/>
        <source>d</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../assets/mindw120h120du/coupon.qml" line="206"/>
        <location filename="../assets/mindw76h128du/coupon.qml" line="206"/>
        <location filename="../assets/mindw80h80du/coupon.qml" line="250"/>
        <source>Brightness</source>
        <translation>亮度</translation>
    </message>
</context>
<context>
    <name>couponCard</name>
    <message>
        <location filename="../assets/mindw120h120du/couponCard.qml" line="207"/>
        <location filename="../assets/mindw76h128du/couponCard.qml" line="207"/>
        <location filename="../assets/mindw80h80du/couponCard.qml" line="245"/>
        <source>Import</source>
        <translation>导入</translation>
    </message>
    <message>
        <location filename="../assets/mindw80h80du/couponCard.qml" line="214"/>
        <location filename="../assets/mindw80h80du/couponCard.qml" line="227"/>
        <source>Barcode</source>
        <translation>条形码</translation>
    </message>
    <message>
        <location filename="../assets/mindw120h120du/couponCard.qml" line="215"/>
        <location filename="../assets/mindw76h128du/couponCard.qml" line="215"/>
        <location filename="../assets/mindw80h80du/couponCard.qml" line="258"/>
        <source>Info</source>
        <translation>信息</translation>
    </message>
    <message>
        <location filename="../assets/mindw80h80du/couponCard.qml" line="214"/>
        <location filename="../assets/mindw80h80du/couponCard.qml" line="218"/>
        <source>Content</source>
        <translation>内容</translation>
    </message>
    <message>
        <location filename="../assets/mindw120h120du/couponCard.qml" line="357"/>
        <location filename="../assets/mindw76h128du/couponCard.qml" line="357"/>
        <location filename="../assets/mindw80h80du/couponCard.qml" line="406"/>
        <source>Can&apos;t add selected pass</source>
        <translation>无法添加选定护照</translation>
    </message>
    <message>
        <location filename="../assets/mindw120h120du/couponCard.qml" line="361"/>
        <location filename="../assets/mindw76h128du/couponCard.qml" line="361"/>
        <location filename="../assets/mindw80h80du/couponCard.qml" line="410"/>
        <source>Pass added successfuly</source>
        <translation>护照已成功添加</translation>
    </message>
    <message>
        <location filename="../assets/mindw120h120du/couponCard.qml" line="363"/>
        <location filename="../assets/mindw76h128du/couponCard.qml" line="363"/>
        <location filename="../assets/mindw80h80du/couponCard.qml" line="412"/>
        <source>Pass updated successfuly</source>
        <translation>护照已成功更新</translation>
    </message>
    <message>
        <location filename="../assets/mindw120h120du/couponCard.qml" line="365"/>
        <location filename="../assets/mindw76h128du/couponCard.qml" line="365"/>
        <location filename="../assets/mindw80h80du/couponCard.qml" line="414"/>
        <source>Pass operation finished successfuly</source>
        <translation>护照操作已成功完成</translation>
    </message>
    <message>
        <location filename="../assets/mindw120h120du/couponCard.qml" line="9"/>
        <location filename="../assets/mindw76h128du/couponCard.qml" line="9"/>
        <location filename="../assets/mindw80h80du/couponCard.qml" line="9"/>
        <source>coupon</source>
        <translation>优惠券</translation>
    </message>
    <message>
        <location filename="../assets/mindw120h120du/couponCard.qml" line="139"/>
        <location filename="../assets/mindw76h128du/couponCard.qml" line="139"/>
        <location filename="../assets/mindw80h80du/couponCard.qml" line="141"/>
        <source>barcode</source>
        <translation>条形码</translation>
    </message>
    <message>
        <location filename="../assets/mindw120h120du/couponCard.qml" line="147"/>
        <location filename="../assets/mindw76h128du/couponCard.qml" line="147"/>
        <location filename="../assets/mindw80h80du/couponCard.qml" line="149"/>
        <source>barcode alternate text: </source>
        <translation>条形码替代文本： </translation>
    </message>
    <message>
        <location filename="../assets/mindw120h120du/couponCard.qml" line="198"/>
        <location filename="../assets/mindw76h128du/couponCard.qml" line="198"/>
        <location filename="../assets/mindw80h80du/couponCard.qml" line="199"/>
        <source>Barcode page</source>
        <translation>条形码页面</translation>
    </message>
    <message>
        <location filename="../assets/mindw80h80du/couponCard.qml" line="207"/>
        <source>p</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../assets/mindw80h80du/couponCard.qml" line="239"/>
        <source>r</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../assets/mindw80h80du/couponCard.qml" line="253"/>
        <source>i</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../assets/mindw80h80du/couponCard.qml" line="268"/>
        <source>d</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../assets/mindw120h120du/couponCard.qml" line="225"/>
        <location filename="../assets/mindw76h128du/couponCard.qml" line="225"/>
        <location filename="../assets/mindw80h80du/couponCard.qml" line="273"/>
        <source>Brightness</source>
        <translation>亮度</translation>
    </message>
</context>
<context>
    <name>event</name>
    <message>
        <location filename="../assets/mindw80h80du/event.qml" line="224"/>
        <location filename="../assets/mindw80h80du/event.qml" line="236"/>
        <source>Barcode</source>
        <translation>条形码</translation>
    </message>
    <message>
        <location filename="../assets/mindw120h120du/event.qml" line="215"/>
        <location filename="../assets/mindw76h128du/event.qml" line="215"/>
        <location filename="../assets/mindw80h80du/event.qml" line="252"/>
        <source>Info</source>
        <translation>信息</translation>
    </message>
    <message>
        <location filename="../assets/mindw80h80du/event.qml" line="224"/>
        <location filename="../assets/mindw80h80du/event.qml" line="228"/>
        <source>Content</source>
        <translation>内容</translation>
    </message>
    <message>
        <location filename="../assets/mindw120h120du/event.qml" line="5"/>
        <location filename="../assets/mindw76h128du/event.qml" line="5"/>
        <location filename="../assets/mindw80h80du/event.qml" line="5"/>
        <source>event ticket</source>
        <translation>入场券</translation>
    </message>
    <message>
        <location filename="../assets/mindw120h120du/event.qml" line="149"/>
        <location filename="../assets/mindw76h128du/event.qml" line="149"/>
        <location filename="../assets/mindw80h80du/event.qml" line="151"/>
        <source>barcode</source>
        <translation>条形码</translation>
    </message>
    <message>
        <location filename="../assets/mindw120h120du/event.qml" line="157"/>
        <location filename="../assets/mindw76h128du/event.qml" line="157"/>
        <location filename="../assets/mindw80h80du/event.qml" line="159"/>
        <source>barcode alternate text: </source>
        <translation>条形码替代文本： </translation>
    </message>
    <message>
        <location filename="../assets/mindw120h120du/event.qml" line="207"/>
        <location filename="../assets/mindw76h128du/event.qml" line="207"/>
        <location filename="../assets/mindw80h80du/event.qml" line="209"/>
        <source>Barcode page</source>
        <translation>条形码页面</translation>
    </message>
    <message>
        <location filename="../assets/mindw80h80du/event.qml" line="217"/>
        <source>p</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../assets/mindw80h80du/event.qml" line="247"/>
        <source>r</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../assets/mindw80h80du/event.qml" line="262"/>
        <source>d</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../assets/mindw120h120du/event.qml" line="225"/>
        <location filename="../assets/mindw76h128du/event.qml" line="225"/>
        <location filename="../assets/mindw80h80du/event.qml" line="267"/>
        <source>Brightness</source>
        <translation>亮度</translation>
    </message>
</context>
<context>
    <name>eventCard</name>
    <message>
        <location filename="../assets/mindw120h120du/eventCard.qml" line="222"/>
        <location filename="../assets/mindw76h128du/eventCard.qml" line="222"/>
        <location filename="../assets/mindw80h80du/eventCard.qml" line="259"/>
        <source>Import</source>
        <translation>导入</translation>
    </message>
    <message>
        <location filename="../assets/mindw80h80du/eventCard.qml" line="230"/>
        <location filename="../assets/mindw80h80du/eventCard.qml" line="242"/>
        <source>Barcode</source>
        <translation>条形码</translation>
    </message>
    <message>
        <location filename="../assets/mindw120h120du/eventCard.qml" line="230"/>
        <location filename="../assets/mindw76h128du/eventCard.qml" line="230"/>
        <location filename="../assets/mindw80h80du/eventCard.qml" line="272"/>
        <source>Info</source>
        <translation>信息</translation>
    </message>
    <message>
        <location filename="../assets/mindw80h80du/eventCard.qml" line="230"/>
        <location filename="../assets/mindw80h80du/eventCard.qml" line="234"/>
        <source>Content</source>
        <translation>内容</translation>
    </message>
    <message>
        <location filename="../assets/mindw120h120du/eventCard.qml" line="375"/>
        <location filename="../assets/mindw76h128du/eventCard.qml" line="375"/>
        <location filename="../assets/mindw80h80du/eventCard.qml" line="423"/>
        <source>Can&apos;t add selected pass</source>
        <translation>无法添加选定护照</translation>
    </message>
    <message>
        <location filename="../assets/mindw120h120du/eventCard.qml" line="379"/>
        <location filename="../assets/mindw76h128du/eventCard.qml" line="379"/>
        <location filename="../assets/mindw80h80du/eventCard.qml" line="427"/>
        <source>Pass added successfuly</source>
        <translation>护照已成功添加</translation>
    </message>
    <message>
        <location filename="../assets/mindw120h120du/eventCard.qml" line="381"/>
        <location filename="../assets/mindw76h128du/eventCard.qml" line="381"/>
        <location filename="../assets/mindw80h80du/eventCard.qml" line="429"/>
        <source>Pass updated successfuly</source>
        <translation>护照已成功更新</translation>
    </message>
    <message>
        <location filename="../assets/mindw120h120du/eventCard.qml" line="383"/>
        <location filename="../assets/mindw76h128du/eventCard.qml" line="383"/>
        <location filename="../assets/mindw80h80du/eventCard.qml" line="431"/>
        <source>Pass operation finished successfuly</source>
        <translation>护照操作已成功完成</translation>
    </message>
    <message>
        <location filename="../assets/mindw120h120du/eventCard.qml" line="9"/>
        <location filename="../assets/mindw76h128du/eventCard.qml" line="9"/>
        <location filename="../assets/mindw80h80du/eventCard.qml" line="9"/>
        <source>event ticket</source>
        <translation>入场券</translation>
    </message>
    <message>
        <location filename="../assets/mindw120h120du/eventCard.qml" line="148"/>
        <location filename="../assets/mindw76h128du/eventCard.qml" line="148"/>
        <location filename="../assets/mindw80h80du/eventCard.qml" line="150"/>
        <source>barcode</source>
        <translation>条形码</translation>
    </message>
    <message>
        <location filename="../assets/mindw120h120du/eventCard.qml" line="156"/>
        <location filename="../assets/mindw76h128du/eventCard.qml" line="156"/>
        <location filename="../assets/mindw80h80du/eventCard.qml" line="158"/>
        <source>barcode alternate text: </source>
        <translation>条形码替代文本： </translation>
    </message>
    <message>
        <location filename="../assets/mindw120h120du/eventCard.qml" line="213"/>
        <location filename="../assets/mindw76h128du/eventCard.qml" line="213"/>
        <location filename="../assets/mindw80h80du/eventCard.qml" line="215"/>
        <source>Barcode page</source>
        <translation>条形码页面</translation>
    </message>
    <message>
        <location filename="../assets/mindw80h80du/eventCard.qml" line="223"/>
        <source>p</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../assets/mindw80h80du/eventCard.qml" line="253"/>
        <source>r</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../assets/mindw80h80du/eventCard.qml" line="267"/>
        <source>i</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../assets/mindw80h80du/eventCard.qml" line="282"/>
        <source>d</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../assets/mindw120h120du/eventCard.qml" line="240"/>
        <location filename="../assets/mindw76h128du/eventCard.qml" line="240"/>
        <location filename="../assets/mindw80h80du/eventCard.qml" line="287"/>
        <source>Brightness</source>
        <translation>亮度</translation>
    </message>
</context>
<context>
    <name>eventStrip</name>
    <message>
        <location filename="../assets/mindw80h80du/eventStrip.qml" line="208"/>
        <location filename="../assets/mindw80h80du/eventStrip.qml" line="221"/>
        <source>Barcode</source>
        <translation>条形码</translation>
    </message>
    <message>
        <location filename="../assets/mindw120h120du/eventStrip.qml" line="200"/>
        <location filename="../assets/mindw76h128du/eventStrip.qml" line="200"/>
        <location filename="../assets/mindw80h80du/eventStrip.qml" line="238"/>
        <source>Info</source>
        <translation>信息</translation>
    </message>
    <message>
        <location filename="../assets/mindw80h80du/eventStrip.qml" line="208"/>
        <location filename="../assets/mindw80h80du/eventStrip.qml" line="212"/>
        <source>Content</source>
        <translation>内容</translation>
    </message>
    <message>
        <location filename="../assets/mindw120h120du/eventStrip.qml" line="7"/>
        <location filename="../assets/mindw76h128du/eventStrip.qml" line="7"/>
        <location filename="../assets/mindw80h80du/eventStrip.qml" line="7"/>
        <source>event ticket</source>
        <translation>入场券</translation>
    </message>
    <message>
        <location filename="../assets/mindw120h120du/eventStrip.qml" line="140"/>
        <location filename="../assets/mindw76h128du/eventStrip.qml" line="140"/>
        <location filename="../assets/mindw80h80du/eventStrip.qml" line="142"/>
        <source>barcode</source>
        <translation>条形码</translation>
    </message>
    <message>
        <location filename="../assets/mindw120h120du/eventStrip.qml" line="148"/>
        <location filename="../assets/mindw76h128du/eventStrip.qml" line="148"/>
        <location filename="../assets/mindw80h80du/eventStrip.qml" line="150"/>
        <source>barcode alternate text: </source>
        <translation>条形码替代文本： </translation>
    </message>
    <message>
        <location filename="../assets/mindw120h120du/eventStrip.qml" line="192"/>
        <location filename="../assets/mindw76h128du/eventStrip.qml" line="192"/>
        <location filename="../assets/mindw80h80du/eventStrip.qml" line="193"/>
        <source>Barcode page</source>
        <translation>条形码页面</translation>
    </message>
    <message>
        <location filename="../assets/mindw80h80du/eventStrip.qml" line="201"/>
        <source>p</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../assets/mindw80h80du/eventStrip.qml" line="233"/>
        <source>r</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../assets/mindw80h80du/eventStrip.qml" line="248"/>
        <source>d</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../assets/mindw120h120du/eventStrip.qml" line="210"/>
        <location filename="../assets/mindw76h128du/eventStrip.qml" line="210"/>
        <location filename="../assets/mindw80h80du/eventStrip.qml" line="253"/>
        <source>Brightness</source>
        <translation>亮度</translation>
    </message>
</context>
<context>
    <name>eventStripCard</name>
    <message>
        <location filename="../assets/mindw120h120du/eventStripCard.qml" line="213"/>
        <location filename="../assets/mindw76h128du/eventStripCard.qml" line="213"/>
        <location filename="../assets/mindw80h80du/eventStripCard.qml" line="249"/>
        <source>Import</source>
        <translation>导入</translation>
    </message>
    <message>
        <location filename="../assets/mindw80h80du/eventStripCard.qml" line="218"/>
        <location filename="../assets/mindw80h80du/eventStripCard.qml" line="231"/>
        <source>Barcode</source>
        <translation>条形码</translation>
    </message>
    <message>
        <location filename="../assets/mindw120h120du/eventStripCard.qml" line="221"/>
        <location filename="../assets/mindw76h128du/eventStripCard.qml" line="221"/>
        <location filename="../assets/mindw80h80du/eventStripCard.qml" line="262"/>
        <source>Info</source>
        <translation>信息</translation>
    </message>
    <message>
        <location filename="../assets/mindw80h80du/eventStripCard.qml" line="218"/>
        <location filename="../assets/mindw80h80du/eventStripCard.qml" line="222"/>
        <source>Content</source>
        <translation>内容</translation>
    </message>
    <message>
        <location filename="../assets/mindw120h120du/eventStripCard.qml" line="364"/>
        <location filename="../assets/mindw76h128du/eventStripCard.qml" line="364"/>
        <location filename="../assets/mindw80h80du/eventStripCard.qml" line="411"/>
        <source>Can&apos;t add selected pass</source>
        <translation>无法添加选定护照</translation>
    </message>
    <message>
        <location filename="../assets/mindw120h120du/eventStripCard.qml" line="368"/>
        <location filename="../assets/mindw76h128du/eventStripCard.qml" line="368"/>
        <location filename="../assets/mindw80h80du/eventStripCard.qml" line="415"/>
        <source>Pass added successfuly</source>
        <translation>护照已成功添加</translation>
    </message>
    <message>
        <location filename="../assets/mindw120h120du/eventStripCard.qml" line="370"/>
        <location filename="../assets/mindw76h128du/eventStripCard.qml" line="370"/>
        <location filename="../assets/mindw80h80du/eventStripCard.qml" line="417"/>
        <source>Pass updated successfuly</source>
        <translation>护照已成功更新</translation>
    </message>
    <message>
        <location filename="../assets/mindw120h120du/eventStripCard.qml" line="372"/>
        <location filename="../assets/mindw76h128du/eventStripCard.qml" line="372"/>
        <location filename="../assets/mindw80h80du/eventStripCard.qml" line="419"/>
        <source>Pass operation finished successfuly</source>
        <translation>护照操作已成功完成</translation>
    </message>
    <message>
        <location filename="../assets/mindw120h120du/eventStripCard.qml" line="9"/>
        <location filename="../assets/mindw76h128du/eventStripCard.qml" line="9"/>
        <location filename="../assets/mindw80h80du/eventStripCard.qml" line="9"/>
        <source>event ticket</source>
        <translation>入场券</translation>
    </message>
    <message>
        <location filename="../assets/mindw120h120du/eventStripCard.qml" line="145"/>
        <location filename="../assets/mindw76h128du/eventStripCard.qml" line="145"/>
        <location filename="../assets/mindw80h80du/eventStripCard.qml" line="144"/>
        <source>barcode</source>
        <translation>条形码</translation>
    </message>
    <message>
        <location filename="../assets/mindw120h120du/eventStripCard.qml" line="153"/>
        <location filename="../assets/mindw76h128du/eventStripCard.qml" line="153"/>
        <location filename="../assets/mindw80h80du/eventStripCard.qml" line="152"/>
        <source>barcode alternate text: </source>
        <translation>条形码替代文本： </translation>
    </message>
    <message>
        <location filename="../assets/mindw120h120du/eventStripCard.qml" line="204"/>
        <location filename="../assets/mindw76h128du/eventStripCard.qml" line="204"/>
        <location filename="../assets/mindw80h80du/eventStripCard.qml" line="203"/>
        <source>Barcode page</source>
        <translation>条形码页面</translation>
    </message>
    <message>
        <location filename="../assets/mindw80h80du/eventStripCard.qml" line="211"/>
        <source>p</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../assets/mindw80h80du/eventStripCard.qml" line="243"/>
        <source>r</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../assets/mindw80h80du/eventStripCard.qml" line="257"/>
        <source>i</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../assets/mindw80h80du/eventStripCard.qml" line="272"/>
        <source>d</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../assets/mindw120h120du/eventStripCard.qml" line="231"/>
        <location filename="../assets/mindw76h128du/eventStripCard.qml" line="231"/>
        <location filename="../assets/mindw80h80du/eventStripCard.qml" line="277"/>
        <source>Brightness</source>
        <translation>亮度</translation>
    </message>
</context>
<context>
    <name>generic</name>
    <message>
        <location filename="../assets/mindw80h80du/generic.qml" line="229"/>
        <location filename="../assets/mindw80h80du/generic.qml" line="242"/>
        <source>Barcode</source>
        <translation>条形码</translation>
    </message>
    <message>
        <location filename="../assets/mindw120h120du/generic.qml" line="219"/>
        <location filename="../assets/mindw76h128du/generic.qml" line="219"/>
        <location filename="../assets/mindw80h80du/generic.qml" line="259"/>
        <source>Info</source>
        <translation>信息</translation>
    </message>
    <message>
        <location filename="../assets/mindw80h80du/generic.qml" line="229"/>
        <location filename="../assets/mindw80h80du/generic.qml" line="233"/>
        <source>Content</source>
        <translation>内容</translation>
    </message>
    <message>
        <location filename="../assets/mindw120h120du/generic.qml" line="5"/>
        <location filename="../assets/mindw76h128du/generic.qml" line="5"/>
        <location filename="../assets/mindw80h80du/generic.qml" line="5"/>
        <source>pass</source>
        <translation>护照</translation>
    </message>
    <message>
        <location filename="../assets/mindw120h120du/generic.qml" line="153"/>
        <location filename="../assets/mindw76h128du/generic.qml" line="153"/>
        <location filename="../assets/mindw80h80du/generic.qml" line="156"/>
        <source>barcode</source>
        <translation>条形码</translation>
    </message>
    <message>
        <location filename="../assets/mindw120h120du/generic.qml" line="161"/>
        <location filename="../assets/mindw76h128du/generic.qml" line="161"/>
        <location filename="../assets/mindw80h80du/generic.qml" line="164"/>
        <source>barcode alternate text: </source>
        <translation>条形码替代文本： </translation>
    </message>
    <message>
        <location filename="../assets/mindw120h120du/generic.qml" line="211"/>
        <location filename="../assets/mindw76h128du/generic.qml" line="211"/>
        <location filename="../assets/mindw80h80du/generic.qml" line="214"/>
        <source>Barcode page</source>
        <translation>条形码页面</translation>
    </message>
    <message>
        <location filename="../assets/mindw80h80du/generic.qml" line="222"/>
        <source>p</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../assets/mindw80h80du/generic.qml" line="254"/>
        <source>r</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../assets/mindw80h80du/generic.qml" line="269"/>
        <source>d</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../assets/mindw120h120du/generic.qml" line="229"/>
        <location filename="../assets/mindw76h128du/generic.qml" line="229"/>
        <location filename="../assets/mindw80h80du/generic.qml" line="274"/>
        <source>Brightness</source>
        <translation>亮度</translation>
    </message>
</context>
<context>
    <name>genericCard</name>
    <message>
        <location filename="../assets/mindw120h120du/genericCard.qml" line="231"/>
        <location filename="../assets/mindw76h128du/genericCard.qml" line="231"/>
        <location filename="../assets/mindw80h80du/genericCard.qml" line="271"/>
        <source>Import</source>
        <translation>导入</translation>
    </message>
    <message>
        <location filename="../assets/mindw80h80du/genericCard.qml" line="240"/>
        <location filename="../assets/mindw80h80du/genericCard.qml" line="253"/>
        <source>Barcode</source>
        <translation>条形码</translation>
    </message>
    <message>
        <location filename="../assets/mindw120h120du/genericCard.qml" line="239"/>
        <location filename="../assets/mindw76h128du/genericCard.qml" line="239"/>
        <location filename="../assets/mindw80h80du/genericCard.qml" line="284"/>
        <source>Info</source>
        <translation>信息</translation>
    </message>
    <message>
        <location filename="../assets/mindw80h80du/genericCard.qml" line="240"/>
        <location filename="../assets/mindw80h80du/genericCard.qml" line="244"/>
        <source>Content</source>
        <translation>内容</translation>
    </message>
    <message>
        <location filename="../assets/mindw120h120du/genericCard.qml" line="384"/>
        <location filename="../assets/mindw76h128du/genericCard.qml" line="384"/>
        <location filename="../assets/mindw80h80du/genericCard.qml" line="435"/>
        <source>Can&apos;t add selected pass</source>
        <translation>无法添加选定护照</translation>
    </message>
    <message>
        <location filename="../assets/mindw120h120du/genericCard.qml" line="388"/>
        <location filename="../assets/mindw76h128du/genericCard.qml" line="388"/>
        <location filename="../assets/mindw80h80du/genericCard.qml" line="439"/>
        <source>Pass added successfuly</source>
        <translation>护照已成功添加</translation>
    </message>
    <message>
        <location filename="../assets/mindw120h120du/genericCard.qml" line="390"/>
        <location filename="../assets/mindw76h128du/genericCard.qml" line="390"/>
        <location filename="../assets/mindw80h80du/genericCard.qml" line="441"/>
        <source>Pass updated successfuly</source>
        <translation>护照已成功更新</translation>
    </message>
    <message>
        <location filename="../assets/mindw120h120du/genericCard.qml" line="392"/>
        <location filename="../assets/mindw76h128du/genericCard.qml" line="392"/>
        <location filename="../assets/mindw80h80du/genericCard.qml" line="443"/>
        <source>Pass operation finished successfuly</source>
        <translation>护照操作已成功完成</translation>
    </message>
    <message>
        <location filename="../assets/mindw120h120du/genericCard.qml" line="9"/>
        <location filename="../assets/mindw76h128du/genericCard.qml" line="9"/>
        <location filename="../assets/mindw80h80du/genericCard.qml" line="9"/>
        <source>pass</source>
        <translation>护照</translation>
    </message>
    <message>
        <location filename="../assets/mindw120h120du/genericCard.qml" line="157"/>
        <location filename="../assets/mindw76h128du/genericCard.qml" line="157"/>
        <location filename="../assets/mindw80h80du/genericCard.qml" line="160"/>
        <source>barcode</source>
        <translation>条形码</translation>
    </message>
    <message>
        <location filename="../assets/mindw120h120du/genericCard.qml" line="165"/>
        <location filename="../assets/mindw76h128du/genericCard.qml" line="165"/>
        <location filename="../assets/mindw80h80du/genericCard.qml" line="168"/>
        <source>barcode alternate text: </source>
        <translation>条形码替代文本： </translation>
    </message>
    <message>
        <location filename="../assets/mindw120h120du/genericCard.qml" line="222"/>
        <location filename="../assets/mindw76h128du/genericCard.qml" line="222"/>
        <location filename="../assets/mindw80h80du/genericCard.qml" line="225"/>
        <source>Barcode page</source>
        <translation>条形码页面</translation>
    </message>
    <message>
        <location filename="../assets/mindw80h80du/genericCard.qml" line="233"/>
        <source>p</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../assets/mindw80h80du/genericCard.qml" line="265"/>
        <source>r</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../assets/mindw80h80du/genericCard.qml" line="279"/>
        <source>i</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../assets/mindw80h80du/genericCard.qml" line="294"/>
        <source>d</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../assets/mindw120h120du/genericCard.qml" line="249"/>
        <location filename="../assets/mindw76h128du/genericCard.qml" line="249"/>
        <location filename="../assets/mindw80h80du/genericCard.qml" line="299"/>
        <source>Brightness</source>
        <translation>亮度</translation>
    </message>
</context>
<context>
    <name>genericSquare</name>
    <message>
        <location filename="../assets/mindw80h80du/genericSquare.qml" line="235"/>
        <location filename="../assets/mindw80h80du/genericSquare.qml" line="247"/>
        <source>Barcode</source>
        <translation>条形码</translation>
    </message>
    <message>
        <location filename="../assets/mindw120h120du/genericSquare.qml" line="219"/>
        <location filename="../assets/mindw76h128du/genericSquare.qml" line="219"/>
        <location filename="../assets/mindw80h80du/genericSquare.qml" line="263"/>
        <source>Info</source>
        <translation>信息</translation>
    </message>
    <message>
        <location filename="../assets/mindw80h80du/genericSquare.qml" line="235"/>
        <location filename="../assets/mindw80h80du/genericSquare.qml" line="239"/>
        <source>Content</source>
        <translation>内容</translation>
    </message>
    <message>
        <location filename="../assets/mindw120h120du/genericSquare.qml" line="5"/>
        <location filename="../assets/mindw76h128du/genericSquare.qml" line="5"/>
        <location filename="../assets/mindw80h80du/genericSquare.qml" line="5"/>
        <source>pass</source>
        <translation>护照</translation>
    </message>
    <message>
        <location filename="../assets/mindw120h120du/genericSquare.qml" line="144"/>
        <location filename="../assets/mindw76h128du/genericSquare.qml" line="144"/>
        <location filename="../assets/mindw80h80du/genericSquare.qml" line="147"/>
        <source>barcode</source>
        <translation>条形码</translation>
    </message>
    <message>
        <location filename="../assets/mindw120h120du/genericSquare.qml" line="152"/>
        <location filename="../assets/mindw76h128du/genericSquare.qml" line="152"/>
        <location filename="../assets/mindw80h80du/genericSquare.qml" line="155"/>
        <source>barcode alternate text: </source>
        <translation>条形码替代文本： </translation>
    </message>
    <message>
        <location filename="../assets/mindw120h120du/genericSquare.qml" line="192"/>
        <location filename="../assets/mindw120h120du/genericSquare.qml" line="211"/>
        <location filename="../assets/mindw76h128du/genericSquare.qml" line="192"/>
        <location filename="../assets/mindw76h128du/genericSquare.qml" line="211"/>
        <location filename="../assets/mindw80h80du/genericSquare.qml" line="195"/>
        <location filename="../assets/mindw80h80du/genericSquare.qml" line="220"/>
        <source>Barcode page</source>
        <translation>条形码页面</translation>
    </message>
    <message>
        <location filename="../assets/mindw80h80du/genericSquare.qml" line="228"/>
        <source>p</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../assets/mindw80h80du/genericSquare.qml" line="258"/>
        <source>r</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../assets/mindw80h80du/genericSquare.qml" line="273"/>
        <source>d</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../assets/mindw120h120du/genericSquare.qml" line="229"/>
        <location filename="../assets/mindw76h128du/genericSquare.qml" line="229"/>
        <location filename="../assets/mindw80h80du/genericSquare.qml" line="278"/>
        <source>Brightness</source>
        <translation>亮度</translation>
    </message>
</context>
<context>
    <name>genericSquareCard</name>
    <message>
        <location filename="../assets/mindw120h120du/genericSquareCard.qml" line="222"/>
        <location filename="../assets/mindw76h128du/genericSquareCard.qml" line="222"/>
        <location filename="../assets/mindw80h80du/genericSquareCard.qml" line="260"/>
        <source>Import</source>
        <translation>导入</translation>
    </message>
    <message>
        <location filename="../assets/mindw80h80du/genericSquareCard.qml" line="231"/>
        <location filename="../assets/mindw80h80du/genericSquareCard.qml" line="243"/>
        <source>Barcode</source>
        <translation>条形码</translation>
    </message>
    <message>
        <location filename="../assets/mindw120h120du/genericSquareCard.qml" line="230"/>
        <location filename="../assets/mindw76h128du/genericSquareCard.qml" line="230"/>
        <location filename="../assets/mindw80h80du/genericSquareCard.qml" line="273"/>
        <source>Info</source>
        <translation>信息</translation>
    </message>
    <message>
        <location filename="../assets/mindw80h80du/genericSquareCard.qml" line="231"/>
        <location filename="../assets/mindw80h80du/genericSquareCard.qml" line="235"/>
        <source>Content</source>
        <translation>内容</translation>
    </message>
    <message>
        <location filename="../assets/mindw120h120du/genericSquareCard.qml" line="375"/>
        <location filename="../assets/mindw76h128du/genericSquareCard.qml" line="375"/>
        <location filename="../assets/mindw80h80du/genericSquareCard.qml" line="424"/>
        <source>Can&apos;t add selected pass</source>
        <translation>无法添加选定护照</translation>
    </message>
    <message>
        <location filename="../assets/mindw120h120du/genericSquareCard.qml" line="379"/>
        <location filename="../assets/mindw76h128du/genericSquareCard.qml" line="379"/>
        <location filename="../assets/mindw80h80du/genericSquareCard.qml" line="428"/>
        <source>Pass added successfuly</source>
        <translation>护照已成功添加</translation>
    </message>
    <message>
        <location filename="../assets/mindw120h120du/genericSquareCard.qml" line="381"/>
        <location filename="../assets/mindw76h128du/genericSquareCard.qml" line="381"/>
        <location filename="../assets/mindw80h80du/genericSquareCard.qml" line="430"/>
        <source>Pass updated successfuly</source>
        <translation>护照已成功更新</translation>
    </message>
    <message>
        <location filename="../assets/mindw120h120du/genericSquareCard.qml" line="383"/>
        <location filename="../assets/mindw76h128du/genericSquareCard.qml" line="383"/>
        <location filename="../assets/mindw80h80du/genericSquareCard.qml" line="432"/>
        <source>Pass operation finished successfuly</source>
        <translation>护照操作已成功完成</translation>
    </message>
    <message>
        <location filename="../assets/mindw120h120du/genericSquareCard.qml" line="9"/>
        <location filename="../assets/mindw76h128du/genericSquareCard.qml" line="9"/>
        <location filename="../assets/mindw80h80du/genericSquareCard.qml" line="9"/>
        <source>pass</source>
        <translation>护照</translation>
    </message>
    <message>
        <location filename="../assets/mindw120h120du/genericSquareCard.qml" line="148"/>
        <location filename="../assets/mindw76h128du/genericSquareCard.qml" line="148"/>
        <location filename="../assets/mindw80h80du/genericSquareCard.qml" line="151"/>
        <source>barcode</source>
        <translation>条形码</translation>
    </message>
    <message>
        <location filename="../assets/mindw120h120du/genericSquareCard.qml" line="156"/>
        <location filename="../assets/mindw76h128du/genericSquareCard.qml" line="156"/>
        <location filename="../assets/mindw80h80du/genericSquareCard.qml" line="159"/>
        <source>barcode alternate text: </source>
        <translation>条形码替代文本： </translation>
    </message>
    <message>
        <location filename="../assets/mindw120h120du/genericSquareCard.qml" line="213"/>
        <location filename="../assets/mindw76h128du/genericSquareCard.qml" line="213"/>
        <location filename="../assets/mindw80h80du/genericSquareCard.qml" line="216"/>
        <source>Barcode page</source>
        <translation>条形码页面</translation>
    </message>
    <message>
        <location filename="../assets/mindw80h80du/genericSquareCard.qml" line="224"/>
        <source>p</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../assets/mindw80h80du/genericSquareCard.qml" line="254"/>
        <source>r</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../assets/mindw80h80du/genericSquareCard.qml" line="268"/>
        <source>i</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../assets/mindw80h80du/genericSquareCard.qml" line="283"/>
        <source>d</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../assets/mindw120h120du/genericSquareCard.qml" line="240"/>
        <location filename="../assets/mindw76h128du/genericSquareCard.qml" line="240"/>
        <location filename="../assets/mindw80h80du/genericSquareCard.qml" line="288"/>
        <source>Brightness</source>
        <translation>亮度</translation>
    </message>
</context>
<context>
    <name>help</name>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">取消</translation>
    </message>
</context>
<context>
    <name>main</name>
    <message>
        <location filename="../assets/main.qml" line="14"/>
        <source>About</source>
        <translation>关于</translation>
    </message>
    <message>
        <location filename="../assets/main.qml" line="43"/>
        <source>Search term</source>
        <translation>搜索词</translation>
    </message>
    <message>
        <location filename="../assets/main.qml" line="58"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <source>pass</source>
        <translation type="obsolete">护照</translation>
    </message>
    <message>
        <location filename="../assets/main.qml" line="92"/>
        <source>There are no passes yet.</source>
        <translation>尚未护照。</translation>
    </message>
    <message>
        <location filename="../assets/main.qml" line="116"/>
        <location filename="../assets/main.qml" line="154"/>
        <source>Import pass help</source>
        <translation>导入护照帮助</translation>
    </message>
    <message>
        <location filename="../assets/main.qml" line="134"/>
        <source>You can import the pass</source>
        <translation>您可以导入该护照</translation>
    </message>
    <message>
        <location filename="../assets/main.qml" line="160"/>
        <source>Importing passes</source>
        <translation>正在导入护照</translation>
    </message>
    <message>
        <source>Passes supports import of existing files. Pass issuer usually gives link to download pkpass pass file or sends it by mail. The application works with pkpass files. PDF file are not supported.</source>
        <translation type="obsolete">护照本支持导入已存在文件。</translation>
    </message>
    <message>
        <location filename="../assets/main.qml" line="163"/>
        <source>Passes supports import of existing files. Pass issuer usually gives link to download pkpass pass file or sends it by mail. The application works with pkpass files. PDF or other files are not supported.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/main.qml" line="173"/>
        <source>Create pass</source>
        <translation>创建护照</translation>
    </message>
    <message>
        <location filename="../assets/main.qml" line="193"/>
        <source>You can create the pass</source>
        <translation>您可以创建该护照</translation>
    </message>
    <message>
        <location filename="../assets/main.qml" line="214"/>
        <source>Create pass help</source>
        <translation>创建护照帮助</translation>
    </message>
    <message>
        <location filename="../assets/main.qml" line="220"/>
        <source>Create new pass</source>
        <translation>创建新护照</translation>
    </message>
    <message>
        <location filename="../assets/main.qml" line="223"/>
        <source>You can create your own pass. There are two types predefined - generic pass and boarding pass.</source>
        <translation>您可以创建您的个人护照。</translation>
    </message>
    <message>
        <location filename="../assets/main.qml" line="236"/>
        <source>Saved passes</source>
        <translation>保存护照</translation>
    </message>
    <message>
        <location filename="../assets/main.qml" line="237"/>
        <source>Cards are stacked. Activate the card to view the full card.</source>
        <translation>卡片已堆叠。激活该卡，以查看完整卡片。</translation>
    </message>
    <message>
        <location filename="../assets/main.qml" line="432"/>
        <source>Delete the pass from</source>
        <translation>删除该护照来自</translation>
    </message>
    <message>
        <location filename="../assets/main.qml" line="447"/>
        <source>Search</source>
        <translation>搜索</translation>
    </message>
    <message>
        <location filename="../assets/main.qml" line="465"/>
        <source>Import</source>
        <translation>导入</translation>
    </message>
    <message>
        <location filename="../assets/main.qml" line="473"/>
        <source>Create</source>
        <translation>创建</translation>
    </message>
    <message>
        <location filename="../assets/main.qml" line="484"/>
        <location filename="../assets/main.qml" line="497"/>
        <source>Grid view</source>
        <translation>网格视图</translation>
    </message>
    <message>
        <location filename="../assets/main.qml" line="484"/>
        <location filename="../assets/main.qml" line="491"/>
        <source>List view</source>
        <translation>列表视图</translation>
    </message>
    <message>
        <location filename="../assets/main.qml" line="507"/>
        <source>Send Feedback</source>
        <translation>发送反馈</translation>
    </message>
    <message>
        <location filename="../assets/main.qml" line="553"/>
        <source>Delete the pass?</source>
        <translation>删除该护照？</translation>
    </message>
    <message>
        <source>Refreshing passes list</source>
        <translation type="obsolete">正在刷新护照列表</translation>
    </message>
    <message>
        <location filename="../assets/main.qml" line="700"/>
        <source>Can&apos;t add selected pass</source>
        <translation>无法添加选定护照</translation>
    </message>
    <message>
        <location filename="../assets/main.qml" line="705"/>
        <source>Pass added successfuly</source>
        <translation>护照已成功添加</translation>
    </message>
    <message>
        <location filename="../assets/main.qml" line="708"/>
        <source>Pass updated successfuly</source>
        <translation>护照已成功更新</translation>
    </message>
    <message>
        <location filename="../assets/main.qml" line="711"/>
        <source>Pass operation finished successfuly</source>
        <translation>护照操作已成功完成</translation>
    </message>
    <message>
        <location filename="../assets/main.qml" line="722"/>
        <source>Processing pass started</source>
        <translation>处理护照已开始</translation>
    </message>
    <message>
        <location filename="../assets/main.qml" line="729"/>
        <source>Pass successfuly removed</source>
        <translation>护照已成功移除</translation>
    </message>
    <message>
        <location filename="../assets/main.qml" line="732"/>
        <source>Removing the pass failed</source>
        <translation>移除该护照时失败</translation>
    </message>
</context>
<context>
    <name>scan</name>
    <message>
        <location filename="../assets/scan.qml" line="10"/>
        <source>Scan Code</source>
        <translation>扫描代码</translation>
    </message>
    <message>
        <location filename="../assets/scan.qml" line="12"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../assets/scan.qml" line="63"/>
        <source>Unable to scan the barcode</source>
        <translation>无法扫描条形码</translation>
    </message>
</context>
<context>
    <name>store</name>
    <message>
        <location filename="../assets/mindw120h120du/store.qml" line="194"/>
        <location filename="../assets/mindw76h128du/store.qml" line="194"/>
        <location filename="../assets/mindw80h80du/store.qml" line="246"/>
        <source>Info</source>
        <translation>信息</translation>
    </message>
    <message>
        <location filename="../assets/mindw80h80du/store.qml" line="218"/>
        <location filename="../assets/mindw80h80du/store.qml" line="230"/>
        <source>Barcode</source>
        <translation>条形码</translation>
    </message>
    <message>
        <location filename="../assets/mindw80h80du/store.qml" line="218"/>
        <location filename="../assets/mindw80h80du/store.qml" line="222"/>
        <source>Content</source>
        <translation>内容</translation>
    </message>
    <message>
        <location filename="../assets/mindw120h120du/store.qml" line="7"/>
        <location filename="../assets/mindw76h128du/store.qml" line="7"/>
        <location filename="../assets/mindw80h80du/store.qml" line="7"/>
        <source>store card</source>
        <translation>存储卡片</translation>
    </message>
    <message>
        <location filename="../assets/mindw120h120du/store.qml" line="134"/>
        <location filename="../assets/mindw76h128du/store.qml" line="134"/>
        <location filename="../assets/mindw80h80du/store.qml" line="151"/>
        <source>barcode</source>
        <translation>条形码</translation>
    </message>
    <message>
        <location filename="../assets/mindw120h120du/store.qml" line="142"/>
        <location filename="../assets/mindw76h128du/store.qml" line="142"/>
        <location filename="../assets/mindw80h80du/store.qml" line="159"/>
        <source>barcode alternate text: </source>
        <translation>条形码替代文本： </translation>
    </message>
    <message>
        <location filename="../assets/mindw120h120du/store.qml" line="186"/>
        <location filename="../assets/mindw76h128du/store.qml" line="186"/>
        <location filename="../assets/mindw80h80du/store.qml" line="203"/>
        <source>Barcode page</source>
        <translation>条形码页面</translation>
    </message>
    <message>
        <location filename="../assets/mindw80h80du/store.qml" line="211"/>
        <source>p</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../assets/mindw80h80du/store.qml" line="241"/>
        <source>r</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../assets/mindw80h80du/store.qml" line="256"/>
        <source>d</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../assets/mindw120h120du/store.qml" line="204"/>
        <location filename="../assets/mindw76h128du/store.qml" line="204"/>
        <location filename="../assets/mindw80h80du/store.qml" line="261"/>
        <source>Brightness</source>
        <translation>亮度</translation>
    </message>
</context>
<context>
    <name>storeCard</name>
    <message>
        <location filename="../assets/mindw120h120du/storeCard.qml" line="199"/>
        <location filename="../assets/mindw76h128du/storeCard.qml" line="199"/>
        <location filename="../assets/mindw80h80du/storeCard.qml" line="236"/>
        <source>Import</source>
        <translation>导入</translation>
    </message>
    <message>
        <location filename="../assets/mindw120h120du/storeCard.qml" line="207"/>
        <location filename="../assets/mindw76h128du/storeCard.qml" line="207"/>
        <location filename="../assets/mindw80h80du/storeCard.qml" line="249"/>
        <source>Info</source>
        <translation>信息</translation>
    </message>
    <message>
        <location filename="../assets/mindw120h120du/storeCard.qml" line="350"/>
        <location filename="../assets/mindw76h128du/storeCard.qml" line="350"/>
        <location filename="../assets/mindw80h80du/storeCard.qml" line="398"/>
        <source>Can&apos;t add selected pass</source>
        <translation>无法添加选定护照</translation>
    </message>
    <message>
        <location filename="../assets/mindw120h120du/storeCard.qml" line="354"/>
        <location filename="../assets/mindw76h128du/storeCard.qml" line="354"/>
        <location filename="../assets/mindw80h80du/storeCard.qml" line="402"/>
        <source>Pass added successfuly</source>
        <translation>护照已成功添加</translation>
    </message>
    <message>
        <location filename="../assets/mindw120h120du/storeCard.qml" line="356"/>
        <location filename="../assets/mindw76h128du/storeCard.qml" line="356"/>
        <location filename="../assets/mindw80h80du/storeCard.qml" line="404"/>
        <source>Pass updated successfuly</source>
        <translation>护照已成功更新</translation>
    </message>
    <message>
        <location filename="../assets/mindw120h120du/storeCard.qml" line="358"/>
        <location filename="../assets/mindw76h128du/storeCard.qml" line="358"/>
        <location filename="../assets/mindw80h80du/storeCard.qml" line="406"/>
        <source>Pass operation finished successfuly</source>
        <translation>护照操作已成功完成</translation>
    </message>
    <message>
        <location filename="../assets/mindw80h80du/storeCard.qml" line="207"/>
        <location filename="../assets/mindw80h80du/storeCard.qml" line="219"/>
        <source>Barcode</source>
        <translation>条形码</translation>
    </message>
    <message>
        <location filename="../assets/mindw80h80du/storeCard.qml" line="207"/>
        <location filename="../assets/mindw80h80du/storeCard.qml" line="211"/>
        <source>Content</source>
        <translation>内容</translation>
    </message>
    <message>
        <location filename="../assets/mindw120h120du/storeCard.qml" line="9"/>
        <location filename="../assets/mindw76h128du/storeCard.qml" line="9"/>
        <location filename="../assets/mindw80h80du/storeCard.qml" line="9"/>
        <source>store card</source>
        <translation>存储卡片</translation>
    </message>
    <message>
        <location filename="../assets/mindw120h120du/storeCard.qml" line="131"/>
        <location filename="../assets/mindw76h128du/storeCard.qml" line="131"/>
        <location filename="../assets/mindw80h80du/storeCard.qml" line="133"/>
        <source>barcode</source>
        <translation>条形码</translation>
    </message>
    <message>
        <location filename="../assets/mindw120h120du/storeCard.qml" line="139"/>
        <location filename="../assets/mindw76h128du/storeCard.qml" line="139"/>
        <location filename="../assets/mindw80h80du/storeCard.qml" line="141"/>
        <source>barcode alternate text: </source>
        <translation>条形码替代文本： </translation>
    </message>
    <message>
        <location filename="../assets/mindw120h120du/storeCard.qml" line="190"/>
        <location filename="../assets/mindw76h128du/storeCard.qml" line="190"/>
        <location filename="../assets/mindw80h80du/storeCard.qml" line="192"/>
        <source>Barcode page</source>
        <translation>条形码页面</translation>
    </message>
    <message>
        <location filename="../assets/mindw80h80du/storeCard.qml" line="200"/>
        <source>p</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../assets/mindw80h80du/storeCard.qml" line="230"/>
        <source>r</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../assets/mindw80h80du/storeCard.qml" line="244"/>
        <source>i</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../assets/mindw80h80du/storeCard.qml" line="259"/>
        <source>d</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../assets/mindw120h120du/storeCard.qml" line="217"/>
        <location filename="../assets/mindw76h128du/storeCard.qml" line="217"/>
        <location filename="../assets/mindw80h80du/storeCard.qml" line="264"/>
        <source>Brightness</source>
        <translation>亮度</translation>
    </message>
</context>
</TS>
