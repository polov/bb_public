<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="cs_CZ">
<context>
    <name>AboutSheet</name>
    <message>
        <location filename="../assets/AboutSheet.qml" line="39"/>
        <source>Passes</source>
        <translation>Lístky</translation>
    </message>
    <message>
        <location filename="../assets/AboutSheet.qml" line="60"/>
        <source>version</source>
        <translation>verze</translation>
    </message>
    <message>
        <location filename="../assets/AboutSheet.qml" line="76"/>
        <source>build by</source>
        <translation>vytvořil</translation>
    </message>
    <message>
        <location filename="../assets/AboutSheet.qml" line="94"/>
        <source>This application can help you managing all passbook files (.pkpass files). You can receive them by mail or download from an issuer through link.</source>
        <translation>Aplikace pomáhá se správou lístků (.pkpass soubory, passes). Tyto soubory můžete od vydavatele lístku obdržet mailem nebo si jej stáhnout od vydavatele lístku pomocí odkazu.</translation>
    </message>
    <message>
        <location filename="../assets/AboutSheet.qml" line="113"/>
        <source>Privacy policy (English)</source>
        <translation>Ochrana osobních údajů (anglicky)</translation>
    </message>
    <message>
        <location filename="../assets/AboutSheet.qml" line="131"/>
        <source>License agreement (English)</source>
        <translation>Licenční ujednání (anglicky)</translation>
    </message>
    <message>
        <location filename="../assets/AboutSheet.qml" line="228"/>
        <source>Done</source>
        <translation>Hotovo</translation>
    </message>
</context>
<context>
    <name>SettingsSheet</name>
    <message>
        <location filename="../assets/SettingsSheet.qml" line="8"/>
        <source>Settings</source>
        <translation>Nastavení</translation>
    </message>
    <message>
        <location filename="../assets/SettingsSheet.qml" line="29"/>
        <source>Use location services</source>
        <translation>Použít služby určení polohy</translation>
    </message>
    <message>
        <location filename="../assets/SettingsSheet.qml" line="63"/>
        <source>There are problems with using location services. Please check permissions.</source>
        <translation>Vyskytl se problém se službami určení polohy. Zkontrolujte oprávnění pro aplikace.</translation>
    </message>
    <message>
        <location filename="../assets/SettingsSheet.qml" line="73"/>
        <source>Application Permissions</source>
        <translation>Oprávnění pro aplikace</translation>
    </message>
    <message>
        <location filename="../assets/SettingsSheet.qml" line="91"/>
        <source>Location Services</source>
        <translation>Služby určení polohy</translation>
    </message>
    <message>
        <location filename="../assets/SettingsSheet.qml" line="50"/>
        <source>Constantly check current location and send notification when you are near any location stored in one of your pass.</source>
        <translation>Pravidelně zjišťovat polohu a poslat notifikaci v případě, že jste blízko místa definovaného v nějakém lístku.</translation>
    </message>
    <message>
        <location filename="../assets/SettingsSheet.qml" line="112"/>
        <source>Done</source>
        <translation>Hotovo</translation>
    </message>
</context>
<context>
    <name>add</name>
    <message>
        <location filename="../assets/add.qml" line="11"/>
        <source>Create Pass</source>
        <translation>Vytvořit lístek</translation>
    </message>
    <message>
        <location filename="../assets/add.qml" line="21"/>
        <source>Create</source>
        <translation>Vytvořit</translation>
    </message>
    <message>
        <location filename="../assets/add.qml" line="40"/>
        <source>Boarding pass</source>
        <translation>Cestovní doklad</translation>
    </message>
    <message>
        <location filename="../assets/add.qml" line="38"/>
        <source>Pass type</source>
        <translation>Typ lístku</translation>
    </message>
    <message>
        <location filename="../assets/add.qml" line="13"/>
        <source>Cancel</source>
        <translation>Zrušit</translation>
    </message>
    <message>
        <location filename="../assets/add.qml" line="44"/>
        <source>Generic pass</source>
        <translation>Zákaznická karta</translation>
    </message>
    <message>
        <location filename="../assets/add.qml" line="52"/>
        <source>Departure label</source>
        <translation>Odjezd - název</translation>
    </message>
    <message>
        <location filename="../assets/add.qml" line="53"/>
        <source>Departure value</source>
        <translation>Odjezd - hodnota</translation>
    </message>
    <message>
        <location filename="../assets/add.qml" line="54"/>
        <source>Arrival label</source>
        <translation>Příjezd - název</translation>
    </message>
    <message>
        <location filename="../assets/add.qml" line="55"/>
        <source>Arrival value</source>
        <translation>Příjezd - hodnota</translation>
    </message>
    <message>
        <location filename="../assets/add.qml" line="78"/>
        <location filename="../assets/add.qml" line="90"/>
        <location filename="../assets/add.qml" line="105"/>
        <source>Company name</source>
        <translation>Společnost</translation>
    </message>
    <message>
        <location filename="../assets/add.qml" line="123"/>
        <location filename="../assets/add.qml" line="135"/>
        <source>Logo and text</source>
        <translation>Logo a text</translation>
    </message>
    <message>
        <location filename="../assets/add.qml" line="165"/>
        <source>Change image</source>
        <translation>Změnit obrázek</translation>
    </message>
    <message>
        <location filename="../assets/add.qml" line="206"/>
        <location filename="../assets/add.qml" line="218"/>
        <source>Colors</source>
        <translation>Barvy</translation>
    </message>
    <message>
        <location filename="../assets/add.qml" line="232"/>
        <source>Background Color</source>
        <translation>Barva pozadí</translation>
    </message>
    <message>
        <location filename="../assets/add.qml" line="253"/>
        <location filename="../assets/add.qml" line="290"/>
        <location filename="../assets/add.qml" line="327"/>
        <source>Change color</source>
        <translation>Změnit barvu</translation>
    </message>
    <message>
        <location filename="../assets/add.qml" line="306"/>
        <source>Label Color</source>
        <translation>Barva názvů</translation>
    </message>
    <message>
        <location filename="../assets/add.qml" line="352"/>
        <location filename="../assets/add.qml" line="364"/>
        <source>Header Field</source>
        <translation>Pole hlavičky</translation>
    </message>
    <message>
        <location filename="../assets/add.qml" line="366"/>
        <source>Header field label and text are displayed on the top of the pass and in the list of your passes. Header field label uses small letters and is above the header field value. Header fiel value uses larger letters and is under the label. You can write label &quot;POINTS&quot; and amount as the value, or in case of boarding pass &quot;TRACK&quot; and the track of the train.</source>
        <translation>Pole hlavičky jsou zobrazeny v horní části lístku a v seznamu lístků. Název pole hlavičky používá menší písmena a je nad hodnotou pole hlavičky. Hodnota pole hlavičky používá větší písmo a je pod názvem pole hlavičky.</translation>
    </message>
    <message>
        <location filename="../assets/add.qml" line="378"/>
        <source>Header field label</source>
        <translation>Název pole hlavičky</translation>
    </message>
    <message>
        <location filename="../assets/add.qml" line="382"/>
        <source>Header field value</source>
        <translation>Hodnota pole hlavičky</translation>
    </message>
    <message>
        <location filename="../assets/add.qml" line="399"/>
        <location filename="../assets/add.qml" line="411"/>
        <source>Primary Field</source>
        <translation>Primární pole</translation>
    </message>
    <message>
        <location filename="../assets/add.qml" line="58"/>
        <location filename="../assets/add.qml" line="429"/>
        <source>Primary field label</source>
        <translation>Název primárního pole</translation>
    </message>
    <message>
        <location filename="../assets/add.qml" line="59"/>
        <location filename="../assets/add.qml" line="433"/>
        <source>Primary field value</source>
        <translation>Hodnota primárního pole</translation>
    </message>
    <message>
        <location filename="../assets/add.qml" line="93"/>
        <source>Name of the company which issued the card/coupon/pass. The name will be used in search.</source>
        <translation>Název společnosti, která vydala lístek. Text se používá při hledání.</translation>
    </message>
    <message>
        <location filename="../assets/add.qml" line="138"/>
        <source>Logo is the image on the top and is followed by the text. The text will be used in search.</source>
        <translation>Logo je obrázek v horní části lístku a za ním následuje text. Text se používá při hledání.</translation>
    </message>
    <message>
        <location filename="../assets/add.qml" line="221"/>
        <source>Every pass has its background color. Text color is color of field values. Label color is color of field labels that are above field values.</source>
        <translation>Každému lístku lze definovat barvu pozadí. Barva textu je barvou hodnot polí. Barva názvů je barva názvů polí.</translation>
    </message>
    <message>
        <location filename="../assets/add.qml" line="417"/>
        <source>Primary Field usually contains important information, such as Member Id, Coupon Id, Member Name. It depends on the purpose of the pass. In case of boarding pass there are deptarture and arrival places with its codes as values. There are primary field label (use smaller letters) and bellow is primary field value (uses larger letters).</source>
        <translation>Primární pole obvykle obsahuje důležité informace, například číslo členství, číslo kupónu, jméno držitele. Záleží na tom, pro jaké účely je lístek vytvořen. V případě, že se jedná o cestovní doklad, tak obsahuje odjezd a příjezd jako primární pole. Primární pole se skládá z názvu (používá menší písmo) a pod ním je hodnota (používá větší písmo).</translation>
    </message>
    <message>
        <location filename="../assets/add.qml" line="463"/>
        <location filename="../assets/add.qml" line="475"/>
        <source>More info</source>
        <translation>Další informace</translation>
    </message>
    <message>
        <location filename="../assets/add.qml" line="494"/>
        <source>Additional information</source>
        <translation>Další informace</translation>
    </message>
    <message>
        <location filename="../assets/add.qml" line="512"/>
        <location filename="../assets/add.qml" line="524"/>
        <source>Barcode</source>
        <translation>Čárový kód</translation>
    </message>
    <message>
        <location filename="../assets/add.qml" line="574"/>
        <source>Information in the barcode</source>
        <translation>Informace v čárovém kódu</translation>
    </message>
    <message>
        <location filename="../assets/add.qml" line="577"/>
        <source>Scan Barcode</source>
        <translation>Načíst</translation>
    </message>
    <message>
        <location filename="../assets/add.qml" line="539"/>
        <source>Barcode type</source>
        <translation>Typ čárového kódu</translation>
    </message>
    <message>
        <location filename="../assets/add.qml" line="189"/>
        <source>Text next to the logo</source>
        <translation>Text vedle loga</translation>
    </message>
    <message>
        <location filename="../assets/add.qml" line="269"/>
        <source>Text Color</source>
        <translation>Barva textu</translation>
    </message>
    <message>
        <location filename="../assets/add.qml" line="478"/>
        <source>Place for additional information that you want to have available together with the pass.</source>
        <translation>Místo pro další informace, které mají být přístupné spolu s lístkem.</translation>
    </message>
    <message>
        <location filename="../assets/add.qml" line="490"/>
        <source>Additional information label</source>
        <translation>Název pro další informace</translation>
    </message>
    <message>
        <location filename="../assets/add.qml" line="526"/>
        <source>You can create 3 types of barcode: QR code, EAN code and Aztec code. You can manually enter message encoded in the barcode and the barcode type or you can use scan feature (e.g. scan an existing card/coupon/etc). If it is possible and if it is nowhere on the pass, it is good to write the message to the alternate text - it is a back up in the case that scanner will be unable to scan the display of your mobile phone.</source>
        <translation>Můžete vytvářet 3 typy čárových kódů: QR kód, EAN kód a Aztec. Hodnotu zakódovánou do čárového kódu a typ čárového kódu můžete zadat ručne nebo použít funkci načtení (načíst kód z existujícího kupónu/karty/atd). Pokud to dává smysl a není to nikde jinde zobrazeno, doporučujeme zvážit uvedení hodnoty zakódováné do čárového kódu jako doplňující text - jako zálohu pro případ, že nebude čtecí zařízení schopno přečíst displej telefonu.</translation>
    </message>
    <message>
        <location filename="../assets/add.qml" line="591"/>
        <source>Alternate text</source>
        <translation>Doplňující text</translation>
    </message>
</context>
<context>
    <name>barcodePage</name>
    <message>
        <location filename="../assets/barcodePage.qml" line="4"/>
        <location filename="../assets/barcodePage.qml" line="17"/>
        <source>barcode view</source>
        <translation>pohled na čárový kód</translation>
    </message>
    <message>
        <location filename="../assets/barcodePage.qml" line="12"/>
        <source>Pinch to zoom the barcode</source>
        <translation>Zvětšení a zmenšení kódu dosáhnete rozevřením a sevřením prstů</translation>
    </message>
</context>
<context>
    <name>boardingPass</name>
    <message>
        <location filename="../assets/mindw80h80du/boardingPass.qml" line="261"/>
        <location filename="../assets/mindw80h80du/boardingPass.qml" line="275"/>
        <source>Barcode</source>
        <translation>Čárový kód</translation>
    </message>
    <message>
        <location filename="../assets/mindw120h120du/boardingPass.qml" line="252"/>
        <location filename="../assets/mindw76h128du/boardingPass.qml" line="252"/>
        <location filename="../assets/mindw80h80du/boardingPass.qml" line="293"/>
        <source>Info</source>
        <translation>Info</translation>
    </message>
    <message>
        <location filename="../assets/mindw80h80du/boardingPass.qml" line="261"/>
        <location filename="../assets/mindw80h80du/boardingPass.qml" line="265"/>
        <source>Content</source>
        <translation>Data</translation>
    </message>
    <message>
        <location filename="../assets/mindw120h120du/boardingPass.qml" line="6"/>
        <location filename="../assets/mindw76h128du/boardingPass.qml" line="6"/>
        <location filename="../assets/mindw80h80du/boardingPass.qml" line="6"/>
        <source>boarding pass</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/mindw120h120du/boardingPass.qml" line="186"/>
        <location filename="../assets/mindw76h128du/boardingPass.qml" line="186"/>
        <location filename="../assets/mindw80h80du/boardingPass.qml" line="189"/>
        <source>barcode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/mindw120h120du/boardingPass.qml" line="194"/>
        <location filename="../assets/mindw76h128du/boardingPass.qml" line="194"/>
        <location filename="../assets/mindw80h80du/boardingPass.qml" line="197"/>
        <source>barcode alternate text: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/mindw120h120du/boardingPass.qml" line="244"/>
        <location filename="../assets/mindw76h128du/boardingPass.qml" line="244"/>
        <location filename="../assets/mindw80h80du/boardingPass.qml" line="246"/>
        <source>Barcode page</source>
        <translation>Stránka s kódem</translation>
    </message>
    <message>
        <location filename="../assets/mindw120h120du/boardingPass.qml" line="262"/>
        <location filename="../assets/mindw76h128du/boardingPass.qml" line="262"/>
        <location filename="../assets/mindw80h80du/boardingPass.qml" line="308"/>
        <source>Brightness</source>
        <translation>Jas</translation>
    </message>
    <message>
        <location filename="../assets/mindw80h80du/boardingPass.qml" line="254"/>
        <source>p</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/mindw80h80du/boardingPass.qml" line="288"/>
        <source>r</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/mindw80h80du/boardingPass.qml" line="303"/>
        <source>d</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>boardingPassCard</name>
    <message>
        <location filename="../assets/mindw120h120du/boardingPassCard.qml" line="263"/>
        <location filename="../assets/mindw76h128du/boardingPassCard.qml" line="263"/>
        <location filename="../assets/mindw80h80du/boardingPassCard.qml" line="304"/>
        <source>Import</source>
        <translation>Import</translation>
    </message>
    <message>
        <location filename="../assets/mindw80h80du/boardingPassCard.qml" line="271"/>
        <location filename="../assets/mindw80h80du/boardingPassCard.qml" line="285"/>
        <source>Barcode</source>
        <translation>Čárový kód</translation>
    </message>
    <message>
        <location filename="../assets/mindw120h120du/boardingPassCard.qml" line="271"/>
        <location filename="../assets/mindw76h128du/boardingPassCard.qml" line="271"/>
        <location filename="../assets/mindw80h80du/boardingPassCard.qml" line="317"/>
        <source>Info</source>
        <translation>Info</translation>
    </message>
    <message>
        <location filename="../assets/mindw80h80du/boardingPassCard.qml" line="271"/>
        <location filename="../assets/mindw80h80du/boardingPassCard.qml" line="275"/>
        <source>Content</source>
        <translation>Data</translation>
    </message>
    <message>
        <location filename="../assets/mindw120h120du/boardingPassCard.qml" line="432"/>
        <location filename="../assets/mindw76h128du/boardingPassCard.qml" line="432"/>
        <location filename="../assets/mindw80h80du/boardingPassCard.qml" line="484"/>
        <source>Can&apos;t add selected pass</source>
        <translation>Nelze přidat vybraný lístek</translation>
    </message>
    <message>
        <location filename="../assets/mindw120h120du/boardingPassCard.qml" line="436"/>
        <location filename="../assets/mindw76h128du/boardingPassCard.qml" line="436"/>
        <location filename="../assets/mindw80h80du/boardingPassCard.qml" line="488"/>
        <source>Pass added successfuly</source>
        <translation>Lístek byl přidán</translation>
    </message>
    <message>
        <location filename="../assets/mindw120h120du/boardingPassCard.qml" line="438"/>
        <location filename="../assets/mindw76h128du/boardingPassCard.qml" line="438"/>
        <location filename="../assets/mindw80h80du/boardingPassCard.qml" line="490"/>
        <source>Pass updated successfuly</source>
        <translation>Lístek byl upraven</translation>
    </message>
    <message>
        <location filename="../assets/mindw120h120du/boardingPassCard.qml" line="440"/>
        <location filename="../assets/mindw76h128du/boardingPassCard.qml" line="440"/>
        <location filename="../assets/mindw80h80du/boardingPassCard.qml" line="492"/>
        <source>Pass operation finished successfuly</source>
        <translation>Manipulace s lístkem proběhla úspěšně</translation>
    </message>
    <message>
        <location filename="../assets/mindw120h120du/boardingPassCard.qml" line="9"/>
        <location filename="../assets/mindw76h128du/boardingPassCard.qml" line="9"/>
        <location filename="../assets/mindw80h80du/boardingPassCard.qml" line="9"/>
        <source>boarding pass</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/mindw120h120du/boardingPassCard.qml" line="189"/>
        <location filename="../assets/mindw76h128du/boardingPassCard.qml" line="189"/>
        <location filename="../assets/mindw80h80du/boardingPassCard.qml" line="192"/>
        <source>barcode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/mindw120h120du/boardingPassCard.qml" line="197"/>
        <location filename="../assets/mindw76h128du/boardingPassCard.qml" line="197"/>
        <location filename="../assets/mindw80h80du/boardingPassCard.qml" line="200"/>
        <source>barcode alternate text: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/mindw120h120du/boardingPassCard.qml" line="254"/>
        <location filename="../assets/mindw76h128du/boardingPassCard.qml" line="254"/>
        <location filename="../assets/mindw80h80du/boardingPassCard.qml" line="256"/>
        <source>Barcode page</source>
        <translation>Stránka s kódem</translation>
    </message>
    <message>
        <location filename="../assets/mindw80h80du/boardingPassCard.qml" line="264"/>
        <source>p</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/mindw80h80du/boardingPassCard.qml" line="298"/>
        <source>r</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/mindw80h80du/boardingPassCard.qml" line="312"/>
        <source>i</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/mindw80h80du/boardingPassCard.qml" line="327"/>
        <source>d</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/mindw120h120du/boardingPassCard.qml" line="281"/>
        <location filename="../assets/mindw76h128du/boardingPassCard.qml" line="281"/>
        <location filename="../assets/mindw80h80du/boardingPassCard.qml" line="332"/>
        <source>Brightness</source>
        <translation>Jas</translation>
    </message>
</context>
<context>
    <name>colorPicker</name>
    <message>
        <location filename="../assets/colorPicker.qml" line="7"/>
        <source>Color</source>
        <translation>Barva</translation>
    </message>
    <message>
        <location filename="../assets/colorPicker.qml" line="9"/>
        <source>Cancel</source>
        <translation>Zrušit</translation>
    </message>
    <message>
        <location filename="../assets/colorPicker.qml" line="23"/>
        <source>Choose</source>
        <translation>Vybrat</translation>
    </message>
    <message>
        <location filename="../assets/colorPicker.qml" line="42"/>
        <source>Selected color</source>
        <translation>Zvolená barva</translation>
    </message>
</context>
<context>
    <name>coupon</name>
    <message>
        <location filename="../assets/mindw80h80du/coupon.qml" line="205"/>
        <location filename="../assets/mindw80h80du/coupon.qml" line="218"/>
        <source>Barcode</source>
        <translation>Čárový kód</translation>
    </message>
    <message>
        <location filename="../assets/mindw120h120du/coupon.qml" line="196"/>
        <location filename="../assets/mindw76h128du/coupon.qml" line="196"/>
        <location filename="../assets/mindw80h80du/coupon.qml" line="235"/>
        <source>Info</source>
        <translation>Info</translation>
    </message>
    <message>
        <location filename="../assets/mindw80h80du/coupon.qml" line="205"/>
        <location filename="../assets/mindw80h80du/coupon.qml" line="209"/>
        <source>Content</source>
        <translation>Data</translation>
    </message>
    <message>
        <location filename="../assets/mindw120h120du/coupon.qml" line="5"/>
        <location filename="../assets/mindw76h128du/coupon.qml" line="5"/>
        <location filename="../assets/mindw80h80du/coupon.qml" line="5"/>
        <source>coupon</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/mindw120h120du/coupon.qml" line="136"/>
        <location filename="../assets/mindw76h128du/coupon.qml" line="136"/>
        <location filename="../assets/mindw80h80du/coupon.qml" line="138"/>
        <source>barcode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/mindw120h120du/coupon.qml" line="144"/>
        <location filename="../assets/mindw76h128du/coupon.qml" line="144"/>
        <location filename="../assets/mindw80h80du/coupon.qml" line="146"/>
        <source>barcode alternate text: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/mindw120h120du/coupon.qml" line="188"/>
        <location filename="../assets/mindw76h128du/coupon.qml" line="188"/>
        <location filename="../assets/mindw80h80du/coupon.qml" line="190"/>
        <source>Barcode page</source>
        <translation>Stránka s kódem</translation>
    </message>
    <message>
        <location filename="../assets/mindw80h80du/coupon.qml" line="198"/>
        <source>p</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/mindw80h80du/coupon.qml" line="230"/>
        <source>r</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/mindw80h80du/coupon.qml" line="245"/>
        <source>d</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/mindw120h120du/coupon.qml" line="206"/>
        <location filename="../assets/mindw76h128du/coupon.qml" line="206"/>
        <location filename="../assets/mindw80h80du/coupon.qml" line="250"/>
        <source>Brightness</source>
        <translation>Jas</translation>
    </message>
</context>
<context>
    <name>couponCard</name>
    <message>
        <location filename="../assets/mindw120h120du/couponCard.qml" line="207"/>
        <location filename="../assets/mindw76h128du/couponCard.qml" line="207"/>
        <location filename="../assets/mindw80h80du/couponCard.qml" line="245"/>
        <source>Import</source>
        <translation>Import</translation>
    </message>
    <message>
        <location filename="../assets/mindw80h80du/couponCard.qml" line="214"/>
        <location filename="../assets/mindw80h80du/couponCard.qml" line="227"/>
        <source>Barcode</source>
        <translation>Čárový kód</translation>
    </message>
    <message>
        <location filename="../assets/mindw120h120du/couponCard.qml" line="215"/>
        <location filename="../assets/mindw76h128du/couponCard.qml" line="215"/>
        <location filename="../assets/mindw80h80du/couponCard.qml" line="258"/>
        <source>Info</source>
        <translation>Info</translation>
    </message>
    <message>
        <location filename="../assets/mindw80h80du/couponCard.qml" line="214"/>
        <location filename="../assets/mindw80h80du/couponCard.qml" line="218"/>
        <source>Content</source>
        <translation>Data</translation>
    </message>
    <message>
        <location filename="../assets/mindw120h120du/couponCard.qml" line="357"/>
        <location filename="../assets/mindw76h128du/couponCard.qml" line="357"/>
        <location filename="../assets/mindw80h80du/couponCard.qml" line="406"/>
        <source>Can&apos;t add selected pass</source>
        <translation>Nelze přidat vybraný lístek</translation>
    </message>
    <message>
        <location filename="../assets/mindw120h120du/couponCard.qml" line="361"/>
        <location filename="../assets/mindw76h128du/couponCard.qml" line="361"/>
        <location filename="../assets/mindw80h80du/couponCard.qml" line="410"/>
        <source>Pass added successfuly</source>
        <translation>Lístek byl přidán</translation>
    </message>
    <message>
        <location filename="../assets/mindw120h120du/couponCard.qml" line="363"/>
        <location filename="../assets/mindw76h128du/couponCard.qml" line="363"/>
        <location filename="../assets/mindw80h80du/couponCard.qml" line="412"/>
        <source>Pass updated successfuly</source>
        <translation>Lístek byl upraven</translation>
    </message>
    <message>
        <location filename="../assets/mindw120h120du/couponCard.qml" line="365"/>
        <location filename="../assets/mindw76h128du/couponCard.qml" line="365"/>
        <location filename="../assets/mindw80h80du/couponCard.qml" line="414"/>
        <source>Pass operation finished successfuly</source>
        <translation>Manipulace s lístkem proběhla úspěšně</translation>
    </message>
    <message>
        <location filename="../assets/mindw120h120du/couponCard.qml" line="9"/>
        <location filename="../assets/mindw76h128du/couponCard.qml" line="9"/>
        <location filename="../assets/mindw80h80du/couponCard.qml" line="9"/>
        <source>coupon</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/mindw120h120du/couponCard.qml" line="139"/>
        <location filename="../assets/mindw76h128du/couponCard.qml" line="139"/>
        <location filename="../assets/mindw80h80du/couponCard.qml" line="141"/>
        <source>barcode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/mindw120h120du/couponCard.qml" line="147"/>
        <location filename="../assets/mindw76h128du/couponCard.qml" line="147"/>
        <location filename="../assets/mindw80h80du/couponCard.qml" line="149"/>
        <source>barcode alternate text: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/mindw120h120du/couponCard.qml" line="198"/>
        <location filename="../assets/mindw76h128du/couponCard.qml" line="198"/>
        <location filename="../assets/mindw80h80du/couponCard.qml" line="199"/>
        <source>Barcode page</source>
        <translation>Stránka s kódem</translation>
    </message>
    <message>
        <location filename="../assets/mindw80h80du/couponCard.qml" line="207"/>
        <source>p</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/mindw80h80du/couponCard.qml" line="239"/>
        <source>r</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/mindw80h80du/couponCard.qml" line="253"/>
        <source>i</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/mindw80h80du/couponCard.qml" line="268"/>
        <source>d</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/mindw120h120du/couponCard.qml" line="225"/>
        <location filename="../assets/mindw76h128du/couponCard.qml" line="225"/>
        <location filename="../assets/mindw80h80du/couponCard.qml" line="273"/>
        <source>Brightness</source>
        <translation>Jas</translation>
    </message>
</context>
<context>
    <name>event</name>
    <message>
        <location filename="../assets/mindw80h80du/event.qml" line="224"/>
        <location filename="../assets/mindw80h80du/event.qml" line="236"/>
        <source>Barcode</source>
        <translation>Čárový kód</translation>
    </message>
    <message>
        <location filename="../assets/mindw120h120du/event.qml" line="215"/>
        <location filename="../assets/mindw76h128du/event.qml" line="215"/>
        <location filename="../assets/mindw80h80du/event.qml" line="252"/>
        <source>Info</source>
        <translation>Info</translation>
    </message>
    <message>
        <location filename="../assets/mindw80h80du/event.qml" line="224"/>
        <location filename="../assets/mindw80h80du/event.qml" line="228"/>
        <source>Content</source>
        <translation>Data</translation>
    </message>
    <message>
        <location filename="../assets/mindw120h120du/event.qml" line="5"/>
        <location filename="../assets/mindw76h128du/event.qml" line="5"/>
        <location filename="../assets/mindw80h80du/event.qml" line="5"/>
        <source>event ticket</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/mindw120h120du/event.qml" line="149"/>
        <location filename="../assets/mindw76h128du/event.qml" line="149"/>
        <location filename="../assets/mindw80h80du/event.qml" line="151"/>
        <source>barcode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/mindw120h120du/event.qml" line="157"/>
        <location filename="../assets/mindw76h128du/event.qml" line="157"/>
        <location filename="../assets/mindw80h80du/event.qml" line="159"/>
        <source>barcode alternate text: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/mindw120h120du/event.qml" line="207"/>
        <location filename="../assets/mindw76h128du/event.qml" line="207"/>
        <location filename="../assets/mindw80h80du/event.qml" line="209"/>
        <source>Barcode page</source>
        <translation>Stránka s kódem</translation>
    </message>
    <message>
        <location filename="../assets/mindw80h80du/event.qml" line="217"/>
        <source>p</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/mindw80h80du/event.qml" line="247"/>
        <source>r</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/mindw80h80du/event.qml" line="262"/>
        <source>d</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/mindw120h120du/event.qml" line="225"/>
        <location filename="../assets/mindw76h128du/event.qml" line="225"/>
        <location filename="../assets/mindw80h80du/event.qml" line="267"/>
        <source>Brightness</source>
        <translation>Jas</translation>
    </message>
</context>
<context>
    <name>eventCard</name>
    <message>
        <location filename="../assets/mindw120h120du/eventCard.qml" line="222"/>
        <location filename="../assets/mindw76h128du/eventCard.qml" line="222"/>
        <location filename="../assets/mindw80h80du/eventCard.qml" line="259"/>
        <source>Import</source>
        <translation>Import</translation>
    </message>
    <message>
        <location filename="../assets/mindw80h80du/eventCard.qml" line="230"/>
        <location filename="../assets/mindw80h80du/eventCard.qml" line="242"/>
        <source>Barcode</source>
        <translation>Čárový kód</translation>
    </message>
    <message>
        <location filename="../assets/mindw120h120du/eventCard.qml" line="230"/>
        <location filename="../assets/mindw76h128du/eventCard.qml" line="230"/>
        <location filename="../assets/mindw80h80du/eventCard.qml" line="272"/>
        <source>Info</source>
        <translation>Info</translation>
    </message>
    <message>
        <location filename="../assets/mindw80h80du/eventCard.qml" line="230"/>
        <location filename="../assets/mindw80h80du/eventCard.qml" line="234"/>
        <source>Content</source>
        <translation>Data</translation>
    </message>
    <message>
        <location filename="../assets/mindw120h120du/eventCard.qml" line="375"/>
        <location filename="../assets/mindw76h128du/eventCard.qml" line="375"/>
        <location filename="../assets/mindw80h80du/eventCard.qml" line="423"/>
        <source>Can&apos;t add selected pass</source>
        <translation>Nelze přidat vybraný lístek</translation>
    </message>
    <message>
        <location filename="../assets/mindw120h120du/eventCard.qml" line="379"/>
        <location filename="../assets/mindw76h128du/eventCard.qml" line="379"/>
        <location filename="../assets/mindw80h80du/eventCard.qml" line="427"/>
        <source>Pass added successfuly</source>
        <translation>Lístek byl přidán</translation>
    </message>
    <message>
        <location filename="../assets/mindw120h120du/eventCard.qml" line="381"/>
        <location filename="../assets/mindw76h128du/eventCard.qml" line="381"/>
        <location filename="../assets/mindw80h80du/eventCard.qml" line="429"/>
        <source>Pass updated successfuly</source>
        <translation>Lístek byl upraven</translation>
    </message>
    <message>
        <location filename="../assets/mindw120h120du/eventCard.qml" line="383"/>
        <location filename="../assets/mindw76h128du/eventCard.qml" line="383"/>
        <location filename="../assets/mindw80h80du/eventCard.qml" line="431"/>
        <source>Pass operation finished successfuly</source>
        <translation>Manipulace s lístkem proběhla úspěšně</translation>
    </message>
    <message>
        <location filename="../assets/mindw120h120du/eventCard.qml" line="9"/>
        <location filename="../assets/mindw76h128du/eventCard.qml" line="9"/>
        <location filename="../assets/mindw80h80du/eventCard.qml" line="9"/>
        <source>event ticket</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/mindw120h120du/eventCard.qml" line="148"/>
        <location filename="../assets/mindw76h128du/eventCard.qml" line="148"/>
        <location filename="../assets/mindw80h80du/eventCard.qml" line="150"/>
        <source>barcode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/mindw120h120du/eventCard.qml" line="156"/>
        <location filename="../assets/mindw76h128du/eventCard.qml" line="156"/>
        <location filename="../assets/mindw80h80du/eventCard.qml" line="158"/>
        <source>barcode alternate text: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/mindw120h120du/eventCard.qml" line="213"/>
        <location filename="../assets/mindw76h128du/eventCard.qml" line="213"/>
        <location filename="../assets/mindw80h80du/eventCard.qml" line="215"/>
        <source>Barcode page</source>
        <translation>Stránka s kódem</translation>
    </message>
    <message>
        <location filename="../assets/mindw80h80du/eventCard.qml" line="223"/>
        <source>p</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/mindw80h80du/eventCard.qml" line="253"/>
        <source>r</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/mindw80h80du/eventCard.qml" line="267"/>
        <source>i</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/mindw80h80du/eventCard.qml" line="282"/>
        <source>d</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/mindw120h120du/eventCard.qml" line="240"/>
        <location filename="../assets/mindw76h128du/eventCard.qml" line="240"/>
        <location filename="../assets/mindw80h80du/eventCard.qml" line="287"/>
        <source>Brightness</source>
        <translation>Jas</translation>
    </message>
</context>
<context>
    <name>eventStrip</name>
    <message>
        <location filename="../assets/mindw80h80du/eventStrip.qml" line="208"/>
        <location filename="../assets/mindw80h80du/eventStrip.qml" line="221"/>
        <source>Barcode</source>
        <translation>Čárový kód</translation>
    </message>
    <message>
        <location filename="../assets/mindw120h120du/eventStrip.qml" line="200"/>
        <location filename="../assets/mindw76h128du/eventStrip.qml" line="200"/>
        <location filename="../assets/mindw80h80du/eventStrip.qml" line="238"/>
        <source>Info</source>
        <translation>Info</translation>
    </message>
    <message>
        <location filename="../assets/mindw80h80du/eventStrip.qml" line="208"/>
        <location filename="../assets/mindw80h80du/eventStrip.qml" line="212"/>
        <source>Content</source>
        <translation>Data</translation>
    </message>
    <message>
        <location filename="../assets/mindw120h120du/eventStrip.qml" line="7"/>
        <location filename="../assets/mindw76h128du/eventStrip.qml" line="7"/>
        <location filename="../assets/mindw80h80du/eventStrip.qml" line="7"/>
        <source>event ticket</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/mindw120h120du/eventStrip.qml" line="140"/>
        <location filename="../assets/mindw76h128du/eventStrip.qml" line="140"/>
        <location filename="../assets/mindw80h80du/eventStrip.qml" line="142"/>
        <source>barcode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/mindw120h120du/eventStrip.qml" line="148"/>
        <location filename="../assets/mindw76h128du/eventStrip.qml" line="148"/>
        <location filename="../assets/mindw80h80du/eventStrip.qml" line="150"/>
        <source>barcode alternate text: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/mindw120h120du/eventStrip.qml" line="192"/>
        <location filename="../assets/mindw76h128du/eventStrip.qml" line="192"/>
        <location filename="../assets/mindw80h80du/eventStrip.qml" line="193"/>
        <source>Barcode page</source>
        <translation>Stránka s kódem</translation>
    </message>
    <message>
        <location filename="../assets/mindw80h80du/eventStrip.qml" line="201"/>
        <source>p</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/mindw80h80du/eventStrip.qml" line="233"/>
        <source>r</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/mindw80h80du/eventStrip.qml" line="248"/>
        <source>d</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/mindw120h120du/eventStrip.qml" line="210"/>
        <location filename="../assets/mindw76h128du/eventStrip.qml" line="210"/>
        <location filename="../assets/mindw80h80du/eventStrip.qml" line="253"/>
        <source>Brightness</source>
        <translation>Jas</translation>
    </message>
</context>
<context>
    <name>eventStripCard</name>
    <message>
        <location filename="../assets/mindw120h120du/eventStripCard.qml" line="213"/>
        <location filename="../assets/mindw76h128du/eventStripCard.qml" line="213"/>
        <location filename="../assets/mindw80h80du/eventStripCard.qml" line="249"/>
        <source>Import</source>
        <translation>Import</translation>
    </message>
    <message>
        <location filename="../assets/mindw80h80du/eventStripCard.qml" line="218"/>
        <location filename="../assets/mindw80h80du/eventStripCard.qml" line="231"/>
        <source>Barcode</source>
        <translation>Čárový kód</translation>
    </message>
    <message>
        <location filename="../assets/mindw120h120du/eventStripCard.qml" line="221"/>
        <location filename="../assets/mindw76h128du/eventStripCard.qml" line="221"/>
        <location filename="../assets/mindw80h80du/eventStripCard.qml" line="262"/>
        <source>Info</source>
        <translation>Info</translation>
    </message>
    <message>
        <location filename="../assets/mindw80h80du/eventStripCard.qml" line="218"/>
        <location filename="../assets/mindw80h80du/eventStripCard.qml" line="222"/>
        <source>Content</source>
        <translation>Data</translation>
    </message>
    <message>
        <location filename="../assets/mindw120h120du/eventStripCard.qml" line="364"/>
        <location filename="../assets/mindw76h128du/eventStripCard.qml" line="364"/>
        <location filename="../assets/mindw80h80du/eventStripCard.qml" line="411"/>
        <source>Can&apos;t add selected pass</source>
        <translation>Nelze přidat vybraný lístek</translation>
    </message>
    <message>
        <location filename="../assets/mindw120h120du/eventStripCard.qml" line="368"/>
        <location filename="../assets/mindw76h128du/eventStripCard.qml" line="368"/>
        <location filename="../assets/mindw80h80du/eventStripCard.qml" line="415"/>
        <source>Pass added successfuly</source>
        <translation>Lístek byl přidán</translation>
    </message>
    <message>
        <location filename="../assets/mindw120h120du/eventStripCard.qml" line="370"/>
        <location filename="../assets/mindw76h128du/eventStripCard.qml" line="370"/>
        <location filename="../assets/mindw80h80du/eventStripCard.qml" line="417"/>
        <source>Pass updated successfuly</source>
        <translation>Lístek byl upraven</translation>
    </message>
    <message>
        <location filename="../assets/mindw120h120du/eventStripCard.qml" line="372"/>
        <location filename="../assets/mindw76h128du/eventStripCard.qml" line="372"/>
        <location filename="../assets/mindw80h80du/eventStripCard.qml" line="419"/>
        <source>Pass operation finished successfuly</source>
        <translation>Manipulace s lístkem proběhla úspěšně</translation>
    </message>
    <message>
        <location filename="../assets/mindw120h120du/eventStripCard.qml" line="9"/>
        <location filename="../assets/mindw76h128du/eventStripCard.qml" line="9"/>
        <location filename="../assets/mindw80h80du/eventStripCard.qml" line="9"/>
        <source>event ticket</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/mindw120h120du/eventStripCard.qml" line="145"/>
        <location filename="../assets/mindw76h128du/eventStripCard.qml" line="145"/>
        <location filename="../assets/mindw80h80du/eventStripCard.qml" line="144"/>
        <source>barcode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/mindw120h120du/eventStripCard.qml" line="153"/>
        <location filename="../assets/mindw76h128du/eventStripCard.qml" line="153"/>
        <location filename="../assets/mindw80h80du/eventStripCard.qml" line="152"/>
        <source>barcode alternate text: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/mindw120h120du/eventStripCard.qml" line="204"/>
        <location filename="../assets/mindw76h128du/eventStripCard.qml" line="204"/>
        <location filename="../assets/mindw80h80du/eventStripCard.qml" line="203"/>
        <source>Barcode page</source>
        <translation>Stránka s kódem</translation>
    </message>
    <message>
        <location filename="../assets/mindw80h80du/eventStripCard.qml" line="211"/>
        <source>p</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/mindw80h80du/eventStripCard.qml" line="243"/>
        <source>r</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/mindw80h80du/eventStripCard.qml" line="257"/>
        <source>i</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/mindw80h80du/eventStripCard.qml" line="272"/>
        <source>d</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/mindw120h120du/eventStripCard.qml" line="231"/>
        <location filename="../assets/mindw76h128du/eventStripCard.qml" line="231"/>
        <location filename="../assets/mindw80h80du/eventStripCard.qml" line="277"/>
        <source>Brightness</source>
        <translation>Jas</translation>
    </message>
</context>
<context>
    <name>generic</name>
    <message>
        <location filename="../assets/mindw80h80du/generic.qml" line="229"/>
        <location filename="../assets/mindw80h80du/generic.qml" line="242"/>
        <source>Barcode</source>
        <translation>Čárový kód</translation>
    </message>
    <message>
        <location filename="../assets/mindw120h120du/generic.qml" line="219"/>
        <location filename="../assets/mindw76h128du/generic.qml" line="219"/>
        <location filename="../assets/mindw80h80du/generic.qml" line="259"/>
        <source>Info</source>
        <translation>Info</translation>
    </message>
    <message>
        <location filename="../assets/mindw80h80du/generic.qml" line="229"/>
        <location filename="../assets/mindw80h80du/generic.qml" line="233"/>
        <source>Content</source>
        <translation>Data</translation>
    </message>
    <message>
        <location filename="../assets/mindw120h120du/generic.qml" line="5"/>
        <location filename="../assets/mindw76h128du/generic.qml" line="5"/>
        <location filename="../assets/mindw80h80du/generic.qml" line="5"/>
        <source>pass</source>
        <translation type="unfinished">lístek</translation>
    </message>
    <message>
        <location filename="../assets/mindw120h120du/generic.qml" line="153"/>
        <location filename="../assets/mindw76h128du/generic.qml" line="153"/>
        <location filename="../assets/mindw80h80du/generic.qml" line="156"/>
        <source>barcode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/mindw120h120du/generic.qml" line="161"/>
        <location filename="../assets/mindw76h128du/generic.qml" line="161"/>
        <location filename="../assets/mindw80h80du/generic.qml" line="164"/>
        <source>barcode alternate text: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/mindw120h120du/generic.qml" line="211"/>
        <location filename="../assets/mindw76h128du/generic.qml" line="211"/>
        <location filename="../assets/mindw80h80du/generic.qml" line="214"/>
        <source>Barcode page</source>
        <translation>Stránka s kódem</translation>
    </message>
    <message>
        <location filename="../assets/mindw80h80du/generic.qml" line="222"/>
        <source>p</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/mindw80h80du/generic.qml" line="254"/>
        <source>r</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/mindw80h80du/generic.qml" line="269"/>
        <source>d</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/mindw120h120du/generic.qml" line="229"/>
        <location filename="../assets/mindw76h128du/generic.qml" line="229"/>
        <location filename="../assets/mindw80h80du/generic.qml" line="274"/>
        <source>Brightness</source>
        <translation>Jas</translation>
    </message>
</context>
<context>
    <name>genericCard</name>
    <message>
        <location filename="../assets/mindw120h120du/genericCard.qml" line="231"/>
        <location filename="../assets/mindw76h128du/genericCard.qml" line="231"/>
        <location filename="../assets/mindw80h80du/genericCard.qml" line="271"/>
        <source>Import</source>
        <translation>Import</translation>
    </message>
    <message>
        <location filename="../assets/mindw80h80du/genericCard.qml" line="240"/>
        <location filename="../assets/mindw80h80du/genericCard.qml" line="253"/>
        <source>Barcode</source>
        <translation>Čárový kód</translation>
    </message>
    <message>
        <location filename="../assets/mindw120h120du/genericCard.qml" line="239"/>
        <location filename="../assets/mindw76h128du/genericCard.qml" line="239"/>
        <location filename="../assets/mindw80h80du/genericCard.qml" line="284"/>
        <source>Info</source>
        <translation>Info</translation>
    </message>
    <message>
        <location filename="../assets/mindw80h80du/genericCard.qml" line="240"/>
        <location filename="../assets/mindw80h80du/genericCard.qml" line="244"/>
        <source>Content</source>
        <translation>Data</translation>
    </message>
    <message>
        <location filename="../assets/mindw120h120du/genericCard.qml" line="384"/>
        <location filename="../assets/mindw76h128du/genericCard.qml" line="384"/>
        <location filename="../assets/mindw80h80du/genericCard.qml" line="435"/>
        <source>Can&apos;t add selected pass</source>
        <translation>Nelze přidat vybraný lístek</translation>
    </message>
    <message>
        <location filename="../assets/mindw120h120du/genericCard.qml" line="388"/>
        <location filename="../assets/mindw76h128du/genericCard.qml" line="388"/>
        <location filename="../assets/mindw80h80du/genericCard.qml" line="439"/>
        <source>Pass added successfuly</source>
        <translation>Lístek byl přidán</translation>
    </message>
    <message>
        <location filename="../assets/mindw120h120du/genericCard.qml" line="390"/>
        <location filename="../assets/mindw76h128du/genericCard.qml" line="390"/>
        <location filename="../assets/mindw80h80du/genericCard.qml" line="441"/>
        <source>Pass updated successfuly</source>
        <translation>Lístek byl upraven</translation>
    </message>
    <message>
        <location filename="../assets/mindw120h120du/genericCard.qml" line="392"/>
        <location filename="../assets/mindw76h128du/genericCard.qml" line="392"/>
        <location filename="../assets/mindw80h80du/genericCard.qml" line="443"/>
        <source>Pass operation finished successfuly</source>
        <translation>Manipulace s lístkem proběhla úspěšně</translation>
    </message>
    <message>
        <location filename="../assets/mindw120h120du/genericCard.qml" line="9"/>
        <location filename="../assets/mindw76h128du/genericCard.qml" line="9"/>
        <location filename="../assets/mindw80h80du/genericCard.qml" line="9"/>
        <source>pass</source>
        <translation type="unfinished">lístek</translation>
    </message>
    <message>
        <location filename="../assets/mindw120h120du/genericCard.qml" line="157"/>
        <location filename="../assets/mindw76h128du/genericCard.qml" line="157"/>
        <location filename="../assets/mindw80h80du/genericCard.qml" line="160"/>
        <source>barcode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/mindw120h120du/genericCard.qml" line="165"/>
        <location filename="../assets/mindw76h128du/genericCard.qml" line="165"/>
        <location filename="../assets/mindw80h80du/genericCard.qml" line="168"/>
        <source>barcode alternate text: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/mindw120h120du/genericCard.qml" line="222"/>
        <location filename="../assets/mindw76h128du/genericCard.qml" line="222"/>
        <location filename="../assets/mindw80h80du/genericCard.qml" line="225"/>
        <source>Barcode page</source>
        <translation>Stránka s kódem</translation>
    </message>
    <message>
        <location filename="../assets/mindw80h80du/genericCard.qml" line="233"/>
        <source>p</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/mindw80h80du/genericCard.qml" line="265"/>
        <source>r</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/mindw80h80du/genericCard.qml" line="279"/>
        <source>i</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/mindw80h80du/genericCard.qml" line="294"/>
        <source>d</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/mindw120h120du/genericCard.qml" line="249"/>
        <location filename="../assets/mindw76h128du/genericCard.qml" line="249"/>
        <location filename="../assets/mindw80h80du/genericCard.qml" line="299"/>
        <source>Brightness</source>
        <translation>Jas</translation>
    </message>
</context>
<context>
    <name>genericSquare</name>
    <message>
        <location filename="../assets/mindw80h80du/genericSquare.qml" line="235"/>
        <location filename="../assets/mindw80h80du/genericSquare.qml" line="247"/>
        <source>Barcode</source>
        <translation>Čárový kód</translation>
    </message>
    <message>
        <location filename="../assets/mindw120h120du/genericSquare.qml" line="219"/>
        <location filename="../assets/mindw76h128du/genericSquare.qml" line="219"/>
        <location filename="../assets/mindw80h80du/genericSquare.qml" line="263"/>
        <source>Info</source>
        <translation>Info</translation>
    </message>
    <message>
        <location filename="../assets/mindw80h80du/genericSquare.qml" line="235"/>
        <location filename="../assets/mindw80h80du/genericSquare.qml" line="239"/>
        <source>Content</source>
        <translation>Data</translation>
    </message>
    <message>
        <location filename="../assets/mindw120h120du/genericSquare.qml" line="5"/>
        <location filename="../assets/mindw76h128du/genericSquare.qml" line="5"/>
        <location filename="../assets/mindw80h80du/genericSquare.qml" line="5"/>
        <source>pass</source>
        <translation type="unfinished">lístek</translation>
    </message>
    <message>
        <location filename="../assets/mindw120h120du/genericSquare.qml" line="144"/>
        <location filename="../assets/mindw76h128du/genericSquare.qml" line="144"/>
        <location filename="../assets/mindw80h80du/genericSquare.qml" line="147"/>
        <source>barcode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/mindw120h120du/genericSquare.qml" line="152"/>
        <location filename="../assets/mindw76h128du/genericSquare.qml" line="152"/>
        <location filename="../assets/mindw80h80du/genericSquare.qml" line="155"/>
        <source>barcode alternate text: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/mindw120h120du/genericSquare.qml" line="192"/>
        <location filename="../assets/mindw120h120du/genericSquare.qml" line="211"/>
        <location filename="../assets/mindw76h128du/genericSquare.qml" line="192"/>
        <location filename="../assets/mindw76h128du/genericSquare.qml" line="211"/>
        <location filename="../assets/mindw80h80du/genericSquare.qml" line="195"/>
        <location filename="../assets/mindw80h80du/genericSquare.qml" line="220"/>
        <source>Barcode page</source>
        <translation>Stránka s kódem</translation>
    </message>
    <message>
        <location filename="../assets/mindw80h80du/genericSquare.qml" line="228"/>
        <source>p</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/mindw80h80du/genericSquare.qml" line="258"/>
        <source>r</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/mindw80h80du/genericSquare.qml" line="273"/>
        <source>d</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/mindw120h120du/genericSquare.qml" line="229"/>
        <location filename="../assets/mindw76h128du/genericSquare.qml" line="229"/>
        <location filename="../assets/mindw80h80du/genericSquare.qml" line="278"/>
        <source>Brightness</source>
        <translation>Jas</translation>
    </message>
</context>
<context>
    <name>genericSquareCard</name>
    <message>
        <location filename="../assets/mindw120h120du/genericSquareCard.qml" line="222"/>
        <location filename="../assets/mindw76h128du/genericSquareCard.qml" line="222"/>
        <location filename="../assets/mindw80h80du/genericSquareCard.qml" line="260"/>
        <source>Import</source>
        <translation>Import</translation>
    </message>
    <message>
        <location filename="../assets/mindw80h80du/genericSquareCard.qml" line="231"/>
        <location filename="../assets/mindw80h80du/genericSquareCard.qml" line="243"/>
        <source>Barcode</source>
        <translation>Čárový kód</translation>
    </message>
    <message>
        <location filename="../assets/mindw120h120du/genericSquareCard.qml" line="230"/>
        <location filename="../assets/mindw76h128du/genericSquareCard.qml" line="230"/>
        <location filename="../assets/mindw80h80du/genericSquareCard.qml" line="273"/>
        <source>Info</source>
        <translation>Info</translation>
    </message>
    <message>
        <location filename="../assets/mindw80h80du/genericSquareCard.qml" line="231"/>
        <location filename="../assets/mindw80h80du/genericSquareCard.qml" line="235"/>
        <source>Content</source>
        <translation>Data</translation>
    </message>
    <message>
        <location filename="../assets/mindw120h120du/genericSquareCard.qml" line="375"/>
        <location filename="../assets/mindw76h128du/genericSquareCard.qml" line="375"/>
        <location filename="../assets/mindw80h80du/genericSquareCard.qml" line="424"/>
        <source>Can&apos;t add selected pass</source>
        <translation>Nelze přidat vybraný lístek</translation>
    </message>
    <message>
        <location filename="../assets/mindw120h120du/genericSquareCard.qml" line="379"/>
        <location filename="../assets/mindw76h128du/genericSquareCard.qml" line="379"/>
        <location filename="../assets/mindw80h80du/genericSquareCard.qml" line="428"/>
        <source>Pass added successfuly</source>
        <translation>Lístek byl přidán</translation>
    </message>
    <message>
        <location filename="../assets/mindw120h120du/genericSquareCard.qml" line="381"/>
        <location filename="../assets/mindw76h128du/genericSquareCard.qml" line="381"/>
        <location filename="../assets/mindw80h80du/genericSquareCard.qml" line="430"/>
        <source>Pass updated successfuly</source>
        <translation>Lístek byl upraven</translation>
    </message>
    <message>
        <location filename="../assets/mindw120h120du/genericSquareCard.qml" line="383"/>
        <location filename="../assets/mindw76h128du/genericSquareCard.qml" line="383"/>
        <location filename="../assets/mindw80h80du/genericSquareCard.qml" line="432"/>
        <source>Pass operation finished successfuly</source>
        <translation>Manipulace s lístkem proběhla úspěšně</translation>
    </message>
    <message>
        <location filename="../assets/mindw120h120du/genericSquareCard.qml" line="9"/>
        <location filename="../assets/mindw76h128du/genericSquareCard.qml" line="9"/>
        <location filename="../assets/mindw80h80du/genericSquareCard.qml" line="9"/>
        <source>pass</source>
        <translation>lístek</translation>
    </message>
    <message>
        <location filename="../assets/mindw120h120du/genericSquareCard.qml" line="148"/>
        <location filename="../assets/mindw76h128du/genericSquareCard.qml" line="148"/>
        <location filename="../assets/mindw80h80du/genericSquareCard.qml" line="151"/>
        <source>barcode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/mindw120h120du/genericSquareCard.qml" line="156"/>
        <location filename="../assets/mindw76h128du/genericSquareCard.qml" line="156"/>
        <location filename="../assets/mindw80h80du/genericSquareCard.qml" line="159"/>
        <source>barcode alternate text: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/mindw120h120du/genericSquareCard.qml" line="213"/>
        <location filename="../assets/mindw76h128du/genericSquareCard.qml" line="213"/>
        <location filename="../assets/mindw80h80du/genericSquareCard.qml" line="216"/>
        <source>Barcode page</source>
        <translation>Stránka s kódem</translation>
    </message>
    <message>
        <location filename="../assets/mindw80h80du/genericSquareCard.qml" line="224"/>
        <source>p</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/mindw80h80du/genericSquareCard.qml" line="254"/>
        <source>r</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/mindw80h80du/genericSquareCard.qml" line="268"/>
        <source>i</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/mindw80h80du/genericSquareCard.qml" line="283"/>
        <source>d</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/mindw120h120du/genericSquareCard.qml" line="240"/>
        <location filename="../assets/mindw76h128du/genericSquareCard.qml" line="240"/>
        <location filename="../assets/mindw80h80du/genericSquareCard.qml" line="288"/>
        <source>Brightness</source>
        <translation>Jas</translation>
    </message>
</context>
<context>
    <name>main</name>
    <message>
        <location filename="../assets/main.qml" line="14"/>
        <source>About</source>
        <translation type="unfinished">O aplikaci</translation>
    </message>
    <message>
        <location filename="../assets/main.qml" line="43"/>
        <source>Search term</source>
        <translation type="unfinished">Hledaný výraz</translation>
    </message>
    <message>
        <location filename="../assets/main.qml" line="58"/>
        <source>Cancel</source>
        <translation type="unfinished">Zrušit</translation>
    </message>
    <message>
        <location filename="../assets/main.qml" line="92"/>
        <source>There are no passes yet.</source>
        <translation type="unfinished">V seznamu nejsou žádné lístky.</translation>
    </message>
    <message>
        <location filename="../assets/main.qml" line="160"/>
        <source>Importing passes</source>
        <translation type="unfinished">Import lístků</translation>
    </message>
    <message>
        <location filename="../assets/main.qml" line="134"/>
        <source>You can import the pass</source>
        <translation type="unfinished">Můžete importovat lístek</translation>
    </message>
    <message>
        <location filename="../assets/main.qml" line="116"/>
        <location filename="../assets/main.qml" line="154"/>
        <source>Import pass help</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/main.qml" line="163"/>
        <source>Passes supports import of existing files. Pass issuer usually gives link to download pkpass pass file or sends it by mail. The application works with pkpass files. PDF or other files are not supported.</source>
        <translation>Aplikace umožňuje importovat existující lístky. Lístek obvykle vydavatel posílá mailem nebo nabízí odkaz ke stažení souboru. Aplikace pracuje pouze s pkpass soubory. Soubory PDF nejsou podporovány.</translation>
    </message>
    <message>
        <location filename="../assets/main.qml" line="173"/>
        <source>Create pass</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/main.qml" line="193"/>
        <source>You can create the pass</source>
        <translation type="unfinished">Můžete vytvořit lístek</translation>
    </message>
    <message>
        <location filename="../assets/main.qml" line="214"/>
        <source>Create pass help</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/main.qml" line="220"/>
        <source>Create new pass</source>
        <translation type="unfinished">Vytvořit nový lístek</translation>
    </message>
    <message>
        <location filename="../assets/main.qml" line="223"/>
        <source>You can create your own pass. There are two types predefined - generic pass and boarding pass.</source>
        <translation type="unfinished">Můžete vytvářet vlastní lístky. V aplikaci jsou přednastavené dva typy - zákaznická karta a cestovní doklad.</translation>
    </message>
    <message>
        <location filename="../assets/main.qml" line="236"/>
        <source>Saved passes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/main.qml" line="237"/>
        <source>Cards are stacked. Activate the card to view the full card.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/main.qml" line="432"/>
        <source>Delete the pass from</source>
        <translation type="unfinished">Smazat lístek od</translation>
    </message>
    <message>
        <location filename="../assets/main.qml" line="447"/>
        <source>Search</source>
        <translation type="unfinished">Hledat</translation>
    </message>
    <message>
        <location filename="../assets/main.qml" line="465"/>
        <source>Import</source>
        <translation type="unfinished">Import</translation>
    </message>
    <message>
        <location filename="../assets/main.qml" line="473"/>
        <source>Create</source>
        <translation type="unfinished">Vytvořit</translation>
    </message>
    <message>
        <location filename="../assets/main.qml" line="484"/>
        <location filename="../assets/main.qml" line="497"/>
        <source>Grid view</source>
        <translation type="unfinished">Mřížka</translation>
    </message>
    <message>
        <location filename="../assets/main.qml" line="484"/>
        <location filename="../assets/main.qml" line="491"/>
        <source>List view</source>
        <translation type="unfinished">Seznam</translation>
    </message>
    <message>
        <location filename="../assets/main.qml" line="507"/>
        <source>Send Feedback</source>
        <translation type="unfinished">Připomínky k aplikaci</translation>
    </message>
    <message>
        <location filename="../assets/main.qml" line="553"/>
        <source>Delete the pass?</source>
        <translation type="unfinished">Smazat lístek?</translation>
    </message>
    <message>
        <location filename="../assets/main.qml" line="700"/>
        <source>Can&apos;t add selected pass</source>
        <translation type="unfinished">Nelze přidat vybraný lístek</translation>
    </message>
    <message>
        <location filename="../assets/main.qml" line="705"/>
        <source>Pass added successfuly</source>
        <translation type="unfinished">Lístek byl přidán</translation>
    </message>
    <message>
        <location filename="../assets/main.qml" line="708"/>
        <source>Pass updated successfuly</source>
        <translation type="unfinished">Lístek byl upraven</translation>
    </message>
    <message>
        <location filename="../assets/main.qml" line="711"/>
        <source>Pass operation finished successfuly</source>
        <translation type="unfinished">Manipulace s lístkem proběhla úspěšně</translation>
    </message>
    <message>
        <location filename="../assets/main.qml" line="722"/>
        <source>Processing pass started</source>
        <translation type="unfinished">Začalo zpracovávání lístku</translation>
    </message>
    <message>
        <location filename="../assets/main.qml" line="729"/>
        <source>Pass successfuly removed</source>
        <translation type="unfinished">Lístek byl odebrán</translation>
    </message>
    <message>
        <location filename="../assets/main.qml" line="732"/>
        <source>Removing the pass failed</source>
        <translation type="unfinished">Odebrání lístku se nezdařilo</translation>
    </message>
</context>
<context>
    <name>scan</name>
    <message>
        <location filename="../assets/scan.qml" line="10"/>
        <source>Scan Code</source>
        <translation>Načíst kód</translation>
    </message>
    <message>
        <location filename="../assets/scan.qml" line="12"/>
        <source>Cancel</source>
        <translation type="unfinished">Zrušit</translation>
    </message>
    <message>
        <location filename="../assets/scan.qml" line="63"/>
        <source>Unable to scan the barcode</source>
        <translation>Kód nelze načíst</translation>
    </message>
</context>
<context>
    <name>store</name>
    <message>
        <location filename="../assets/mindw120h120du/store.qml" line="194"/>
        <location filename="../assets/mindw76h128du/store.qml" line="194"/>
        <location filename="../assets/mindw80h80du/store.qml" line="246"/>
        <source>Info</source>
        <translation>Info</translation>
    </message>
    <message>
        <location filename="../assets/mindw80h80du/store.qml" line="218"/>
        <location filename="../assets/mindw80h80du/store.qml" line="230"/>
        <source>Barcode</source>
        <translation>Čárový kód</translation>
    </message>
    <message>
        <location filename="../assets/mindw80h80du/store.qml" line="218"/>
        <location filename="../assets/mindw80h80du/store.qml" line="222"/>
        <source>Content</source>
        <translation>Data</translation>
    </message>
    <message>
        <location filename="../assets/mindw120h120du/store.qml" line="7"/>
        <location filename="../assets/mindw76h128du/store.qml" line="7"/>
        <location filename="../assets/mindw80h80du/store.qml" line="7"/>
        <source>store card</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/mindw120h120du/store.qml" line="134"/>
        <location filename="../assets/mindw76h128du/store.qml" line="134"/>
        <location filename="../assets/mindw80h80du/store.qml" line="151"/>
        <source>barcode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/mindw120h120du/store.qml" line="142"/>
        <location filename="../assets/mindw76h128du/store.qml" line="142"/>
        <location filename="../assets/mindw80h80du/store.qml" line="159"/>
        <source>barcode alternate text: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/mindw120h120du/store.qml" line="186"/>
        <location filename="../assets/mindw76h128du/store.qml" line="186"/>
        <location filename="../assets/mindw80h80du/store.qml" line="203"/>
        <source>Barcode page</source>
        <translation>Stránka s kódem</translation>
    </message>
    <message>
        <location filename="../assets/mindw80h80du/store.qml" line="211"/>
        <source>p</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/mindw80h80du/store.qml" line="241"/>
        <source>r</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/mindw80h80du/store.qml" line="256"/>
        <source>d</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/mindw120h120du/store.qml" line="204"/>
        <location filename="../assets/mindw76h128du/store.qml" line="204"/>
        <location filename="../assets/mindw80h80du/store.qml" line="261"/>
        <source>Brightness</source>
        <translation>Jas</translation>
    </message>
</context>
<context>
    <name>storeCard</name>
    <message>
        <location filename="../assets/mindw120h120du/storeCard.qml" line="199"/>
        <location filename="../assets/mindw76h128du/storeCard.qml" line="199"/>
        <location filename="../assets/mindw80h80du/storeCard.qml" line="236"/>
        <source>Import</source>
        <translation>Import</translation>
    </message>
    <message>
        <location filename="../assets/mindw120h120du/storeCard.qml" line="207"/>
        <location filename="../assets/mindw76h128du/storeCard.qml" line="207"/>
        <location filename="../assets/mindw80h80du/storeCard.qml" line="249"/>
        <source>Info</source>
        <translation>Info</translation>
    </message>
    <message>
        <location filename="../assets/mindw120h120du/storeCard.qml" line="350"/>
        <location filename="../assets/mindw76h128du/storeCard.qml" line="350"/>
        <location filename="../assets/mindw80h80du/storeCard.qml" line="398"/>
        <source>Can&apos;t add selected pass</source>
        <translation>Nelze přidat vybraný lístek</translation>
    </message>
    <message>
        <location filename="../assets/mindw120h120du/storeCard.qml" line="354"/>
        <location filename="../assets/mindw76h128du/storeCard.qml" line="354"/>
        <location filename="../assets/mindw80h80du/storeCard.qml" line="402"/>
        <source>Pass added successfuly</source>
        <translation>Lístek byl přidán</translation>
    </message>
    <message>
        <location filename="../assets/mindw120h120du/storeCard.qml" line="356"/>
        <location filename="../assets/mindw76h128du/storeCard.qml" line="356"/>
        <location filename="../assets/mindw80h80du/storeCard.qml" line="404"/>
        <source>Pass updated successfuly</source>
        <translation>Lístek byl upraven</translation>
    </message>
    <message>
        <location filename="../assets/mindw120h120du/storeCard.qml" line="358"/>
        <location filename="../assets/mindw76h128du/storeCard.qml" line="358"/>
        <location filename="../assets/mindw80h80du/storeCard.qml" line="406"/>
        <source>Pass operation finished successfuly</source>
        <translation>Manipulace s lístkem proběhla úspěšně</translation>
    </message>
    <message>
        <location filename="../assets/mindw80h80du/storeCard.qml" line="207"/>
        <location filename="../assets/mindw80h80du/storeCard.qml" line="219"/>
        <source>Barcode</source>
        <translation>Čárový kód</translation>
    </message>
    <message>
        <location filename="../assets/mindw80h80du/storeCard.qml" line="207"/>
        <location filename="../assets/mindw80h80du/storeCard.qml" line="211"/>
        <source>Content</source>
        <translation>Data</translation>
    </message>
    <message>
        <location filename="../assets/mindw120h120du/storeCard.qml" line="9"/>
        <location filename="../assets/mindw76h128du/storeCard.qml" line="9"/>
        <location filename="../assets/mindw80h80du/storeCard.qml" line="9"/>
        <source>store card</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/mindw120h120du/storeCard.qml" line="131"/>
        <location filename="../assets/mindw76h128du/storeCard.qml" line="131"/>
        <location filename="../assets/mindw80h80du/storeCard.qml" line="133"/>
        <source>barcode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/mindw120h120du/storeCard.qml" line="139"/>
        <location filename="../assets/mindw76h128du/storeCard.qml" line="139"/>
        <location filename="../assets/mindw80h80du/storeCard.qml" line="141"/>
        <source>barcode alternate text: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/mindw120h120du/storeCard.qml" line="190"/>
        <location filename="../assets/mindw76h128du/storeCard.qml" line="190"/>
        <location filename="../assets/mindw80h80du/storeCard.qml" line="192"/>
        <source>Barcode page</source>
        <translation>Stránka s kódem</translation>
    </message>
    <message>
        <location filename="../assets/mindw80h80du/storeCard.qml" line="200"/>
        <source>p</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/mindw80h80du/storeCard.qml" line="230"/>
        <source>r</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/mindw80h80du/storeCard.qml" line="244"/>
        <source>i</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/mindw80h80du/storeCard.qml" line="259"/>
        <source>d</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/mindw120h120du/storeCard.qml" line="217"/>
        <location filename="../assets/mindw76h128du/storeCard.qml" line="217"/>
        <location filename="../assets/mindw80h80du/storeCard.qml" line="264"/>
        <source>Brightness</source>
        <translation>Jas</translation>
    </message>
</context>
</TS>
