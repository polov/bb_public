<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0">
<context>
    <name>AboutSheet</name>
    <message>
        <source>Passes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>version</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>build by</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Done</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>License agreement (English)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>This application can help you managing all passbook files (.pkpass files). You can receive them by mail or download from an issuer through link.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Privacy policy (English)</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SettingsSheet</name>
    <message>
        <source>Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Done</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Constantly check current location and send notification when you are near any location stored in one of your pass.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Application Permissions</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Location Services</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Use location services</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>There are problems with using location services. Please check permissions.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>add</name>
    <message>
        <source>Create Pass</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Create</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Pass type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Boarding pass</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Generic pass</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Departure label</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Departure value</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Arrival label</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Arrival value</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Primary field label</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Primary field value</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Logo and text</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Text next to the logo</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Colors</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Background Color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Text Color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Label Color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Header Field</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Header field label</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Header field value</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Primary Field</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Barcode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Barcode type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Information in the barcode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Scan Barcode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Alternate text</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Every pass has its background color. Text color is color of field values. Label color is color of field labels that are above field values.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Header field label and text are displayed on the top of the pass and in the list of your passes. Header field label uses small letters and is above the header field value. Header fiel value uses larger letters and is under the label. You can write label &quot;POINTS&quot; and amount as the value, or in case of boarding pass &quot;TRACK&quot; and the track of the train.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Primary Field usually contains important information, such as Member Id, Coupon Id, Member Name. It depends on the purpose of the pass. In case of boarding pass there are deptarture and arrival places with its codes as values. There are primary field label (use smaller letters) and bellow is primary field value (uses larger letters).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>You can create 3 types of barcode: QR code, EAN code and Aztec code. You can manually enter message encoded in the barcode and the barcode type or you can use scan feature (e.g. scan an existing card/coupon/etc). If it is possible and if it is nowhere on the pass, it is good to write the message to the alternate text - it is a back up in the case that scanner will be unable to scan the display of your mobile phone.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Change image</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Change color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Logo is the image on the top and is followed by the text. The text will be used in search.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Company name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Name of the company which issued the card/coupon/pass. The name will be used in search.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>More info</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Additional information</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Place for additional information that you want to have available together with the pass.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Additional information label</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>barcodePage</name>
    <message>
        <source>barcode view</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Pinch to zoom the barcode</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>boardingPass</name>
    <message>
        <source>Barcode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Info</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Content</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>boarding pass</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Barcode page</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>barcode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>barcode alternate text: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>p</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>d</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>r</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Brightness</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>boardingPassCard</name>
    <message>
        <source>Barcode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Info</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Content</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Import</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Can&apos;t add selected pass</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Pass added successfuly</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Pass updated successfuly</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Pass operation finished successfuly</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Barcode page</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>boarding pass</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>barcode alternate text: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>barcode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>p</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>i</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>d</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>r</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Brightness</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>colorPicker</name>
    <message>
        <source>Color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Choose</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Selected color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>coupon</name>
    <message>
        <source>Barcode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Info</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Content</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>coupon</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>barcode alternate text: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Barcode page</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>barcode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>p</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>d</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>r</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Brightness</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>couponCard</name>
    <message>
        <source>Barcode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Info</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Content</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Import</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Can&apos;t add selected pass</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Pass added successfuly</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Pass updated successfuly</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Pass operation finished successfuly</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>coupon</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>barcode alternate text: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Barcode page</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>barcode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>p</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>i</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>d</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>r</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Brightness</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>event</name>
    <message>
        <source>Barcode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Info</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Content</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>event ticket</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>barcode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>barcode alternate text: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Barcode page</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>p</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>d</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>r</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Brightness</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>eventCard</name>
    <message>
        <source>Barcode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Info</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Content</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Import</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Can&apos;t add selected pass</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Pass added successfuly</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Pass updated successfuly</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Pass operation finished successfuly</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>event ticket</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>barcode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>barcode alternate text: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Barcode page</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>p</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>i</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>d</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>r</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Brightness</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>eventStrip</name>
    <message>
        <source>Barcode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Info</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Content</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>event ticket</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>barcode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>barcode alternate text: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Barcode page</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>p</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>d</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>r</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Brightness</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>eventStripCard</name>
    <message>
        <source>Barcode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Info</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Content</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Import</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Can&apos;t add selected pass</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Pass added successfuly</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Pass updated successfuly</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Pass operation finished successfuly</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>event ticket</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>barcode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>barcode alternate text: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Barcode page</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>p</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>i</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>d</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>r</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Brightness</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>generic</name>
    <message>
        <source>Barcode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Info</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Content</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>pass</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>barcode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>barcode alternate text: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Barcode page</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>p</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>d</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>r</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Brightness</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>genericCard</name>
    <message>
        <source>Barcode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Info</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Content</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Import</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Can&apos;t add selected pass</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Pass added successfuly</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Pass updated successfuly</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Pass operation finished successfuly</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>pass</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>barcode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>barcode alternate text: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Barcode page</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>p</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>i</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>d</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>r</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Brightness</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>genericSquare</name>
    <message>
        <source>Barcode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Info</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Content</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>pass</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>barcode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>barcode alternate text: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Barcode page</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>p</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>d</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>r</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Brightness</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>genericSquareCard</name>
    <message>
        <source>Barcode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Info</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Content</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Import</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Can&apos;t add selected pass</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Pass added successfuly</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Pass updated successfuly</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Pass operation finished successfuly</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>pass</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>barcode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>barcode alternate text: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Barcode page</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>p</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>i</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>d</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>r</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Brightness</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>main</name>
    <message>
        <source>About</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Search term</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>There are no passes yet.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Import pass help</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>You can import the pass</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Importing passes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Create pass</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>You can create the pass</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Create pass help</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Create new pass</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>You can create your own pass. There are two types predefined - generic pass and boarding pass.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Saved passes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Cards are stacked. Activate the card to view the full card.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Delete the pass from</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Search</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Import</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Create</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Grid view</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>List view</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Send Feedback</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Delete the pass?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Can&apos;t add selected pass</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Pass added successfuly</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Pass updated successfuly</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Pass operation finished successfuly</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Processing pass started</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Pass successfuly removed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Removing the pass failed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Passes supports import of existing files. Pass issuer usually gives link to download pkpass pass file or sends it by mail. The application works with pkpass files. PDF or other files are not supported.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>scan</name>
    <message>
        <source>Scan Code</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Unable to scan the barcode</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>store</name>
    <message>
        <source>Barcode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Info</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Content</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>store card</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>barcode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>barcode alternate text: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Barcode page</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>p</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>d</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>r</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Brightness</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>storeCard</name>
    <message>
        <source>Barcode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Info</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Content</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Import</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Can&apos;t add selected pass</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Pass added successfuly</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Pass updated successfuly</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Pass operation finished successfuly</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>store card</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>barcode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>barcode alternate text: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Barcode page</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>p</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>i</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>d</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>r</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Brightness</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
