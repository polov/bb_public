/*
 * GeoHelper.hpp
 *
 *  Created on: 5. 1. 2015
 *      Author: martin
 */

#ifndef GEOHELPER_HPP_
#define GEOHELPER_HPP_

#include <QObject>
#include <QTimer>

#include <QtLocationSubset/QGeoPositionInfo>
#include <QtLocationSubset/QGeoPositionInfoSource>
#include <QtLocationSubset/QGeoSatelliteInfo>
#include <QtLocationSubset/QGeoSatelliteInfoSource>

using namespace QtMobilitySubset;

class GeoHelper: public QObject
{
    Q_OBJECT
    Q_PROPERTY(bool geolocationEnabled READ getGeolocationEnabled WRITE setGeolocationEnabled NOTIFY geolocationEnabledChanged)
    Q_PROPERTY(int geolocationStatus READ getGeolocationStatus WRITE setGeolocationStatus NOTIFY geolocationStatusChanged)

public:
    GeoHelper();
    virtual ~GeoHelper();

    Q_INVOKABLE
    void updateLocationsList(QVariantList passes);Q_INVOKABLE
    void findNearby();

    void start();
    void stop();

    bool getGeolocationEnabled();
    void setGeolocationEnabled(const bool geolocationEnabled);

    int getGeolocationStatus();
    void setGeolocationStatus(int status);

    void checkGeolocationStatus();

signals:
    void nearbyLocation(QString);
    void geolocationEnabledChanged();
    void geolocationStatusChanged();

private slots:
    void requestPosition();
    void onPositionUpdated(const QGeoPositionInfo &);

private:
    QVariantList locations;
    QTimer timer;
    QGeoPositionInfoSource *geosrc;
    QGeoPositionInfo position;
    bool positionUpdatedConnected;
    int geolocationStatus;
};

#endif /* GEOHELPER_HPP_ */
