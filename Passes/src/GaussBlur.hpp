/*
 * GaussBlur.hpp
 *
 *  Created on: 15. 10. 2014
 *      Author: martin
 */

#ifndef GAUSSBLUR_HPP_
#define GAUSSBLUR_HPP_

#include <QObject>
#include <QDebug>
#include <QtGui/QImage>

class GaussBlur: public QObject
{
Q_OBJECT
public:
    GaussBlur();
    virtual ~GaussBlur();

    void blurImages(QString path, bool card);

private:
    QImage readImageFile(QString identificator, QString name, bool card);
    QImage gaussBlurQuick2(const QImage & original, unsigned int radius, unsigned int boxes);

};

#endif /* GAUSSBLUR_HPP_ */
