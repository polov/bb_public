/*
 * GeoHelper.cpp
 *
 *  Created on: 5. 1. 2015
 *      Author: martin
 */

#include <src/GeoHelper.hpp>

#include <QDebug>
#include <QSettings>

#include <bb/platform/Notification>
#include <bb/platform/NotificationDefaultApplicationSettings>
#include <bb/system/InvokeRequest>
#include <bb/location/PositionErrorCode>
#include "FileSystemHelper.hpp"

using namespace bb::platform;
using namespace bb::system;

GeoHelper::GeoHelper() :
        QObject(), locations(), timer(this)
{
    geosrc = QGeoPositionInfoSource::createDefaultSource(this);
    geosrc->setPreferredPositioningMethods(QGeoPositionInfoSource::NonSatellitePositioningMethods);
    positionUpdatedConnected = connect(geosrc, SIGNAL(positionUpdated(const QGeoPositionInfo &)),
            this, SLOT(onPositionUpdated(const QGeoPositionInfo &)));

    connect(&timer, SIGNAL(timeout()), this, SLOT(requestPosition()));

    QSettings settings("polovincak", "Passes");
    bool geolocationEnabled = settings.value("geolocationEnabled", QVariant(true)).toBool();

    if (positionUpdatedConnected && geolocationEnabled) {
        checkGeolocationStatus();
        if(geolocationStatus==1) {
            start();
        }
    } else {
        qDebug() << "Something went wrong" << positionUpdatedConnected << geolocationEnabled;
    }

}

GeoHelper::~GeoHelper()
{

}

void GeoHelper::updateLocationsList(QVariantList passes)
{
    locations.clear();
    QString identificator;
    QString organizationName;
    QString latitude, longitude;
    for (int i = 0; i < passes.size(); i++) {
        identificator = passes.at(i).toMap().value("identificator").toString();
        organizationName = passes.at(i).toMap().value("organizationName").toString();
        QVariantList list = passes.at(i).toMap().value("locations").toList();
        for (int j = 0; j < list.size(); j++) {
            QVariant listItem = list.at(j);
            QVariantMap map;
            map.insert("identificator", identificator);
            map.insert("organizationName", organizationName);
            map.insert("latitude", listItem.toMap().value("latitude").toString());
            map.insert("longitude", listItem.toMap().value("longitude").toString());
            map.insert("altitude", listItem.toMap().value("altitude").toString());
            map.insert("relevantText", listItem.toMap().value("relevantText").toString());
            locations.append(map);
            //qDebug() << "adding " << map;
        }
    }
}

void GeoHelper::findNearby()
{
    FileSystemHelper fsh;
    bool first = true;

    bb::platform::NotificationDefaultApplicationSettings settings;
    settings.setPreview(bb::platform::NotificationPriorityPolicy::Allow);
    settings.apply();

    QSettings settings2("polovincak", "Passes");

    for (int i = 0; i < locations.size(); i++) {
        QGeoCoordinate coordinate;
        QVariant latitude = locations.at(i).toMap().value("latitude");
        coordinate.setLatitude(latitude.toDouble());
        QVariant longitude = locations.at(i).toMap().value("longitude");
        coordinate.setLongitude(longitude.toDouble());
        qreal distance = position.coordinate().distanceTo(coordinate);
        QString identificator = locations.at(i).toMap().value("identificator").toString();
        qDebug() << identificator << "distance" << distance<< settings2.value(identificator + "/sendNotification"+ "_" + latitude.toString() + "_" + longitude.toString(), "true").toString();

        if (distance < 15.0) {

            QString title = locations.at(i).toMap().value("organizationName").toString();
            QString body = locations.at(i).toMap().value("relevantText").toString();
            //qDebug() << "post notification, distance is ok" << identificator;

            QString sendNotification = settings2.value(identificator + "/sendNotification"+ "_" + latitude.toString() + "_" + longitude.toString(), "true").toString();
            //qDebug() << "XXXXXXXX" << identificator << sendNotification << settings2.value(identificator + "/sendNotification");

            if(sendNotification == "true") {
                //qDebug() << "post notification requested" << identificator;

                Notification *notification = new Notification();
                notification->setTitle(title);
                notification->setBody(body);
                notification->setIconUrl(QUrl("asset:///images/icon_notification.png"));

                InvokeRequest invokeRequest;
                invokeRequest.setTarget("com.polov.passes.invoke.open");
                invokeRequest.setAction("bb.action.OPEN");
                invokeRequest.setMimeType("text/plain");
                invokeRequest.setData(
                        locations.at(i).toMap().value("identificator").toString().toUtf8());

                if (first) {
                    first = false;
                    notification->deleteAllFromInbox();
                }
                notification->setInvokeRequest(invokeRequest);
                notification->notify();
                delete notification;

                settings2.setValue(identificator + "/sendNotification"+ "_" + latitude.toString() + "_" + longitude.toString(), "false");
                //qDebug() << "sendNotification set to false" << identificator << settings2.value(identificator + "/sendNotification").toString();
            }

        } else {
            //qDebug() << "do not post notification, distance is nok" << identificator;
            if(settings2.value(identificator + "/sendNotification"+ "_" + latitude.toString() + "_" + longitude.toString(), "true") == "false") {
                settings2.setValue(identificator + "/sendNotification"+ "_" + latitude.toString() + "_" + longitude.toString(), "true");
                //qDebug() << "sendNotification set" << identificator << settings2.value(identificator + "/sendNotification").toString();
            }
        }
    }
}

void GeoHelper::requestPosition()
{
//stop the timer
    stop();
//check, if we should be asking for position
    QSettings settings("polovincak", "Passes");
    if (!settings.value("geolocationEnabled", QVariant(true)).toBool()) {
        stop();
        return;
    }
//ask for position
    if (positionUpdatedConnected && settings.value("geolocationEnabled", QVariant(true)).toBool()) {
        geosrc->requestUpdate();
        qDebug() << "Position update requested";
    } else {
        qDebug() << "Cannot request for location data!";
    }
}

void GeoHelper::onPositionUpdated(const QGeoPositionInfo &p)
{
    position = p;
    qDebug() << "User position updated, latitude" << p.coordinate().latitude() << "longtitude"
            << p.coordinate().longitude() << "type" << p.coordinate().type();
    findNearby();
//start the timer
    start();
}

void GeoHelper::start()
{
    timer.start(30000);
}

void GeoHelper::stop()
{
    timer.stop();
}

bool GeoHelper::getGeolocationEnabled()
{
    QSettings settings("polovincak", "Passes");
    bool geolocationEnabled = settings.value("geolocationEnabled", QVariant(true)).toBool();
    qDebug() << "returning geolocation settings" << geolocationEnabled;
    return geolocationEnabled;
}
void GeoHelper::setGeolocationEnabled(const bool geolocationEnabled)
{
    QSettings settings("polovincak", "Passes");
    settings.setValue("geolocationEnabled", geolocationEnabled);
    qDebug() << "saving geolocation settings" << geolocationEnabled;
    if(geolocationEnabled) {
        checkGeolocationStatus();
    }
    if (geolocationEnabled && geolocationStatus == 1) {
        start();
    }
}

int GeoHelper::getGeolocationStatus()
{
    return geolocationStatus;
}

void GeoHelper::setGeolocationStatus(int status)
{
    this->geolocationStatus = status;
}

void GeoHelper::checkGeolocationStatus()
{
    if (geosrc != NULL) {
        geosrc->requestUpdate();
        if (geosrc->property("replyErrorCode").isValid()) {
            bb::location::PositionErrorCode::Type errorCode =
                    geosrc->property("replyErrorCode").value<bb::location::PositionErrorCode::Type>();
            switch (errorCode) {
                case bb::location::PositionErrorCode::None:
                    geolocationStatus = 1;
                    qDebug() << "geolocation status check passed, status 1 (OK)";
                    emit geolocationStatusChanged();
                    break;
                default:
                    geolocationStatus = 0;
                    qDebug() << "geolocation status check passed, status 0 (ERR)";
                    emit geolocationStatusChanged();
                    break;
            }
        }
    }
}
