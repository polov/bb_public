/*
 * FilterableGroupDataModel.hpp
 *
 *  Created on: 20. 11. 2014
 *      Author: martin
 */

#ifndef FILTERABLEGROUPDATAMODEL_HPP_
#define FILTERABLEGROUPDATAMODEL_HPP_

#include <QtCore/QObject>
#include <QList>
#include <QVariantMap>
#include <bb/cascades/GroupDataModel>

class FilterableGroupDataModel: public bb::cascades::GroupDataModel
{
Q_OBJECT
Q_PROPERTY(QString filterText READ getFilterText WRITE setFilterText NOTIFY filterTextChanged FINAL)
Q_PROPERTY(bool refreshOriginalItems READ getRefreshOriginalItems WRITE setRefreshOriginalItems NOTIFY refreshOriginalItemsChanged)

public:
    FilterableGroupDataModel(QObject *parent = 0);
    virtual ~FilterableGroupDataModel();

    virtual QVariant data(const QVariantList& indexPath);

    void setFilterText(const QString filterText);
    QString getFilterText() const;
    void setRefreshOriginalItems(const bool value);
    bool getRefreshOriginalItems() const;

    Q_INVOKABLE
    void itemToBeginning(QVariantList indexPath);

signals:
    void filterTextChanged();
    void refreshOriginalItemsChanged();

private:
    QString m_filterText;
    QList<QVariantMap> m_originalItems;
    bool m_refreshOriginalItems;

};

#endif /* FILTERABLEGROUPDATAMODEL_HPP_ */
