/*
 * GaussBlur.cpp
 *
 *  Created on: 15. 10. 2014
 *      Author: martin
 */

#include "GaussBlur.hpp"

#include <cmath>
#include <QFile>
#include <bb/device/DisplayInfo>

#include "FileSystemHelper.hpp"

GaussBlur::GaussBlur() :
        QObject()
{

}

GaussBlur::~GaussBlur()
{

}

void GaussBlur::blurImages(QString identificator, bool card)
{
    QImage qImage = readImageFile(identificator, "background", card);

    if (qImage.isNull()) {
        return;
    }

    bb::device::DisplayInfo display;
    int height = display.pixelSize().height();
    int width = display.pixelSize().width();

    qImage = gaussBlurQuick2(qImage, 7, 4);
    qImage = qImage.scaledToHeight(height);
    if (width > qImage.width()) {
        qImage = qImage.scaledToWidth(width);
    }

    int deltaWidth = (qImage.width() - display.pixelSize().width()) / 2;
    int deltaHeight = (qImage.height() - display.pixelSize().height()) / 2;

    qImage = qImage.copy(deltaWidth, deltaHeight, qImage.width() - deltaWidth,
            qImage.height() - deltaHeight);

    QImage swappedImage = qImage.rgbSwapped();

    QString passesDir = (!card) ? "/passes/" : "/passes_cards/";

    FileSystemHelper fsh;

    QFile outFile(fsh.getHomePath() + passesDir + identificator + "/blured_background@2x.png");
    outFile.open(QIODevice::WriteOnly);
    qImage.save(&outFile, "PNG");
    outFile.close();

    QFile outFile2(
            fsh.getHomePath() + passesDir + identificator + "/blured_background_listview@2x.png");
    outFile2.open(QIODevice::WriteOnly);
    qImage = qImage.copy(0, 0, qImage.width(), 86);
    qImage.save(&outFile2, "PNG");
    outFile2.close();
}

QImage GaussBlur::readImageFile(QString identificator, QString name, bool card)
{
    FileSystemHelper fsh;
    QString passesDir = (!card) ? "/passes/" : "/passes_cards/";
    QString path2x = fsh.getHomePath() + passesDir + identificator + "/" + name + "@2x.png";
    QString path = fsh.getHomePath() + passesDir + identificator + "/" + name + ".png";
    if (fsh.exists(path2x)) {
        path = path2x;
    }

    QImage qImage;
    QFile file(path);
    if (!file.open(QIODevice::ReadOnly)) {
        qDebug() << "Failed to open file" << path << "for reading";
        return qImage;
    }
    if (!qImage.loadFromData(file.readAll())) {
        qDebug() << "Image data null " << path;
    }
    file.close();
    return qImage;
}

static void quickBoxBlur(const QImage & original, QImage & output, unsigned int radius)
{
    // horizontal blur
    for (int y = 0; y != original.height(); ++y) {
        unsigned int red = 0, green = 0, blue = 0, count = 0;
        for (unsigned int x = 0; x != radius; ++x) {
            red += qRed(original.pixel(x, y));
            green += qGreen(original.pixel(x, y));
            blue += qBlue(original.pixel(x, y));
            ++count;
        }
        for (int x = radius; x != (radius << 1); ++x) {
            red += qRed(original.pixel(x, y));
            green += qGreen(original.pixel(x, y));
            blue += qBlue(original.pixel(x, y));
            ++count;
            output.setPixel(x - radius, y, qRgb(red / count, green / count, blue / count));
        }
        for (int x = (radius << 1); x != original.width(); ++x) {
            red = red + qRed(original.pixel(x, y)) - qRed(original.pixel(x - (radius << 1), y));
            green = green + qGreen(original.pixel(x, y))
                    - qGreen(original.pixel(x - (radius << 1), y));
            blue = blue + qBlue(original.pixel(x, y)) - qBlue(original.pixel(x - (radius << 1), y));
            output.setPixel(x - radius, y, qRgb(red / count, green / count, blue / count));
        }
        for (int x = original.width() - radius; x != original.width(); ++x) {
            red -= qRed(original.pixel(x - radius, y));
            green -= qGreen(original.pixel(x - radius, y));
            blue -= qBlue(original.pixel(x - radius, y));
            --count;
            output.setPixel(x, y, qRgb(red / count, green / count, blue / count));
        }
    }

    // vertical blur
    for (int x = 0; x != original.width(); ++x) {
        unsigned int red = 0, green = 0, blue = 0, count = 0;
        for (unsigned int y = 0; y != radius; ++y) {
            red += qRed(original.pixel(x, y));
            green += qGreen(original.pixel(x, y));
            blue += qBlue(original.pixel(x, y));
            ++count;
        }
        for (int y = radius; y != (radius << 1); ++y) {
            red += qRed(original.pixel(x, y));
            green += qGreen(original.pixel(x, y));
            blue += qBlue(original.pixel(x, y));
            ++count;
            output.setPixel(x, y - radius,
                    qRgb(((red / count) + qRed(output.pixel(x, y - radius))) / 2,
                            ((green / count) + qGreen(output.pixel(x, y - radius))) / 2,
                            ((blue / count) + qBlue(output.pixel(x, y - radius))) / 2));
        }
        for (int y = (radius << 1); y != original.height(); ++y) {
            red = red + qRed(original.pixel(x, y)) - qRed(original.pixel(x, y - (radius << 1)));
            green = green + qGreen(original.pixel(x, y))
                    - qGreen(original.pixel(x, y - (radius << 1)));
            blue = blue + qBlue(original.pixel(x, y)) - qBlue(original.pixel(x, y - (radius << 1)));
            output.setPixel(x, y - radius,
                    qRgb(((red / count) + qRed(output.pixel(x, y - radius))) / 2,
                            ((green / count) + qGreen(output.pixel(x, y - radius))) / 2,
                            ((blue / count) + qBlue(output.pixel(x, y - radius))) / 2));
        }
        for (int y = original.height() - radius; y != original.height(); ++y) {
            red -= qRed(original.pixel(x, y - radius));
            green -= qGreen(original.pixel(x, y - radius));
            blue -= qBlue(original.pixel(x, y - radius));
            --count;
            output.setPixel(x, y,
                    qRgb(((red / count) + qRed(output.pixel(x, y))) / 2,
                            ((green / count) + qGreen(output.pixel(x, y))) / 2,
                            ((blue / count) + qBlue(output.pixel(x, y))) / 2));
        }
    }
}

static void quickBoxBlurAlt(const QImage & original, QImage & output, unsigned int radius)
{
    // vertical blur
    for (int x = 0; x != original.width(); ++x) {
        unsigned int red = 0, green = 0, blue = 0, count = 0;
        for (unsigned int y = 0; y != radius; ++y) {
            red += qRed(original.pixel(x, y));
            green += qGreen(original.pixel(x, y));
            blue += qBlue(original.pixel(x, y));
            ++count;
        }
        for (int y = radius; y != (radius << 1); ++y) {
            red += qRed(original.pixel(x, y));
            green += qGreen(original.pixel(x, y));
            blue += qBlue(original.pixel(x, y));
            ++count;
            output.setPixel(x, y - radius, qRgb(red / count, green / count, blue / count));
        }
        for (int y = (radius << 1); y != original.height(); ++y) {
            red = red + qRed(original.pixel(x, y)) - qRed(original.pixel(x, y - (radius << 1)));
            green = green + qGreen(original.pixel(x, y))
                    - qGreen(original.pixel(x, y - (radius << 1)));
            blue = blue + qBlue(original.pixel(x, y)) - qBlue(original.pixel(x, y - (radius << 1)));
            output.setPixel(x, y - radius, qRgb(red / count, green / count, blue / count));

        }
        for (int y = original.height() - radius; y != original.height(); ++y) {
            red -= qRed(original.pixel(x, y - radius));
            green -= qGreen(original.pixel(x, y - radius));
            blue -= qBlue(original.pixel(x, y - radius));
            --count;
            output.setPixel(x, y, qRgb(red / count, green / count, blue / count));
        }
    }

    // horizontal blur
    for (int y = 0; y != original.height(); ++y) {
        unsigned int red = 0, green = 0, blue = 0, count = 0;
        for (unsigned int x = 0; x != radius; ++x) {
            red += qRed(original.pixel(x, y));
            green += qGreen(original.pixel(x, y));
            blue += qBlue(original.pixel(x, y));
            ++count;
        }
        for (int x = radius; x != (radius << 1); ++x) {
            red += qRed(original.pixel(x, y));
            green += qGreen(original.pixel(x, y));
            blue += qBlue(original.pixel(x, y));
            ++count;

            output.setPixel(x - radius, y,
                    qRgb(((red / count) + qRed(output.pixel(x - radius, y))) / 2,
                            ((green / count) + qGreen(output.pixel(x - radius, y))) / 2,
                            ((blue / count) + qBlue(output.pixel(x - radius, y))) / 2));
        }
        for (int x = (radius << 1); x != original.width(); ++x) {
            red = red + qRed(original.pixel(x, y)) - qRed(original.pixel(x - (radius << 1), y));
            green = green + qGreen(original.pixel(x, y))
                    - qGreen(original.pixel(x - (radius << 1), y));
            blue = blue + qBlue(original.pixel(x, y)) - qBlue(original.pixel(x - (radius << 1), y));
            output.setPixel(x - radius, y,
                    qRgb(((red / count) + qRed(output.pixel(x - radius, y))) / 2,
                            ((green / count) + qGreen(output.pixel(x - radius, y))) / 2,
                            ((blue / count) + qBlue(output.pixel(x - radius, y))) / 2));
        }
        for (int x = original.width() - radius; x != original.width(); ++x) {
            red -= qRed(original.pixel(x - radius, y));
            green -= qGreen(original.pixel(x - radius, y));
            blue -= qBlue(original.pixel(x - radius, y));
            --count;
            output.setPixel(x, y,
                    qRgb(((red / count) + qRed(output.pixel(x, y))) / 2,
                            ((green / count) + qGreen(output.pixel(x, y))) / 2,
                            ((blue / count) + qBlue(output.pixel(x, y))) / 2));
        }
    }
}

QImage GaussBlur::gaussBlurQuick2(const QImage & original, unsigned int radius, unsigned int boxes)
{
    QImage temporary1 = QImage(original.width(), original.height(), original.format());
    QImage temporary2 = QImage(original.width(), original.height(), original.format());

    int wl = floor(sqrt((12 * radius * radius / (double) boxes) + 1));
    if (!(wl % 2))
        --wl;
    int wu = wl + 2;

    unsigned int m = round(
            (12 * radius * radius - boxes * wl * wl - 4 * boxes * wl - 3 * boxes)
                    / (double) (-4 * wl - 4));

    unsigned int i = 0;
    for (; i < boxes; ++i) {
        unsigned int radius2 = i < m ? wl : wu;
        if (i == 0)
            quickBoxBlur(original, temporary1, radius2);
        else if (i % 2)
            quickBoxBlurAlt(temporary1, temporary2, radius2);
        else
            quickBoxBlur(temporary2, temporary1, radius2);
    }

    if (i % 2)
        return temporary1;
    else
        return temporary2;
}
