/*
 * CardController.cpp
 *
 *  Created on: 25. 11. 2014
 *      Author: martin
 */

#include <src/CardController.hpp>

#include <bb/system/InvokeRequest>
#include <bb/system/InvokeManager>

#include "FileSystemHelper.hpp"
#include "GaussBlur.hpp"
#include "PassHelper.hpp"
#include "ZipHelper.hpp"

CardController::CardController(QObject* parent) :
        QObject(parent)
{
    qml = NULL;
    root = NULL;
    uiHelper = new UiHelper();
}

CardController::~CardController()
{

}

void CardController::showCard(const QString uri)
{
    qDebug() << "showCard() uri " << uri;

    QString qmlUri;
    if (uri == NULL) {
        qmlUri = "asset:///errorCard.qml";

    } else {
        //unzip uri to tmp_card
        FileSystemHelper fsHelper;
        QString destination = fsHelper.getHomePath() + "/passes_cards/tmp";
        ZipHelper zipHelper;
        qDebug() << "showCard() unzipping file" << uri << "to" << destination;
        zipHelper.unzipFile(uri, destination, false);

        qDebug() << "showCard() parsing pass data";
        PassHelper passHelper;
        QString identificator = passHelper.getPassIdentification(destination);
        qDebug() << "showCard() got pass identificator" << identificator;

        QString passPath = fsHelper.getHomePath() + "/passes_cards/" + identificator;
        if (fsHelper.exists(passPath)) {
            qDebug() << "showCard() deleting existing path" << passPath;
            fsHelper.removeDirectory(passPath);
        }

        if (!fsHelper.rename(destination, passPath)) {
            fsHelper.removeDirectory(destination);
            qDebug() << "showCard() error moving pass data";
            //continue?
        }
        GaussBlur gaussBlur;
        gaussBlur.blurImages(identificator, true);
        qDebug() << "showCard() pass data ready in" << passPath;

        //choose template according to file type
        QVariantMap pass = passHelper.getPassData(passPath);

        QString passType = pass.value("passType").toString();
        qDebug() << "showCard() pass type is" << passType;

        if (passType == "boardingPass") {
            qmlUri = "asset:///boardingPassCard.qml";
        } else if (passType == "coupon") {
            qmlUri = "asset:///couponCard.qml";
        } else if (passType == "eventTicket") {
            if (uiHelper->hasImageCard(identificator, "strip")) {
                qmlUri = "asset:///eventStripCard.qml";
            } else {
                qmlUri = "asset:///eventCard.qml";
            }
        } else if (passType == "generic") {
            if (pass.value("barcodeFormat").toString() == "PKBarcodeFormatQR"
                    || pass.value("barcodeFormat").toString() == "PKBarcodeFormatAztec") {
                qmlUri = "asset:///genericSquareCard.qml";
            } else {
                qmlUri = "asset:///genericCard.qml";
            }
        } else if (passType == "storeCard") {
            qmlUri = "asset:///storeCard.qml";
        } else {
            qmlUri = "asset:///errorCard.qml";
        }

        qml = QmlDocument::create(qmlUri).parent(this);

        qml->setContextProperty("controller", this);
        qml->setContextProperty("uiHelper", uiHelper);

        // Create root object for the UI
        root = qml->createRootObject<AbstractPane>();

        root->setProperty("identificator", QVariant(pass.value("identificator").toString()));
        qDebug() << "showCard() set property identificator"
                << pass.value("identificator").toString();

        // Set created root object as the application scene
        Application::instance()->setScene(root);
    }
}

QVariantMap CardController::getPassData(const QString identificator)
{
    PassHelper passHelper;
    FileSystemHelper fsHelper;
    return passHelper.getPassData(fsHelper.getHomePath() + "/passes_cards/" + identificator);
}

void CardController::importPassFromCard(const QString identificator)
{
    qDebug() << "importPassFromCard() save pass from card" << identificator;
    FileSystemHelper fsHelper;
    QString op_type;
    if (!fsHelper.exists(fsHelper.getHomePath() + "/passes/")) {
        fsHelper.createDir(fsHelper.getHomePath() + "/passes/");
    }
    if (fsHelper.exists(fsHelper.getHomePath() + "/passes/" + identificator)) {
        op_type = "update";
    } else {
        op_type = "insert";
    }
    bool result = fsHelper.copyDirectoryContent(
            fsHelper.getHomePath() + "/passes_cards/" + identificator,
            fsHelper.getHomePath() + "/passes/" + identificator);

    QSettings settings("polovincak", "Passes");
    settings.setValue("shouldReloadPasses", QVariant(true));

    emit importPassDataFinished(!result, op_type);
}

void CardController::invokeDisplaySettings() {
    qDebug() << "display settings invocation";
    bb::system::InvokeManager invokeManager(new bb::system::InvokeManager(this));
    bb::system::InvokeRequest request;
    request.setTarget("sys.settings.card");
    request.setAction("bb.action.OPEN");
    request.setMimeType("settings/view");
    request.setUri(QUrl("settings://display"));
    invokeManager.invoke(request);
}
