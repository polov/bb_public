/*
 * UiHelper.hpp
 *
 *  Created on: 21. 11. 2014
 *      Author: martin
 */

#ifndef UIHELPER_HPP_
#define UIHELPER_HPP_

#include <QObject>
#include <bb/cascades/Color>
#include <bb/cascades/Image>

class UiHelper: public QObject
{
Q_OBJECT
Q_PROPERTY(QString listviewLayoutType READ getListviewLayoutType WRITE setListviewLayoutType NOTIFY listviewLayoutTypeChanged)

public:
    UiHelper(QObject* parent = 0);
    virtual ~UiHelper();

    QString getListviewLayoutType();
    void setListviewLayoutType(const QString value);

    Q_INVOKABLE
    bb::cascades::Color getColor(QString appleColorNotation);
    Q_INVOKABLE
    QString getColorString(QString appleColorNotation);
    Q_INVOKABLE
    bool hasImage(const QString identificator, const QString name) const;
    Q_INVOKABLE
    bool hasImageCard(const QString identificator, const QString name) const;
    Q_INVOKABLE
    QString getImagePath(const QString identificator, const QString name) const;
    Q_INVOKABLE
    QString getImagePathCard(const QString identificator, const QString name) const;
    Q_INVOKABLE
    QString getFieldLabel(QVariant v, int i);
    Q_INVOKABLE
    QString getFieldValue(QVariant v, int i);
    Q_INVOKABLE
    bb::cascades::Image getBarcodeImage(QString type, QString message);
    Q_INVOKABLE
    QString getBackFieldContentHtml(QVariant backfields);

    Q_INVOKABLE
    bb::cascades::Image getImage(const QString identificator, const QString name, const int cropWidth, const int cropHeight);

signals:
    void filepickerOpenedChanged();
    void listviewLayoutTypeChanged();

private:
    QString listviewLayoutType;

};

#endif /* UIHELPER_HPP_ */
