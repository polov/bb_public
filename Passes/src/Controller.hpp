/*
 * Controller.hpp
 *
 *  Created on: 21. 11. 2014
 *      Author: martin
 */

#ifndef CONTROLLER_HPP_
#define CONTROLLER_HPP_

#include <QObject>
#include <bb/system/InvokeManager>

#include "PictureEditorLauncher.h"
#include "ZipHelper.hpp"

class Controller: public QObject
{
Q_OBJECT

public:
    Controller(QObject* parent = 0);
    virtual ~Controller();

    Q_INVOKABLE
    QVariantList getRefreshedPassesData();
    Q_INVOKABLE
    QVariantList getRefreshedReducedPassesData();
    Q_INVOKABLE
    QVariantMap getPassData(const QString identificator);
    Q_INVOKABLE
    void importPassData(QString uri);
    Q_INVOKABLE
    void removePassData(const QString identificator);
    Q_INVOKABLE
    int passesCount();
    Q_INVOKABLE
    void checkShouldReloadPasses();
    Q_INVOKABLE
    void invokeEmail(QString subject);
    Q_INVOKABLE
    void invokeDisplaySettings();
    Q_INVOKABLE
    void invokePermissionsSettings();
    Q_INVOKABLE
    void invokeLocationSettings();
    Q_INVOKABLE
    void updateLastUsedTime(QString identificator);

    void invokePass(QString identificator);

    void processTempData();

    Q_INVOKABLE
    void createPass(const QString barcodeType, const QString logoText,
            const QString headerFieldLabelText, const QString headerFieldValueText,
            const QString primaryFieldLabelText, const QString primaryFieldValueText,
            const QString primaryFieldLabelText2, const QString primaryFieldValueText2,
            const QString backFieldLabelText, const QString backFieldTextArea,
            const QString barcodeTypeOfAdded, const QString barcodeMessage,
            const QString barcodeAltText, const QString logoImagePath,
            const QString bgColorValue, const QString fgColorValue, const QString lblColorValue,
            const QString companyName);

signals:
    void importPassDataStarted();
    void importPassDataFinished(bool, QString, QString);
    void removePassDataFinished(bool);
    void updatePasses();
    void showInvokedPass(QString);

private slots:
    void onArchiveDecodingFinished(ZipHelper*);
    void appInFullScreen();

private:
    QVariantList passes;
    bb::system::InvokeManager* invokeManager;

};

#endif /* CONTROLLER_HPP_ */
