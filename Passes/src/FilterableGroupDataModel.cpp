/*
 * FilterableGroupDataModel.cpp
 *
 *  Created on: 20. 11. 2014
 *      Author: martin
 */

#include <QDebug>

#include "FilterableGroupDataModel.hpp"

using namespace bb::cascades;

FilterableGroupDataModel::FilterableGroupDataModel(QObject *parent) :
        GroupDataModel(parent), m_filterText(""), m_originalItems(), m_refreshOriginalItems(true)
{

}

FilterableGroupDataModel::~FilterableGroupDataModel()
{

}

void FilterableGroupDataModel::setFilterText(const QString filterText)
{
    m_filterText = filterText.trimmed();
    if (m_refreshOriginalItems) {
        QList<QVariantMap> items = toListOfMaps();
        m_originalItems = items;
        m_refreshOriginalItems = false;
    }
    if (m_filterText == "") {
        m_filterText = "";
        clear();
        insertList(m_originalItems);
    } else {
        //leave as is
        setGrouping(ItemGrouping::None);
        QVariantMap item;
        QList<QVariantMap> m_filteredItems;
        clear();

        emit itemsChanged(bb::cascades::DataModelChangeType::AddRemove);

        foreach(item, m_originalItems){
            QString str = item["organizationName"].value<QString>(); // ---
            QString str2 = item["logoText"].value<QString>();
            if (str.contains(filterText, Qt::CaseInsensitive) || str2.contains(filterText, Qt::CaseInsensitive)) {
                insert(item);
            }
        }
    }

    emit itemsChanged(bb::cascades::DataModelChangeType::AddRemove);
}

QString FilterableGroupDataModel::getFilterText() const
{
    return m_filterText;
}

QVariant FilterableGroupDataModel::data(const QVariantList& indexPath)
{

    QVariant data = GroupDataModel::data(indexPath);

    if (m_filterText == NULL || m_filterText == "") {
        return data;
    }

    QVariantMap dataMap = data.toMap();

    QString str = dataMap["organizationName"].value<QString>(); // ---
    QString str2 = dataMap["logoText"].value<QString>();

    if (str.contains(m_filterText, Qt::CaseInsensitive) || str2.contains(m_filterText, Qt::CaseInsensitive)) {
        return data;
    }

    return QVariant::Invalid;

}

void FilterableGroupDataModel::setRefreshOriginalItems(const bool value)
{
    m_refreshOriginalItems = value;
}

bool FilterableGroupDataModel::getRefreshOriginalItems() const
{
    return m_refreshOriginalItems;
}

void FilterableGroupDataModel::itemToBeginning(QVariantList indexPath)
{
    if (GroupDataModel::data(indexPath).toMap().value("identificator")
            != GroupDataModel::data(this->first()).toMap().value("identificator")) {
        QVariant data = GroupDataModel::data(indexPath);
        QVariantMap dataMap = data.toMap();
        dataMap.insert("last_used", QDateTime::currentDateTime());
        updateItem(indexPath, dataMap);
        itemMoved(indexPath, this->first());
        emit itemsChanged(bb::cascades::DataModelChangeType::Update);
    } else {
        qDebug() << "Item is at the beginning";
    }
}
