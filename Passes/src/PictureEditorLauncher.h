/*
 * PictureEditorLauncher.h
 *
 *  Created on: 25. 6. 2015
 *      Author: martin
 */

#ifndef PICTUREEDITORLAUNCHER_H_
#define PICTUREEDITORLAUNCHER_H_

#include <QObject>
#include <bb/system/InvokeManager>
#include <bb/system/InvokeTargetReply.hpp>
#include <bb/system/CardDoneMessage.hpp>

class PictureEditorLauncher : public QObject {
    Q_OBJECT
public:
    PictureEditorLauncher(QObject* pParent = NULL);
    virtual ~PictureEditorLauncher();

    Q_INVOKABLE
    void doLaunch (const QString &app, const QString &msg,
            const QString &sizeString, bool upScale, bool delTmpFile);

public slots:
    void childCardDone (const bb::system::CardDoneMessage& cardDoneMessage);
    void invokeRequestFinished();

signals:
    void imageReady(PictureEditorLauncher*, const QString&);

private:
    bool mDeleteTmpFile;
    bb::system::InvokeManager* m_invokeManager;
    bb::system::InvokeTargetReply* m_targetReply;
};

#endif /* PICTUREEDITORLAUNCHER_H_ */
