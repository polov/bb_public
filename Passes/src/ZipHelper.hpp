/*
 * ZipHelper.hpp
 *
 *  Created on: 22.4.2014
 *      Author: martin
 */

#ifndef ZIPHELPER_HPP_
#define ZIPHELPER_HPP_

#include <QObject>

class ZipHelper: public QObject {
Q_OBJECT

public:
	ZipHelper();
	virtual ~ZipHelper();

	Q_INVOKABLE
	void unzipFile(const QString source, const QString destination, bool emitSignals);

	void unzipFile(const QString path);

	int getErrorCode() const;
	void setErrorCode(int errorCode);
	const QString& getErrorMessage() const;
	void setErrorMessage(const QString& errorMessage);

signals:
	void archiveDecodingFinished(ZipHelper*);

private:
	int copy_data(struct archive *ar, struct archive *aw);

	int errorCode;
	QString errorMessage;
};

#endif /* ZIPHELPER_HPP_ */
