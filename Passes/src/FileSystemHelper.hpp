/*
 * FileSystemHelper.hpp
 *
 *  Created on: 23.4.2014
 *      Author: martin
 */

#ifndef FILESYSTEMHELPER_HPP_
#define FILESYSTEMHELPER_HPP_

#include <qobject.h>

class FileSystemHelper: public QObject {
Q_OBJECT

public:
	FileSystemHelper();
	virtual ~FileSystemHelper();

	bool rename(const QString pathOld, const QString pathNew);
	bool removeDirectory(const QString passTypeIdentifier, const QString serialNumber);
	bool removeDirectory(const QString name);
	bool removeDirectoryContent(const QString directoryName);
	bool copyDirectoryContent(const QString source, const QString destination);
	bool copyFile(const QString source, const QString destination);
	void prepareTempDirectory();
	QString getHomePath();
	QString getTempPath();
	bool exists(const QString path);
	bool createDir(const QString path);
	QList<QString> getDirectoryChildren(const QString path);
};

#endif /* FILESYSTEMHELPER_HPP_ */
