/*
 * PictureEditorLauncher.cpp
 *
 *  Created on: 25. 6. 2015
 *      Author: martin
 */

#include <src/PictureEditorLauncher.h>

#include <bb/system/SystemToast>
#include <bb/system/InvokeRequest>
#include <bb/system/InvokeReplyError>
#include <bb/PpsObject>

using namespace bb::system;

PictureEditorLauncher::PictureEditorLauncher(QObject* pParent) :
        QObject(pParent), mDeleteTmpFile(true), m_invokeManager(0), m_targetReply(0)
{
    m_invokeManager = new InvokeManager();
    QObject::connect(m_invokeManager, SIGNAL(childCardDone(const bb::system::CardDoneMessage&)),
            this, SLOT(childCardDone (const bb::system::CardDoneMessage&)));
}

PictureEditorLauncher::~PictureEditorLauncher()
{
    m_invokeManager->deleteLater();
}

void PictureEditorLauncher::childCardDone(const bb::system::CardDoneMessage& cardDoneMessage)
{
    qDebug() << "emit imageReady" << cardDoneMessage.data();
    emit imageReady(this, cardDoneMessage.data());
}

/**
 * mode can be normal, fixedsize, or setwallpaper
 * filePath represents the path to the file
 *
 */

void PictureEditorLauncher::doLaunch(const QString &mode, const QString &filePath,
        const QString &sizeString, bool upScale, bool delTmpFile)
{
    mDeleteTmpFile = delTmpFile;

    // card request
    InvokeRequest cardRequest;
    cardRequest.setAction("bb.action.EDIT");
    cardRequest.setFileTransferMode(bb::system::FileTransferMode::CopyReadOnly);

    if (mode == "normal") {
        QByteArray data;
        data = "Normal";
        cardRequest.setData(data);
        cardRequest.setTarget("sys.pictureeditor.cardeditor");
    } else if (mode == "fixedsize" || mode == "setwallpaper") {
        qDebug() << "Launch Picture Editor in fixedsize mode";
        QVariantMap data = QVariantMap();
        data["size"] = sizeString;
        QString upScaleString = QString(upScale);
        data["upScale"] = upScaleString;
        bool ok;
        QByteArray encData = bb::PpsObject::encode(data, &ok);
        cardRequest.setData(encData);
        cardRequest.setTarget("sys.pictureeditor.cardeditor");
        // "wallpaper" mode
        if (mode == "setwallpaper") {
            cardRequest.setTarget("sys.pictureeditor.setaswallpaper");
            cardRequest.setAction("bb.action.SET");
        }
    } else {
        cardRequest.setTarget("sys.pictureeditor.app");
    }

    cardRequest.setUri(QUrl::fromLocalFile(filePath));

    qDebug() << "Launching pictureEditor:" << cardRequest.uri().toString();
    qDebug() << "Launching target:" << cardRequest.target();
    m_targetReply = m_invokeManager->invoke(cardRequest);

    QObject::connect(m_targetReply, SIGNAL(finished()), this, SLOT(invokeRequestFinished()));
}

void PictureEditorLauncher::invokeRequestFinished()
{
    qDebug() << "invokeRequestFinished";
}
