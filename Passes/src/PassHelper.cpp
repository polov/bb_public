/*
 * PassHelper.cpp
 *
 *  Created on: 21. 11. 2014
 *      Author: martin
 */

#include "PassHelper.hpp"

#include <QDateTime>
#include <QDebug>
#include <QDir>
#include <QFile>
#include <QSettings>
#include <bb/data/JsonDataAccess>

#include "FileSystemHelper.hpp"

PassHelper::PassHelper()
{

}

PassHelper::~PassHelper()
{

}

QString PassHelper::getPassIdentification(const QString directoryPath)
{
    FileSystemHelper fsHelper;
    if (!fsHelper.exists(directoryPath)) {
        qDebug() << "getPassIdentification() directory doesn't exists" << directoryPath;
        return "NOT_NOT";
    }
    QString passTypeIdentifier;
    QString serialNumber;
    bb::data::JsonDataAccess jda;
    QVariant content = jda.load(directoryPath + "/pass.json");
    if (jda.hasError()) {
        qDebug() << "getPassIdentification() error while parsing pass.json"
                << jda.error().errorMessage();
        qDebug() << "getPassIdentification() error while parsing pass.json" << content;
        return "NOT_NOT";
    } else {
        qDebug() << "getPassIdentification() file parsed" << (directoryPath + "/pass.json");
    }
    if (!content.toMap().value("passTypeIdentifier").isNull()) {
        passTypeIdentifier = content.toMap().value("passTypeIdentifier").toString();
    } else {
        passTypeIdentifier = "NOT";
    }

    if (!content.toMap().value("serialNumber").isNull()) {
        serialNumber = content.toMap().value("serialNumber").toString();
    } else {
        serialNumber = "NOT";
    }
    return passTypeIdentifier.append('_').append(serialNumber);

}

QVariantMap PassHelper::getPassData(const QString passDirectoryPath)
{
    QVariantMap map;
    FileSystemHelper fsHelper;
    if (!fsHelper.exists(passDirectoryPath)) {
        qDebug() << "getPassData() directory doesn't exists" << passDirectoryPath;
        return map;
    }

    bb::data::JsonDataAccess jda;

    QVariant content = jda.load(passDirectoryPath + "/pass.json");
    if (jda.hasError()) {
        qDebug() << "getPassData() error while parsing json file"
                << passDirectoryPath + "/pass.json" << jda.error().errorMessage();
        return map;
    }

    QString serialNumber;
    if (!content.toMap().value("serialNumber").isNull()) {
        serialNumber = content.toMap().value("serialNumber").toString();
        map.insert("setSerialNumber", serialNumber);
    }

    QString passTypeIdentifier;
    if (!content.toMap().value("passTypeIdentifier").isNull()) {
        passTypeIdentifier = content.toMap().value("passTypeIdentifier").toString();
        map.insert("passTypeIdentifier", passTypeIdentifier);
    }

    QString identificator = passTypeIdentifier + "_" + serialNumber;
    map.insert("identificator", identificator);
    QMap<QString, QString> localizedStrings = loadLocalizedStrings(passDirectoryPath);

    if (!content.toMap().value("teamIdentifier").isNull()) {
        map.insert("setTeamIdentifier", content.toMap().value("teamIdentifier").toString());
    }

    if (!content.toMap().value("description").isNull()) {
        map.insert("description",
                getLocalizedString(content.toMap().value("description").toString(),
                        localizedStrings));
    }

    if (!content.toMap().value("formatVersion").isNull()) {
        map.insert("formatVersion", content.toMap().value("formatVersion"));
    }

    if (!content.toMap().value("organizationName").isNull()) {
        map.insert("organizationName",
                getLocalizedString(content.toMap().value("organizationName").toString(),
                        localizedStrings));
    }

    //barcode
    if (!content.toMap().value("barcode").isNull()) {
        QVariant barcodeContent = content.toMap().value("barcode");
        if (!barcodeContent.toMap().value("messageEncoding").isNull()) {
            map.insert("barcodeMessageEncoding",
                    barcodeContent.toMap().value("messageEncoding").toString());
        }
        if (!barcodeContent.toMap().value("message").isNull()) {
            map.insert("barcodeMessage", barcodeContent.toMap().value("message").toString());
        }
        if (!barcodeContent.toMap().value("format").isNull()) {
            map.insert("barcodeFormat", barcodeContent.toMap().value("format").toString());
        }
        if (!barcodeContent.toMap().value("altText").isNull()) {
            map.insert("barcodeAltText",
                    getLocalizedString(barcodeContent.toMap().value("altText").toString(),
                            localizedStrings));
        }
    }

    if (!content.toMap().value("backgroundColor").isNull()) {
        map.insert("backgroundColor", content.toMap().value("backgroundColor").toString());
    }

    if (!content.toMap().value("foregroundColor").isNull()) {
        map.insert("foregroundColor", content.toMap().value("foregroundColor").toString());
    }

    if (!content.toMap().value("groupingIdentifier").isNull()) {
        map.insert("groupingIdentifier", content.toMap().value("groupingIdentifier").toString());
    }

    if (!content.toMap().value("labelColor").isNull()) {
        map.insert("labelColor", content.toMap().value("labelColor").toString());
    }

    if (!content.toMap().value("logoText").isNull()) {
        map.insert("logoText",
                getLocalizedString(content.toMap().value("logoText").toString(), localizedStrings));
    }

    QString passType;
    if (!content.toMap().value("boardingPass").isNull()) {
        map.insert("passType", "boardingPass");
        passType = "boardingPass";
        //boarding pass specific values
        //pass->setTransitType(
        //        content.toMap().value(passType).toMap().value("transitType").toString());
    }

    else if (!content.toMap().value("coupon").isNull()) {
        map.insert("passType", "coupon");
        passType = "coupon";
    }

    else if (!content.toMap().value("eventTicket").isNull()) {
        map.insert("passType", "eventTicket");
        passType = "eventTicket";
    }

    else if (!content.toMap().value("generic").isNull()) {
        map.insert("passType", "generic");
        passType = "generic";
    }

    else if (!content.toMap().value("storeCard").isNull()) {
        map.insert("passType", "storeCard");
        passType = "storeCard";
    }

    QList<QVariant> list;
    QVariant listItem;
    list = content.toMap().value(passType).toMap().value("headerFields").toList();
    for (int i = 0; i < list.size(); i++) {
        listItem = list.at(i);
        QVariantMap tempMap;
        tempMap.insert("label",
                getLocalizedString(listItem.toMap().value("label").toString(), localizedStrings));
        tempMap.insert("value",
                getLocalizedString(listItem.toMap().value("value").toString(), localizedStrings));

        list.replace(i, tempMap);
    }
    map.insert("headerFields", QVariant(list));

    list = content.toMap().value(passType).toMap().value("primaryFields").toList();
    for (int i = 0; i < list.size(); i++) {
        listItem = list.at(i);
        QVariantMap tempMap;
        tempMap.insert("label",
                getLocalizedString(listItem.toMap().value("label").toString(), localizedStrings));
        tempMap.insert("value",
                getLocalizedString(listItem.toMap().value("value").toString(), localizedStrings));

        list.replace(i, tempMap);
    }
    map.insert("primaryFields", QVariant(list));

    list = content.toMap().value(passType).toMap().value("auxiliaryFields").toList();
    for (int i = 0; i < list.size(); i++) {
        listItem = list.at(i);
        QVariantMap tempMap;
        tempMap.insert("label",
                getLocalizedString(listItem.toMap().value("label").toString(), localizedStrings));
        tempMap.insert("value",
                getLocalizedString(listItem.toMap().value("value").toString(), localizedStrings));

        list.replace(i, tempMap);
    }
    map.insert("auxiliaryFields", QVariant(list));

    list = content.toMap().value(passType).toMap().value("secondaryFields").toList();
    for (int i = 0; i < list.size(); i++) {
        listItem = list.at(i);
        QVariantMap tempMap;
        tempMap.insert("label",
                getLocalizedString(listItem.toMap().value("label").toString(), localizedStrings));
        tempMap.insert("value",
                getLocalizedString(listItem.toMap().value("value").toString(), localizedStrings));

        list.replace(i, tempMap);
    }
    map.insert("secondaryFields", QVariant(list));

    list = content.toMap().value(passType).toMap().value("backFields").toList();
    for (int i = 0; i < list.size(); i++) {
        listItem = list.at(i);
        QVariantMap tempMap;
        tempMap.insert("label",
                getLocalizedString(listItem.toMap().value("label").toString(), localizedStrings));
        tempMap.insert("value",
                getLocalizedString(listItem.toMap().value("value").toString(), localizedStrings));

        list.replace(i, tempMap);
    }
    map.insert("backFields", QVariant(list));

    //locations
    QVariantList locations;
    if (!content.toMap().value("locations").isNull()) {
        list = content.toMap().value("locations").toList();
        for (int i = 0; i < list.size(); i++) {
            listItem = list.at(i);
            QVariantMap tempMap;
            tempMap.insert("latitude", listItem.toMap().value("latitude").toString());
            tempMap.insert("longitude", listItem.toMap().value("longitude").toString());
            tempMap.insert("altitude", listItem.toMap().value("altitude").toString());
            tempMap.insert("relevantText", listItem.toMap().value("relevantText").toString());
            locations.insert(i, QVariant(tempMap));
        }
    }
    map.insert("locations", QVariant(locations));

    QSettings settings("polovincak", "Passes");
    QVariant used_time = settings.value(identificator + "/used_time");
    if (used_time.isNull()) {
        used_time = QVariant(QDateTime::currentDateTime().toTime_t());
        settings.setValue(identificator + "/used_time", used_time);
    }
    map.insert("used_time", used_time);

    if (map.value("labelColor").isNull()) {
        if (!map.value("foregroundColor").isNull()
                && map.value("foregroundColor").toString().size() > 6) {
            map.insert("labelColor", map.value("foregroundColor"));
        } else {
            map.insert("labelColor", QVariant("#000000"));
        }
    }

    return map;
}

QVariantMap PassHelper::getReducedPassData(const QString passDirectoryPath)
{
    QVariantMap map;
    FileSystemHelper fsHelper;
    if (!fsHelper.exists(passDirectoryPath)) {
        qDebug() << "getPassData() directory doesn't exists" << passDirectoryPath;
        return map;
    }

    bb::data::JsonDataAccess jda;

    QVariant content = jda.load(passDirectoryPath + "/pass.json");
    if (jda.hasError()) {
        qDebug() << "getPassData() error while parsing json file"
                << passDirectoryPath + "/pass.json" << jda.error().errorMessage();
        return map;
    }

    QString serialNumber;
    if (!content.toMap().value("serialNumber").isNull()) {
        serialNumber = content.toMap().value("serialNumber").toString();
        map.insert("setSerialNumber", serialNumber);
    }

    QString passTypeIdentifier;
    if (!content.toMap().value("passTypeIdentifier").isNull()) {
        passTypeIdentifier = content.toMap().value("passTypeIdentifier").toString();
        map.insert("passTypeIdentifier", passTypeIdentifier);
    }

    QString identificator = passTypeIdentifier + "_" + serialNumber;
    map.insert("identificator", identificator);
    QMap<QString, QString> localizedStrings = loadLocalizedStrings(passDirectoryPath);

    if (!content.toMap().value("backgroundColor").isNull()) {
        map.insert("backgroundColor", content.toMap().value("backgroundColor").toString());
    }

    if (!content.toMap().value("foregroundColor").isNull()) {
        map.insert("foregroundColor", content.toMap().value("foregroundColor").toString());
    }

    if (!content.toMap().value("labelColor").isNull()) {
        map.insert("labelColor", content.toMap().value("labelColor").toString());
    }

    if (!content.toMap().value("logoText").isNull()) {
        map.insert("logoText",
                getLocalizedString(content.toMap().value("logoText").toString(), localizedStrings));
    }

    if (content.toMap().contains("organizationName")) {
        map.insert("organizationName",
                getLocalizedString(content.toMap().value("organizationName").toString(),
                        localizedStrings));
    }

    QString passType;
    if (content.toMap().contains("boardingPass")) {
        map.insert("passType", "boardingPass");
        passType = "boardingPass";
    }

    else if (content.toMap().contains("coupon")) {
        map.insert("passType", "coupon");
        passType = "coupon";
    }

    else if (content.toMap().contains("eventTicket")) {
        map.insert("passType", "eventTicket");
        passType = "eventTicket";
    }

    else if (content.toMap().contains("generic")) {
        map.insert("passType", "generic");
        passType = "generic";
    }

    else if (content.toMap().contains("storeCard")) {
        map.insert("passType", "storeCard");
        passType = "storeCard";
    }

    QList<QVariant> list;
    QVariant listItem;
    list = content.toMap().value(passType).toMap().value("headerFields").toList();
    for (int i = 0; i < list.size(); i++) {
        listItem = list.at(i);
        QVariantMap tempMap;
        tempMap.insert("label",
                getLocalizedString(listItem.toMap().value("label").toString(), localizedStrings));
        tempMap.insert("value",
                getLocalizedString(listItem.toMap().value("value").toString(), localizedStrings));

        list.replace(i, tempMap);
    }
    map.insert("headerFields", QVariant(list));

    if (map.value("labelColor").isNull()) {
        if (!map.value("foregroundColor").isNull()
                && map.value("foregroundColor").toString().size() > 6) {
            map.insert("labelColor", map.value("foregroundColor"));
        } else {
            map.insert("labelColor", QVariant("#000000"));
        }
    }

    return map;
}

QMap<QString, QString> PassHelper::loadLocalizedStrings(QString passPath)
{
    QMap<QString, QString> strings;

    //check presence of localization
    QString locale = QLocale().name().mid(0, 2);
    //qDebug() << "trying to load file for locale" << locale;
    QString localizationSourceFilePath = passPath + "/"
            + locale + ".lproj/pass.strings";
    //qDebug() << "Trying" << localizationSourceFilePath;
    if (!QFile::exists(localizationSourceFilePath)) {
        localizationSourceFilePath = passPath
                + "/en.lproj/pass.strings";
        //qDebug() << "Trying" << localizationSourceFilePath;
        if (!QFile::exists(localizationSourceFilePath)) {
            return strings;
        }
    }
    QFile localizationSourceFile(localizationSourceFilePath);
    if (localizationSourceFile.open(QIODevice::ReadOnly | QIODevice::Text)) {
        QTextStream in(&localizationSourceFile);
        while (!in.atEnd()) {
            QString line = in.readLine();
            int keyStart = line.indexOf("\"", 0, Qt::CaseInsensitive);
            int keyEnd = line.indexOf("\"", keyStart + 1, Qt::CaseInsensitive);
            int valueStart = line.indexOf("\"", keyEnd + 1, Qt::CaseInsensitive);
            int valueEnd = line.indexOf("\"", valueStart + 1, Qt::CaseInsensitive);
            //qDebug() << keyStart << keyEnd << valueStart << valueEnd;
            QString key = line.mid(keyStart + 1, keyEnd - keyStart - 1);
            QString value = line.mid(valueStart + 1, valueEnd - valueStart - 1);
            strings.insert(key, value);
            //qDebug() << "localization item inserted" << key << value;
        }
        localizationSourceFile.close();
    }
    //qDebug() << "localization file reading finished";
    return strings;
}

QString PassHelper::getLocalizedString(const QString key, QMap<QString, QString> strings) const
{
    QString value = strings.value(key);
    if (value != NULL) {
        return value;
    }
    return key;
}

void PassHelper::savePassData(const QString identificator, QVariantMap content) {
    bb::data::JsonDataAccess jda;
    FileSystemHelper fsHelper;
    QString passPath = fsHelper.getHomePath() + "/passes/" + identificator;
    fsHelper.createDir(passPath);
    jda.save(QVariant::fromValue(content), passPath + "/pass.json");
}
