/*
 * PassHelper.hpp
 *
 *  Created on: 21. 11. 2014
 *      Author: martin
 */

#ifndef PASSHELPER_HPP_
#define PASSHELPER_HPP_

class PassHelper: public QObject
{
public:
    PassHelper();
    virtual ~PassHelper();

    QString getPassIdentification(const QString directoryPath);
    QVariantMap getPassData(const QString passDirectoryPath);
    QVariantMap getReducedPassData(const QString passDirectoryPath);
    void savePassData(const QString identificator, QVariantMap content);

    QMap<QString, QString> loadLocalizedStrings(QString identification);
    QString getLocalizedString(const QString key, QMap<QString, QString> strings) const;
};

#endif /* PASSHELPER_HPP_ */
