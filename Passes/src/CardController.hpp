/*
 * CardController.hpp
 *
 *  Created on: 25. 11. 2014
 *      Author: martin
 */

#ifndef CARDCONTROLLER_HPP_
#define CARDCONTROLLER_HPP_

#include <QObject>
#include <bb/system/CardDoneMessage>

#include <bb/cascades/AbstractPane>
#include <bb/cascades/Application>
#include <bb/cascades/QmlDocument>

#include "UiHelper.hpp"

using namespace bb::cascades;

class CardController: public QObject
{
Q_OBJECT

public:
    CardController(QObject* parent = 0);
    virtual ~CardController();

    void showCard(const QString uri);

    Q_INVOKABLE
    QVariantMap getPassData(const QString identificator);
    Q_INVOKABLE
    void importPassFromCard(const QString identificator);

    Q_INVOKABLE
    void invokeDisplaySettings();

signals:
    void importPassDataFinished(bool, QString);

//private slots:
//    void onChildCardDone(const bb::system::CardDoneMessage&);

private:
    QmlDocument* qml;
    AbstractPane* root;
    UiHelper* uiHelper;

};

#endif /* CARDCONTROLLER_HPP_ */
