/*
 * FileSystemHelper.cpp
 *
 *  Created on: 23.4.2014
 *      Author: martin
 */

#include "FileSystemHelper.hpp"

#include <QDebug>
#include <QDir>

FileSystemHelper::FileSystemHelper() :
        QObject()
{
}

FileSystemHelper::~FileSystemHelper()
{
}

bool FileSystemHelper::rename(const QString pathOld, const QString pathNew)
{
    QDir qdir;
    if (qdir.exists(pathNew)) {
        return false;
    }
    bool result = qdir.rename(pathOld, pathNew);
    qDebug() << "directory" << pathOld << "renamed to" << pathNew;
    return result;
}

bool FileSystemHelper::removeDirectory(const QString passTypeIdentifier, const QString serialNumber)
{
    QString path = QDir::homePath() + "/passes/" + passTypeIdentifier + "_" + serialNumber;
    bool result = removeDirectory(path);
    return result;
}

bool FileSystemHelper::removeDirectory(const QString path)
{
    bool result = removeDirectoryContent(path);
    if (result) {
        QDir qDir;
        result = qDir.rmdir(path);
        if (result) {
            qDebug() << "removeDirectory() directory removed" << path;
        } else {
            qDebug() << "removeDirectory() error removing directory" << path;
        }
    }
    return result;
}

bool FileSystemHelper::removeDirectoryContent(const QString directoryName)
{
    bool result = true;
    qDebug() << "Removing directory content" << directoryName;
    QDir qDir = QDir(directoryName);
    QFileInfoList list = qDir.entryInfoList(
            QDir::Dirs | QDir::Files | QDir::NoDot | QDir::NoDotDot | QDir::Hidden);
    QFile file;
    foreach(QFileInfo item, list){
    QString path = item.absoluteFilePath();
    qDebug() << "Removing directory item" << path;
    if (item.isFile()) {
        qDebug() << "Removing item type file" << path;
        file.setFileName(item.absoluteFilePath());
        if(file.remove()) {
            qDebug() << "File removed" << path;
        } else {
            qDebug() << "Error removing file" << path;
            return false;
        }
    } else if (item.isDir()) {
        qDebug() << "Removing item type directory" << path;
        removeDirectoryContent(path);
        if(qDir.rmdir(path)) {
            qDebug() << "Directory removed" << path;
        } else {
            qDebug() << "Error removing directory" << path;
        }
    }
}
    return result;
}

QString FileSystemHelper::getHomePath()
{
    return QDir::homePath();
}

QString FileSystemHelper::getTempPath()
{
    return QDir::homePath() + "/passes/tmp";
}

bool FileSystemHelper::exists(QString path)
{
    return QFile::exists(path);
}

bool FileSystemHelper::createDir(const QString path) {
    QDir dir;
    return dir.mkpath(path);
}

void FileSystemHelper::prepareTempDirectory()
{
    QDir dir;
    if (!dir.exists(getTempPath())) {
        dir.mkpath(getTempPath());
    } else {
        FileSystemHelper fsh;
        fsh.removeDirectoryContent(getTempPath());
    }
    qDebug() << "temp path ready";
}

QList<QString> FileSystemHelper::getDirectoryChildren(const QString path)
{
    QList<QString> list;

    QDir dir(path);
    dir.setFilter(QDir::Dirs | QDir::NoDot | QDir::NoDotAndDotDot);
    QFileInfoList dirContent = dir.entryInfoList();
    for (int i = 0; i < dirContent.size(); ++i) {
        QFileInfo fileInfo = dirContent.at(i);
        list.append(fileInfo.absoluteFilePath());
    }

    qDebug() << "getDirectoryChildren() returning" << list.size() << "elements";

    return list;
}

// TODO
bool FileSystemHelper::copyDirectoryContent(const QString source, const QString destination)
{
    bool result = true;
    QDir qdir;

    if (!qdir.exists(source)) {
        qDebug() << "copyDirectoryContent() source does not exists" << source;
        return false;
    }

    if (qdir.exists(destination)) {
        result = removeDirectoryContent(destination);
    } else {
        //result = qdir.mkdir(destination);
        result = qdir.mkpath(destination);
    }

    if (!result) {
        qDebug() << "copyDirectoryContent() unable to prepare destination" << destination;
        return result;
    }

    QDir sourceDir = QDir(source);
    QFileInfoList list = sourceDir.entryInfoList(
            QDir::Dirs | QDir::Files | QDir::NoDot | QDir::NoDotDot);
    QFile file;
    foreach(QFileInfo item, list){
    QString path = item.absoluteFilePath();
        if (item.isFile()) {
            qDebug() << "copyDirectoryContent() copying item type file" << path << "to" << (destination + "/" + item.completeBaseName());
            file.setFileName(item.absoluteFilePath());
            if(file.copy(destination + "/" + item.completeBaseName() + "." + item.completeSuffix())) {
                qDebug() << "copyDirectoryContent() file copied" << path;
            } else {
                qDebug() << "copyDirectoryContent() error copying file" << path;
                return false;
            }
        } else if (item.isDir()) {
            qDebug() << "copyDirectoryContent() copying item type directory" << path;
            copyDirectoryContent(path, (destination + "/" + item.completeBaseName() + "." + item.completeSuffix()));
        }
    }

    return result;
}

bool FileSystemHelper::copyFile(const QString source, const QString destination) {
    QFile file;
    file.setFileName(source);
    if(!file.copy(destination)) {
        return false;
    }
    return true;
}
