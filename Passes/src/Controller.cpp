/*
 * Controller.cpp
 *
 *  Created on: 21. 11. 2014
 *      Author: martin
 */

#include <src/Controller.hpp>

#include <bb/cascades/Application>
#include <bb/system/InvokeRequest>

#include "FileSystemHelper.hpp"
#include "GaussBlur.hpp"
#include "PassHelper.hpp"

Controller::Controller(QObject* parent) :
        QObject(parent), passes(), invokeManager(new bb::system::InvokeManager(this))
{
    bool connectResult;
    Q_UNUSED(connectResult);
    connectResult = QObject::connect(bb::cascades::Application::instance(), SIGNAL(fullscreen()),
            this, SLOT(appInFullScreen()));
    Q_ASSERT(connectResult);
}

Controller::~Controller()
{

}

void Controller::importPassData(QString uri)
{
    emit importPassDataStarted();
    ZipHelper zipHelper;
    connect(&zipHelper, SIGNAL(archiveDecodingFinished(ZipHelper*)), this,
            SLOT(onArchiveDecodingFinished(ZipHelper*)));
    zipHelper.unzipFile(uri);
}

void Controller::onArchiveDecodingFinished(ZipHelper* zipHelper)
{
    disconnect(zipHelper, SIGNAL(archiveDecodingFinished(ZipHelper*)), this,
            SLOT(onArchiveDecodingFinished(ZipHelper*)));
    if (zipHelper->getErrorCode() != 0) {
        emit importPassDataFinished(true, "", ""); //true, there was an error
        return;
    }
    processTempData();
}

void Controller::processTempData()
{
    FileSystemHelper fsHelper;
    PassHelper passHelper;
    QString identificator = passHelper.getPassIdentification(fsHelper.getTempPath());

    if (identificator.startsWith("NOT", Qt::CaseSensitive)) {
        emit importPassDataFinished(true, "", "");
        return;
    }

    QString passPath = fsHelper.getHomePath() + "/passes/" + identificator;
    bool update = false;

    if (fsHelper.exists(passPath)) {
        qDebug() << "directory exists" << passPath;
        update = true;
        fsHelper.removeDirectory(passPath);
    }

    if (!fsHelper.rename(fsHelper.getTempPath(), passPath)) {
        fsHelper.removeDirectory(fsHelper.getTempPath());
        emit importPassDataFinished(true, (update) ? "update" : "insert", "");
        return;
    }

    GaussBlur gaussBlur;
    gaussBlur.blurImages(identificator, false);

    emit importPassDataFinished(false, (update) ? "update" : "insert", identificator);

}

QVariantList Controller::getRefreshedPassesData()
{
    PassHelper passHelper;
    FileSystemHelper fsHelper;
    QVariantMap map;

    passes.clear();

    QList<QString> passDirectoryPathList = fsHelper.getDirectoryChildren(
            fsHelper.getHomePath() + "/passes/");

    for (int i = 0; i < passDirectoryPathList.size(); i++) {
        //qDebug() << passDirectoryPathList[i];
        QVariantMap map = passHelper.getPassData(passDirectoryPathList[i]);
        if (map.size() > 0) {
            passes.append(map);
        }
    }

    return passes;
}

QVariantList Controller::getRefreshedReducedPassesData()
{
    PassHelper passHelper;
    FileSystemHelper fsHelper;
    QVariantMap map;

    passes.clear();

    QList<QString> passDirectoryPathList = fsHelper.getDirectoryChildren(
            fsHelper.getHomePath() + "/passes/");

    for (int i = 0; i < passDirectoryPathList.size(); i++) {
        //directory content listing qDebug() << passDirectoryPathList[i];
        QVariantMap map = passHelper.getReducedPassData(passDirectoryPathList[i]);
        if (map.size() > 0) {
            passes.append(map);
        }
    }

    return passes;
}

void Controller::updateLastUsedTime(QString identificator)
{
    QVariant used_time = QVariant(QDateTime::currentDateTime().toTime_t());
    QSettings settings("polovincak", "Passes");
    settings.setValue(identificator + "/used_time", used_time);
}

int Controller::passesCount()
{
    FileSystemHelper fsHelper;
    return fsHelper.getDirectoryChildren(fsHelper.getHomePath() + "/passes/").size();
}

void Controller::removePassData(const QString identificator)
{
    FileSystemHelper fsh;
    bool result = fsh.removeDirectory(fsh.getHomePath() + "/passes/" + identificator);
    emit removePassDataFinished(result);
}

QVariantMap Controller::getPassData(const QString identificator)
{
    PassHelper passHelper;
    FileSystemHelper fsHelper;
    return passHelper.getPassData(fsHelper.getHomePath() + "/passes/" + identificator);
}

void Controller::appInFullScreen()
{
    QSettings settings("polovincak", "Passes");
    if (settings.value("shouldReloadPasses", QVariant(false)).toBool()) {
        settings.setValue("shouldReloadPasses", QVariant(false));
        emit updatePasses();
    }
}

void Controller::checkShouldReloadPasses()
{
    appInFullScreen();
}

void Controller::invokeEmail(QString subject)
{
    qDebug() << "email invocation, subject:" << subject;
    bb::system::InvokeRequest request;
    request.setTarget("sys.pim.uib.email.hybridcomposer");
    request.setAction("bb.action.SENDEMAIL");
    request.setUri(QUrl("mailto:support@polovincak.eu?subject=" + subject.replace(" ", "%20")));
    invokeManager->invoke(request);
}

void Controller::invokeDisplaySettings() {
    qDebug() << "display settings invocation";
    bb::system::InvokeRequest request;
    request.setTarget("sys.settings.card");
    request.setAction("bb.action.OPEN");
    request.setMimeType("settings/view");
    request.setUri(QUrl("settings://display"));
    invokeManager->invoke(request);
}

void Controller::invokePermissionsSettings() {
    qDebug() << "display settings invocation";
    bb::system::InvokeRequest request;
    request.setTarget("sys.settings.card");
    request.setAction("bb.action.OPEN");
    request.setMimeType("settings/view");
    request.setUri(QUrl("settings://permissions"));
    invokeManager->invoke(request);
}

void Controller::invokeLocationSettings() {
    qDebug() << "display settings invocation";
    bb::system::InvokeRequest request;
    request.setTarget("sys.settings.card");
    request.setAction("bb.action.OPEN");
    request.setMimeType("settings/view");
    request.setUri(QUrl("settings://location"));
    invokeManager->invoke(request);
}

void Controller::invokePass(QString identificator)
{
    emit showInvokedPass(identificator);
}

void Controller::createPass(const QString passType, const QString logoText,
        const QString headerFieldLabelText, const QString headerFieldValueText,
        const QString primaryFieldLabelText, const QString primaryFieldValueText,
        const QString primaryFieldLabelText2, const QString primaryFieldValueText2,
        const QString backFieldLabelText, const QString backFieldTextArea,
        const QString barcodeTypeOfAdded, const QString barcodeMessage, const QString barcodeAltText,
        const QString logoImagePath,
        const QString bgColorValue, const QString fgColorValue, const QString lblColorValue,
        const QString companyName)
{
    qDebug() << "create pass type:" << passType;
    qDebug() << "create pass logoText:" << logoText;
    qDebug() << "create pass header field label" << headerFieldLabelText;
    qDebug() << "create pass header field value" << headerFieldValueText;
    qDebug() << "create pass primary field label" << primaryFieldLabelText;
    qDebug() << "create pass primary field value" << primaryFieldValueText;
    qDebug() << "create pass primary field label 2" << primaryFieldLabelText2;
    qDebug() << "create pass primary field value 2" << primaryFieldValueText2;
    qDebug() << "create pass back field value" << backFieldTextArea;
    qDebug() << "create pass barcode message:" << barcodeMessage;
    qDebug() << "create pass barcode alt text:" << barcodeAltText;
    qDebug() << "create pass barcode type" << barcodeTypeOfAdded;
    qDebug() << "create pass logoImagePath:" << logoImagePath;
    QVariantMap map;
    QString serialNumber = QDateTime::currentDateTimeUtc().toString("yyyyMMddhhmmsss");
    QString passTypeIdentifier = "pass.com.polov.passes";
    map.insert("serialNumber", serialNumber);
    map.insert("passTypeIdentifier", passTypeIdentifier);
    map.insert("teamIdentifier", "QWERTZUIOP");
    map.insert("description", "Manually created pass from Passes BlackBerry application Passes - www.polovincak.eu");
    map.insert("formatVersion", "1");
    map.insert("organizationName", (logoText == "") ? "my" : companyName);
    map.insert("logoText", logoText);
    map.insert("labelColor", lblColorValue);
    map.insert("foregroundColor", fgColorValue);
    map.insert("backgroundColor",bgColorValue);
    if(barcodeMessage!="") {
        QVariantMap barcode;
        barcode.insert("message", barcodeMessage);
        if(barcodeTypeOfAdded=="") {
            barcode.insert("format", "PKBarcodeFormatQR");
        } else {
            barcode.insert("format", barcodeTypeOfAdded);
        }
        barcode.insert("altText", barcodeAltText);
        map["barcode"] = barcode;
    }
    QVariantMap passContent;
    QVariantMap headerField;
    headerField.insert("label", headerFieldLabelText);
    headerField.insert("value", headerFieldValueText);
    QVariantList headerFields;
    headerFields.append(QVariant::fromValue(headerField));
    passContent["headerFields"] = headerFields;
    QVariantMap primaryField;
    primaryField.insert("label", primaryFieldLabelText);
    primaryField.insert("value", primaryFieldValueText);
    QVariantList primaryFields;
    primaryFields.append(QVariant::fromValue(primaryField));
    if(passType=="boardingPass") {
        QVariantMap primaryField2;
        primaryField2.insert("label", primaryFieldLabelText2);
        primaryField2.insert("value", primaryFieldValueText2);
        primaryFields.append(QVariant::fromValue(primaryField2));
    }
    passContent.insert("primaryFields", primaryFields);
    if(backFieldTextArea != "") {
        QVariantMap backField;
        backField.insert("key", "key");
        backField.insert("label", backFieldLabelText);
        backField.insert("value", backFieldTextArea);
        QVariantList backFields;
        backFields.append(QVariant::fromValue(backField));
        passContent.insert("backFields", backFields);
    }
    map.insert(passType, passContent);
    PassHelper passHelper;
    QString identificator = passTypeIdentifier.append('_').append(serialNumber);
    passHelper.savePassData(identificator, map);
    FileSystemHelper fsh;
    if(logoImagePath!="") {
        qDebug() << "copy" << logoImagePath << "to location" << fsh.getHomePath() + "/passes/" + identificator + "/logo@2x.png";
        fsh.copyFile(logoImagePath,fsh.getHomePath() + "/passes/" + identificator + "/logo@2x.png");
    }
    emit importPassDataFinished(false, "insert", identificator);
}
