/*
 * Copyright (c) 2011-2014 BlackBerry Limited.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "applicationui.hpp"

#include <bb/device/DisplayInfo>
#include <bb/cascades/Application>
#include <bb/cascades/QmlDocument>
#include <bb/cascades/AbstractPane>
#include <bb/cascades/LocaleHandler>
#include <bb/system/InvokeRequest>

#include "Controller.hpp"
#include "CardController.hpp"
#include "FilterableGroupDataModel.hpp"
#include "GeoHelper.hpp"
#include "UiHelper.hpp"

using namespace bb::cascades;

ApplicationUI::ApplicationUI() :
        QObject(), m_invokeManager(new bb::system::InvokeManager(this))
{
    QCoreApplication::setOrganizationName("polovincak");
    QCoreApplication::setOrganizationDomain("polovincak.eu");
    QCoreApplication::setApplicationName("Passes");

    // prepare the localization
    m_pTranslator = new QTranslator(this);
    m_pLocaleHandler = new LocaleHandler(this);

    bool res = QObject::connect(m_pLocaleHandler, SIGNAL(systemLanguageChanged()), this,
            SLOT(onSystemLanguageChanged()));
    // This is only available in Debug builds
    Q_ASSERT(res);
    // Since the variable is not used in the app, this is added to avoid a
    // compiler warning
    Q_UNUSED(res);

    // initial load
    onSystemLanguageChanged();

    qmlRegisterType<bb::device::DisplayInfo>("bb.displayInfo", 1, 0, "DisplayInfo");
    qmlRegisterType<FilterableGroupDataModel>("com.polov", 1, 0, "FilterableGroupDataModel");

    // Create scene document from main.qml asset, the parent is set
    // to ensure the document gets destroyed properly at shut down.
    //QmlDocument *qml = QmlDocument::create("asset:///main.qml").parent(this);

    //Controller* controller = new Controller();
    //qml->setContextProperty("controller", controller);
    //UiHelper* uiHelper = new UiHelper();
    //qml->setContextProperty("uiHelper", uiHelper);

    bool connectResult;
    Q_UNUSED(connectResult);
    connectResult = QObject::connect(m_invokeManager,
            SIGNAL(invoked(const bb::system::InvokeRequest&)), this,
            SLOT(onInvoke(const bb::system::InvokeRequest&)));
    if (!connectResult) {
        qDebug() << "Invoked signal NOT connected";
    } else {
        qDebug() << "controller connected to invokeManager.invoked signal";
    }
    Q_ASSERT(connectResult);

    switch (m_invokeManager->startupMode()) {
        case bb::system::ApplicationStartupMode::InvokeCard:
            qDebug() << "ApplicationStartupMode::InvokeCard";

            cardController = new CardController();

            break;

        default:
            qDebug() << "ApplicationStartupMode::Default";

            QmlDocument *qml = QmlDocument::create("asset:///main.qml").parent(this);

            controller = new Controller();
            qml->setContextProperty("controller", controller);
            GeoHelper *geoHelper = new GeoHelper();
            qml->setContextProperty("geoHelper", geoHelper);

            UiHelper* uiHelper = new UiHelper();
            qml->setContextProperty("uiHelper", uiHelper);

            // Create root object for the UI
            AbstractPane *root = qml->createRootObject<AbstractPane>();

            // Set created root object as the application scene
            Application::instance()->setScene(root);

            break;
    }
}

void ApplicationUI::onSystemLanguageChanged()
{
    QCoreApplication::instance()->removeTranslator(m_pTranslator);
    // Initiate, load and install the application translation files.
    QString locale_string = QLocale().name();
    QString file_name = QString("Passes_%1").arg(locale_string);
    if (m_pTranslator->load(file_name, "app/native/qm")) {
        QCoreApplication::instance()->installTranslator(m_pTranslator);
    }
}

// Slot that gets called when the invoke signal is emitted
void ApplicationUI::onInvoke(const bb::system::InvokeRequest& request)
{
    QString uri = request.uri().toString();
    if (uri.startsWith("file://", Qt::CaseInsensitive)) {
        uri = request.uri().toString().mid(7);
    } else {
        uri = request.uri().toString();
    }

    QString data = QString(request.data());

    qDebug() << "Invocation framework signal " << request.mimeType() << request.action() << uri
            << data;

    if (uri.isEmpty() && !data.isEmpty()) {
        qDebug() << "show in app";
        controller->invokePass(data);
    } else if (!uri.isEmpty() && data.isEmpty()) {
        qDebug() << "show as card ";
        if (cardController != NULL) {
            cardController->showCard(uri);
        } else {
            qDebug() << "card controller cannot be found";
        }
    }

}
