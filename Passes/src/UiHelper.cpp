/*
 * UiHelper.cpp
 *
 *  Created on: 21. 11. 2014
 *      Author: martin
 */

#include "UiHelper.hpp"

#include <QSettings>
#include <QStringList>
#include <bb/cascades/multimedia/BarcodeGenerator>
#include <bb/cascades/multimedia/BarcodeGeneratorFormat>

#include "FileSystemHelper.hpp"
#include "GaussBlur.hpp"

UiHelper::UiHelper(QObject* parent) :
        QObject(parent), listviewLayoutType()
{
    listviewLayoutType = getListviewLayoutType();
}

UiHelper::~UiHelper()
{

}

bb::cascades::Color UiHelper::getColor(QString appleColorNotation)
{
    //qDebug() << "getColor() get color from string" << appleColorNotation;
    if (appleColorNotation.size() == 7) {
        bool ok;
        int r = ("0x" + appleColorNotation.mid(1, 2)).toInt(&ok, 16);
        int g = ("0x" + appleColorNotation.mid(3, 2)).toInt(&ok, 16);
        int b = ("0x" + appleColorNotation.mid(5, 2)).toInt(&ok, 16);
        return bb::cascades::Color::fromRGBA((float) r / 255, (float) g / 255, (float) b / 255,
                1.0f);
    } else {
        if (appleColorNotation.size() < 10 || appleColorNotation.size() > 18) {
            qDebug() << "getColor wrong size of string" << appleColorNotation;
            return bb::cascades::Color::fromRGBA(1.0f, 1.0F, 1.0f, 1.0f);
        }

        QString substring = appleColorNotation.mid(4, appleColorNotation.size() - 5);
        QStringList values = substring.split(",");
        //qDebug() << "getColor r,g,b values" << values;
        if (values.length() != 3) {
            qDebug() << "getColor error values count";
            return bb::cascades::Color::fromRGBA(1.0f, 1.0F, 1.0f, 1.0f);
        }

        bool ok;
        float r = values.at(0).toFloat(&ok) / 255;
        float g = values.at(1).toFloat(&ok) / 255;
        float b = values.at(2).toFloat(&ok) / 255;
        //qDebug() << "returning color from" << r << g << b;
        return bb::cascades::Color::fromRGBA(r, g, b, 1.0f);
    }
}

QString UiHelper::getColorString(QString appleColorNotation)
{
    //qDebug() << "apple color notation received" << appleColorNotation;
    // web notation #(aa)rrggbb
    if (appleColorNotation.startsWith("#")) {
        return appleColorNotation;
    } else {
        if (appleColorNotation.size() < 10 || appleColorNotation.size() > 18) {
            qDebug() << "getColor wrong size of string" << appleColorNotation;
            return "#ff000000";
        }

        QString substring = appleColorNotation.mid(4, appleColorNotation.size() - 5);
        QStringList values = substring.split(",");
        //qDebug() << "getColor r,g,b values" << values;
        if (values.length() != 3) {
            qDebug() << "getColor error values count";
            return "#ff000000";
        }

        bool ok;
        int r = values.at(0).toInt(&ok);
        int g = values.at(1).toInt(&ok);
        int b = values.at(2).toInt(&ok);
        QString rStr = QString("%1").arg(r, 2, 16, QLatin1Char('0'));
        QString gStr = QString("%1").arg(g, 2, 16, QLatin1Char('0'));
        QString bStr = QString("%1").arg(b, 2, 16, QLatin1Char('0'));
        QString argb = QString("#ff").append(rStr).append(gStr).append(bStr);
        //qDebug() << "returning color" << argb;
        return argb;
    }

    return "#ff000000";
}

bool UiHelper::hasImage(const QString identificator, const QString name) const
{
    FileSystemHelper fsh;
    QString path2x = fsh.getHomePath() + "/passes/" + identificator + "/" + name + "@2x.png";
    QString path = fsh.getHomePath() + "/passes/" + identificator + "/" + name + ".png";

    if (fsh.exists(path2x)) {
        return true;
    } else if (fsh.exists(path)) {
        return true;
    }
    return false;
}

bool UiHelper::hasImageCard(const QString identificator, const QString name) const
{
    FileSystemHelper fsh;
    QString path2x = fsh.getHomePath() + "/passes_cards/" + identificator + "/" + name + "@2x.png";
    QString path = fsh.getHomePath() + "/passes_cards/" + identificator + "/" + name + ".png";

    if (fsh.exists(path2x)) {
        return true;
    } else if (fsh.exists(path)) {
        return true;
    }
    return false;
}

QString UiHelper::getImagePath(const QString identificator, const QString name) const
{
    FileSystemHelper fsh;
    QString path2x = fsh.getHomePath() + "/passes/" + identificator + "/" + name + "@2x.png";
    QString path = fsh.getHomePath() + "/passes/" + identificator + "/" + name + ".png";

    if (fsh.exists(path2x)) {
        return path2x;
    } else if (fsh.exists(path)) {
        return path;
    }
    qDebug() << "image not found " << identificator << name;
    return "";
}

QString UiHelper::getImagePathCard(const QString identificator, const QString name) const
{
    FileSystemHelper fsh;
    QString path2x = fsh.getHomePath() + "/passes_cards/" + identificator + "/" + name + "@2x.png";
    QString path = fsh.getHomePath() + "/passes_cards/" + identificator + "/" + name + ".png";

    if (fsh.exists(path2x)) {
        return path2x;
    } else if (fsh.exists(path)) {
        return path;
    }
    return "";
}

QString UiHelper::getFieldLabel(QVariant v, int i)
{
    QVariantList list = v.toList();
    int listSize = list.size();
    if (i < listSize) {
        return list.at(i).toMap().value("label").toString();
    }
    return "";
}

QString UiHelper::getFieldValue(QVariant v, int i)
{
    QVariantList list = v.toList();
    int listSize = list.size();
    if (i < listSize) {
        return list.at(i).toMap().value("value").toString();
    }
    return "";
}

QString UiHelper::getListviewLayoutType()
{
    QSettings settings("polovincak", "Passes");
    if (!settings.value("listviewLayoutType").isNull()) {
        listviewLayoutType = settings.value("listviewLayoutType").toString();
    }
    return listviewLayoutType;
}

void UiHelper::setListviewLayoutType(const QString value)
{
    QSettings settings("polovincak", "Passes");
    settings.setValue("listviewLayoutType", QVariant(value));
}

bb::cascades::Image UiHelper::getBarcodeImage(QString type, QString message)
{
    bb::cascades::multimedia::BarcodeGenerator barcodeGenerator;
    bb::cascades::multimedia::BarcodeGeneratorFormat::Type barcodeType;
    if (type == "PKBarcodeFormatQR") {
        barcodeType = bb::cascades::multimedia::BarcodeGeneratorFormat::QrCode;
    } else if (type == "PKBarcodeFormatPDF417") {
        barcodeType = bb::cascades::multimedia::BarcodeGeneratorFormat::Pdf417;
    } else if (type == "PKBarcodeFormatAztec") {
        barcodeType = bb::cascades::multimedia::BarcodeGeneratorFormat::Aztec;
    } else if (type == "PKBarcodeFormatEAN") {
        barcodeType = bb::cascades::multimedia::BarcodeGeneratorFormat::Ean;
    } else {
        barcodeType = bb::cascades::multimedia::BarcodeGeneratorFormat::QrCode;
    }
    bb::ImageData imageData = barcodeGenerator.generate(barcodeType, message, 3.0f);
    if (!imageData.isValid()) {
        qDebug() << "getBarcodeImage() invalid image data";
        return NULL;
    }
    bb::cascades::Image image(imageData);
    return image;

}

QString UiHelper::getBackFieldContentHtml(QVariant backfields)
{
    QString content;
    content.append("<html>");
    content.append("<head>");
    content.append("<title>Pass Info</title>");
    content.append(
            "<style type=\"text/css\">h2{font-weight:bold;font-size:large}p{margin-bottom:1em}</style>");
    content.append("</head>");
    QVariantList list = backfields.toList();
    int listSize = list.size();
    for (int i = 0; i < listSize; i++) {
        content.append("<h2>" + list.at(i).toMap().value("label").toString() + "</h2>");
        content.append("<p>" + list.at(i).toMap().value("value").toString() + "</p>");
    }
    content.append("</html>");
    return content;
}

bb::cascades::Image UiHelper::getImage(const QString identificator, const QString name,
        const int cropWidth, const int cropHeight)
{
    QString path = getImagePathCard(identificator, name);
    QImage qImage;
    QFile file(path);
    if (!file.open(QIODevice::ReadOnly)) {
        qDebug() << "Failed to open file" << path << "for reading";
        return NULL;
    }
    if (!qImage.loadFromData(file.readAll())) {
        qDebug() << "Image data null " << path;
        return NULL;
    }

    file.close();

    if (cropWidth > qImage.width()) {
        qImage = qImage.scaledToWidth(cropWidth);
    }
    if (cropHeight > qImage.height()) {
        qImage = qImage.scaledToHeight(cropHeight);
    }

    int deltaWidth = (qImage.width() - cropWidth) / 2;
    int deltaHeight = (qImage.height() - cropHeight) / 2;

    qImage = qImage.copy(deltaWidth, deltaHeight, qImage.width() - deltaWidth,
            qImage.height() - deltaHeight);

    QImage swappedImage = qImage.rgbSwapped();
    bb::ImageData imageData = bb::ImageData::fromPixels(swappedImage.bits(), bb::PixelFormat::RGBX,
            swappedImage.width(), swappedImage.height(), swappedImage.bytesPerLine());
    qDebug() << "Return requested content " << path;
    return bb::cascades::Image(imageData);
}
