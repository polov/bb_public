/*
 * ZipHelper.cpp
 *
 *  Created on: 22.4.2014
 *      Author: martin
 */

#include "ZipHelper.hpp"

#include <QDebug>
#include <QDir>

#include <archive.h>
#include <archive_entry.h>

ZipHelper::ZipHelper() :
        QObject(), errorCode(0), errorMessage("")
{
}

ZipHelper::~ZipHelper()
{
}

void ZipHelper::unzipFile(const QString source, const QString destination, bool emitSignals)
{
    //QString unzip_string = QDir::homePath() + "/passes/tmp/%s";
    //QString unzip_path = QDir::homePath() + "/passes/tmp";
    QString unzip_string = destination + "/%s";
    QString unzip_path = destination;

    qDebug() << "unzipFile() path=" << source << "unzip_path=" << unzip_path;

    QDir dir = QDir(unzip_path);

    if (!dir.exists()) {
        dir.mkpath(unzip_path);
        qDebug() << "unzip_path created" << unzip_path;
    }

    struct archive *arch;
    struct archive *ext;
    struct archive_entry *entry;
    int flags;
    int r;

    /* Select which attributes we want to restore. */
    flags = ARCHIVE_EXTRACT_TIME;
    flags |= ARCHIVE_EXTRACT_PERM;
    flags |= ARCHIVE_EXTRACT_ACL;
    flags |= ARCHIVE_EXTRACT_FFLAGS;

    arch = archive_read_new();
    archive_read_support_filter_all(arch);
    archive_read_support_format_all(arch);

    ext = archive_write_disk_new();
    archive_write_disk_set_options(ext, flags);
    archive_write_disk_set_standard_lookup(ext);

    r = archive_read_open_filename(arch, source.toLatin1().data(), 8192); // Note 1
    if (r != ARCHIVE_OK) {
        qDebug() << "unzipFile() error: " << r << archive_error_string(arch);
        this->setErrorCode(r);
        if (emitSignals) {
            emit archiveDecodingFinished(this);
        }
        return;
    }
    for (;;) {
        r = archive_read_next_header(arch, &entry);
        if (r == ARCHIVE_EOF) {
            break;
        }
        if (r != ARCHIVE_OK) {
            qDebug() << "unzipFile() error archive_read_next_header1: " << r
                    << archive_error_string(arch);
            this->setErrorCode(r);
            if (emitSignals) {
                emit archiveDecodingFinished(this);
            }
            return;
        }
        if (r < ARCHIVE_WARN) {
            qDebug() << "unzipFile() error archive_read_next_header2: " << r
                    << archive_error_string(arch);
            this->setErrorCode(r);
            if (emitSignals) {
                emit archiveDecodingFinished(this);
            }
            return;
        }

        qDebug() << "unzipFile() archive_entry=" << archive_entry_pathname(entry);

        const char* source = archive_entry_pathname(entry);
        char newPath[PATH_MAX + 1];
        snprintf(newPath, PATH_MAX, unzip_string.toLatin1().data(), source);
        archive_entry_set_pathname(entry, newPath);

        r = archive_write_header(ext, entry);

        if (r != ARCHIVE_OK) {
            qDebug() << "unzipFile() error archive_write_header: " << r
                    << archive_error_string(ext);
            this->setErrorCode(r);
        } else {
            copy_data(arch, ext);
            r = archive_write_finish_entry(ext);
            if (r != ARCHIVE_OK) {
                qDebug() << "unzipFile() archive_write_finish_entry failed:" << r
                        << archive_error_string(ext);
                this->setErrorCode(r);
            } else {
                qDebug() << "unzipFile() extracted successfuly archive_entry="
                        << archive_entry_pathname(entry);
            }
        }
    }

    archive_read_close(arch);
    archive_read_free(arch);
    archive_write_close(ext);
    archive_write_free(ext);

    qDebug() << "unzipFile() finished for path=" << source;

    if (emitSignals) {
        emit archiveDecodingFinished(this);
    }

    return;
}

void ZipHelper::unzipFile(const QString path)
{
    QString unzip_string = QDir::homePath() + "/passes/tmp/%s";
    QString unzip_path = QDir::homePath() + "/passes/tmp";

    qDebug() << "unzipFile() path=" << path << "unzip_path=" << unzip_path;

    QDir dir = QDir(unzip_path);

    if (!dir.exists()) {
        dir.mkpath(unzip_path);
        qDebug() << "unzip_path created" << unzip_path;
    }

    struct archive *arch;
    struct archive *ext;
    struct archive_entry *entry;
    int flags;
    int r;

    /* Select which attributes we want to restore. */
    flags = ARCHIVE_EXTRACT_TIME;
    flags |= ARCHIVE_EXTRACT_PERM;
    flags |= ARCHIVE_EXTRACT_ACL;
    flags |= ARCHIVE_EXTRACT_FFLAGS;

    arch = archive_read_new();
    archive_read_support_filter_all(arch);
    archive_read_support_format_all(arch);

    ext = archive_write_disk_new();
    archive_write_disk_set_options(ext, flags);
    archive_write_disk_set_standard_lookup(ext);

    r = archive_read_open_filename(arch, path.toLatin1().data(), 8192); // Note 1
    if (r != ARCHIVE_OK) {
        qDebug() << "unzipFile() error: " << r << archive_error_string(arch);
        this->setErrorCode(r);
        emit archiveDecodingFinished(this);
        return;
    }
    for (;;) {
        r = archive_read_next_header(arch, &entry);
        if (r == ARCHIVE_EOF) {
            break;
        }
        if (r != ARCHIVE_OK) {
            qDebug() << "unzipFile() error archive_read_next_header1: " << r
                    << archive_error_string(arch);
            this->setErrorCode(r);
            emit archiveDecodingFinished(this);
            return;
        }
        if (r < ARCHIVE_WARN) {
            qDebug() << "unzipFile() error archive_read_next_header2: " << r
                    << archive_error_string(arch);
            this->setErrorCode(r);
            emit archiveDecodingFinished(this);
            return;
        }

        qDebug() << "unzipFile() archive_entry=" << archive_entry_pathname(entry);

        const char* path = archive_entry_pathname(entry);
        char newPath[PATH_MAX + 1];
        snprintf(newPath, PATH_MAX, unzip_string.toLatin1().data(), path);
        archive_entry_set_pathname(entry, newPath);

        r = archive_write_header(ext, entry);

        if (r != ARCHIVE_OK) {
            qDebug() << "unzipFile() error archive_write_header: " << r
                    << archive_error_string(ext);
            this->setErrorCode(r);
        } else {
            copy_data(arch, ext);
            r = archive_write_finish_entry(ext);
            if (r != ARCHIVE_OK) {
                qDebug() << "unzipFile() archive_write_finish_entry failed:" << r
                        << archive_error_string(ext);
                this->setErrorCode(r);
            } else {
                qDebug() << "unzipFile() extracted successfuly archive_entry="
                        << archive_entry_pathname(entry);
            }
        }
    }

    archive_read_close(arch);
    archive_read_free(arch);
    archive_write_close(ext);
    archive_write_free(ext);

    qDebug() << "unzipFile() finished for path=" << path;

    emit archiveDecodingFinished(this);

    return;
}

/*
 * take bytes from compressed archive (arch) and put to extracted archive (ext).
 */
int ZipHelper::copy_data(struct archive *arch, struct archive *ext)
{
    int r;
    const void *buff;
    size_t size;

    int64_t offset;

    for (;;) {
        r = archive_read_data_block(arch, &buff, &size, &offset);
        if (r == ARCHIVE_EOF) {
            return (ARCHIVE_OK);
        }
        if (r != ARCHIVE_OK) {
            return (r);
        }
        r = archive_write_data_block(ext, buff, size, offset);
        if (r != ARCHIVE_OK) {
            qDebug() << "Unzip archive_write_finish_entry failed:" << r
                    << archive_error_string(ext);
            return (r);
        }
    }

    return ARCHIVE_WARN;
}

int ZipHelper::getErrorCode() const
{
    return errorCode;
}

void ZipHelper::setErrorCode(int errorCode)
{
    this->errorCode = errorCode;
}

const QString& ZipHelper::getErrorMessage() const
{
    return errorMessage;
}

void ZipHelper::setErrorMessage(const QString& errorMessage)
{
    this->errorMessage = errorMessage;
}
