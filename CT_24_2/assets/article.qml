import bb.cascades 1.4

Page {
    id: articlePage
    content: ScrollView {
        Container {
            Container {
                layout: StackLayout {
                    orientation: LayoutOrientation.LeftToRight
                }
                Container {
                    id: imageContainer
                    preferredWidth: controller.displayWidth
                    minHeight: ui.du(30.0)
                    maxHeight: ui.du(30.0)
                    attachedObjects: [
                        ImagePaintDefinition {
                            id: back
                            repeatPattern: RepeatPattern.XY
                        }
                    ]
                    gestureHandlers: [
                        TapHandler {
                            onTapped: {
                                if (imageContainer.bigger) {
                                    imageContainer.bigger = false;
                                    imageContainer.minHeight = ui.du(30.0);
                                } else {
                                    imageContainer.bigger = true;
                                    imageContainer.minHeight = 480.0;
                                }
                            }
                        }
                    ]
                    property bool bigger: false
                }
            }
            Container {
                visible: false
                topMargin: ui.du(3.0)
                leftPadding: ui.du(3.0)
                rightPadding: ui.du(3.0)
                layout: StackLayout {
                    orientation: LayoutOrientation.LeftToRight
                }
                Container {
                    topPadding: ui.du(1.0)
                    bottomPadding: ui.du(1.6)
                    leftPadding: ui.du(0.0)
                    rightPadding: ui.du(0.0)
                    Label {
                        text: "Štítky:"
                        textStyle {
                            fontSize: FontSize.Small
                            fontWeight: FontWeight.W300
                        }
                    }
                }
                Container {
                    leftMargin: ui.du(2.0)
                    topPadding: ui.du(1.0)
                    bottomPadding: ui.du(1.6)
                    leftPadding: ui.du(2.0)
                    rightPadding: ui.du(2.0)
                    background: Color.create("#0092CC")
                    Label {
                        text: section
                        textStyle {
                            color: Color.White
                            fontSize: FontSize.Small
                            fontWeight: FontWeight.W300
                        }
                    }
                }
                Container {
                    visible: (hasVideo == "1")
                    leftMargin: ui.du(2.0)
                    topPadding: ui.du(1.0)
                    bottomPadding: ui.du(1.6)
                    leftPadding: ui.du(2.0)
                    rightPadding: ui.du(2.0)
                    background: Color.create("#0092CC")
                    Label {
                        text: "Video"
                        textStyle {
                            color: Color.White
                            fontSize: FontSize.Small
                            fontWeight: FontWeight.W300
                        }
                    }
                }
            }
            Container {
                topMargin: ui.du(3.0)
                leftPadding: ui.du(3.0)
                rightPadding: ui.du(3.0)
                id: titleContainer
                Label {
                    text: title
                    multiline: true
                    textStyle {
                        fontSize: FontSize.Large
                        fontWeight: FontWeight.W500
                    }
                }
            }
            Container {
                topMargin: ui.du(3.0)
                leftPadding: ui.du(3.0)
                rightPadding: ui.du(3.0)
                id: pubDateContainer
                layout: StackLayout {
                    orientation: LayoutOrientation.LeftToRight
                }
                Container {
                    verticalAlignment: VerticalAlignment.Center
                    Label {
                        text: pubDate
                        textStyle {
                            fontSize: FontSize.Small
                            fontWeight: FontWeight.W300
                        }
                    }
                }
                Container {
                    verticalAlignment: VerticalAlignment.Center
                    layoutProperties: StackLayoutProperties {
                        spaceQuota: 1
                    }
                    ImageView {
                        visible: (hasVideo == "1")
                        horizontalAlignment: HorizontalAlignment.Right
                        verticalAlignment: VerticalAlignment.Center
                        maxHeight: ui.du(5.0)
                        imageSource: "asset:///images/camera.png"
                        scalingMethod: ScalingMethod.AspectFit
                    }
                }    
                Container {
                    verticalAlignment: VerticalAlignment.Center
                    Label {
                        visible: (hasVideo == "1")
                        horizontalAlignment: HorizontalAlignment.Right
                        text: "článek obsahuje video"
                        multiline: true
                        textStyle {
                            fontSize: FontSize.Small
                            fontWeight: FontWeight.W300
                        }
                    }
                }            
            }
            Container {
                id: descriptionContainer
                topMargin: ui.du(3.0)
                leftPadding: ui.du(3.0)
                rightPadding: ui.du(3.0)
                Label {
                    text: description
                    multiline: true
                    textStyle.lineHeight: 1.10
                    textFormat: TextFormat.Html
                    textStyle.fontWeight: FontWeight.W300
                    textStyle.fontSize: FontSize.Medium
                }
            }
        }
    }
    actions: [
        ActionItem {
            title: "Přečíst"
            ActionBar.placement: ActionBarPlacement.Signature
            imageSource: "asset:///images/ic_open.png"
            onTriggered: {
                var webContent = web.createObject();
                webContent.link = link;
                navigationPane.push(webContent);
            }
        },
        InvokeActionItem {
            ActionBar.placement: ActionBarPlacement.InOverflow
            query {
                mimeType: "text/plain"
                invokeActionId: "bb.action.SHARE"
            }
            onTriggered: {
                data = articlePage.title + " " + articlePage.link
                console.log("share triggered");
            }
        }
    ]
    attachedObjects: [
        ComponentDefinition {
            id: web
            source: "web.qml"
        },
        PageLayoutUpdateHandler {
            id: pageLayoutUpdateHandler
            onBottomOverlayHeightChanged: {
                descriptionContainer.bottomPadding = bottomOverlayHeight
            }
        }
    ]
    onImageChanged: {
        back.image = image;
        imageContainer.background = back.imagePaint;
    }
    property string section: "Hlavní zprávy"
    property string title: "Uprchlická vina dopadla na politiku. V Horních Rakousech uspěli populisté"
    property string description: "Výsledky regionálních voleb v rakouské spolkové zemi Horní Rakousy ukázaly výrazné posílení pravicové populistické Svobodné strany Rakouska (FPÖ)."
    property string link
    property string hasVideo: "0"
    property string pubDate: ""
    property variant image
}
