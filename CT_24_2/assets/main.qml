/*
 * Copyright (c) 2011-2015 BlackBerry Limited.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import bb.cascades 1.4
import bb.system 1.0
import bb.data 1.0

TabbedPane {
    id: tabbedPane
    showTabsOnActionBar: false
    Menu.definition: MenuDefinition {
        actions: [
            ActionItem {
                title: qsTr("About")
                imageSource: "asset:///images/ic_info.png"
                onTriggered: {
                    var aboutPageContent = aboutPage.createObject();
                    navigationPane.push(aboutPageContent);
                }
            },
            ActionItem {
                title: qsTr("Send Feedback") + Retranslate.onLanguageChanged
                imageSource: "asset:///images/ic_feedback.png"
                onTriggered: {
                    controller.invokeEmail("", qsTr("Zprávy ČT24 Feedback") + Retranslate.onLanguageChanged, "");
                }
            }
        ]
        settingsAction: SettingsActionItem {
            onTriggered: {
                var settingsPageContent = settingsPage.createObject();
                navigationPane.push(settingsPageContent);
            }
        }
    }
    activePane: NavigationPane {
        id: navigationPane
        Page {
            id: mainPage
            titleBar: TitleBar {
                id: titlebar
                title: "Hlavní zprávy"
            }
            actionBarVisibility: ChromeVisibility.Overlay
            content: Container {
                ListView {
                    id: feedList
                    layout: FlowListLayout {
                    }
                    leadingVisual: Container {                        
                        Container {
                            id: updateActivityContainer
                            layout: StackLayout {
                                orientation: LayoutOrientation.LeftToRight
                            }
                            visible: false
                            Container {
                                id: activityIndicatorContent
                                topPadding: ui.du(4.0)
                                bottomPadding: ui.du(4.0)
                                layoutProperties: StackLayoutProperties {
                                    spaceQuota: 1
                                }
                                Container {
                                    horizontalAlignment: HorizontalAlignment.Center
                                    layout: StackLayout {
                                        orientation: LayoutOrientation.LeftToRight
                                    }
                                    ActivityIndicator {
                                        id: activityIndicator
                                    }
                                    Label {
                                        id: activityLabel
                                        text: "Aktualizace dat"
                                    }
                                }
                            }
                        }
                        Container {
                            id: leadingvisualContainer
                            layout: StackLayout {
                                orientation: LayoutOrientation.LeftToRight
                            }
                            Container {
                                topPadding: ui.du(4.0)
                                bottomPadding: ui.du(4.0)
                                layoutProperties: StackLayoutProperties {
                                    spaceQuota: 1
                                }
                                Label {
                                    id: refreshLabel
                                    horizontalAlignment: HorizontalAlignment.Center
                                    text: "Obnovte potažením dolů."
                                }
                            }
                        }
                        attachedObjects: [
                            LayoutUpdateHandler {
                                id: refreshHandler
                                onLayoutFrameChanged: {
                                    if (layoutFrame.y >= ui.du(15.0)) {
                                        refreshLabel.text = "Obnovte uvolněním."
                                        shouldRefresh = true;
                                    } else if (layoutFrame.y > 0) {
                                        refreshLabel.text = "Obnovte potažením dolů."
                                    } else if (layoutFrame.y == 0) {
                                        //feedList.scrollToPosition(0, ScrollAnimation.None)
                                        //feedList.scroll(- ui.du(10.0), ScrollAnimation.Smooth)
                                        feedList.hideLeadingVisual();
                                        if (shouldRefresh && ! updating) {
                                            console.log("start update");
                                            //updating = true;
                                            shouldRefresh = false;
                                            mainPage.updateFeeds();
                                        }
                                    }
                                }
                            }
                        ]
                    }
                    listItemComponents: [
                        ListItemComponent {
                            type: "header"
                            Container {
                                enabled: false
                            }
                        },
                        ListItemComponent {
                            id: listItemComponent
                            type: "item"
                            content: Container {
                                id: listItemContainer
                                Container {
                                    layout: StackLayout {
                                        orientation: LayoutOrientation.LeftToRight
                                    }
                                    topPadding: ui.du(1.0)
                                    bottomPadding: ui.du(1.0)
                                    leftPadding: ui.du(1.0)
                                    rightPadding: ui.du(1.0)
                                    Container {
                                        minWidth: ui.du(28.0)
                                        maxWidth: ui.du(28.0)
                                        ImageView {
                                            id: placeholder_ct24
                                            imageSource: "asset:///images/placeholder_big.png"
                                            visible: ! ListItemData.enclosureImageLoaded
                                            scalingMethod: ScalingMethod.AspectFit

                                        }
                                        ImageView {
                                            visible: ListItemData.enclosureImageLoaded
                                            image: ListItemData.enclosureImage
                                            scalingMethod: ScalingMethod.AspectFit
                                        }
                                    }
                                    Container {
                                        //topPadding: ui.du(0.0)
                                        Container {
                                            leftPadding: ui.du(2.0)
                                            Label {
                                                text: ListItemData.title
                                                multiline: true
                                                autoSize.maxLineCount: 3
                                                //textStyle.fontWeight: FontWeight.W300
                                                textStyle.fontSize: FontSize.Medium
                                            }
                                        }
                                        Container {
                                            topPadding: ui.du(0.5)
                                            leftPadding: ui.du(2.0)
                                            Label {
                                                text: Qt.formatDateTime(ListItemData.pubDate, "d.M.yyyy, h:mm")
                                                textStyle {
                                                    fontSize: FontSize.Small
                                                    fontWeight: FontWeight.W300
                                                }
                                            }
                                        }
                                    }
                                }
                                Container {
                                    layout: StackLayout {
                                        orientation: LayoutOrientation.LeftToRight
                                    }
                                    Container {
                                        background: Color.Gray
                                        minHeight: 2.0
                                        maxHeight: 2.0
                                        layoutProperties: StackLayoutProperties {
                                            spaceQuota: 1
                                        }
                                    }
                                }
                            }
                        }
                    ]
                    onTriggered: {
                        var chosenItem = feedList.dataModel.data(indexPath);
                        openArticleDescription(chosenItem.title, chosenItem.description, chosenItem.link, chosenItem.enclosureImage, chosenItem.hasVideo, chosenItem.pubDate);
                    }
                    function openArticleDescription(title, description, link, enclosureImage, hasVideo, pubDate) {
                        var articlePageContent = articlePage.createObject();
                        articlePageContent.section = titlebar.title;
                        articlePageContent.title = title;
                        articlePageContent.description = description;
                        articlePageContent.link = link;
                        articlePageContent.hasVideo = hasVideo;
                        articlePageContent.pubDate = Qt.formatDateTime(pubDate, "d.M.yyyy, h:mm");
                        articlePageContent.image = enclosureImage;
                        navigationPane.push(articlePageContent);
                    }
                    function hideLeadingVisual() {
                        scrollToPosition(ScrollPosition.Beginning, ScrollAnimation.Default);
                    }
                }
            }
            actions: [
                ActionItem {
                    title: "Obnovit"
                    imageSource: "asset:///images/ic_reload.png"
                    ActionBar.placement: ActionBarPlacement.Signature
                    onTriggered: {
                        mainPage.updateFeeds();
                    }
                },
                ActionItem {
                    title: "Live"
                    imageSource: "asset:///images/ic_play.png"
                    ActionBar.placement: ActionBarPlacement.OnBar
                    onTriggered: {
                        var webContent = web.createObject();
                        webContent.link = "http://www.ceskatelevize.cz/ct24#live";
                        navigationPane.push(webContent);
                    }
                }
            ]
            attachedObjects: [
                PageLayoutUpdateHandler {
                    id: pageLayoutUpdateHandler
                    onBottomOverlayHeightChanged: {
                        feedList.bottomPadding = bottomOverlayHeight
                    }
                }
            ]
            function updateFeeds() {
                if (! updating) {
                    updatedCount = 0;
                    updating = true;
                    leadingvisualContainer.visible = false;
                    updateActivityContainer.visible = true;
                    activityIndicator.start();
                    feedList.scrollToPosition(0, ScrollAnimation.None)
                    feedList.scroll(- ui.du(10.0), ScrollAnimation.Smooth)
                    controller.downloadFromUrl("hlavni-zpravy.xml", "http://www.ceskatelevize.cz/ct24/rss/hlavni-zpravy");
                    controller.downloadFromUrl("domaci.xml", "http://www.ceskatelevize.cz/ct24/rss/domaci");
                    controller.downloadFromUrl("regiony.xml", "http://www.ceskatelevize.cz/ct24/rss/regiony");
                    controller.downloadFromUrl("svet.xml", "http://www.ceskatelevize.cz/ct24/rss/svet");
                    controller.downloadFromUrl("ekonomika.xml", "http://www.ceskatelevize.cz/ct24/rss/ekonomika");
                    controller.downloadFromUrl("kultura.xml", "http://www.ceskatelevize.cz/ct24/rss/kultura");
                    controller.downloadFromUrl("media.xml", "http://www.ceskatelevize.cz/ct24/rss/media");
                    controller.downloadFromUrl("relax.xml", "http://www.ceskatelevize.cz/ct24/rss/relax");
                } else {
                    console.log("already updating");
                }
            }
        }
        onPopTransitionEnded: {
            page.destroy();
        }
    }
    attachedObjects: [
        ComponentDefinition {
            id: articlePage
            source: "article.qml"
        },
        ComponentDefinition {
            id: aboutPage
            source: "about.qml"
        },
        ComponentDefinition {
            id: settingsPage
            source: "settings.qml"
        },
        ComponentDefinition {
            id: web
            source: "web.qml"
        }
    ]
    onCreationCompleted: {
        controller.newFeedDataAvailable.connect(tabbedPane.onNewFeedDataAvailable);
        controller.refreshContent("hlavni-zpravy.xml");
        feedList.dataModel = controller.getFeedDataModel(feedFile);
        controller.refreshContent("domaci.xml");
        controller.refreshContent("regiony.xml");
        controller.refreshContent("svet.xml");
        controller.refreshContent("ekonomika.xml");
        controller.refreshContent("kultura.xml");
        controller.refreshContent("media.xml");
        controller.refreshContent("relax.xml");
    }
    function onNewFeedDataAvailable(id) {
        controller.refreshContent(id);
        if (feedFile == id) {
            feedList.dataModel = controller.getFeedDataModel(id);
        }
        updatedCount ++;
        console.log("new feed data available, updatedCount = " + updatedCount);
        if (updatedCount == 8) {
            updating = false;
            console.log("update finished");
            updateActivityContainer.visible = false;
            leadingvisualContainer.visible = true;
            activityIndicator.stop();
        }
    }

    Tab {
        title: "Hlavní zprávy"
        imageSource: "asset:///images/ct24_main.png"
        onTriggered: {
            titlebar.title = "Hlavní zprávy";
            feedFile = "hlavni-zpravy.xml";
            feedList.dataModel = controller.getFeedDataModel(feedFile);
        }
    }

    Tab {
        title: "Domácí"
        imageSource: "asset:///images/879.png"
        onTriggered: {
            titlebar.title = "Domácí";
            feedFile = "domaci.xml";
            feedList.dataModel = controller.getFeedDataModel(feedFile);
        }
    }

    Tab {
        title: "Regiony"
        imageSource: "asset:///images/880.png"
        onTriggered: {
            titlebar.title = "Regiony";
            feedFile = "regiony.xml";
            feedList.dataModel = controller.getFeedDataModel(feedFile);
        }
    }

    Tab {
        title: "Ekonomika"
        imageSource: "asset:///images/884.png"
        onTriggered: {
            titlebar.title = "Ekonomika";
            feedFile = "ekonomika.xml";
            feedList.dataModel = controller.getFeedDataModel(feedFile);
        }
    }

    Tab {
        title: "Svět"
        imageSource: "asset:///images/885.png"
        onTriggered: {
            titlebar.title = "Svět"
            feedFile = "svet.xml";
            feedList.dataModel = controller.getFeedDataModel(feedFile);
        }
    }

    Tab {
        title: "Kultura"
        imageSource: "asset:///images/889.png"
        onTriggered: {
            titlebar.title = "Kultura"
            feedFile = "kultura.xml";
            feedList.dataModel = controller.getFeedDataModel(feedFile);
        }
    }

    Tab {
        title: "Média"
        imageSource: "asset:///images/912.png"
        onTriggered: {
            titlebar.title = "Média"
            feedFile = "media.xml";
            feedList.dataModel = controller.getFeedDataModel(feedFile);
        }
    }

    Tab {
        title: "Relax"
        imageSource: "asset:///images/1229.png"
        onTriggered: {
            titlebar.title = "Relax";
            feedFile = "relax.xml";
            feedList.dataModel = controller.getFeedDataModel(feedFile);
        }
    }
    property string feedFile: "hlavni-zpravy.xml"
    property int updatedCount: 0
    property bool updating: false
    property bool shouldRefresh: false
}
