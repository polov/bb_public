import bb.cascades 1.4

Page {
    id: settingsPage
    titleBar: TitleBar {
        id: titlebar
        title: qsTr("Settings") + Retranslate.onLanguageChanged
    }
    content: ScrollView {
        content: Container {
            leftPadding: ui.du(2.0)
            rightPadding: ui.du(2.0)
            Container {
                topPadding: ui.du(2.5)
                DropDown {
                    id: visualStyleDropDown
                    title: "Téma"
                    options: [
                        Option {
                            value: "bright"
                            text: "Světlé"
                            selected: (controller.visualStyle == "bright")
                        },
                        Option {
                            value: "dark"
                            text: "Tmavé"
                            selected: (controller.visualStyle == "dark")
                        }
                    ]
                    onSelectedValueChanged: {
                        console.log("change visual style to " + selectedValue);
                        controller.visualStyle = selectedValue;
                        if (selectedValue == "bright") {
                            Application.themeSupport.setVisualStyle(VisualStyle.Bright);
                        } else if (selectedValue == "dark") {
                            Application.themeSupport.setVisualStyle(VisualStyle.Dark);
                        }
                    }
                }
            }
        }
    }
}