/*
 * FeedItem.cpp
 *
 *  Created on: 28. 9. 2015
 *      Author: martin
 */

#include <src/FeedItem.h>

#include <QDebug>
#include <QDir>
#include <QFile>

#include <bb/ImageData>

#include <bb/device/DisplayInfo>

#include "NetworkHelper.h"

FeedItem::FeedItem() :
        QObject(), guid(""), title(""), description(""), link(""), hasVideo("0"), pubDate(), enclosureImage(), enclosureImageLoaded(
                false), homeDir(QDir::homePath()), networkHelper(new NetworkHelper("", ""))
{
    //constructor
    //networkHelper = new NetworkHelper("", "");
    bool connectResult;
    Q_UNUSED(connectResult);
    connectResult = connect(networkHelper, SIGNAL(contentUpdated(QString, bool)), this,
            SLOT(onContentUpdated(QString, bool)));
    Q_ASSERT(connectResult);
}

const QString& FeedItem::getLink() const
{
    return link;
}

void FeedItem::setLink(const QString& link)
{
    this->link = link;
}

FeedItem::~FeedItem()
{
    /*
     if (networkHelper != 0) {

     bool connectResult;
     Q_UNUSED(connectResult);
     connectResult = disconnect(networkHelper, SIGNAL(contentUpdated(QString, bool)), this,
     SLOT(onContentUpdated(QString, bool)));
     Q_ASSERT(connectResult);
     networkHelper->deleteLater();
     }
     */
}

const QString& FeedItem::getDescription() const
{
    return description;
}

void FeedItem::setDescription(const QString& description)
{
    this->description = description;
}

const bb::cascades::Image& FeedItem::getEnclosureImage()
{
    return enclosureImage;
}

void FeedItem::setEnclosureImage(const bb::cascades::Image& enclosureImage)
{
    this->enclosureImage = enclosureImage;
    //emit enclosureImageChanged();
    //emit enclosureImageChanged(); not needed, depends on enclosureImageLoaded
}

bool FeedItem::isEnclosureImageLoaded()
{
    fetchEnclosureImage();
    return enclosureImageLoaded;
}

void FeedItem::setEnclosureImageLoaded(bool enclosureImageLoaded)
{
    this->enclosureImageLoaded = enclosureImageLoaded;
    //emit enclosureImageLoadedChanged();
}

const QString& FeedItem::getGuid() const
{
    return guid;
}

void FeedItem::setGuid(const QString& guid)
{
    this->guid = guid;
}

const QString& FeedItem::getTitle() const
{
    return title;
}

void FeedItem::setTitle(const QString& title)
{
    this->title = title;
}

const QString& FeedItem::getEnclosureImageUrl() const
{
    return enclosureImageUrl;
}

void FeedItem::setEnclosureImageUrl(const QString& enclosureImageUrl)
{
    this->enclosureImageUrl = enclosureImageUrl;
}

void FeedItem::fetchEnclosureImage()
{
    if (this->enclosureImageLoaded) {
        return;
    }
    QString path = getDownloadedImagePath(enclosureImageUrl);
    QString file = getDownloadedImageFilename(enclosureImageUrl);
    if (!QFile::exists(path)) {
        //qDebug() << "download image" << enclosureImageUrl; //file, enclosureImageUrl
        networkHelper->setId(file);
        networkHelper->setUrl(enclosureImageUrl);
        networkHelper->refreshData();
        return;
    } else {
        //qDebug() << "image already downloaded" << enclosureImageUrl;
        loadEnclosureImageFromPath(path);
        return;
    }
}

const QDateTime& FeedItem::getPubDate() const
{
    return pubDate;
}

void FeedItem::setPubDate(const QDateTime& pubDate)
{
    this->pubDate = pubDate;
}

void FeedItem::onContentUpdated(QString id, bool error)
{
    QString path = getDownloadedImagePath(enclosureImageUrl);
    loadEnclosureImageFromPath(path);
    return;
}

void FeedItem::loadEnclosureImageFromPath(const QString& path)
{
    QFile imageFile(path);
    if (imageFile.exists()) {
        //qDebug() << "image ready" << path;
        if (imageFile.open(QIODevice::ReadOnly)) {
            QImage qImage;
            if (qImage.loadFromData(imageFile.readAll())) {
                imageFile.close();
                bb::device::DisplayInfo display;
                qImage = qImage.scaledToWidth(display.pixelSize().width());
                QImage swappedImage = qImage.rgbSwapped();
                bb::ImageData imageData = bb::ImageData::fromPixels(swappedImage.bits(),
                        bb::PixelFormat::RGBX, swappedImage.width(), swappedImage.height(),
                        swappedImage.bytesPerLine());
                this->setEnclosureImage(bb::cascades::Image(imageData));
                this->setEnclosureImageLoaded(true);
                emit enclosureImageChanged();
                emit enclosureImageLoadedChanged();
            } else {
                qDebug() << "Image data null " << path;
            }
        } else {
            qDebug() << "Failed to open file" << path << "for reading";
        }
    }
}

QString FeedItem::getDownloadedImagePath(QString url)
{
    int lastSlash = url.lastIndexOf("/");
    int questionMark = url.indexOf("?");
    if (lastSlash != -1 && questionMark != -1) {
        return homeDir + "/" + url.mid((lastSlash + 1), (questionMark - lastSlash - 1));
    }
    return "";
}

QString FeedItem::getDownloadedImageFilename(QString url)
{
    int lastSlash = url.lastIndexOf("/");
    int questionMark = url.indexOf("?");
    if (lastSlash != -1 && questionMark != -1) {
        return url.mid((lastSlash + 1), (questionMark - lastSlash - 1));
    }
    return "";
}

const QString& FeedItem::getHasVideo() const
{
    return hasVideo;
}

void FeedItem::setHasVideo(const QString& hasVideo)
{
    this->hasVideo = hasVideo;
}

bb::cascades::Image FeedItem::getActiveFrameImage()
{
    bb::device::DisplayInfo display;
    int width = display.pixelSize().width();
    int height = display.pixelSize().height();
    int wantedHeight = 334; //Z10 screen
    int wantedWidth = 396; //Z10 screen
    /*
     720x1280 - 319x437
     720x720 - 310x211
     768x1280 - 334x396
     1440x1440L - 440x486
     1440x1440S - 440x195
     */
    if (width == 720) {
        if (height == 720) {
            wantedWidth = 310;
            wantedHeight = 211;
        } else if (height == 1280) {
            wantedWidth = 319;
            wantedHeight = 437;
        }
    } else if (width == 768) {
        wantedWidth = 334;
        wantedHeight = 396;
    } else if (width == 1440) {
        wantedWidth = 440;
        wantedHeight = 486;
    }
    QString path;
    if (enclosureImageLoaded) {
        path = getDownloadedImagePath(enclosureImageUrl);
    } else {
        path = QDir::currentPath() + "/app/native/assets/images/placeholder_big.png";
    }
    QFile imageFile(path);
    if (imageFile.exists()) {
        //qDebug() << "image ready" << path;
        if (imageFile.open(QIODevice::ReadOnly)) {
            QImage qImage;
            if (qImage.loadFromData(imageFile.readAll())) {
                imageFile.close();
                qImage = qImage.scaledToWidth(wantedWidth);
                if (qImage.height() < wantedHeight) {
                    qImage = qImage.scaledToHeight(wantedHeight);
                }
                int deltaWidth = qImage.width() - wantedWidth;
                int deltaHeight = qImage.height() - wantedHeight;
                qImage = qImage.copy(deltaWidth/2, deltaHeight/2, wantedWidth, wantedHeight);
                QImage swappedImage = qImage.rgbSwapped();
                bb::ImageData imageData = bb::ImageData::fromPixels(swappedImage.bits(),
                        bb::PixelFormat::RGBX, swappedImage.width(), swappedImage.height(),
                        swappedImage.bytesPerLine());
                return bb::cascades::Image(imageData);
            }
        }
    }
    qDebug() << "error loading file" << path;
    return bb::cascades::Image();
}
