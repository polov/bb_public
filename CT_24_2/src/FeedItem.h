/*
 * FeedItem.h
 *
 *  Created on: 28. 9. 2015
 *      Author: martin
 */

#ifndef FEEDITEM_H_
#define FEEDITEM_H_

#include <QObject>
#include <bb/cascades/Image>

#include "NetworkHelper.h"

class FeedItem : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString guid READ getGuid WRITE setGuid NOTIFY guidChanged)
    Q_PROPERTY(QString title READ getTitle WRITE setTitle NOTIFY titleChanged)
    Q_PROPERTY(QString description READ getDescription WRITE setDescription NOTIFY descriptionChanged)
    Q_PROPERTY(QString link READ getLink WRITE setLink NOTIFY linkChanged)
    Q_PROPERTY(QString hasVideo READ getHasVideo WRITE setHasVideo NOTIFY hasVideoChanged)
    Q_PROPERTY(QDateTime pubDate READ getPubDate WRITE setPubDate NOTIFY pubDateChanged)

    Q_PROPERTY(bb::cascades::Image enclosureImage READ getEnclosureImage WRITE setEnclosureImage NOTIFY enclosureImageChanged)
    Q_PROPERTY(bool enclosureImageLoaded READ isEnclosureImageLoaded WRITE setEnclosureImageLoaded NOTIFY enclosureImageLoadedChanged)
public:
    FeedItem();
    virtual ~FeedItem();

    const QString& getDescription() const;
    void setDescription(const QString& description);
    const bb::cascades::Image& getEnclosureImage();
    void setEnclosureImage(const bb::cascades::Image& enclosureImage);
    bool isEnclosureImageLoaded();// const;
    void setEnclosureImageLoaded(bool enclosureImageLoaded);
    const QString& getGuid() const;
    void setGuid(const QString& guid);
    const QString& getTitle() const;
    void setTitle(const QString& title);
    const QString& getLink() const;
    void setLink(const QString& link);
    const QString& getHasVideo() const;
    void setHasVideo(const QString& hasVideo);
    const QDateTime& getPubDate() const;
    void setPubDate(const QDateTime& pubDate);

    const QString& getEnclosureImageUrl() const;
    void setEnclosureImageUrl(const QString& enclosureImageUrl);

    QString getDownloadedImagePath(QString url);
    QString getDownloadedImageFilename(QString url);

    void fetchEnclosureImage();
    void loadEnclosureImageFromPath(const QString& path);
    bb::cascades::Image getActiveFrameImage();

signals:
    void guidChanged();
    void titleChanged();
    void descriptionChanged();
    void linkChanged();
    void hasVideoChanged();
    void pubDateChanged();
    void enclosureImageChanged();
    void enclosureImageLoadedChanged();

private slots:
    void onContentUpdated(QString, bool);

private:
    QString guid;
    QString title;
    QString description;
    QString link;
    QString hasVideo;
    QDateTime pubDate;
    bb::cascades::Image enclosureImage;
    bool enclosureImageLoaded;
    QString enclosureImageUrl;
    QString homeDir;
    NetworkHelper* networkHelper;

    Q_DISABLE_COPY (FeedItem)
};

Q_DECLARE_METATYPE(FeedItem*)

#endif /* FEEDITEM_H_ */
