/*
 * Controller.h
 *
 *  Created on: 27. 9. 2015
 *      Author: martin
 */

#ifndef CONTROLLER_H_
#define CONTROLLER_H_

#include <QObject>

#include <bb/cascades/ImageView>
#include <bb/cascades/GroupDataModel>
#include <bb/system/InvokeManager>

#include "NetworkHelper.h"

class Controller: public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString homeDir READ getHomeDir WRITE setHomeDir NOTIFY homeDirChanged)
    Q_PROPERTY(bb::cascades::GroupDataModel* feedDataModel READ getFeedDataModel NOTIFY feedDataModelChanged)
    Q_PROPERTY(int displayWidth READ getDisplayWidth NOTIFY displayWidthChanged)
    Q_PROPERTY(int displayHeight READ getDisplayHeight NOTIFY displayHeightChanged)
    Q_PROPERTY(QString visualStyle READ getVisualStyle WRITE setVisualStyle NOTIFY visualStyleChanged)

public:
    Controller();
    virtual ~Controller();

    Q_INVOKABLE
    QString extractCData(QString data);
    Q_INVOKABLE
    void refreshContent(QString filename);
    Q_INVOKABLE
    bb::cascades::GroupDataModel* getFeedDataModel(const QString& id);
    Q_INVOKABLE
    void downloadFromUrl(const QString& id, const QString& url);
    Q_INVOKABLE
    void preprocess(QList<QVariant> data);
    Q_INVOKABLE
    void invokeEmail(const QString& address, const QString& subject, const QString& body);

    const QString& getHomeDir() const;
    void setHomeDir(const QString& homeDir);
    bb::cascades::GroupDataModel* getFeedDataModel();
    int getDisplayWidth() const;
    int getDisplayHeight() const;
    const QString& getVisualStyle() const;
    void setVisualStyle(const QString& visualStyle);

    void houseKeep();

signals:
    void homeDirChanged();
    void feedDataModelChanged();
    void displayWidthChanged();
    void displayHeightChanged();
    void visualStyleChanged();
    void newFeedDataAvailable(const QString&);

private slots:
    void onContentUpdated(QString, bool);

private:
    QHash<QString, NetworkHelper*> requests;
    QString homeDir;
    bb::cascades::GroupDataModel* feedDataModel;
    QMap<QString, bb::cascades::GroupDataModel*> contentModels;
    int displayWidth;
    int displayHeight;
    QString visualStyle;
    bb::system::InvokeManager* invokeManager;
};

#endif /* CONTROLLER_H_ */
