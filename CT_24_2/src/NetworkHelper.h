/*
 * NetworkHelper.h
 *
 *  Created on: 11. 4. 2015
 *      Author: martin
 */

#ifndef NETWORKHELPER_H_
#define NETWORKHELPER_H_

#include <QObject>
#include <QtNetwork/QNetworkAccessManager>
#include <QtNetwork/QNetworkReply>

class NetworkHelper: public QObject
{
    Q_OBJECT
public:
    NetworkHelper(QString id, QString url);
    virtual ~NetworkHelper();
    void refreshData();
    void refreshData(QString timestamp);
    void sendPostData(QByteArray data);
    QByteArray gUncompress(const QByteArray &data);
    const QString& getId() const;
    void setId(const QString& id);
    const QString& getUrl() const;
    void setUrl(const QString& url);

signals:
    //emited after content update of data/* files
    void contentUpdated(QString id, bool error);

private slots:
    //called when http reply is pushed to application by device
    void requestFinished(QNetworkReply* reply);

private:
    // send http request to given url string
    void sendRequest(const QString url);
    // send http request to given url
    void sendRequest(const QUrl url);
    // send http post request to given url
    void sendRequest(const QString url, QByteArray data);
    // send http post request to given url
    void sendRequest(const QUrl url, QByteArray data);

    QNetworkAccessManager networkAccessManager;
    QString id;
    QString url;
    const int redirectMaxCount;
    int redirectCount;
    QUrl redirectLast;
};

#endif /* NETWORKHELPER_H_ */
