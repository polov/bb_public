import bb.cascades 1.4

Page {
    id: settingsPage
    titleBar: TitleBar {
        id: titlebar
        title: qsTr("Settings") + Retranslate.onLanguageChanged
    }
    content: ScrollView {
        content: Container {
            leftPadding: ui.du(2.0)
            rightPadding: ui.du(2.0)
            Container {
                topPadding: ui.du(2.5)
                DropDown {
                    id: visualStyleDropDown
                    title: qsTr("Visual style") + Retranslate.onLanguageChanged
                    options: [
                        Option {
                            value: "bright"
                            text: qsTr("Bright") + Retranslate.onLanguageChanged
                            selected: (server.visualStyle == "bright")
                        },
                        Option {
                            value: "dark"
                            text: qsTr("Dark") + Retranslate.onLanguageChanged
                            selected: (server.visualStyle == "dark")
                        }
                    ]
                    onSelectedValueChanged: {
                        console.log("change visual style to " + selectedValue);
                        server.visualStyle = selectedValue;
                        if (selectedValue == "bright") {
                            Application.themeSupport.setVisualStyle(VisualStyle.Bright);
                        } else if (selectedValue == "dark") {
                            Application.themeSupport.setVisualStyle(VisualStyle.Dark);
                        }
                    }
                }
            }
            Divider {
            }
            Container {
                id: httpsToggleContainer
                layout: StackLayout {
                    orientation: LayoutOrientation.LeftToRight
                }
                Container {
                    layoutProperties: StackLayoutProperties {
                        spaceQuota: 1
                    }
                    layout: DockLayout {

                    }
                    horizontalAlignment: HorizontalAlignment.Fill
                    Label {
                        text: qsTr("Secure connection") + Retranslate.onLanguageChanged
                        textStyle.fontSize: FontSize.Medium
                        verticalAlignment: VerticalAlignment.Center
                        multiline: true
                    }
                    ToggleButton {
                        id: httpsToggle
                        onCheckedChanged: {
                            if (needStop) {
                                runningToggle.checked = false;
                                server.https = httpsToggle.checked;
                            }
                        }
                        horizontalAlignment: HorizontalAlignment.Right
                        verticalAlignment: VerticalAlignment.Center
                    }
                }
            }
            Container {
                topMargin: ui.du(1.5)
                Label {
                    text: qsTr("If checked, secure communication will be used. Web browser will probably show some sort of warning because the application uses the self-signed certificate - you can trust certificate/safely continue to the page.") + Retranslate.onLanguageChanged
                    multiline: true
                    textStyle.fontSize: FontSize.Small
                }
            }
            Divider {
            }
            Container {
                id: authToggleContainer
                layout: StackLayout {
                    orientation: LayoutOrientation.LeftToRight
                }
                Container {
                    layoutProperties: StackLayoutProperties {
                        spaceQuota: 1
                    }
                    layout: DockLayout {

                    }
                    horizontalAlignment: HorizontalAlignment.Fill
                    Label {
                        text: qsTr("Authentication") + Retranslate.onLanguageChanged
                        textStyle.fontSize: FontSize.Medium
                        verticalAlignment: VerticalAlignment.Center
                        multiline: true
                    }
                    ToggleButton {
                        id: authToggle
                        onCheckedChanged: {
                            authFields.visible = checked;
                            server.auth = authToggle.checked;
                        }
                        horizontalAlignment: HorizontalAlignment.Right
                        verticalAlignment: VerticalAlignment.Center
                    }
                }
            }
            Container {
                id: authFields
                topMargin: ui.du(1.5)
                Container {
                    layout: GridLayout {
                        columnCount: 2
                    }
                    Container {
                        rightPadding: ui.du(1.0)
                        verticalAlignment: VerticalAlignment.Center
                        Label {
                            text: qsTr("Username") + Retranslate.onLanguageChanged
                        }
                    }
                    Container {
                        TextField {
                            id: usernameTextField
                            onTextChanged: {
                                server.username = usernameTextField.text;
                            }
                        }
                    }
                    Container {
                        topPadding: ui.du(1.0)
                        verticalAlignment: VerticalAlignment.Center
                        Label {
                            text: qsTr("Password") + Retranslate.onLanguageChanged
                        }
                    }
                    Container {
                        topPadding: ui.du(1.0)
                        TextField {
                            id: passwordTextField
                            onTextChanged: {
                                server.password = passwordTextField.text;
                            }
                        }
                    }
                }
            }
            Container {
                topMargin: ui.du(1.5)
                Label {
                    text: qsTr("If checked, password will be required. Do not use ':' sign.") + Retranslate.onLanguageChanged
                    multiline: true
                    textStyle.fontSize: FontSize.Small
                }
            }
            Divider {
            }
        }
    }
    onCreationCompleted: {
        console.log("https:" + server.https);
        httpsToggle.checked = server.https;
        authToggle.checked = server.auth;
        if (server.username != "") {
            usernameTextField.text = server.username;
        }
        if (server.password != "") {
            passwordTextField.text = server.password;
        }
        authFields.visible = server.auth;
        needStop = true;
    }
    property bool needStop: false
}