/*
 * Copyright (c) 2011-2015 BlackBerry Limited.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import bb.cascades 1.4
import bb.cascades.pickers 1.0
import bb.system 1.2

NavigationPane {
    id: navigationPane

    Menu.definition: MenuDefinition {
        actions: [
            ActionItem {
                title: qsTr("About") + Retranslate.onLanguageChanged
                imageSource: "asset:///img/ic_info.png"
                onTriggered: {
                    var aboutPageContent = aboutPage.createObject();
                    navigationPane.push(aboutPageContent);
                }
                attachedObjects: [
                    ComponentDefinition {
                        id: aboutPage
                        source: "about.qml"
                    }
                ]
            },
            ActionItem {
                title: qsTr("Send Feedback") + Retranslate.onLanguageChanged
                imageSource: "asset:///img/ic_feedback.png"
                onTriggered: {
                    server.invokeEmail("", qsTr("FileShare Feedback") + Retranslate.onLanguageChanged, "");
                }
            }
        ]
        settingsAction: SettingsActionItem {
            onTriggered: {
                var settingsPageContent = settingsPage.createObject();
                navigationPane.push(settingsPageContent);
            }
            attachedObjects: [
                ComponentDefinition {
                    id: settingsPage
                    source: "settings.qml"
                }
            ]
        }
    }
    Page {
        id: mainPage
        titleBar: TitleBar {
            title: qsTr("File Share") + Retranslate.onLanguageChanged
        }
        content: ScrollView {
            Container {
                leftPadding: ui.du(2.0)
                rightPadding: ui.du(2.0)
                Container {
                    topPadding: ui.du(2.0)
                    bottomPadding: ui.du(2.0)
                    visible: false
                    layout: StackLayout {
                        orientation: LayoutOrientation.LeftToRight
                    }
                    Label {
                        layoutProperties: StackLayoutProperties {
                            spaceQuota: 1
                        }
                        text: qsTr("Share") + Retranslate.onLanguageChanged
                        textStyle.fontSize: FontSize.Medium
                        verticalAlignment: VerticalAlignment.Center
                    }
                    ToggleButton {
                        id: runningToggle
                        enabled: server.rootPath != ""
                        onCheckedChanged: {
                            server.running = checked;
                        }
                    }
                }
                Divider {
                    topMargin: 0.0
                    bottomMargin: 0.0
                }
                Container {
                    topPadding: ui.du(2.0)
                    bottomPadding: ui.du(2.0)
                    Container {
                        bottomPadding: ui.du(1.0)
                        layout: StackLayout {
                            orientation: LayoutOrientation.LeftToRight
                        }
                        Container {
                            layoutProperties: StackLayoutProperties {
                                spaceQuota: 1
                            }
                            verticalAlignment: VerticalAlignment.Center
                            Label {
                                text: qsTr("Content") + Retranslate.onLanguageChanged
                                textStyle.fontSize: FontSize.Medium
                            }
                        }
                        Container {
                            preferredWidth: ui.du(20.0)
                            Button {
                                id: choose
                                text: qsTr("Change") + Retranslate.onLocaleOrLanguageChanged
                                onClicked: {
                                    directoryPicker.open();
                                }
                            }
                        }
                    }
                    Container {
                        Label {
                            text: (server.rootPath == "") ? (qsTr("No content selected") + Retranslate.onLanguageChanged) : server.rootPath
                        }
                    }
                }
                Divider {
                    topMargin: 0.0
                    bottomMargin: 0.0
                }
                Container {
                    topPadding: ui.du(2.0)
                    bottomPadding: ui.du(2.0)
                    visible: server.url != ""
                    Container {
                        bottomPadding: ui.du(1.0)
                        Label {
                            multiline: true
                            text: qsTr("You can now access shared content with the web browser. Remember you need to be on the same WiFi network. It will work only if the network supports connection between connected devices. Use following address:") + Retranslate.onLanguageChanged
                        }
                    }
                    Container {
                        layout: StackLayout {
                            orientation: LayoutOrientation.LeftToRight
                        }
                        Label {
                            layoutProperties: StackLayoutProperties {
                                spaceQuota: 1
                            }
                            verticalAlignment: VerticalAlignment.Center
                            text: server.url
                        }
                        ImageButton {
                            preferredHeight: ui.du(7.5)
                            preferredWidth: ui.du(7.5)
                            defaultImageSource: getShareImageSource()
                            onClicked: {
                                //server.shareUrl();
                                invokeQuery.mimeType = "text/plain"
                                invokeQuery.data = server.url;
                                invokeQuery.updateQuery();
                            }
                            attachedObjects: [
                                Invocation {
                                    id: invokeShare
                                    query: InvokeQuery {
                                        id: invokeQuery
                                        mimeType: "text/plain"
                                    }
                                    onArmed: {
                                        if (invokeQuery.data != "") {
                                            trigger("bb.action.SHARE");
                                        }
                                    }
                                }
                            ]
                            function getShareImageSource() {
                                switch (Application.themeSupport.theme.colorTheme.style) {
                                    case VisualStyle.Bright:
                                        return "asset:///img/ic_share_black.png";
                                    case VisualStyle.Dark:
                                        return "asset:///img/ic_share_white.png";
                                }
                            }
                        }
                    }
                }
                Divider {
                    visible: server.url != ""
                    topMargin: 0.0
                    bottomMargin: 0.0
                }
                Container {
                    topPadding: ui.du(2.0)
                    bottomPadding: ui.du(2.0)
                    visible: server.rootPath == ""
                    Container {
                        Label {
                            multiline: true
                            text: qsTr("Select the content that you want to share and turn sharing on") + Retranslate.onLanguageChanged
                            textStyle.color: Color.Red
                        }
                    }
                }
                Divider {
                    visible: server.rootPath == ""
                    topMargin: 0.0
                    bottomMargin: 0.0
                }
                attachedObjects: [
                    FilePicker {
                        id: directoryPicker
                        title: qsTr("Directory to share") + Retranslate.onLanguageChanged
                        mode: FilePickerMode.SaverMultiple
                        onFileSelected: {
                            //server.rootPath = "file://" + selectedFiles[0];
                            server.rootPath = selectedFiles[0];
                        }
                    }
                ]
            }
        }
        actions: [
            /*
            ActionItem {
                title: qsTr("Choose Content") + Retranslate.onLanguageChanged
                imageSource: "asset:///img/ic_add_folder.png"
                ActionBar.placement: ActionBarPlacement.OnBar
                onTriggered: {
                    directoryPicker.open();
                }
            },
            */
            ActionItem {
                title: runningToggle.checked ? qsTr("Sharing off") + Retranslate.onLanguageChanged : qsTr("Sharing on") + Retranslate.onLanguageChanged
                imageSource: "asset:///img/ic_on_off.png"
                ActionBar.placement: ActionBarPlacement.Signature
                onTriggered: {
                    if (server.rootPath != "") {
                        runningToggle.checked = ! runningToggle.checked;
                    } else {
                        chooseContentInfoToast.show();
                    }
                }
                attachedObjects: [
                    SystemToast {
                        id: chooseContentInfoToast
                        body: qsTr("You must select the content that you want to share.") + Retranslate.onLanguageChanged
                    }
                ]
            }
        ]
        function visualStyleChanged() {
            var selectedValue = server.visualStyle;
            console.log("setting visual style to " + selectedValue);
            if (selectedValue == "bright") {
                Application.themeSupport.setVisualStyle(VisualStyle.Bright);
            } else if (selectedValue == "dark") {
                Application.themeSupport.setVisualStyle(VisualStyle.Dark);
            }
        }
        onCreationCompleted: {
            server.visualStyleChanged.connect(mainPage.visualStyleChanged);
        }
    }
}