/*
 * ResponseHelper.cpp
 *
 *  Created on: 2. 9. 2015
 *      Author: martin
 */

#include "ResponseHelper.h"
#include <QDateTime>
#include <QDebug>
#include <QDir>
#include <QSettings>
#include <QTranslator>
#include <QUrl>

ResponseHelper::ResponseHelper(QObject* parent) :
        QObject(parent), rootPath(""), mtypes()
{
    //images
    mtypes.insert("bmp", "image/bmp");
    mtypes.insert("jpg", "image/jpeg");
    mtypes.insert("jpeg", "image/jpeg");
    mtypes.insert("png", "image/png");
    mtypes.insert("svg", "image/svg+xml");
    //video
    /*
     mtypes.insert("avi", "video/avi");
     mtypes.insert("m4v", "video/mp4");
     mtypes.insert("mov", "video/quicktime");
     mtypes.insert("mp4", "video/mp4");
     mtypes.insert("mpg", "video/mpeg");
     mtypes.insert("mpeg", "video/mpeg");
     mtypes.insert("ogv", "video/ogg");
     */
    //music
    mtypes.insert("flac", "audio/flac");
    mtypes.insert("m4a", "audio/mp4");
    mtypes.insert("mp3", "audio/mpeg");
    mtypes.insert("oga", "audio/ogg");
    mtypes.insert("oggg", "audio/ogg");
    mtypes.insert("wav", "audio/wav");
    //web
    mtypes.insert("css", "text/css");
    mtypes.insert("htm", "text/html");
    mtypes.insert("html", "text/html");
    mtypes.insert("js", "application/javascript");
    //documents
    mtypes.insert("doc", "application/msword");
    mtypes.insert("docx", "application/msword");
    mtypes.insert("dot", "application/msword");
    mtypes.insert("ppt", "application/mspowerpoint");
    mtypes.insert("pptx", "application/mspowerpoint");
    mtypes.insert("rtf", "text/richtext");
    mtypes.insert("txt", "text/plain");
    mtypes.insert("xls", "application/msexcel");
    mtypes.insert("xlsx", "application/msexcel");
    //compressing
    mtypes.insert("bz2", "application/application/x-bzip2");
    //others
    mtypes.insert("log", "text/plain");
    mtypes.insert("exe", "application/octet-stream");
    mtypes.insert("pkpass", "application/vnd.apple.pkpass");
}

ResponseHelper::~ResponseHelper()
{
}

void ResponseHelper::sendResponse(QTcpSocket* socket, QString rootPath)
{
    QByteArray request = socket->readAll();
    //qDebug() << "request" << request;
    this->rootPath = rootPath;
    if (request.length() > 3 && request.startsWith("GET")) {
        int space1pos = request.indexOf(' ');
        int space2pos = request.indexOf(' ', (space1pos + 1));
        if (space1pos < 0 || space2pos < space1pos) {
            qDebug() << "invalid request:\n" << request;
            socket->write(errorPageContent());
            return;
        }
        int questionPos = request.indexOf('?', (space1pos + 1));
        if(questionPos != -1 && questionPos < space2pos) {
            qDebug() << "strip parameters from the request";
            space2pos = questionPos;
        }
        QString requestPath = request.mid(space1pos + 1, space2pos - space1pos - 1);
        requestPath = QUrl::fromPercentEncoding(requestPath.toAscii());
        qDebug() << "request path" << requestPath;

        //
        QSettings settings("com.polov", "FileShare");
        bool auth = settings.value("auth").toBool();
        if (auth) {
            if (!request.contains("Authorization: ")) {
                qDebug() << "Missing authorization, sending authorization request";
                QString requestAuth = "HTTP/1.1 401 Unauthorized\nWWW-Authenticate: Basic realm=\""
                        + tr("FileShare for BlackBerry 10") + "\"\n\n";
                socket->write(requestAuth.toUtf8());
                socket->flush();
                qDebug() << socket->readAll();
                return;
            } else {
                qDebug() << "Got authorization, verify";
                int pos = request.indexOf("Authorization: ");
                if (pos > 0) {
                    int spacePos = request.indexOf("\n", (pos + 21));
                    qDebug() << "pos" << pos << "spacePos" << spacePos;
                    QString authValue = request.mid((pos + 21), (spacePos - pos - 21 - 1));
                    qDebug() << "auth string" << authValue;
                    QString username = settings.value("username", "").toString();
                    QString password = settings.value("password", "").toString();
                    QString toHash = username + ":" + password;
                    QString hash = toHash.toAscii().toBase64();
                    qDebug() << "computed" << hash << "header" << authValue;
                    if (hash != authValue) {
                        QString requestAuth =
                                "HTTP/1.1 401 Unauthorized\nWWW-Authenticate: Basic realm=\""
                                        + tr("FileShare for BlackBerry 10") + "\"\n\n";
                        socket->write(requestAuth.toUtf8());
                        socket->flush();
                        qDebug() << socket->readAll();
                    }
                } else {
                    QString requestAuth =
                            "HTTP/1.1 401 Unauthorized\nWWW-Authenticate: Basic realm=\""
                                    + tr("FileShare for BlackBerry 10") + "\"\n\n";
                    socket->write(requestAuth.toUtf8());
                    socket->flush();
                    qDebug() << socket->readAll();
                }
            }
        }
        //

        QString path;
        if (requestPath.startsWith("/embed_img/")) {
            path = "app/native/assets/" + requestPath.mid(7, request.size() - 7);
        } else if (requestPath == "/favicon.ico") {
            path = "app/native/assets/img/favicon.ico";
        } else if (requestPath == "/css/fileshare.css") {
            path = "app/native/assets/css/fileshare.css";
        } else {
            path = rootPath + requestPath;
        }
        qDebug() << "file system path" << path;

        if (!QFile::exists(path)) {
            qDebug() << "requested content does not exists";
            socket->write(errorPageContent());
            return;
        }

        QFileInfo fileInfo(path);
        if (fileInfo.isFile()) {
            QFile file(path);
            qint64 fileSize = file.size();
            QString fileSizeString = QString::number(fileSize);
            qDebug() << "file requested" << path << fileSizeString;

            QString suffix = fileInfo.suffix();
            suffix = suffix.toLower();
            QString mimeTypeString;
            if (mtypes.contains(suffix)) {
                mimeTypeString = mtypes.value(suffix);
            } else {
                mimeTypeString = "application/x-binary";
            }
            //file content
            QString content = "HTTP/1.1 200 OK\r\nServer: FileShareForBlackBerry10\r\nContent-Type: "
                    + mimeTypeString + "\r\nContent-Length: " + fileSizeString + "\r\nLast-Modified: "
                    + fileInfo.lastModified().toUTC().toString("ddd, dd MMM yyyy hh:mm:ss")
                    + " GMT\r\n\r\n";
            socket->write(content.toAscii());
            //qDebug() << "header written";
            //qDebug() << content;
            if (!file.open(QIODevice::ReadOnly)) {
                qDebug() << "unable to open file in read-only mode" << path;
                socket->write(errorPageContent());
                return;
            }
            char fileContent[128000];
            qint64 bytes = 0;
            qint64 bytesRead = 0;
            do {
                bytesRead = file.read(fileContent, 128000);
                //qDebug() << "bytes read" << QString::number(bytesRead);
                if (bytesRead < 128000) {
                    bytes += socket->write(fileContent, bytesRead); //TODO stci jen toto, druha vetev ifu je zbytecna
                    //bytes += socket->write(fileContent, 512000);
                    //qDebug() << "bytes written" << QString::number(bytes);
                } else {
                    bytes += socket->write(fileContent, 128000);
                    //qDebug() << "bytes written" << QString::number(bytes);
                }
            } while (bytesRead == 128000);
            file.close();
            qDebug() << "file content written" << path << bytes;
            return;
        } else if (fileInfo.isDir()) {
            socket->write(dirPageContent(path, requestPath));
            socket->flush();
            socket->close();
            return;
        }
    } else {
        qDebug() << "invalid request:\n" << request;
    }
    socket->write(errorPageContent());
    return;
}

QByteArray ResponseHelper::dirPageContent(const QString& path, const QString& requestPath)
{
    QString content = ""; //<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">";
    content += "<html><head>";
    content += "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />";
    content += "<title>" + tr("BlackBerry 10 Smartphone") + "</title>";
    content += "<meta name=\"author\" content=\"Martin Polovinčák\" />";
    content += "<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">";
    content += "<link rel=\"apple-touch-icon\" sizes=\"57x57\" href=\"/embed_img/apple-touch-icon-57x57.png\">";
    content += "<link rel=\"apple-touch-icon\" sizes=\"114x114\" href=\"/embed_img/apple-touch-icon-114x114.png\">";
    content += "<link rel=\"apple-touch-icon\" sizes=\"72x72\" href=\"/embed_img/apple-touch-icon-72x72.png\">";
    content += "<link rel=\"apple-touch-icon\" sizes=\"144x144\" href=\"/embed_img/apple-touch-icon-144x144.png\">";
    content += "<link rel=\"apple-touch-icon\" sizes=\"60x60\" href=\"/embed_img/apple-touch-icon-60x60.png\">";
    content += "<link rel=\"apple-touch-icon\" sizes=\"120x120\" href=\"/embed_img/apple-touch-icon-120x120.png\">";
    content += "<link rel=\"apple-touch-icon\" sizes=\"76x76\" href=\"/embed_img/apple-touch-icon-76x76.png\">";
    content += "<link rel=\"apple-touch-icon\" sizes=\"152x152\" href=\"/embed_img/apple-touch-icon-152x152.png\">";
    content +=
            "<style>* {padding: 0;margin: 0;} body {font-family: Helvetica Neue, Verdana, Arial, sans-serif;} .header {padding: 10pt 5pt 10pt 10pt;} .headerBold {line-height:normal;font-weight: bold;font-size:14pt;} .headerNormal {line-height:normal;font-weight: normal;font-size:14pt;color: #4684FF;border-left: solid 1px black;margin-left: 5pt;} .path {border-top: solid 1px #E9E9E9;border-bottom: solid 1px #E9E9E9;padding-top: 2pt;padding-bottom: 2pt;width: 100%;} .pathText {font-size: 12pt;padding-left: 10pt;color: #494949;} .pathImage {height: 15pt;vertical-align: bottom;} .content {padding: 10pt 10pt 10pt 10pt;} .content th {text-align: left;padding-bottom: 5pt;padding-left: 2pt;} .content td {padding-bottom: 3pt;padding-left: 2pt;padding-right: 15pt;} .content a {color: #4684FF;text-decoration: none;} .filesize { text-align: right; }</style>";
    content += "</head><body>";
    content +=
            "<div class='header'><span class='headerBold'>FileShare</span><span class='headerNormal'> " + tr("Device content") + "</span></div>";
    content +=
            "<div class='path'><span class='pathText'><a href='/'><img class='pathImage' src='/embed_img/ic_home.png' alt='Home' /></a>"
                    + requestPath + "</span></div>";
    content += "<table class='content'>";
    content += "<tr><th>" + tr("Name") + "</th><th>" + tr("Size") + "</th><th>" + tr("Type")
            + "</th></tr>";
    content += getDirContentLinks(path);
    content += "</table>";
    content += "</div>";
    content += "</body></html>";
    int length = content.toUtf8().length();
    content =
            "HTTP/1.1 200 OK\r\nServer: FileShareForBlackBerry10\r\nContent-Type: text/html; charset=UTF-8\r\nContent-Length: "
                    + QString::number(length) + "\r\nConnection: close\r\n\r\n" + content;
    return content.toUtf8();
}

QString ResponseHelper::getDirContentLinks(const QString& path)
{
    int rootPathLen = rootPath.length();

    QString item = "";
    QDir dir(path);
    QFileInfoList dirContent = dir.entryInfoList(QDir::Dirs | QDir::Files | QDir::NoDot);
    QString tmp;
    qDebug() << path << dirContent.size();
    for (int i = 0; i < dirContent.size(); ++i) {
        QFileInfo fileInfo = dirContent.at(i);
        qDebug() << fileInfo.absoluteFilePath();
        tmp = fileInfo.absoluteFilePath();
        if (tmp.length() > rootPathLen) {
            tmp = tmp.right(tmp.length() - rootPathLen);
            if (fileInfo.isDir()) {
                item += "<tr><td><a href='" + tmp + "'>" + fileInfo.fileName()
                        + "</a></td><td></td><td class='filetype'>" + tr("Folder") + "</td></tr>";
            } else if (fileInfo.isFile()) {
                item += "<tr><td><a href='" + tmp + "'>" + fileInfo.fileName()
                        + "</a></td><td class='filesize'>" + QString::number(fileInfo.size())
                        + +"</td><td class='filetype'>" + tr("File") + "</td></tr>";
            }
        } else if (tmp.length() == rootPathLen) {
            item += "<tr><td><a href='/'>" + fileInfo.fileName()
                    + "</a></td><td></td><td class='filetype'>" + tr("Folder") + "</td></tr>";
        }
    }
    return item;
}

QByteArray ResponseHelper::errorPageContent()
{
    QString content = ""; //<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">";
    content += "<html><head>";
    content += "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />";
    content += "<title>" + tr("BlackBerry 10 Smartphone") + "</title>";
    content += "<meta name=\"author\" content=\"Martin Polovinčák\" />";
    content += "<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">";
    content += "<link rel=\"apple-touch-icon\" sizes=\"57x57\" href=\"/embed_img/apple-touch-icon-57x57.png\">";
    content += "<link rel=\"apple-touch-icon\" sizes=\"114x114\" href=\"/embed_img/apple-touch-icon-114x114.png\">";
    content += "<link rel=\"apple-touch-icon\" sizes=\"72x72\" href=\"/embed_img/apple-touch-icon-72x72.png\">";
    content += "<link rel=\"apple-touch-icon\" sizes=\"144x144\" href=\"/embed_img/apple-touch-icon-144x144.png\">";
    content += "<link rel=\"apple-touch-icon\" sizes=\"60x60\" href=\"/embed_img/apple-touch-icon-60x60.png\">";
    content += "<link rel=\"apple-touch-icon\" sizes=\"120x120\" href=\"/embed_img/apple-touch-icon-120x120.png\">";
    content += "<link rel=\"apple-touch-icon\" sizes=\"76x76\" href=\"/embed_img/apple-touch-icon-76x76.png\">";
    content += "<link rel=\"apple-touch-icon\" sizes=\"152x152\" href=\"/embed_img/apple-touch-icon-152x152.png\">";
    content +=
            "<style>* {padding: 0;margin: 0;} body {font-family: Helvetica Neue, Verdana, Arial, sans-serif;} .header {padding: 10pt 5pt 10pt 10pt;} .headerBold {line-height:normal;font-weight: bold;font-size:14pt;} .headerNormal {line-height:normal;font-weight: normal;font-size:14pt;color: #4684FF;border-left: solid 1px black;margin-left: 5pt;} .path {border-top: solid 1px #E9E9E9;border-bottom: solid 1px #E9E9E9;padding-top: 2pt;padding-bottom: 2pt;width: 100%;} .pathText {font-size: 12pt;padding-left: 10pt;color: #494949;} .pathImage {height: 15pt;vertical-align: bottom;} .content {padding: 10pt 10pt 10pt 10pt;} .content a {color: #4684FF;text-decoration: none;}</style>";
    content += "</head><body>";
    content +=
            "<div class='header'><span class='headerBold'>FileShare</span><span class='headerNormal'> " + tr("Device content") + "</span></div>";
    content += "<div class='content'>";
    content += tr("Ups! Something went wrong. Unable to provide the content.");
    content += "</div>";
    content += "</body></html>";

    int length = content.toUtf8().length();
    content =
            "HTTP/1.1 500 Internal Server Error\r\nServer: FileShareForBlackBerry10\r\nContent-Type: text/html; charset=UTF-8\r\nContent-Length: "
                    + QString::number(length) + "\r\nConnection: close\r\n\r\n" + content;
    return content.toUtf8();
}
