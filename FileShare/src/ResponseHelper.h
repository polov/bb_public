/*
 * ResponseHelper.h
 *
 *  Created on: 2. 9. 2015
 *      Author: martin
 */

#ifndef RESPONSEHELPER_H_
#define RESPONSEHELPER_H_

#include <QObject>
#include <QtNetwork/QSslSocket>

class ResponseHelper: public QObject
{
    Q_OBJECT
public:
    ResponseHelper(QObject* parent = 0);
    virtual ~ResponseHelper();

    void setMimeTypes(QMap<QString, QString> mtypes);
    void sendResponse(QTcpSocket* socket, QString rootPath);
    QByteArray errorPageContent();
    QByteArray dirPageContent(const QString& path, const QString& requestPath);
    QString getDirContentLinks(const QString& path);

private:
    QString rootPath;
    QMap<QString, QString> mtypes;
};

#endif /* RESPONSEHELPER_H_ */
