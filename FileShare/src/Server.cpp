/*
 * Server.cpp
 *
 *  Created on: 26. 8. 2015
 *      Author: martin
 */

#include "Server.h"

#include <bps/netstatus.h>

#include <QMap>
#include <QRegExp>
#include <QSettings>
#include <QtNetwork/QSslSocket>

#include <bb/cascades/Application>
#include <bb/cascades/Theme>
#include <bb/cascades/ThemeSupport>
#include <bb/cascades/VisualStyle>
#include <bb/system/InvokeManager>
#include <bb/system/InvokeRequest>

#include "SslServer.h"

Server::Server(QObject *parent) :
        QObject(parent), serverPort(8080), rootPath(""), actualPath(""), running(false), responseHelper(), https(
                false)
{
    QSettings settings("com.polov", "FileShare");
    https = settings.value("https", false).toBool();
    emit httpsChanged();

    //ssl
    QByteArray key;
    QFile fileKey("app/native/assets/crt/device.key");
    //QFile fileKey("app/native/assets/crt/server.key_self");
    //QFile fileKey("app/native/assets/crt/server.key_comodo.org");
    if (fileKey.open(QIODevice::ReadOnly)) {
        key = fileKey.readAll();
        fileKey.close();
    } else {
        qDebug() << fileKey.errorString() << "/app/native/assets/crt/server.key";
    }
    sslKey = QSslKey(key, QSsl::Rsa);

    QByteArray cert;
    QFile fileCert("app/native/assets/crt/device.crt");
    //QFile fileCert("app/native/assets/crt/server.crt_self");
    //QFile fileCert("app/native/assets/crt/www.polovincak.eu.crt");
    if (fileCert.open(QIODevice::ReadOnly)) {
        cert = fileCert.readAll();
        fileCert.close();
    } else {
        qDebug() << fileCert.errorString() << "app/native/assets/crt/server.crt";
    }
    sslCert = QSslCertificate(cert);

    //auth
    auth = settings.value("auth", false).toBool();
    emit authChanged();
    username = settings.value("username", "").toString();
    emit usernameChanged();
    password = settings.value("password", "").toString();
    emit passwordChanged();

    //theme
    visualStyle = settings.value("visualStyle", "bright").toString();
    if (visualStyle == "bright") {
        bb::cascades::Application::instance()->themeSupport()->setVisualStyle(
                bb::cascades::VisualStyle::Bright);
    } else if (visualStyle == "dark") {
        bb::cascades::Application::instance()->themeSupport()->setVisualStyle(
                bb::cascades::VisualStyle::Dark);
    }

    server = new QTcpServer(this);
    sslServer = new SslServer(this);

}

Server::~Server()
{
    server->close();
    server->deleteLater();
    sslServer->close();
    sslServer->deleteLater();
    qDebug() << "server closed";
}

void Server::listen()
{
    QRegExp rxHeader("^(?:[0-9]{1,3}\.){3}[0-9]{1,3}$");
    int pos;
    QHostAddress serverAddress;

    netstatus_interface_details_t* details;
    if (BPS_SUCCESS == netstatus_get_interface_details(NULL, &details)) {
        int nipcnt = netstatus_interface_get_num_ip_addresses(details);
        for (int i = 0; i < nipcnt; i++) {
            const char* nipaddr = netstatus_interface_get_ip_address(details, i);
            const char* nipmask = netstatus_interface_get_ip_address_netmask(details, i);
            const char* nipgway = netstatus_interface_get_ip_gateway(details, i);
            qDebug() << nipaddr << nipmask << nipgway;

            pos = rxHeader.indexIn(nipaddr);
            if (pos > -1) {
                serverAddress.setAddress(nipaddr);
            }
        }
        netstatus_free_interface_details(&details);
    }

    if (https) {
        sslServer->listen(QHostAddress::Any, serverPort);
    } else {
        server->listen(QHostAddress::Any, serverPort);
    }
    if (https) {
        setUrl("https://" + serverAddress.toString() + ":" + QString::number(serverPort));
    } else {
        setUrl("http://" + serverAddress.toString() + ":" + QString::number(serverPort));
    }
    qDebug() << "server listening URL" << url;

    bool ok;
    if (https) {
        ok = connect(sslServer, SIGNAL(newIncomingSslConnection(int)), this,
                SLOT(onIncomingConnection(int)));
    } else {
        ok = connect(server, SIGNAL(newConnection()), this, SLOT(onNewConnection()));
    }
    Q_ASSERT(ok);
    Q_UNUSED(ok);

}

void Server::close()
{
    qDebug() << "close called" << https;
    setUrl("");
    if (https) {
        disconnect(sslServer, SIGNAL(newIncomingSslConnection(int)), this,
                SLOT(onIncomingConnection(int)));
    } else {
        disconnect(server, SIGNAL(newConnection()), this, SLOT(onNewConnectionTest()));
    }
    server->close();
    sslServer->close();
    qDebug() << "server closed from close()";
}

void Server::onNewConnection()
{
    QTcpSocket* socket = server->nextPendingConnection();
    if (socket == NULL) {
        qDebug() << "socket null !!";
        return;
    }
    qDebug() << "server established new connection" << socket->peerAddress().toString();

    bool ok = connect(socket, SIGNAL(disconnected()), this, SLOT(onDisconnect()));
    Q_ASSERT(ok);
    Q_UNUSED(ok);
    socket->waitForConnected(2000);
    socket->waitForReadyRead(2000);
    responseHelper.sendResponse(socket, rootPath);
    socket->flush();
    socket->waitForBytesWritten(2000);
    socket->close();
}

void Server::onIncomingConnection(int socketDescriptor)
{
    QSslSocket* socket = new QSslSocket(this);
    bool ok = connect(socket, SIGNAL(disconnected()), this, SLOT(onDisconnect()));
    Q_ASSERT(ok);
    Q_UNUSED(ok);
    socket->setSocketDescriptor(socketDescriptor);

    qDebug() << "server established new connection" << socket->peerAddress().toString();

    socket->setPrivateKey(sslKey);
    socket->setLocalCertificate(sslCert);
    socket->setPeerVerifyMode(QSslSocket::VerifyNone);
    socket->startServerEncryption();
    socket->waitForEncrypted(2000);
    socket->waitForReadyRead(2000);
    //responseHelper.sendResponse(socket, rootPath);
    socket->flush();
    socket->waitForBytesWritten(2000);
    socket->close();
}

bool Server::isRunning() const
{
    return running;
}

void Server::setRunning(bool running)
{
    this->running = running;
    qDebug() << "running changed" << this->running;
    if (running) {
        qDebug() << "start listening requested";
        listen();
    } else {
        qDebug() << "stop listening requested";
        close();
    }
    emit runningChanged();
}

const QString& Server::getUrl() const
{
    return url;
}

void Server::setUrl(const QString& url)
{
    this->url = url;
    emit urlChanged();
}

const QString& Server::getActualPath() const
{
    return actualPath;
}

void Server::setActualPath(const QString& actualPath)
{
    this->actualPath = actualPath;
    emit actualPathChanged();
}

const QString& Server::getRootPath() const
{
    return rootPath;
}

void Server::setRootPath(const QString& rootPath)
{
    this->rootPath = rootPath;
    emit rootPathChanged();
}

bool Server::isAuth() const
{
    return auth;
}

void Server::setAuth(bool auth)
{
    this->auth = auth;
    QSettings settings("com.polov", "FileShare");
    settings.setValue("auth", QVariant::fromValue(auth));
    qDebug() << "auth changed" << this->auth;
    emit authChanged();
}

void Server::onDisconnect()
{
    qDebug() << "onSocketFinished";
    QTcpSocket* socket = dynamic_cast<QTcpSocket*>(sender());
    qDebug() << "disconnecting connection to" << socket->peerAddress().toString();
    bool ok = disconnect(socket, SIGNAL(disconnected()), this, SLOT(onDisconnect()));
    Q_ASSERT(ok);
    Q_UNUSED(ok);
    if (socket != NULL) {
        socket->deleteLater();
        qDebug() << "socket destroy requested";
    }
}

bool Server::isHttps() const
{
    return https;
}

void Server::setHttps(bool https)
{
    this->https = https;
    QSettings settings("com.polov", "FileShare");
    settings.setValue("https", QVariant::fromValue(https));
    qDebug() << "https changed" << this->https;
    emit httpsChanged();
}

void Server::invokeEmail(const QString& address, const QString& subject, const QString& body)
{
    bb::system::InvokeManager invokeManager;
    QString validAddress = (address == "") ? "support@polovincak.eu" : address;
    QString validSubject = subject;
    QString validBody = body;
    qDebug() << "email invocation, receiver:" << validAddress << validSubject << validBody;
    bb::system::InvokeRequest request;
    request.setTarget("sys.pim.uib.email.hybridcomposer");
    request.setAction("bb.action.SENDEMAIL");
    request.setUri(
            QUrl(
                    "mailto:" + validAddress + "?subject=" + validSubject.replace(" ", "%20")
                            + "&body=" + validBody.replace(" ", "%20")));
    invokeManager.invoke(request);
}

const QString& Server::getPassword() const
{
    return password;
}

void Server::setPassword(const QString& password)
{
    this->password = password;
    QSettings settings("com.polov", "FileShare");
    settings.setValue("password", QVariant::fromValue(password));
}

const QString& Server::getUsername() const
{
    return username;
}

void Server::setUsername(const QString& username)
{
    this->username = username;
    QSettings settings("com.polov", "FileShare");
    settings.setValue("username", QVariant::fromValue(username));
}

const QString& Server::getVisualStyle() const
{
    return this->visualStyle;
}

void Server::setVisualStyle(const QString& visualStyle)
{
    this->visualStyle = visualStyle;
    QSettings settings("com.polov", "FileShare");
    settings.setValue("visualStyle", QVariant::fromValue(visualStyle));
    qDebug() << "visualStyle changed to " << visualStyle;
}
