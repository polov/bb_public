/*
 * SslServer.h
 *
 *  Created on: 1. 9. 2015
 *      Author: martin
 */

#ifndef SSLSERVER_H_
#define SSLSERVER_H_

#include <QObject>
#include <QtNetwork/QTcpServer>

class SslServer: public QTcpServer
{
    Q_OBJECT
public:
    SslServer(QObject *parent = 0);
    virtual ~SslServer();

signals:
    void newIncomingSslConnection(int);

protected:
    void incomingConnection(int socketDescriptor);
};

#endif /* SSLSERVER_H_ */
