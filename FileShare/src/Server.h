/*
 * Server.h
 *
 *  Created on: 26. 8. 2015
 *      Author: martin
 */

#ifndef SERVER_H_
#define SERVER_H_

#include <QObject>
#include <QtNetwork/QTcpServer>
#include <QtNetwork/QTcpSocket>
#include <QtNetwork/QSslKey>
#include <QtNetwork/QSslCertificate>
#include "SslServer.h"
#include "ResponseHelper.h"

class Server: public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString actualPath READ getActualPath WRITE setActualPath NOTIFY actualPathChanged)
    Q_PROPERTY(QString rootPath READ getRootPath WRITE setRootPath NOTIFY rootPathChanged)
    Q_PROPERTY(QString url READ getUrl WRITE setUrl NOTIFY urlChanged)
    Q_PROPERTY(bool running READ isRunning WRITE setRunning NOTIFY runningChanged)
    Q_PROPERTY(bool https READ isHttps WRITE setHttps NOTIFY httpsChanged)
    Q_PROPERTY(bool auth READ isAuth WRITE setAuth NOTIFY authChanged)
    Q_PROPERTY(QString username READ getUsername WRITE setUsername NOTIFY usernameChanged)
    Q_PROPERTY(QString password READ getPassword WRITE setPassword NOTIFY passwordChanged)
    Q_PROPERTY(QString visualStyle READ getVisualStyle WRITE setVisualStyle NOTIFY visualStyleChanged)

public:
    Server(QObject* parent = 0);
    virtual ~Server();

    const QString& getActualPath() const;
    void setActualPath(const QString& actualPath);
    const QString& getRootPath() const;
    void setRootPath(const QString& rootPath);
    const QString& getUrl() const;
    void setUrl(const QString& url);

    void listen();
    void close();
    bool isRunning() const;
    void setRunning(bool running);
    bool isHttps() const;
    void setHttps(bool https);
    bool isAuth() const;
    void setAuth(bool auth);
    const QString& getPassword() const;
    void setPassword(const QString& password);
    const QString& getUsername() const;
    void setUsername(const QString& username);
    const QString& getVisualStyle() const;
    void setVisualStyle(const QString& visualStyle);

    Q_INVOKABLE
    void invokeEmail(const QString& address, const QString& subject, const QString& body);

signals:
    void actualPathChanged();
    void rootPathChanged();
    void urlChanged();
    void runningChanged();
    void httpsChanged();
    void authChanged();
    void usernameChanged();
    void passwordChanged();
    void visualStyleChanged();

private slots:
    void onNewConnection();
    void onIncomingConnection(int socketDescriptor);
    void onDisconnect();

private:
    int serverPort;
    QTcpServer *server;
    SslServer *sslServer;
    QString url;
    QString rootPath;
    QString actualPath;
    bool running;
    ResponseHelper responseHelper;
    bool https;
    bool auth;
    QString username;
    QString password;
    QSslKey sslKey;
    QSslCertificate sslCert;
    QString visualStyle;
};

#endif /* SERVER_H_ */
