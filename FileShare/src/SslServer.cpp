/*
 * SslServer.cpp
 *
 *  Created on: 1. 9. 2015
 *      Author: martin
 */

#include <src/SslServer.h>

SslServer::SslServer(QObject *parent) :
        QTcpServer(parent)
{
}

SslServer::~SslServer()
{
}

void SslServer::incomingConnection(int socketDescriptor)
{
    qDebug() << "emiting new incoming ssl connection signal";
    emit newIncomingSslConnection(socketDescriptor);
}
