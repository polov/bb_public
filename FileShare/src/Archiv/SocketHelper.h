/*
 * SocketHelper.h
 *
 *  Created on: 26. 8. 2015
 *      Author: martin
 */

#ifndef SOCKETHELPER_H_
#define SOCKETHELPER_H_

#include <QObject>
#include <QMap>
#include <QtNetwork/QTcpSocket>

class SocketHelper: public QObject
{
    Q_OBJECT
public:
    SocketHelper(QMap<QString, QString> mtypes, QObject* parent = 0, QTcpSocket* soc = 0, QString path = "");
    virtual ~SocketHelper();

    void start();

signals:
    void sockethelperFinished(SocketHelper*);

private slots:
    void onDisconnect();
    void onReadyRead();
    void onReadyWrite();

private:
    QTcpSocket* socket;
    QString rootPath;
    QMap<QString, QString> mimeTypes;

    QString getDirContentLinks(QString path);
};

#endif /* SOCKETHELPER_H_ */
