/*
 * SocketHelper.cpp
 *
 *  Created on: 26. 8. 2015
 *      Author: martin
 */

#include <src/SocketHelper.h>
#include <QHostAddress>
#include <QDir>
#include <QUrl>

SocketHelper::SocketHelper(QMap<QString, QString> mtypes, QObject* parent, QTcpSocket* soc,
        QString path) :
        QObject(parent), socket(soc), rootPath(path), mimeTypes(mtypes)
{
    qDebug() << "created socket helper" << socket->peerAddress().toString() << rootPath;
}

void SocketHelper::start()
{
    bool ok = connect(socket, SIGNAL(disconnected()), this, SLOT(onDisconnect()));
    Q_ASSERT(ok);
    ok = connect(socket, SIGNAL(readyRead()), this, SLOT(onReadyRead()));
    Q_ASSERT(ok);
    Q_UNUSED(ok);
}

void SocketHelper::onDisconnect()
{
    qDebug() << "disconnect requested" << socket->peerAddress().toString();
    socket->disconnectFromHost();
    emit sockethelperFinished(this);
}

SocketHelper::~SocketHelper()
{
    if (socket) {
        socket->close();
        socket->deleteLater();
        qDebug() << "socket will be destroyed";
    }
    qDebug() << "socket helper will be destroyed";
}

void SocketHelper::onReadyRead()
{
    QByteArray ba = socket->readAll();
    qDebug() << ba;
    qDebug() << "read finished";

    if (ba.startsWith("GET")) {
        //support only GET requests now
        qDebug() << "GET request" << socket->peerAddress().toString();
        int space1pos = ba.indexOf(' ');
        int space2pos = ba.indexOf(' ', (space1pos + 1));
        QString request = ba.mid(space1pos + 1, space2pos - space1pos - 1);

        request = QUrl::fromPercentEncoding(request.toAscii());

        qDebug() << "request" << request;

        QString path;
        if (request.startsWith("/embed_img/ic")) {
            path = "app/native/assets/" + request.mid(7, request.size() - 7);
            qDebug() << "path updated" << path;
        } else {
            path = rootPath + request;
            qDebug() << "path request" << path;
        }

        if (!QFile::exists(path)) {
            //unsupported request
            QString content =
                    "<html><head><title>BlackBerry 10 Smartphone</title></head><body><p>Something went wrong. Unable to provide the content.</p></body></html>";
            int length = content.length();
            content =
                    "HTTP/1.1 500 Internal Server Error\nServer: FileShareForBlackBerry10\nContent-Type: text/html; charset=\nContent-Length: "
                            + QString::number(length) + "\nConnection: close\n\n" + content;
            socket->write(content.toAscii());
            socket->flush();
        }

        QFileInfo fileInfo(path);

        if (fileInfo.isFile()) {

            QFile file(path);
            qint64 fileSize = file.size();
            QString fileSizeString = QString::number(fileSize);
            qDebug() << "file requested" << path << fileSizeString;

            QString suffix = fileInfo.suffix();
            suffix = suffix.toLower();
            QString mimeTypeString;
            if (mimeTypes.contains(suffix)) {
                mimeTypeString = mimeTypes.value(suffix);
            } else {
                mimeTypeString = "application/x-binary";
            }

            //file content
            QString content = "HTTP/1.1 200 OK\nServer: FileShareForBlackBerry10\nContent-Type: "
                    + mimeTypeString + "\nContent-Length: " + fileSizeString
                    + "\nConnection: close\n\n";
            socket->write(content.toAscii());
            qDebug() << "header written";

            file.open(QIODevice::ReadOnly);
            char fileContent[512000];
            qint64 bytes = 0;
            while (file.read(fileContent, 512000) > 0) {
                bytes += socket->write(fileContent, 512000);
                qDebug() << "bytes written" << QString::number(bytes);
            }
            file.close();
            socket->flush();

        } else if (fileInfo.isDir()) {
            //dir list
            QString content = "<html><head><title>BlackBerry 10 Smartphone</title></head><body>";
            content += "<div><a href='/'>Home</a></div>";
            content += getDirContentLinks(path);
            content += "</body></html>";
            int length = content.toUtf8().length();
            content =
                    "HTTP/1.1 200 OK\nServer: FileShareForBlackBerry10\nContent-Type: text/html; charset=UTF-8\nContent-Length: "
                            + QString::number(length) + "\nConnection: close\n\n" + content;
            socket->write(content.toUtf8());
            socket->flush();
        }

    } else {
        //unsupported request
        QString content =
                "<html><head><title>BlackBerry 10 Smartphone</title></head><body><p>Something went wrong. Unable to provide the content.</p></body></html>";
        int length = content.length();
        content =
                "HTTP/1.1 500 Internal Server Error\nServer: FileShareForBlackBerry10\nContent-Type: text/html; charset=\nContent-Length: "
                        + QString::number(length) + "\nConnection: close\n\n" + content;
        socket->write(content.toAscii());
        socket->flush();
    }
}

void SocketHelper::onReadyWrite()
{
    QString content =
            "<html><head><title>From Classic</title></head><body><h1>Hello from Classic!</h1></body></html>";
    socket->write(content.toAscii());
    socket->flush();
}

QString SocketHelper::getDirContentLinks(QString path)
{
    int rootPathLen = rootPath.length();

    QString item = "";
    QDir dir(path);
    QFileInfoList dirContent = dir.entryInfoList(QDir::Dirs | QDir::Files | QDir::NoDot);
    QString tmp;
    qDebug() << path << dirContent.size();
    for (int i = 0; i < dirContent.size(); ++i) {
        QFileInfo fileInfo = dirContent.at(i);
        qDebug() << fileInfo.absoluteFilePath();
        tmp = fileInfo.absoluteFilePath();
        if (tmp.length() > rootPathLen) {
            tmp = tmp.right(tmp.length() - rootPathLen);
            if (fileInfo.isDir()) {
                item += "<div>D <a href='" + tmp + "'>" + fileInfo.fileName() + "</a></div>";
            } else if (fileInfo.isFile()) {
                item += "<div>F <a href='" + tmp + "'>" + fileInfo.fileName() + "</a></div>";
            }
        } else if (tmp.length() == rootPathLen) {
            item += "<div>D <a href='/'>" + fileInfo.fileName() + "</a></div>";
        }
    }
    return item;
}

