/*
 * SslSocketHelper.h
 *
 *  Created on: 1. 9. 2015
 *      Author: martin
 */

#ifndef SSLSOCKETHELPER_H_
#define SSLSOCKETHELPER_H_

#include <QObject>
#include <QMap>
#include <QtNetwork/QSslSocket>
#include <QtNetwork/QSslKey>

class SslSocketHelper: public QObject
{
    Q_OBJECT
public:
    SslSocketHelper(QMap<QString, QString> mtypes, QSslKey key, QSslCertificate cert, QObject* parent = 0, int socketDescriptor = 0, QString path = "");
    virtual ~SslSocketHelper();

    void start();

signals:
    void sslSockethelperFinished(SslSocketHelper*);

private slots:
    void onDisconnect();
    void onReadyRead();
    void onEncrypted();
    void onSslErrors(QList<QSslError> errors);

private:
    QString rootPath;
    QMap<QString, QString> mimeTypes;
    int socketDescriptor;
    QSslKey sslKey;
    QSslCertificate sslCert;
    QSslSocket* socket;

    QString getDirContentLinks(QString path);
};

#endif /* SSLSOCKETHELPER_H_ */
