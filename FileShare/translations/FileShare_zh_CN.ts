<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="zh_CN">
<context>
    <name>ResponseHelper</name>
    <message>
        <location filename="../src/ResponseHelper.cpp" line="98"/>
        <location filename="../src/ResponseHelper.cpp" line="119"/>
        <location filename="../src/ResponseHelper.cpp" line="127"/>
        <source>FileShare for BlackBerry 10</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ResponseHelper.cpp" line="218"/>
        <location filename="../src/ResponseHelper.cpp" line="287"/>
        <source>BlackBerry 10 Smartphone</source>
        <translation>黑莓10 智能手机</translation>
    </message>
    <message>
        <location filename="../src/ResponseHelper.cpp" line="233"/>
        <location filename="../src/ResponseHelper.cpp" line="302"/>
        <source>Device content</source>
        <translation>设备内容</translation>
    </message>
    <message>
        <location filename="../src/ResponseHelper.cpp" line="238"/>
        <source>Name</source>
        <translation>名称</translation>
    </message>
    <message>
        <location filename="../src/ResponseHelper.cpp" line="238"/>
        <source>Size</source>
        <translation>大小</translation>
    </message>
    <message>
        <location filename="../src/ResponseHelper.cpp" line="238"/>
        <source>Type</source>
        <translation>类型</translation>
    </message>
    <message>
        <location filename="../src/ResponseHelper.cpp" line="268"/>
        <location filename="../src/ResponseHelper.cpp" line="276"/>
        <source>Folder</source>
        <translation>文件夹</translation>
    </message>
    <message>
        <location filename="../src/ResponseHelper.cpp" line="272"/>
        <source>File</source>
        <translation>文件</translation>
    </message>
    <message>
        <location filename="../src/ResponseHelper.cpp" line="304"/>
        <source>Ups! Something went wrong. Unable to provide the content.</source>
        <translation>唉！发生一些错误。无法提供内容。</translation>
    </message>
</context>
<context>
    <name>about</name>
    <message>
        <location filename="../assets/about.qml" line="46"/>
        <source>Build by</source>
        <translation>开发者</translation>
    </message>
    <message>
        <location filename="../assets/about.qml" line="61"/>
        <source>Privacy policy (English)</source>
        <translation>隐私政策（英文）</translation>
    </message>
    <message>
        <location filename="../assets/about.qml" line="71"/>
        <source>If you like the app, please do not forget to write a review.</source>
        <translation>如果您喜欢这款应用，请记得写下评价。</translation>
    </message>
    <message>
        <location filename="../assets/about.qml" line="81"/>
        <source>THE AUTHOR DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE, INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS, IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, INDIRECT OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>main</name>
    <message>
        <location filename="../assets/main.qml" line="27"/>
        <source>About</source>
        <translation>关于</translation>
    </message>
    <message>
        <location filename="../assets/main.qml" line="41"/>
        <source>Send Feedback</source>
        <translation>发送反馈</translation>
    </message>
    <message>
        <location filename="../assets/main.qml" line="44"/>
        <source>FileShare Feedback</source>
        <translation>文件共享 反馈</translation>
    </message>
    <message>
        <location filename="../assets/main.qml" line="64"/>
        <source>File Share</source>
        <translation>文件共享</translation>
    </message>
    <message>
        <location filename="../assets/main.qml" line="81"/>
        <source>Share</source>
        <translation>共享</translation>
    </message>
    <message>
        <location filename="../assets/main.qml" line="111"/>
        <source>Content</source>
        <translation>内容</translation>
    </message>
    <message>
        <location filename="../assets/main.qml" line="119"/>
        <source>Change</source>
        <translation>更改</translation>
    </message>
    <message>
        <location filename="../assets/main.qml" line="128"/>
        <source>No content selected</source>
        <translation>未选定内容</translation>
    </message>
    <message>
        <location filename="../assets/main.qml" line="144"/>
        <source>You can now access shared content with the web browser. Remember you need to be on the same WiFi network. It will work only if the network supports connection between connected devices. Use following address:</source>
        <translation>您现在可以使用网络浏览器访问共享内容。记住，你必须在同一WiFi网络。如果连接设备之间的连接，它将工作。使用以下地址：</translation>
    </message>
    <message>
        <location filename="../assets/main.qml" line="205"/>
        <source>Select the content that you want to share and turn sharing on</source>
        <translation>选择您想要共享的内容并打开共享</translation>
    </message>
    <message>
        <location filename="../assets/main.qml" line="240"/>
        <source>Sharing off</source>
        <translation>共享关闭</translation>
    </message>
    <message>
        <location filename="../assets/main.qml" line="240"/>
        <source>Sharing on</source>
        <translation>共享打开</translation>
    </message>
    <message>
        <location filename="../assets/main.qml" line="253"/>
        <source>You must select the content that you want to share.</source>
        <translation>您必须选择要共享的内容。</translation>
    </message>
    <message>
        <location filename="../assets/main.qml" line="218"/>
        <source>Directory to share</source>
        <translation>目录共享</translation>
    </message>
</context>
<context>
    <name>settings</name>
    <message>
        <location filename="../assets/settings.qml" line="7"/>
        <source>Settings</source>
        <translation>设置</translation>
    </message>
    <message>
        <location filename="../assets/settings.qml" line="17"/>
        <source>Visual style</source>
        <translation>视觉主题</translation>
    </message>
    <message>
        <location filename="../assets/settings.qml" line="21"/>
        <source>Bright</source>
        <translation>亮色</translation>
    </message>
    <message>
        <location filename="../assets/settings.qml" line="26"/>
        <source>Dark</source>
        <translation>暗色</translation>
    </message>
    <message>
        <location filename="../assets/settings.qml" line="57"/>
        <source>Secure connection</source>
        <translation>安全连接</translation>
    </message>
    <message>
        <location filename="../assets/settings.qml" line="78"/>
        <source>If checked, secure communication will be used. Web browser will probably show some sort of warning because the application uses the self-signed certificate - you can trust certificate/safely continue to the page.</source>
        <translation>如果检查，将使用安全通信。网络浏览器可能会显示某种警告，因为应用程序使用自签名证书-您可以信任证书/安全地继续到该页。</translation>
    </message>
    <message>
        <location filename="../assets/settings.qml" line="99"/>
        <source>Authentication</source>
        <translation>认证</translation>
    </message>
    <message>
        <location filename="../assets/settings.qml" line="126"/>
        <source>Username</source>
        <translation>用户名</translation>
    </message>
    <message>
        <location filename="../assets/settings.qml" line="141"/>
        <source>Password</source>
        <translation>密码</translation>
    </message>
    <message>
        <location filename="../assets/settings.qml" line="158"/>
        <source>If checked, password will be required. Do not use &apos;:&apos; sign.</source>
        <translation>如果检查，密码将被要求。不要使用&apos;：&apos;标志。</translation>
    </message>
</context>
</TS>
