<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0">
<context>
    <name>ResponseHelper</name>
    <message>
        <source>Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Size</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Folder</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Device content</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Ups! Something went wrong. Unable to provide the content.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>FileShare for BlackBerry 10</source>
        <translation>FileShare for BlackBerry® 10</translation>
    </message>
    <message>
        <source>BlackBerry 10 Smartphone</source>
        <translation>BlackBerry® 10 Smartphone</translation>
    </message>
</context>
<context>
    <name>about</name>
    <message>
        <source>Build by</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Privacy policy (English)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>If you like the app, please do not forget to write a review.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>THE AUTHOR DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE, INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS, IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, INDIRECT OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>main</name>
    <message>
        <source>File Share</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Change</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>No content selected</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Share</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Content</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>You can now access shared content with the web browser. Remember you need to be on the same WiFi network. It will work only if the network supports connection between connected devices. Use following address:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>About</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Send Feedback</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>FileShare Feedback</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Directory to share</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>You must select the content that you want to share.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Sharing off</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Sharing on</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Select the content that you want to share and turn sharing on</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>settings</name>
    <message>
        <source>Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Visual style</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Bright</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Dark</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Secure connection</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>If checked, secure communication will be used. Web browser will probably show some sort of warning because the application uses the self-signed certificate - you can trust certificate/safely continue to the page.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Authentication</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Username</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>If checked, password will be required. Do not use &apos;:&apos; sign.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
