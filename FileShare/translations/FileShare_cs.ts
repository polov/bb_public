<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="cs_CZ">
<context>
    <name>ResponseHelper</name>
    <message>
        <source>BlackBerryÂ® 10 Smartphone</source>
        <translation type="obsolete">Zařízení s BlackBerry® 10</translation>
    </message>
    <message>
        <location filename="../src/ResponseHelper.cpp" line="98"/>
        <location filename="../src/ResponseHelper.cpp" line="119"/>
        <location filename="../src/ResponseHelper.cpp" line="127"/>
        <source>FileShare for BlackBerry 10</source>
        <translation>Zařízení s BlackBerry® 10</translation>
    </message>
    <message>
        <location filename="../src/ResponseHelper.cpp" line="218"/>
        <location filename="../src/ResponseHelper.cpp" line="287"/>
        <source>BlackBerry 10 Smartphone</source>
        <translation>FileShare pro BlackBerry® 10</translation>
    </message>
    <message>
        <location filename="../src/ResponseHelper.cpp" line="233"/>
        <location filename="../src/ResponseHelper.cpp" line="302"/>
        <source>Device content</source>
        <translation>Obsah zařízení</translation>
    </message>
    <message>
        <location filename="../src/ResponseHelper.cpp" line="238"/>
        <source>Name</source>
        <translation>Jméno</translation>
    </message>
    <message>
        <location filename="../src/ResponseHelper.cpp" line="238"/>
        <source>Size</source>
        <translation>Velikost</translation>
    </message>
    <message>
        <location filename="../src/ResponseHelper.cpp" line="238"/>
        <source>Type</source>
        <translation>Typ</translation>
    </message>
    <message>
        <location filename="../src/ResponseHelper.cpp" line="268"/>
        <location filename="../src/ResponseHelper.cpp" line="276"/>
        <source>Folder</source>
        <translation>Složka</translation>
    </message>
    <message>
        <location filename="../src/ResponseHelper.cpp" line="272"/>
        <source>File</source>
        <translation>Soubor</translation>
    </message>
    <message>
        <location filename="../src/ResponseHelper.cpp" line="304"/>
        <source>Ups! Something went wrong. Unable to provide the content.</source>
        <translation>Ups! Něco se porouchalo. Nelze poskytnout obsah.</translation>
    </message>
</context>
<context>
    <name>about</name>
    <message>
        <location filename="../assets/about.qml" line="46"/>
        <source>Build by</source>
        <translation>Vytvořil</translation>
    </message>
    <message>
        <location filename="../assets/about.qml" line="61"/>
        <source>Privacy policy (English)</source>
        <translation>Zásady ochrany soukromí</translation>
    </message>
    <message>
        <location filename="../assets/about.qml" line="71"/>
        <source>If you like the app, please do not forget to write a review.</source>
        <translation>Pokud se vám aplikace líbí, nezapomeňte ji doporučit.</translation>
    </message>
    <message>
        <location filename="../assets/about.qml" line="81"/>
        <source>THE AUTHOR DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE, INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS, IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, INDIRECT OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.</source>
        <translation>Používání aplikace FileShare je na vlastní zodpovědnost. Autor aplikace neodpovídá za jakoukoli škodu nebo jinou újmu, která by uživateli aplikace mohla vzniknout v důsledku jejího použití.</translation>
    </message>
</context>
<context>
    <name>main</name>
    <message>
        <location filename="../assets/main.qml" line="27"/>
        <source>About</source>
        <translation>O aplikaci</translation>
    </message>
    <message>
        <location filename="../assets/main.qml" line="41"/>
        <source>Send Feedback</source>
        <translation>Zpětná vazba</translation>
    </message>
    <message>
        <location filename="../assets/main.qml" line="44"/>
        <source>FileShare Feedback</source>
        <translation>Zpětná vazba na aplikaci FileShare</translation>
    </message>
    <message>
        <location filename="../assets/main.qml" line="64"/>
        <source>File Share</source>
        <translation>Sdílení souborů</translation>
    </message>
    <message>
        <location filename="../assets/main.qml" line="81"/>
        <source>Share</source>
        <translation>Sdílet</translation>
    </message>
    <message>
        <location filename="../assets/main.qml" line="111"/>
        <source>Content</source>
        <translation>Zdroj dat</translation>
    </message>
    <message>
        <location filename="../assets/main.qml" line="119"/>
        <source>Change</source>
        <translation>Změnit</translation>
    </message>
    <message>
        <location filename="../assets/main.qml" line="128"/>
        <source>No content selected</source>
        <translation>Nebyl vybrán zdroj dat</translation>
    </message>
    <message>
        <location filename="../assets/main.qml" line="144"/>
        <source>You can now access shared content with the web browser. Remember you need to be on the same WiFi network. It will work only if the network supports connection between connected devices. Use following address:</source>
        <translation>Ke zdroji dat můžete přistoupit z webového prohlížeče. Musíte být na stejné WiFi síti jako zařízení. Sdílení bude fungovat pouze tehdy, když síť bude umožňovat spojení mezi připojenými zařízeními. Použijte následující adresu:</translation>
    </message>
    <message>
        <location filename="../assets/main.qml" line="205"/>
        <source>Select the content that you want to share and turn sharing on</source>
        <translation>Vyberte zdroj dat, který chcete sdílet, a zapněte sdílení</translation>
    </message>
    <message>
        <location filename="../assets/main.qml" line="218"/>
        <source>Directory to share</source>
        <translation>Složka ke sdílení</translation>
    </message>
    <message>
        <source>Choose Content</source>
        <translation type="obsolete">Vybrat obsah</translation>
    </message>
    <message>
        <location filename="../assets/main.qml" line="240"/>
        <source>Sharing off</source>
        <translation>Vypnout</translation>
    </message>
    <message>
        <location filename="../assets/main.qml" line="240"/>
        <source>Sharing on</source>
        <translation>Zapnout</translation>
    </message>
    <message>
        <location filename="../assets/main.qml" line="253"/>
        <source>You must select the content that you want to share.</source>
        <translation>Nejprve vyberte zdroj dat, který chcete sdílet.</translation>
    </message>
</context>
<context>
    <name>settings</name>
    <message>
        <location filename="../assets/settings.qml" line="7"/>
        <source>Settings</source>
        <translation>Nastavení</translation>
    </message>
    <message>
        <location filename="../assets/settings.qml" line="17"/>
        <source>Visual style</source>
        <translation>Téma</translation>
    </message>
    <message>
        <location filename="../assets/settings.qml" line="21"/>
        <source>Bright</source>
        <translation>Světlé</translation>
    </message>
    <message>
        <location filename="../assets/settings.qml" line="26"/>
        <source>Dark</source>
        <translation>Tmavé</translation>
    </message>
    <message>
        <location filename="../assets/settings.qml" line="57"/>
        <source>Secure connection</source>
        <translation>Zabezpečené připojení</translation>
    </message>
    <message>
        <location filename="../assets/settings.qml" line="78"/>
        <source>If checked, secure communication will be used. Web browser will probably show some sort of warning because the application uses the self-signed certificate - you can trust certificate/safely continue to the page.</source>
        <translation>Je-li zapnuto, bude použita zabezpečená komunikace. Prohlížeč pravděpodobně zobrazí varování díky použití vlastního podepsaného certifikátu - můžete bezpečně pokračovat na stránku.</translation>
    </message>
    <message>
        <location filename="../assets/settings.qml" line="99"/>
        <source>Authentication</source>
        <translation>Ověření</translation>
    </message>
    <message>
        <location filename="../assets/settings.qml" line="126"/>
        <source>Username</source>
        <translation>Jméno</translation>
    </message>
    <message>
        <location filename="../assets/settings.qml" line="141"/>
        <source>Password</source>
        <translation>Heslo</translation>
    </message>
    <message>
        <location filename="../assets/settings.qml" line="158"/>
        <source>If checked, password will be required. Do not use &apos;:&apos; sign.</source>
        <translation>Je-li zapnuto, bude při připojení vyžadováno ověření jménem a heslem. Nepoužívejte znak &apos;:&apos;.</translation>
    </message>
</context>
</TS>
