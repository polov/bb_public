import bb.cascades 1.4

Page {
    content: ScrollView {
        content: Container {
            leftPadding: ui.du(2.0)
            rightPadding: ui.du(2.0)
            topPadding: ui.du(2.0)
            bottomPadding: ui.du(2.0)
            Container {
                layout: StackLayout {
                    orientation: LayoutOrientation.LeftToRight
                }
                topPadding: ui.du(5.0)
                Container {
                    layoutProperties: StackLayoutProperties {
                        spaceQuota: 1
                    }
                    Container {
                        horizontalAlignment: HorizontalAlignment.Center
                        ImageView {
                            imageSource: "asset:///images/placeholder_big.png"
                            preferredWidth: ui.du(40.0)
                            scalingMethod: ScalingMethod.AspectFit
                        }
                    }
                    Container {
                        topMargin: ui.du(4.0)
                        horizontalAlignment: HorizontalAlignment.Center
                        Label {
                            text: "Zprávy ČT Sport verze 3.0.0"
                            textStyle.fontSize: FontSize.Large
                            textStyle.fontWeight: FontWeight.W500
                        }
                    }
                    Container {
                        layout: StackLayout {
                            orientation: LayoutOrientation.LeftToRight
                        }
                        Container {
                            topPadding: ui.du(4.0)
                            layoutProperties: StackLayoutProperties {
                                spaceQuota: 1
                            }
                            Label {
                                horizontalAlignment: HorizontalAlignment.Center
                                text: "Vytvořil <a mimetype=\"application/x-bb-appworld\" href=\"appworld://vendorpage/63768\">Martin Polovinčák</a>"
                                multiline: true
                                textFormat: TextFormat.Html
                            }
                        }
                    }
                    Container {
                        topPadding: ui.du(4.0)
                        horizontalAlignment: HorizontalAlignment.Center
                        layout: StackLayout {
                            orientation: LayoutOrientation.LeftToRight
                        }
                        Container {
                            Label {
                                horizontalAlignment: HorizontalAlignment.Center
                                text: "<a href=\"http://www.polovincak.eu/privacy_policy.html\">Zásady ochrany soukromí</a>"
                                multiline: true
                                textFormat: TextFormat.Html
                            }
                        }
                    }
                    Container {
                        topPadding: ui.du(8.0)
                        Container {
                            Label {
                                text: "Pokud se vám aplikace líbí, nezapomeňte ji doporučit."
                                multiline: true
                            }
                        }
                    }
                    Container {
                        topPadding: ui.du(8.0)
                        Container {
                            Label {
                                text: "Používání aplikace Zprávy ČT Sport je na vlastní zodpovědnost. Autor aplikace neodpovídá za jakoukoli škodu nebo jinou újmu, která by uživateli aplikace Zprávy ČT Sport mohla vzniknout v důsledku jejího použití."
                                multiline: true
                            }
                        }
                    }
                    Container {
                        topPadding: ui.du(8.0)
                        Container {
                            Label {
                                text: "Údaje zveřejněné podle zákona 231/201 Sb. a 132/2010 Sb. Provozovatelem televizního vysílání a poskytovatelem audiovizuálních mediálních služeb na vyžádání je Česká televize, Kavčí Hory, 140 70 Praha 4, IČ00027383. Orgánem dohledu je Rada pro rozhlasové a televizní vysílání."
                                multiline: true
                            }
                        }
                    }
                    Container {
                        topPadding: ui.du(2.0)
                        Container {
                            Label {
                                text: "Tel.: +420 2 6113 6113"
                                multiline: true
                            }
                        }
                    }
                    Container {
                        topPadding: ui.du(2.0)
                        Container {
                            Label {
                                text: "Mail: <a href=\"mailto:info@ceskatelevize.cz\">info@ceskatelevize.cz</a>"
                                multiline: true
                                textFormat: TextFormat.Html
                            }
                        }
                    }
                }
            }
        }
    }
}
