import bb.cascades 1.0

Container {
    objectName: "activeFrameContainer"
    background: back.imagePaint
    layout: DockLayout {
    }
    Container {
        background: Color.Black
        opacity: 0.8
        topPadding: 5.0
        bottomPadding: 5.0
        leftPadding: 5.0
        rightPadding: 5.0
        Label {
            objectName: "activeFrameLabel"
            multiline: true
            text: "Hlavní zprávy"
            textStyle.color: Color.White
            textStyle {
                fontSize: FontSize.Small
            }
        }
        horizontalAlignment: HorizontalAlignment.Fill
        verticalAlignment: VerticalAlignment.Bottom

    }
    attachedObjects: [
        ImagePaintDefinition {
            id: back
            imageSource: "asset:///images/placeholder_big.png"
            objectName: "back"
            repeatPattern: RepeatPattern.XY
        }
    ]
}
