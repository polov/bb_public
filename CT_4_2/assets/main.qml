/*
 * Copyright (c) 2011-2015 BlackBerry Limited.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import bb.cascades 1.4
import bb.system 1.0
import bb.data 1.0

TabbedPane {
    id: tabbedPane
    showTabsOnActionBar: false
    Menu.definition: MenuDefinition {
        actions: [
            ActionItem {
                title: qsTr("About")
                imageSource: "asset:///images/ic_info.png"
                onTriggered: {
                    var aboutPageContent = aboutPage.createObject();
                    navigationPane.push(aboutPageContent);
                }
            },
            ActionItem {
                title: qsTr("Send Feedback") + Retranslate.onLanguageChanged
                imageSource: "asset:///images/ic_feedback.png"
                onTriggered: {
                    controller.invokeEmail("", qsTr("Zprávy ČT Sport Feedback") + Retranslate.onLanguageChanged, "");
                }
            }
        ]
        settingsAction: SettingsActionItem {
            onTriggered: {
                var settingsPageContent = settingsPage.createObject();
                navigationPane.push(settingsPageContent);
            }
        }
    }
    activePane: NavigationPane {
        id: navigationPane
        Page {
            id: mainPage
            titleBar: TitleBar {
                id: titlebar
                title: "Hlavní zprávy"
            }
            actionBarVisibility: ChromeVisibility.Overlay
            content: Container {
                ListView {
                    id: feedList
                    layout: FlowListLayout {
                    }
                    leadingVisual: Container {                        
                        Container {
                            id: updateActivityContainer
                            layout: StackLayout {
                                orientation: LayoutOrientation.LeftToRight
                            }
                            visible: false
                            Container {
                                id: activityIndicatorContent
                                topPadding: ui.du(4.0)
                                bottomPadding: ui.du(4.0)
                                layoutProperties: StackLayoutProperties {
                                    spaceQuota: 1
                                }
                                Container {
                                    horizontalAlignment: HorizontalAlignment.Center
                                    layout: StackLayout {
                                        orientation: LayoutOrientation.LeftToRight
                                    }
                                    ActivityIndicator {
                                        id: activityIndicator
                                    }
                                    Label {
                                        id: activityLabel
                                        text: "Aktualizace dat"
                                    }
                                }
                            }
                        }
                        Container {
                            id: leadingvisualContainer
                            layout: StackLayout {
                                orientation: LayoutOrientation.LeftToRight
                            }
                            Container {
                                topPadding: ui.du(4.0)
                                bottomPadding: ui.du(4.0)
                                layoutProperties: StackLayoutProperties {
                                    spaceQuota: 1
                                }
                                Label {
                                    id: refreshLabel
                                    horizontalAlignment: HorizontalAlignment.Center
                                    text: "Obnovte potažením dolů."
                                }
                            }
                        }
                        attachedObjects: [
                            LayoutUpdateHandler {
                                id: refreshHandler
                                onLayoutFrameChanged: {
                                    if (layoutFrame.y >= ui.du(15.0)) {
                                        refreshLabel.text = "Obnovte uvolněním."
                                        shouldRefresh = true;
                                    } else if (layoutFrame.y > 0) {
                                        refreshLabel.text = "Obnovte potažením dolů."
                                    } else if (layoutFrame.y == 0) {
                                        //feedList.scrollToPosition(0, ScrollAnimation.None)
                                        //feedList.scroll(- ui.du(10.0), ScrollAnimation.Smooth)
                                        feedList.hideLeadingVisual();
                                        if (shouldRefresh && ! updating) {
                                            console.log("start update");
                                            //updating = true;
                                            shouldRefresh = false;
                                            mainPage.updateFeeds();
                                        }
                                    }
                                }
                            }
                        ]
                    }
                    listItemComponents: [
                        ListItemComponent {
                            type: "header"
                            Container {
                                enabled: false
                            }
                        },
                        ListItemComponent {
                            id: listItemComponent
                            type: "item"
                            content: Container {
                                id: listItemContainer
                                Container {
                                    layout: StackLayout {
                                        orientation: LayoutOrientation.LeftToRight
                                    }
                                    topPadding: ui.du(1.0)
                                    bottomPadding: ui.du(1.0)
                                    leftPadding: ui.du(1.0)
                                    rightPadding: ui.du(1.0)
                                    Container {
                                        minWidth: ui.du(28.0)
                                        maxWidth: ui.du(28.0)
                                        ImageView {
                                            id: placeholder_ct24
                                            imageSource: "asset:///images/placeholder_big.png"
                                            visible: ! ListItemData.enclosureImageLoaded
                                            scalingMethod: ScalingMethod.AspectFit

                                        }
                                        ImageView {
                                            visible: ListItemData.enclosureImageLoaded
                                            image: ListItemData.enclosureImage
                                            scalingMethod: ScalingMethod.AspectFit
                                        }
                                    }
                                    Container {
                                        //topPadding: ui.du(0.0)
                                        Container {
                                            leftPadding: ui.du(2.0)
                                            Label {
                                                text: ListItemData.title
                                                multiline: true
                                                autoSize.maxLineCount: 3
                                                //textStyle.fontWeight: FontWeight.W300
                                                textStyle.fontSize: FontSize.Medium
                                            }
                                        }
                                        Container {
                                            topPadding: ui.du(0.5)
                                            leftPadding: ui.du(2.0)
                                            Label {
                                                text: Qt.formatDateTime(ListItemData.pubDate, "d.M.yyyy, h:mm")
                                                textStyle {
                                                    fontSize: FontSize.Small
                                                    fontWeight: FontWeight.W300
                                                }
                                            }
                                        }
                                    }
                                }
                                Container {
                                    layout: StackLayout {
                                        orientation: LayoutOrientation.LeftToRight
                                    }
                                    Container {
                                        background: Color.create("#7DB840")
                                        minHeight: 2.0
                                        maxHeight: 2.0
                                        layoutProperties: StackLayoutProperties {
                                            spaceQuota: 1
                                        }
                                    }
                                }
                            }
                        }
                    ]
                    onTriggered: {
                        var chosenItem = feedList.dataModel.data(indexPath);
                        openArticleDescription(chosenItem.title, chosenItem.description, chosenItem.link, chosenItem.enclosureImage, chosenItem.hasVideo, chosenItem.pubDate);
                    }
                    function openArticleDescription(title, description, link, enclosureImage, hasVideo, pubDate) {
                        var articlePageContent = articlePage.createObject();
                        articlePageContent.section = titlebar.title;
                        articlePageContent.title = title;
                        articlePageContent.description = description;
                        articlePageContent.link = link;
                        articlePageContent.hasVideo = hasVideo;
                        articlePageContent.pubDate = Qt.formatDateTime(pubDate, "d.M.yyyy, h:mm");
                        articlePageContent.image = enclosureImage;
                        navigationPane.push(articlePageContent);
                    }
                    function hideLeadingVisual() {
                        scrollToPosition(ScrollPosition.Beginning, ScrollAnimation.Default);
                    }
                }
            }
            actions: [
                ActionItem {
                    title: "Obnovit"
                    imageSource: "asset:///images/ic_reload.png"
                    ActionBar.placement: ActionBarPlacement.Signature
                    onTriggered: {
                        mainPage.updateFeeds();
                    }
                },
                ActionItem {
                    title: "Live"
                    imageSource: "asset:///images/ic_play.png"
                    ActionBar.placement: ActionBarPlacement.OnBar
                    onTriggered: {
                        var webContent = web.createObject();
                        webContent.link = "http://m.ceskatelevize.cz/sport/zive/";
                        navigationPane.push(webContent);
                    }
                }
            ]
            attachedObjects: [
                PageLayoutUpdateHandler {
                    id: pageLayoutUpdateHandler
                    onBottomOverlayHeightChanged: {
                        feedList.bottomPadding = bottomOverlayHeight
                    }
                }
            ]
            function updateFeeds() {
                if (! updating) {
                    updatedCount = 0;
                    updating = true;
                    leadingvisualContainer.visible = false;
                    updateActivityContainer.visible = true;
                    activityIndicator.start();
                    feedList.scrollToPosition(0, ScrollAnimation.None)
                    feedList.scroll(- ui.du(10.0), ScrollAnimation.Smooth)
                    controller.downloadFromUrl("hlavni-zpravy.xml", "http://www.ceskatelevize.cz/sport/rss/hlavni-zpravy");
                    controller.downloadFromUrl("hokej.xml", "http://www.ceskatelevize.cz/sport/rss/hokej");
                    controller.downloadFromUrl("fotbal.xml", "http://www.ceskatelevize.cz/sport/rss/fotbal");
                    controller.downloadFromUrl("tenis.xml", "http://www.ceskatelevize.cz/sport/rss/tenis/");
                    controller.downloadFromUrl("atletika.xml", "http://www.ceskatelevize.cz/sport/rss/atletika");
                    controller.downloadFromUrl("moto.xml", "http://www.ceskatelevize.cz/sport/rss/moto");
                    controller.downloadFromUrl("micove-sporty.xml", "http://www.ceskatelevize.cz/sport/rss/micove-sporty/");
                    controller.downloadFromUrl("golf.xml", "http://www.ceskatelevize.cz/sport/rss/ostatni/golf/");
                    controller.downloadFromUrl("cyklistika.xml", "http://www.ceskatelevize.cz/sport/rss/ostatni/cyklistika/");
                    controller.downloadFromUrl("zimni-sporty.xml", "http://www.ceskatelevize.cz/sport/rss/zimni-sporty/");
                } else {
                    console.log("already updating");
                }
            }
        }
        onPopTransitionEnded: {
            page.destroy();
        }
    }
    attachedObjects: [
        ComponentDefinition {
            id: articlePage
            source: "article.qml"
        },
        ComponentDefinition {
            id: aboutPage
            source: "about.qml"
        },
        ComponentDefinition {
            id: settingsPage
            source: "settings.qml"
        },
        ComponentDefinition {
            id: web
            source: "web.qml"
        }
    ]
    onCreationCompleted: {
        controller.newFeedDataAvailable.connect(tabbedPane.onNewFeedDataAvailable);
        controller.refreshContent("hlavni-zpravy.xml");
        feedList.dataModel = controller.getFeedDataModel(feedFile);
        controller.refreshContent("hokej.xml");
        controller.refreshContent("fotbal.xml");
        controller.refreshContent("tenis.xml");
        controller.refreshContent("atletika.xml");
        controller.refreshContent("moto.xml");
        controller.refreshContent("micove-sporty.xml");
        controller.refreshContent("golf.xml");
        controller.refreshContent("cyklistika.xml");
        controller.refreshContent("zimni-sporty.xml");
    }
    function onNewFeedDataAvailable(id) {
        controller.refreshContent(id);
        if (feedFile == id) {
            feedList.dataModel = controller.getFeedDataModel(id);
        }
        updatedCount ++;
        console.log("new feed data available, updatedCount = " + updatedCount);
        if (updatedCount == 8) {
            updating = false;
            console.log("update finished");
            updateActivityContainer.visible = false;
            leadingvisualContainer.visible = true;
            activityIndicator.stop();
        }
    }

    Tab {
        title: "Hlavní zprávy"
        imageSource: "asset:///images/sport_main.png"
        onTriggered: {
            titlebar.title = "Hlavní zprávy";
            feedFile = "hlavni-zpravy.xml";
            feedList.dataModel = controller.getFeedDataModel(feedFile);
        }
    }

    Tab {
        title: "Hokej"
        imageSource: "asset:///images/11.png"
        onTriggered: {
            titlebar.title = "Hokej";
            feedFile = "hokej.xml";
            feedList.dataModel = controller.getFeedDataModel(feedFile);
        }
    }

    Tab {
        title: "Fotbal"
        imageSource: "asset:///images/12.png"
        onTriggered: {
            titlebar.title = "Fotbal";
            feedFile = "fotbal.xml";
            feedList.dataModel = controller.getFeedDataModel(feedFile);
        }
    }
    
    Tab {
        title: "Tenis"
        imageSource: "asset:///images/13.png"
        onTriggered: {
            titlebar.title = "Tenis";
            feedFile = "tenis.xml";
            feedList.dataModel = controller.getFeedDataModel(feedFile);
        }
    }

    Tab {
        title: "Atletika"
        imageSource: "asset:///images/124.png"
        onTriggered: {
            titlebar.title = "Atletika";
            feedFile = "atletika.xml";
            feedList.dataModel = controller.getFeedDataModel(feedFile);
        }
    }

    Tab {
        title: "Moto"
        imageSource: "asset:///images/123.png"
        onTriggered: {
            titlebar.title = "Moto"
            feedFile = "moto.xml";
            feedList.dataModel = controller.getFeedDataModel(feedFile);
        }
    }

    Tab {
        title: "Míčové sporty"
        imageSource: "asset:///images/122.png"
        onTriggered: {
            titlebar.title = "Míčové sporty"
            feedFile = "micove-sporty.xml";
            feedList.dataModel = controller.getFeedDataModel(feedFile);
        }
    }

    Tab {
        title: "Golf"
        imageSource: "asset:///images/637.png"
        onTriggered: {
            titlebar.title = "Golf"
            feedFile = "golf.xml";
            feedList.dataModel = controller.getFeedDataModel(feedFile);
        }
    }

    Tab {
        title: "Cyklistika"
        imageSource: "asset:///images/498.png"
        onTriggered: {
            titlebar.title = "Cyklistika";
            feedFile = "cyklistika.xml";
            feedList.dataModel = controller.getFeedDataModel(feedFile);
        }
    }
    
    Tab {
        title: "Zimní sporty"
        imageSource: "asset:///images/121.png"
        onTriggered: {
            titlebar.title = "Zimní sporty";
            feedFile = "zimni-sporty.xml";
            feedList.dataModel = controller.getFeedDataModel(feedFile);
        }
    }
    property string feedFile: "hlavni-zpravy.xml"
    property int updatedCount: 0
    property bool updating: false
    property bool shouldRefresh: false
}
