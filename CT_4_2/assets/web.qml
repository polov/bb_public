import bb.cascades 1.4

Page {
    property alias link: webContent.url
    id: webpage
    Container {
        layout: DockLayout {}
        Container {
            horizontalAlignment: HorizontalAlignment.Center
            verticalAlignment: VerticalAlignment.Center
            ScrollView {
                scrollViewProperties.scrollMode: ScrollMode.Both
                scrollRole: ScrollRole.Main
                WebView {
                    id: webContent
                    settings.userAgent: "Mozilla/5.0 (iPhone; CPU iPhone OS 8_4 like Mac OS X) AppleWebKit/600.1.4 (KHTML, like Gecko) Version/8.0 Mobile/12H4098c Safari/600.1.4"
                    //settings.userAgent: "Mozilla/5.0 (Linux; Android 5.0.1; Nexus 5 Build/LRX22C) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.93 Mobile Safari/537.36"
                    settings.userStyleSheetLocation: "css/style.css"
                    onLoadProgressChanged: {
                        progressIndicator.value = loadProgress / 100.0;
                    }
                    onLoadingChanged: {
                        if (loadRequest.status == WebLoadStatus.Started) {
                            progressIndicator.opacity = 1.0;
                        } else if (loadRequest.status == WebLoadStatus.Succeeded) {
                            progressIndicator.opacity = 0.0;
                        } else if (loadRequest.status == WebLoadStatus.Failed) {
                            progressIndicator.opacity = 0.0;
                        }
                    }
                }
            }            
        }
        Container {
            horizontalAlignment: HorizontalAlignment.Center
            verticalAlignment: VerticalAlignment.Bottom
            ProgressIndicator {
                id: progressIndicator
                opacity: 0.0
            }   
        }
    }
}
