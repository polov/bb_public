/*
 * Controller.cpp
 *
 *  Created on: 27. 9. 2015
 *      Author: martin
 */

#include "Controller.h"

#include <QDir>
#include <QFile>
#include <QSettings>

#include <bb/cascades/Application>
#include <bb/cascades/Theme>
#include <bb/cascades/ThemeSupport>
#include <bb/cascades/VisualStyle>
#include <bb/data/XmlDataAccess>
#include <bb/device/DisplayInfo>

#include "FeedItem.h"

Controller::Controller() :
        QObject(), requests(), homeDir(QDir::homePath()), feedDataModel(
                new bb::cascades::GroupDataModel(this)), contentModels(), invokeManager(
                new bb::system::InvokeManager(this))
{
    QSettings settings("eu.polovincak", "CT4");
    visualStyle = settings.value("visualStyle", "bright").toString();
    if (visualStyle == "bright") {
        bb::cascades::Application::instance()->themeSupport()->setVisualStyle(
                bb::cascades::VisualStyle::Bright);
    } else if (visualStyle == "dark") {
        bb::cascades::Application::instance()->themeSupport()->setVisualStyle(
                bb::cascades::VisualStyle::Dark);
    }
    bb::device::DisplayInfo displayInfo;
    displayWidth = displayInfo.pixelSize().width();
    displayHeight = displayInfo.pixelSize().height();
    contentModels.insert("hlavni-zpravy.xml", new bb::cascades::GroupDataModel(this));
    contentModels.insert("hokej.xml", new bb::cascades::GroupDataModel(this));
    contentModels.insert("fotbal.xml", new bb::cascades::GroupDataModel(this));
    contentModels.insert("tenis.xml", new bb::cascades::GroupDataModel(this));
    contentModels.insert("atletika.xml", new bb::cascades::GroupDataModel(this));
    contentModels.insert("moto.xml", new bb::cascades::GroupDataModel(this));
    contentModels.insert("micove-sporty.xml", new bb::cascades::GroupDataModel(this));
    contentModels.insert("golf.xml", new bb::cascades::GroupDataModel(this));
    contentModels.insert("cyklistika.xml", new bb::cascades::GroupDataModel(this));
    contentModels.insert("zimni-sporty.xml", new bb::cascades::GroupDataModel(this));
    houseKeep();
}

Controller::~Controller()
{

}

QString Controller::extractCData(QString data)
{
    return data.remove(QRegExp("<[^>]*>")).remove("\r").remove("\n");
}

void Controller::downloadFromUrl(const QString& id, const QString& url)
{
    qDebug() << "content requested" << url;
    if (requests.contains(id)) {
        qDebug() << "request stopped, already processing";
        return;
    }
    NetworkHelper* networkHelper = new NetworkHelper(id, url);
    bool connectResult;
    Q_UNUSED(connectResult);
    connectResult = connect(networkHelper, SIGNAL(contentUpdated(QString, bool)), this,
            SLOT(onContentUpdated(QString, bool)));
    Q_ASSERT(connectResult);
    requests.insert(id, networkHelper);
    networkHelper->refreshData();
    return;
}

void Controller::onContentUpdated(QString id, bool error)
{
    qDebug() << "content updated" << id;
    if (!requests.contains(id)) {
        qDebug() << "requests not found" << id;
        return;
    }
    NetworkHelper* networkHelper = requests.value(id);
    bool connectResult;
    Q_UNUSED(connectResult);
    connectResult = disconnect(networkHelper, SIGNAL(contentUpdated(QString, bool)), this,
            SLOT(onContentUpdated(QString, bool)));
    Q_ASSERT(connectResult);
    requests.remove(id);
    emit newFeedDataAvailable(id);
}

void Controller::refreshContent(QString id)
{
    qDebug() << "refreshing content" << id;
    bb::data::XmlDataAccess xda;
    QVariant content = xda.load(homeDir + "/" + id, "/rss/channel/item");
    if (xda.hasError()) {
        qDebug() << xda.error().errorMessage();
        return;
    }
    QList<QVariant> list = content.toList();
    bb::cascades::GroupDataModel* dataModel = contentModels.value(id);
    if (dataModel == NULL) {
        qDebug() << "data model is null !!!";
        return;
    }
    dataModel->clear();
    dataModel->setSortingKeys(QStringList() << "pubDate");
    dataModel->setSortedAscending(false);
    int size = list.size();
    QVariantMap map;
    FeedItem* item;
    QString pubDate;
    QLocale::setDefault(QLocale::English);
    for (int i = 0; i < size; i++) {
        map = list.at(i).toMap();
        item = new FeedItem();
        item->setGuid(map["guid"].toString());
        item->setTitle(extractCData(map["title"].toString()));
        item->setDescription(extractCData(map["description"].toString()));
        item->setLink(map["link"].toString());
        item->setHasVideo(map["hasVideo"].toString());
        pubDate = map["pubDate"].toString();
        pubDate = pubDate.mid(0, pubDate.length() - 6);
        item->setPubDate(QLocale().toDateTime(pubDate, "ddd, dd MMM yyyy hh:mm:ss"));
        //qDebug() << "pubDate" << pubDate << QLocale().toDateTime(pubDate, "ddd, dd MMM yyyy hh:mm:ss");
        item->setEnclosureImageUrl(map["enclosure"].toMap().value("url").toString());
        dataModel->insert(item);
    }
    QLocale::setDefault(QLocale::Czech);
    qDebug() << "model refreshed" << id << dataModel->size();

}

bb::cascades::GroupDataModel* Controller::getFeedDataModel(const QString& id)
{
    bb::cascades::GroupDataModel* model = contentModels.value(id);
    /*
     if (model->size() > 0) {
     QObject *obj;
     FeedItem *item;
     for (QVariantList indexPath = model->first(); !indexPath.isEmpty(); indexPath =
     model->after(indexPath)) {
     obj = qvariant_cast<QObject *>(model->data(indexPath));
     item = qobject_cast<FeedItem *>(obj);
     item->fetchEnclosureImage();
     }
     }
     */
    return model;
}

const QString& Controller::getHomeDir() const
{
    return homeDir;
}

void Controller::setHomeDir(const QString& homeDir)
{
    this->homeDir = homeDir;
}

bb::cascades::GroupDataModel* Controller::getFeedDataModel()
{
    return feedDataModel;
}

int Controller::getDisplayWidth() const
{
    return displayWidth;
}

int Controller::getDisplayHeight() const
{
    return displayHeight;
}

// deprecated!!!
void Controller::preprocess(QList<QVariant> data)
{
    int size = data.size();
    qDebug() << "preprocessing list items" << size;
    QVariantList preprocessed;
    feedDataModel->clear();
    QVariantMap map;
    FeedItem* item;
    for (int i = 0; i < size; i++) {
        map = data.at(i).toMap();
        item = new FeedItem();
        item->setGuid(map["guid"].toString());
        item->setTitle(extractCData(map["title"].toString()));
        item->setDescription(extractCData(map["description"].toString()));
        item->setLink(map["link"].toString());
        item->setHasVideo(map["hasVideo"].toString());
        item->setPubDate(map["pubDate"].toDateTime());
        item->setEnclosureImageUrl(map["enclosure"].toMap().value("url").toString());
        feedDataModel->insert(item);
    }
    qDebug() << "got model" << feedDataModel->size();
}

const QString& Controller::getVisualStyle() const
{
    return visualStyle;
}

void Controller::setVisualStyle(const QString& visualStyle)
{
    this->visualStyle = visualStyle;
    QSettings settings("eu.polovincak", "CT4");
    settings.setValue("visualStyle", QVariant::fromValue(visualStyle));
}

void Controller::invokeEmail(const QString& address, const QString& subject, const QString& body)
{
    QString validAddress = (address == "") ? "support@polovincak.eu" : address;
    QString validSubject = subject;
    QString validBody = body;
    qDebug() << "email invocation, receiver:" << validAddress << validSubject << validBody;
    bb::system::InvokeRequest request;
    request.setTarget("sys.pim.uib.email.hybridcomposer");
    request.setAction("bb.action.SENDEMAIL");
    request.setUri(
            QUrl(
                    "mailto:" + validAddress + "?subject=" + validSubject.replace(" ", "%20")
                            + "&body=" + validBody.replace(" ", "%20")));
    invokeManager->invoke(request);
}

void Controller::houseKeep()
{
    QString path = QDir::homePath() + "/data";

    QDir directory(path);
    directory.setNameFilters(QStringList() << "*.png" << "*.PNG" << "*.jpg" << "*.JPG");

    if (directory.count() > 250) {
        QFileInfoList list = directory.entryInfoList(QDir::Files, QDir::Time);
        qDebug() << "removing" << list.at(list.size() - 1).absoluteFilePath();
        QFile::remove(list.at(list.size() - 1).absoluteFilePath());
        houseKeep();
    }
}
