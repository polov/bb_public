/*
 * ActiveFrame.cpp
 *
 *  Created on: May 28, 2013
 *      Author: martin
 */
#include "ActiveFrame.hpp"

#include <QDebug>
#include <bb/cascades/Label>
#include <bb/cascades/ImagePaint>

#include "FeedItem.h"

ActiveFrame::ActiveFrame(Controller* controller) :
        SceneCover(this), titles(), titlesPosition(0), images()
{
    this->controller = controller;
    sceneCoverQml = QmlDocument::create("asset:///activeFrame.qml").parent(this);
    sceneCoverContainer = sceneCoverQml->createRootObject<Container>();
    setContent(sceneCoverContainer);

    timer = new QTimer(this);
    connect(timer, SIGNAL(timeout()), this, SLOT(appInScreenCoverUpdate()));

    QObject::connect(Application::instance(), SIGNAL(thumbnail()), this, SLOT(appInThumbnail()));
    QObject::connect(Application::instance(), SIGNAL(fullscreen()), this, SLOT(appInFullScreen()));
    QObject::connect(Application::instance(), SIGNAL(invisible()), this, SLOT(appInInvisible()));
}

ActiveFrame::~ActiveFrame()
{
    QObject::disconnect(timer, SIGNAL(timeout()), this, SLOT(appInScreenCoverUpdate()));
    QObject::disconnect(Application::instance(), SIGNAL(thumbnail()), this, SLOT(appInThumbnail()));
    QObject::disconnect(Application::instance(), SIGNAL(fullscreen()), this,
            SLOT(appInFullScreen()));
    QObject::disconnect(Application::instance(), SIGNAL(invisible()), this, SLOT(appInInvisible()));
    timer->deleteLater();
}

void ActiveFrame::appInThumbnail()
{
    qDebug() << "App thumbnailed";

    GroupDataModel *dataModel = controller->getFeedDataModel("hlavni-zpravy.xml");
    QObject *obj;
    FeedItem *item;
    titles.clear();
    for (QVariantList indexPath = dataModel->first(); !indexPath.isEmpty();
            indexPath = dataModel->after(indexPath)) {
        obj = qvariant_cast<QObject *>(dataModel->data(indexPath));
        item = qobject_cast<FeedItem *>(obj);
        titles.append(item->getTitle());
        images.append(item->getActiveFrameImage());
    }
    if (titles.size() > 0) {
        timer->start(4000);
    }
}

void ActiveFrame::appInFullScreen()
{
    qDebug() << "App fullscreened";
    timer->stop();
}

void ActiveFrame::appInInvisible()
{
    qDebug() << "App invisible";
    timer->stop();
}

void ActiveFrame::appInScreenCoverUpdate()
{
    qDebug() << "appInScreenCoverUpdate() fired";
    if (titlesPosition == titles.size()) {
        titlesPosition = 0;
    }
    Label* label2 = sceneCoverContainer->findChild<Label*>("activeFrameLabel");
    if (label2) {
        //qDebug() << "Show title" << titles.at(titlesPosition).toMap().value("title").toString();
        label2->setText(titles.at(titlesPosition));
    } else {
        qDebug() << "activeFrameLabel not found";
    }
    bb::cascades::ImagePaint paint(images.at(titlesPosition));
    sceneCoverContainer->setBackground(paint);
    titlesPosition++;
}
