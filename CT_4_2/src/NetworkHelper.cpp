/*
 * NetworkHelper.cpp
 *
 *  Created on: 11. 4. 2015
 *      Author: martin
 */

#include <src/NetworkHelper.h>

#include <QDebug>
#include <QFile>
#include <QDir>

#include <zlib.h>

NetworkHelper::NetworkHelper(QString id, QString url) :
        QObject(), networkAccessManager(this), id(id), url(url), redirectMaxCount(5), redirectCount(
                0), redirectLast()
{
    connect(&networkAccessManager, SIGNAL(finished(QNetworkReply*)), this,
            SLOT(requestFinished(QNetworkReply*)));
    //qDebug() << "NetworkHelper created, id " << id;

}

NetworkHelper::~NetworkHelper()
{
    disconnect(&networkAccessManager, SIGNAL(finished(QNetworkReply*)), this,
            SLOT(requestFinished(QNetworkReply*)));
}

void NetworkHelper::refreshData()
{
    qDebug() << "NetworkHelper refreshing data, id" << id << ", url" << url;
    sendRequest(url);
}

void NetworkHelper::refreshData(QString timestamp)
{
    if (timestamp == NULL) {
        qDebug() << "timestamp is null, call finished" << id;
        return;
    }
    url =
            "http://www.pumpdroid.com/blackberry/fetch?offset=0&limit=50&scope=cz-pump-change-list&from="
                    + timestamp;
    qDebug() << "NetworkHelper refreshing data, id" << id << ", url" << url;
    sendRequest(url);
}

void NetworkHelper::sendPostData(QByteArray data)
{
    qDebug() << "NetworkHelper post data, id" << id << ", url" << url;
    qDebug() << "NetworkHelper post data, data" << data;
    sendRequest(url, data);
}

void NetworkHelper::sendRequest(const QString url)
{
    qDebug() << "NetworkHelper sending request to (QString)" << url;
    QNetworkRequest request;
    request.setUrl(QUrl(url));
    //request.setRawHeader("Accept-Encoding", "gzip, deflate");
    //request.setRawHeader("User-Agent",
    //        "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.71 Safari/537.36");
    networkAccessManager.get(request);
    qDebug() << "NetworkCommunicationManager request sent to " << url;
}

void NetworkHelper::sendRequest(const QUrl url)
{
    qDebug() << "NetworkHelper sending request to (URL)" << url.toString();
    QNetworkRequest request;
    request.setUrl(url);
    networkAccessManager.get(request);
    qDebug() << "NetworkHelper request sent to " << url;
}

void NetworkHelper::sendRequest(const QString url, QByteArray data)
{
    qDebug() << "NetworkHelper sending post request to (QString)" << url;
    QNetworkRequest request;
    request.setUrl(QUrl(url));
    request.setHeader(QNetworkRequest::ContentTypeHeader, "application/xml");
    request.setRawHeader("User-Agent",
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.71 Safari/537.36");
    networkAccessManager.post(request, data);
    qDebug() << "NetworkCommunicationManager post request sent to " << url;
}

void NetworkHelper::sendRequest(const QUrl url, QByteArray data)
{
    qDebug() << "NetworkHelper sending post request to (URL)" << url.toString();
    QNetworkRequest request;
    request.setUrl(url);
    networkAccessManager.post(request, data);
    qDebug() << "NetworkHelper post request sent to " << url;
}

void NetworkHelper::requestFinished(QNetworkReply* reply)
{
    qDebug() << "NetworkHelper id" << id << ", reply to request arrived";
    //redirection check
    QVariant possibleRedirectUrl = reply->attribute(QNetworkRequest::RedirectionTargetAttribute);
    if (!possibleRedirectUrl.toUrl().isEmpty()) {
        redirectCount++;
        if (redirectCount > redirectMaxCount) {
            qDebug() << "NetworkHelper max redirect count reached for " << id << ", returning";
            emit contentUpdated(id, true);
            return;
        } else if (redirectLast == possibleRedirectUrl) {
            qDebug() << "NetworkHelper cyclic redirect for " << id << ", returning";
            emit contentUpdated(id, true);
            return;
        }
        qDebug() << "NetworkHelper redirect found for id" << id << ", redirecting to"
                << possibleRedirectUrl.toUrl();
        sendRequest(reply->url().resolved(possibleRedirectUrl.toUrl()));
        return;
    }

    if (id.startsWith("post")) {
        QByteArray data = reply->readAll();
        qDebug() << "post reply:" << QString(data);
        emit contentUpdated(id, false);
        return;
    }

    QString path = QDir::currentPath() + "/data/" + id;

    qDebug() << "NetworkHelper id" << id << ", reply to request arrived, saving to file " << path;

    QFile contentFile(path);
    qDebug() << "got file object at" << path;
    if (reply->error() == QNetworkReply::NoError) {
        //if (!contentFile.open(QIODevice::ReadWrite)) {
        if (!contentFile.open(QIODevice::WriteOnly)) {
            qDebug() << "NetworkHelper id" << id << ", failed to open" << path
                    << "file for read write:" << contentFile.errorString();
            emit contentUpdated(id, true);
            reply->deleteLater();
            return;
        }
        QByteArray data = reply->readAll();
        contentFile.write(data);
        //qDebug() << "data read:" << data.toHex();
        //if(id.contains(".jpg", Qt::CaseInsensitive) || id.contains(".jpeg", Qt::CaseInsensitive)) {
        //    contentFile.write(data);
        //} else {
        //    contentFile.write(gUncompress(data));
        //}
        //qDebug() << "data uncompressed" << gUncompress(data).toHex();
        contentFile.flush();
        contentFile.close();

    } else {
        qDebug() << "NetworkHelper id" << id << ",error reading reply" << reply->error()
                << reply->errorString();
        emit contentUpdated(id, true);
        reply->deleteLater();
        return;
    }
    qDebug() << "NetworkHelper id" << id << ", processing the reply finished, id=" << id;
    emit contentUpdated(id, false);
    reply->deleteLater();
    this->deleteLater();
}

QByteArray NetworkHelper::gUncompress(const QByteArray &data)
{
    if (data.size() <= 4) {
        qWarning("gUncompress: Input data is truncated");
        return QByteArray();
    }

    QByteArray result;

    int ret;
    z_stream strm;
    static const int CHUNK_SIZE = 1024;
    char out[CHUNK_SIZE];

    /* allocate inflate state */
    strm.zalloc = Z_NULL;
    strm.zfree = Z_NULL;
    strm.opaque = Z_NULL;
    strm.avail_in = data.size();
    strm.next_in = (Bytef*) (data.data());

    ret = inflateInit2(&strm, 15 + 32); // gzip decoding
    if (ret != Z_OK)
        return QByteArray();

    // run inflate()
    do {
        strm.avail_out = CHUNK_SIZE;
        strm.next_out = (Bytef*) (out);

        ret = inflate(&strm, Z_NO_FLUSH);
        Q_ASSERT(ret != Z_STREAM_ERROR); // state not clobbered

        switch (ret) {
            case Z_NEED_DICT:
                ret = Z_DATA_ERROR; // and fall through
            case Z_DATA_ERROR:
            case Z_MEM_ERROR:
                (void) inflateEnd(&strm);
                return QByteArray();
        }

        result.append(out, CHUNK_SIZE - strm.avail_out);
    } while (strm.avail_out == 0);

    // clean up and return
    inflateEnd(&strm);
    return result;
}

const QString& NetworkHelper::getId() const
{
    return id;
}

void NetworkHelper::setId(const QString& id)
{
    this->id = id;
}

const QString& NetworkHelper::getUrl() const
{
    return url;
}

void NetworkHelper::setUrl(const QString& url)
{
    this->url = url;
}
