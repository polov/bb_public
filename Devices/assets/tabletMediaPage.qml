import bb.cascades 1.0

Page {
    property string titleBarText: ""
    property string imgsrc: ""
    property string cameraf: ""
    property string camerar: ""
    property string formats: ""
    property string encdec: ""
    property string image: ""
    property string sharing: ""
    
    titleBar: TitleBar {
        kind: TitleBarKind.Default
        title: "Media"
    }
    
    ScrollView {
        scrollViewProperties.overScrollEffectMode: OverScrollEffectMode.None
        Container {
            Container {
                id: header
                layout: StackLayout {
                    orientation: LayoutOrientation.LeftToRight
                }
                background: Color.create("#fff0eff4")
                Container {
                    leftPadding: 30.0
                    rightPadding: 30.0
                    topPadding: 35.0
                    bottomPadding: 35.0
                    ImageView {
                        id: productImage
                    }
                }
                Container {
                    layout: StackLayout {
                        orientation: LayoutOrientation.TopToBottom
                    }
                    layoutProperties: StackLayoutProperties {
                        spaceQuota: 1
                    }
                    verticalAlignment: VerticalAlignment.Center
                    Label {
                        text: titleBarText
                        multiline: true
                        textStyle.fontSizeValue: 160.0
                        textStyle {
                            fontSize: FontSize.PercentageValue
                            color: Color.Black
                        }
                    }
                }
            }
            Container {
                background: Color.create("#fff0eff4")
                layout: StackLayout {
                    orientation: LayoutOrientation.LeftToRight
                }
                leftPadding: 10.0
                rightPadding: 10.0
                bottomPadding: 10.0
                topPadding: 10.0
                Label {
                    text: "Camera"
                    multiline: true
                    textStyle.color: Color.create("#ff96959A")
                    layoutProperties: StackLayoutProperties {
                        spaceQuota: 1
                    }
                }
            }
            
            Container {
                leftPadding: 10.0
                rightPadding: 10.0
                topPadding: 5.0
                bottomPadding: 5.0
                Label {
                    topMargin: 3.0
                    bottomMargin: 3.0
                    text: "Rear Camera"
                    multiline: true
                }
                Label {
                    topMargin: 3.0
                    bottomMargin: 3.0
                    text: camerar
                    multiline: true
                    textStyle.color: Color.create("#ff96959A")
                }
            }
            
            // divider
            Container {
                layout: StackLayout {
                    orientation: LayoutOrientation.LeftToRight
                }
                leftPadding: 10.0
                Container {
                    layoutProperties: StackLayoutProperties {
                        spaceQuota: 1
                    }
                    minHeight: 2.0
                    background: Color.create("#ffe9e9e9")
                }
            }
            
            Container {
                leftPadding: 10.0
                rightPadding: 10.0
                topPadding: 5.0
                bottomPadding: 20.0
                Label {
                    topMargin: 3.0
                    bottomMargin: 3.0
                    text: "Front Camera"
                    multiline: true
                }
                Label {
                    topMargin: 3.0
                    bottomMargin: 3.0
                    text: cameraf
                    multiline: true
                    textStyle.color: Color.create("#ff96959A")
                }
            }
            
            Container {
                background: Color.create("#fff0eff4")
                layout: StackLayout {
                    orientation: LayoutOrientation.LeftToRight
                }
                leftPadding: 10.0
                rightPadding: 10.0
                bottomPadding: 10.0
                topPadding: 10.0
                Label {
                    text: "Audio/Video"
                    multiline: true
                    textStyle.color: Color.create("#ff96959A")
                    layoutProperties: StackLayoutProperties {
                        spaceQuota: 1
                    }
                }
            }
            
            Container {
                leftPadding: 10.0
                rightPadding: 10.0
                topPadding: 5.0
                bottomPadding: 5.0
                Label {
                    topMargin: 3.0
                    bottomMargin: 3.0
                    text: "Image Formats"
                    multiline: true
                }
                Label {
                    topMargin: 3.0
                    bottomMargin: 3.0
                    text: image
                    multiline: true
                    textStyle.color: Color.create("#ff96959A")
                }
            }
            
            // divider
            Container {
                layout: StackLayout {
                    orientation: LayoutOrientation.LeftToRight
                }
                leftPadding: 10.0
                Container {
                    layoutProperties: StackLayoutProperties {
                        spaceQuota: 1
                    }
                    minHeight: 2.0
                    background: Color.create("#ffe9e9e9")
                }
            }
            
            Container {
                leftPadding: 10.0
                rightPadding: 10.0
                topPadding: 5.0
                bottomPadding: 5.0
                Label {
                    topMargin: 3.0
                    bottomMargin: 3.0
                    text: "Audio/Video Formats"
                    multiline: true
                }
                Label {
                    topMargin: 3.0
                    bottomMargin: 3.0
                    text: formats
                    multiline: true
                    textStyle.color: Color.create("#ff96959A")
                }
            }
            
            // divider
            Container {
                layout: StackLayout {
                    orientation: LayoutOrientation.LeftToRight
                }
                leftPadding: 10.0
                Container {
                    layoutProperties: StackLayoutProperties {
                        spaceQuota: 1
                    }
                    minHeight: 2.0
                    background: Color.create("#ffe9e9e9")
                }
            }
            
            Container {
                leftPadding: 10.0
                rightPadding: 10.0
                topPadding: 5.0
                bottomPadding: 20.0
                Label {
                    topMargin: 3.0
                    bottomMargin: 3.0
                    text: "Audio/Video encoding, decoding"
                    multiline: true
                }
                Label {
                    topMargin: 3.0
                    bottomMargin: 3.0
                    text: encdec
                    multiline: true
                    textStyle.color: Color.create("#ff96959A")
                }
            }
            
            // divider
            Container {
                layout: StackLayout {
                    orientation: LayoutOrientation.LeftToRight
                }
                leftPadding: 10.0
                Container {
                    layoutProperties: StackLayoutProperties {
                        spaceQuota: 1
                    }
                    minHeight: 2.0
                    background: Color.create("#ffe9e9e9")
                }
            }
            
            Container {
                background: Color.create("#fff0eff4")
                layout: StackLayout {
                    orientation: LayoutOrientation.LeftToRight
                }
                leftPadding: 10.0
                rightPadding: 10.0
                bottomPadding: 10.0
                topPadding: 10.0
                Label {
                    text: "Media Sharing"
                    multiline: true
                    textStyle.color: Color.create("#ff96959A")
                    layoutProperties: StackLayoutProperties {
                        spaceQuota: 1
                    }
                }
            }
            
            // divider
            Container {
                layout: StackLayout {
                    orientation: LayoutOrientation.LeftToRight
                }
                leftPadding: 10.0
                Container {
                    layoutProperties: StackLayoutProperties {
                        spaceQuota: 1
                    }
                    minHeight: 2.0
                    background: Color.create("#ffe9e9e9")
                }
            }
            
            Container {
                leftPadding: 10.0
                rightPadding: 10.0
                topPadding: 5.0
                bottomPadding: 20.0
                Label {
                    topMargin: 3.0
                    bottomMargin: 3.0
                    text: "Media Sharing"
                    multiline: true
                }
                Label {
                    topMargin: 3.0
                    bottomMargin: 3.0
                    text: sharing
                    multiline: true
                    textStyle.color: Color.create("#ff96959A")
                }
            }
        }
    }
    
    onImgsrcChanged: {
        productImage.imageSource = "asset:///data/products/tablets/images/" + imgsrc;
    }

}