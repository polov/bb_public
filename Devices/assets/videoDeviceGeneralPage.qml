import bb.cascades 1.0

Page {

    titleBar: TitleBar {
        kind: TitleBarKind.Default
        title: "Product Info"
    }
    
    ScrollView {
        scrollViewProperties.overScrollEffectMode: OverScrollEffectMode.None
        Container {
            id: contentContainer
            Container {
                id: header
                layout: StackLayout {
                    orientation: LayoutOrientation.LeftToRight
                }
                background: Color.create("#fff0eff4")
                Container {
                    id: imageContainer
                    leftPadding: 30.0
                    rightPadding: 30.0
                    topPadding: 35.0
                    bottomPadding: 35.0
                    ImageView {
                        id: productImage
                        gestureHandlers: TapHandler {
                            onTapped: {
                                var picturePageContent = picturePage.createObject();
                                picturePageContent.imgsrc = imgsrc;
                                navigationPane.push(picturePageContent);
                            }
                        }
                    }
                }
                Container {
                    layout: StackLayout {
                        orientation: LayoutOrientation.TopToBottom
                    }
                    layoutProperties: StackLayoutProperties {
                        spaceQuota: 1
                    }
                    verticalAlignment: VerticalAlignment.Center
                    Label {
                        text: titleBarText
                        multiline: true
                        textStyle.fontSizeValue: 160.0
                        textStyle {
                            fontSize: FontSize.PercentageValue
                            color: Color.Black
                        }
                    }
                }
            }
            Container {
                background: Color.create("#fff0eff4")
                layout: StackLayout {
                    orientation: LayoutOrientation.LeftToRight
                }
                leftPadding: 10.0
                rightPadding: 10.0
                bottomPadding: 10.0
                topPadding: 10.0
                Label {
                    text: "OVERVIEW"
                    multiline: true
                    textStyle.color: Color.create("#ff96959A")
                    layoutProperties: StackLayoutProperties {
                        spaceQuota: 1
                    }
                }
            }

            // divider
            Container {
                layout: StackLayout {
                    orientation: LayoutOrientation.LeftToRight
                }
                Container {
                    layoutProperties: StackLayoutProperties {
                        spaceQuota: 1
                    }
                    minHeight: 2.0
                    background: Color.create("#ffe9e9e9")
                }
            }

            Container {
                leftPadding: 10.0
                rightPadding: 10.0
                topPadding: 5.0
                bottomPadding: 5.0
                Label {
                    topMargin: 3.0
                    bottomMargin: 3.0
                    text: "Introduced"
                    multiline: true
                }
                Label {
                    topMargin: 3.0
                    bottomMargin: 3.0
                    text: introduced
                    multiline: true
                    textStyle.color: Color.create("#ff96959A")
                }
            }

            // divider
            Container {
                layout: StackLayout {
                    orientation: LayoutOrientation.LeftToRight
                }
                leftPadding: 10.0
                Container {
                    layoutProperties: StackLayoutProperties {
                        spaceQuota: 1
                    }
                    minHeight: 2.0
                    background: Color.create("#ffe9e9e9")
                }
            }

            Container {
                background: Color.create("#fff0eff4")
                layout: StackLayout {
                    orientation: LayoutOrientation.LeftToRight
                }
                leftPadding: 10.0
                rightPadding: 10.0
                bottomPadding: 10.0
                topPadding: 10.0
                Label {
                    text: "INTERFACES"
                    multiline: true
                    textStyle.color: Color.create("#ff96959A")
                    layoutProperties: StackLayoutProperties {
                        spaceQuota: 1
                    }
                }
            }

            // divider
            Container {
                layout: StackLayout {
                    orientation: LayoutOrientation.LeftToRight
                }
                Container {
                    layoutProperties: StackLayoutProperties {
                        spaceQuota: 1
                    }
                    minHeight: 2.0
                    background: Color.create("#ffe9e9e9")
                }
            }
            
            Container {
                leftPadding: 10.0
                rightPadding: 10.0
                topPadding: 5.0
                bottomPadding: 5.0
                Label {
                    topMargin: 3.0
                    bottomMargin: 3.0
                    text: "USB"
                    multiline: true
                }
                Label {
                    topMargin: 3.0
                    bottomMargin: 3.0
                    text: usb
                    multiline: true
                    textStyle.color: Color.create("#ff96959A")
                }
            }
            
            // divider
            Container {
                layout: StackLayout {
                    orientation: LayoutOrientation.LeftToRight
                }
                leftPadding: 10.0
                Container {
                    layoutProperties: StackLayoutProperties {
                        spaceQuota: 1
                    }
                    minHeight: 2.0
                    background: Color.create("#ffe9e9e9")
                }
            }

            Container {
                leftPadding: 10.0
                rightPadding: 10.0
                topPadding: 5.0
                bottomPadding: 5.0
                Label {
                    topMargin: 3.0
                    bottomMargin: 3.0
                    text: "Bluetooth"
                    multiline: true
                }
                Label {
                    topMargin: 3.0
                    bottomMargin: 3.0
                    text: bt
                    multiline: true
                    textStyle.color: Color.create("#ff96959A")
                }
            }

            // divider
            Container {
                layout: StackLayout {
                    orientation: LayoutOrientation.LeftToRight
                }
                leftPadding: 10.0
                Container {
                    layoutProperties: StackLayoutProperties {
                        spaceQuota: 1
                    }
                    minHeight: 2.0
                    background: Color.create("#ffe9e9e9")
                }
            }

            Container {
                leftPadding: 10.0
                rightPadding: 10.0
                topPadding: 5.0
                bottomPadding: 5.0
                Label {
                    topMargin: 3.0
                    bottomMargin: 3.0
                    text: "WLAN"
                    multiline: true
                }
                Label {
                    topMargin: 3.0
                    bottomMargin: 3.0
                    text: wlan
                    multiline: true
                    textStyle.color: Color.create("#ff96959A")
                }
            }

            // divider
            Container {
                layout: StackLayout {
                    orientation: LayoutOrientation.LeftToRight
                }
                leftPadding: 10.0
                Container {
                    layoutProperties: StackLayoutProperties {
                        spaceQuota: 1
                    }
                    minHeight: 2.0
                    background: Color.create("#ffe9e9e9")
                }
            }

            Container {
                leftPadding: 10.0
                rightPadding: 10.0
                topPadding: 5.0
                bottomPadding: 5.0
                Label {
                    topMargin: 3.0
                    bottomMargin: 3.0
                    text: "NFC"
                    multiline: true
                }
                Label {
                    topMargin: 3.0
                    bottomMargin: 3.0
                    text: nfc
                    multiline: true
                    textStyle.color: Color.create("#ff96959A")
                }
            }

            // divider
            Container {
                layout: StackLayout {
                    orientation: LayoutOrientation.LeftToRight
                }
                leftPadding: 10.0
                Container {
                    layoutProperties: StackLayoutProperties {
                        spaceQuota: 1
                    }
                    minHeight: 2.0
                    background: Color.create("#ffe9e9e9")
                }
            }

            Container {
                leftPadding: 10.0
                rightPadding: 10.0
                topPadding: 5.0
                bottomPadding: 5.0
                Label {
                    topMargin: 3.0
                    bottomMargin: 3.0
                    text: "Video"
                    multiline: true
                }
                Label {
                    topMargin: 3.0
                    bottomMargin: 3.0
                    text: video
                    multiline: true
                    textStyle.color: Color.create("#ff96959A")
                }
            }

            // divider
            Container {
                layout: StackLayout {
                    orientation: LayoutOrientation.LeftToRight
                }
                leftPadding: 10.0
                Container {
                    layoutProperties: StackLayoutProperties {
                        spaceQuota: 1
                    }
                    minHeight: 2.0
                    background: Color.create("#ffe9e9e9")
                }
            }

            Container {
                leftPadding: 10.0
                rightPadding: 10.0
                topPadding: 5.0
                bottomPadding: 20.0
                Label {
                    topMargin: 3.0
                    bottomMargin: 3.0
                    text: "Battery Usage"
                    multiline: true
                }
                Label {
                    topMargin: 3.0
                    bottomMargin: 3.0
                    text: batteryusage
                    multiline: true
                    textStyle.color: Color.create("#ff96959A")
                }
            }
        }
    }
    attachedObjects: [
        ComponentDefinition {
            id: picturePage
            source: "avDevicePicturePage.qml"
        }
    ]
    actions: [
        ActionItem {
            ActionBar.placement: ActionBarPlacement.InOverflow
            id: pictureAction
            title: "Product Image"
            imageSource: "asset:///data/images/icons/ic_picture.png"
            onTriggered: {
                var picturePageContent = picturePage.createObject();
                picturePageContent.imgsrc = imgsrc;
                navigationPane.push(picturePageContent);
            }
        }
    ]
    
    onImgsrcChanged: {
        productImage.imageSource = "asset:///data/products/devices/images/" + imgsrc;
    }

    property NavigationPane navigationPane
    property string fileBase: ""
    property string titleBarText: ""
    property string imgsrc: ""
    property string introduced: ""
    property string type: ""
    property string bt: ""
    property string wlan: ""
    property string nfc: ""
    property string audio: ""
    property string video: ""
    property string usb: ""
    property string batteryusage: ""
}
