import bb.cascades 1.0

Page {
    property string titleBarText: ""
    property string imgsrc: ""
    property string ports: ""
    property string wifi: ""
    property string bt: ""
    property string nfc: ""
    property string networks: ""
    property string sim: ""
    property string loc: ""
    property string sensors: ""
    property string alerts: ""
    
    titleBar: TitleBar {
        kind: TitleBarKind.Default
        title: "Connections"
    }
    
    ScrollView {
        scrollViewProperties.overScrollEffectMode: OverScrollEffectMode.None
        Container {
            Container {
                id: header
                layout: StackLayout {
                    orientation: LayoutOrientation.LeftToRight
                }
                background: Color.create("#fff0eff4")
                Container {
                    leftPadding: 30.0
                    rightPadding: 30.0
                    topPadding: 35.0
                    bottomPadding: 35.0
                    ImageView {
                        id: productImage
                        //imageSource: "asset:///data/products/smartphones/images/" + imgsrc
                    }
                }
                Container {
                    layout: StackLayout {
                        orientation: LayoutOrientation.TopToBottom
                    }
                    layoutProperties: StackLayoutProperties {
                        spaceQuota: 1
                    }
                    verticalAlignment: VerticalAlignment.Center
                    Label {
                        text: titleBarText
                        multiline: true
                        textStyle.fontSizeValue: 160.0
                        textStyle {
                            fontSize: FontSize.PercentageValue
                            color: Color.Black
                        }
                    }
                }
            }
            Container {
                background: Color.create("#fff0eff4")
                layout: StackLayout {
                    orientation: LayoutOrientation.LeftToRight
                }
                leftPadding: 10.0
                rightPadding: 10.0
                bottomPadding: 10.0
                topPadding: 10.0
                Label {
                    text: "Connections"
                    multiline: true
                    textStyle.color: Color.create("#ff96959A")
                    layoutProperties: StackLayoutProperties {
                        spaceQuota: 1
                    }
                }
            }
            
            Container {
                leftPadding: 10.0
                rightPadding: 10.0
                topPadding: 5.0
                bottomPadding: 5.0
                Label {
                    topMargin: 3.0
                    bottomMargin: 3.0
                    text: "Ports"
                    multiline: true
                }
                Label {
                    topMargin: 3.0
                    bottomMargin: 3.0
                    text: ports
                    multiline: true
                    textStyle.color: Color.create("#ff96959A")
                }
            }
            
            // divider
            Container {
                layout: StackLayout {
                    orientation: LayoutOrientation.LeftToRight
                }
                leftPadding: 10.0
                Container {
                    layoutProperties: StackLayoutProperties {
                        spaceQuota: 1
                    }
                    minHeight: 2.0
                    background: Color.create("#ffe9e9e9")
                }
            }
            
            Container {
                leftPadding: 10.0
                rightPadding: 10.0
                topPadding: 5.0
                bottomPadding: 5.0
                Label {
                    topMargin: 3.0
                    bottomMargin: 3.0
                    text: "WiFi"
                    multiline: true
                }
                Label {
                    topMargin: 3.0
                    bottomMargin: 3.0
                    text: wifi
                    multiline: true
                    textStyle.color: Color.create("#ff96959A")
                }
            }
            
            // divider
            Container {
                layout: StackLayout {
                    orientation: LayoutOrientation.LeftToRight
                }
                leftPadding: 10.0
                Container {
                    layoutProperties: StackLayoutProperties {
                        spaceQuota: 1
                    }
                    minHeight: 2.0
                    background: Color.create("#ffe9e9e9")
                }
            }
            
            Container {
                leftPadding: 10.0
                rightPadding: 10.0
                topPadding: 5.0
                bottomPadding: 5.0
                Label {
                    topMargin: 3.0
                    bottomMargin: 3.0
                    text: "Bluetooth"
                    multiline: true
                }
                Label {
                    topMargin: 3.0
                    bottomMargin: 3.0
                    text: bt
                    multiline: true
                    textStyle.color: Color.create("#ff96959A")
                }
            }
            
            // divider
            Container {
                layout: StackLayout {
                    orientation: LayoutOrientation.LeftToRight
                }
                leftPadding: 10.0
                Container {
                    layoutProperties: StackLayoutProperties {
                        spaceQuota: 1
                    }
                    minHeight: 2.0
                    background: Color.create("#ffe9e9e9")
                }
            }
            
            Container {
                leftPadding: 10.0
                rightPadding: 10.0
                topPadding: 5.0
                bottomPadding: 5.0
                Label {
                    topMargin: 3.0
                    bottomMargin: 3.0
                    text: "NFC"
                    multiline: true
                }
                Label {
                    topMargin: 3.0
                    bottomMargin: 3.0
                    text: nfc
                    multiline: true
                    textStyle.color: Color.create("#ff96959A")
                }
            }
            
            // divider
            Container {
                layout: StackLayout {
                    orientation: LayoutOrientation.LeftToRight
                }
                leftPadding: 10.0
                Container {
                    layoutProperties: StackLayoutProperties {
                        spaceQuota: 1
                    }
                    minHeight: 2.0
                    background: Color.create("#ffe9e9e9")
                }
            }
            
            Container {
                leftPadding: 10.0
                rightPadding: 10.0
                topPadding: 5.0
                bottomPadding: 5.0
                Label {
                    topMargin: 3.0
                    bottomMargin: 3.0
                    text: "Networks"
                    multiline: true
                }
                Label {
                    topMargin: 3.0
                    bottomMargin: 3.0
                    text: networks
                    multiline: true
                    textStyle.color: Color.create("#ff96959A")
                }
            }
            
            // divider
            Container {
                layout: StackLayout {
                    orientation: LayoutOrientation.LeftToRight
                }
                leftPadding: 10.0
                Container {
                    layoutProperties: StackLayoutProperties {
                        spaceQuota: 1
                    }
                    minHeight: 2.0
                    background: Color.create("#ffe9e9e9")
                }
            }
            
            Container {
                leftPadding: 10.0
                rightPadding: 10.0
                topPadding: 5.0
                bottomPadding: 5.0
                Label {
                    topMargin: 3.0
                    bottomMargin: 3.0
                    text: "SIM"
                    multiline: true
                }
                Label {
                    topMargin: 3.0
                    bottomMargin: 3.0
                    text: sim
                    multiline: true
                    textStyle.color: Color.create("#ff96959A")
                }
            }
            
            // divider
            Container {
                layout: StackLayout {
                    orientation: LayoutOrientation.LeftToRight
                }
                leftPadding: 10.0
                Container {
                    layoutProperties: StackLayoutProperties {
                        spaceQuota: 1
                    }
                    minHeight: 2.0
                    background: Color.create("#ffe9e9e9")
                }
            }
            
            Container {
                leftPadding: 10.0
                rightPadding: 10.0
                topPadding: 5.0
                bottomPadding: 5.0
                Label {
                    topMargin: 3.0
                    bottomMargin: 3.0
                    text: "Location"
                    multiline: true
                }
                Label {
                    topMargin: 3.0
                    bottomMargin: 3.0
                    text: loc
                    multiline: true
                    textStyle.color: Color.create("#ff96959A")
                }
            }
            
            // divider
            Container {
                layout: StackLayout {
                    orientation: LayoutOrientation.LeftToRight
                }
                leftPadding: 10.0
                Container {
                    layoutProperties: StackLayoutProperties {
                        spaceQuota: 1
                    }
                    minHeight: 2.0
                    background: Color.create("#ffe9e9e9")
                }
            }
            
            Container {
                leftPadding: 10.0
                rightPadding: 10.0
                topPadding: 5.0
                bottomPadding: 5.0
                Label {
                    topMargin: 3.0
                    bottomMargin: 3.0
                    text: "Sensors"
                    multiline: true
                }
                Label {
                    topMargin: 3.0
                    bottomMargin: 3.0
                    text: sensors
                    multiline: true
                    textStyle.color: Color.create("#ff96959A")
                }
            }
            
            // divider
            Container {
                layout: StackLayout {
                    orientation: LayoutOrientation.LeftToRight
                }
                leftPadding: 10.0
                Container {
                    layoutProperties: StackLayoutProperties {
                        spaceQuota: 1
                    }
                    minHeight: 2.0
                    background: Color.create("#ffe9e9e9")
                }
            }
            
            Container {
                leftPadding: 10.0
                rightPadding: 10.0
                topPadding: 5.0
                bottomPadding: 20.0
                Label {
                    topMargin: 3.0
                    bottomMargin: 3.0
                    text: "Alerts"
                    multiline: true
                }
                Label {
                    topMargin: 3.0
                    bottomMargin: 3.0
                    text: alerts
                    multiline: true
                    textStyle.color: Color.create("#ff96959A")
                }
            }
        }
    }
    
    onImgsrcChanged: {
        productImage.imageSource = "asset:///data/products/smartphones/images/" + imgsrc;
    }

}