import bb.cascades 1.0

Page {

    titleBar: TitleBar {
        kind: TitleBarKind.Default
        title: "Software"
        //title: "asset:///data/images/" + imgsrc
    }

    ScrollView {
        scrollViewProperties.overScrollEffectMode: OverScrollEffectMode.None
        Container {
            Container {
                id: header
                layout: StackLayout {
                    orientation: LayoutOrientation.LeftToRight
                }
                background: Color.create("#fff0eff4")
                Container {
                    leftPadding: 30.0
                    rightPadding: 30.0
                    topPadding: 35.0
                    bottomPadding: 35.0
                    ImageView {
                        id: productImage
                        //imageSource: "asset:///data/products/smartphones/images/" + imgsrc
                    }
                }
                Container {
                    layout: StackLayout {
                        orientation: LayoutOrientation.TopToBottom
                    }
                    layoutProperties: StackLayoutProperties {
                        spaceQuota: 1
                    }
                    verticalAlignment: VerticalAlignment.Center
                    Label {
                        text: titleBarText
                        multiline: true
                        textStyle.fontSizeValue: 160.0
                        textStyle {
                            fontSize: FontSize.PercentageValue
                            color: Color.Black
                        }
                    }
                }
            }
            Container {
                background: Color.create("#fff0eff4")
                layout: StackLayout {
                    orientation: LayoutOrientation.LeftToRight
                }
                leftPadding: 10.0
                rightPadding: 10.0
                bottomPadding: 10.0
                topPadding: 10.0
                Label {
                    text: "SOFTWARE"
                    multiline: true
                    textStyle.color: Color.create("#ff96959A")
                    layoutProperties: StackLayoutProperties {
                        spaceQuota: 1
                    }
                }
            }
            
            // divider
            Container {
                layout: StackLayout {
                    orientation: LayoutOrientation.LeftToRight
                }
                Container {
                    layoutProperties: StackLayoutProperties {
                        spaceQuota: 1
                    }
                    minHeight: 2.0
                    background: Color.create("#ffe9e9e9")
                }
            }
            
            Container {
                leftPadding: 10.0
                rightPadding: 10.0
                topPadding: 5.0
                bottomPadding: 5.0
                Label {
                    topMargin: 3.0
                    bottomMargin: 3.0
                    text: "Original OS"
                    multiline: true
                }
                Label {
                    topMargin: 3.0
                    bottomMargin: 3.0
                    text: osorig
                    multiline: true
                    textStyle.color: Color.create("#ff96959A")
                }
            }
            
            // divider
            Container {
                layout: StackLayout {
                    orientation: LayoutOrientation.LeftToRight
                }
                leftPadding: 10.0
                Container {
                    layoutProperties: StackLayoutProperties {
                        spaceQuota: 1
                    }
                    minHeight: 2.0
                    background: Color.create("#ffe9e9e9")
                }
            }
            Container {
                leftPadding: 10.0
                rightPadding: 10.0
                topPadding: 5.0
                bottomPadding: 5.0
                Label {
                    topMargin: 3.0
                    bottomMargin: 3.0
                    text: "Maximum OS"
                    multiline: true
                }
                Label {
                    topMargin: 3.0
                    bottomMargin: 3.0
                    text: osmax
                    multiline: true
                    textStyle.color: Color.create("#ff96959A")
                }
            }
            
            // divider
            Container {
                layout: StackLayout {
                    orientation: LayoutOrientation.LeftToRight
                }
                leftPadding: 10.0
                Container {
                    layoutProperties: StackLayoutProperties {
                        spaceQuota: 1
                    }
                    minHeight: 2.0
                    background: Color.create("#ffe9e9e9")
                }
            }
            
            Container {
                leftPadding: 10.0
                rightPadding: 10.0
                topPadding: 5.0
                bottomPadding: 5.0
                Label {
                    topMargin: 3.0
                    bottomMargin: 3.0
                    text: "Sync Software"
                    multiline: true
                }
                Label {
                    topMargin: 3.0
                    bottomMargin: 3.0
                    text: syncsw
                    multiline: true
                    textStyle.color: Color.create("#ff96959A")
                }
            }
            
            // divider
            Container {
                layout: StackLayout {
                    orientation: LayoutOrientation.LeftToRight
                }
                leftPadding: 10.0
                Container {
                    layoutProperties: StackLayoutProperties {
                        spaceQuota: 1
                    }
                    minHeight: 2.0
                    background: Color.create("#ffe9e9e9")
                }
            }
            
            Container {
                leftPadding: 10.0
                rightPadding: 10.0
                topPadding: 5.0
                bottomPadding: 5.0
                Label {
                    topMargin: 3.0
                    bottomMargin: 3.0
                    text: "Sync Software Requirements"
                    multiline: true
                }
                Label {
                    topMargin: 3.0
                    bottomMargin: 3.0
                    text: sysreq
                    multiline: true
                    textStyle.color: Color.create("#ff96959A")
                }
            }
            
            // divider
            Container {
                layout: StackLayout {
                    orientation: LayoutOrientation.LeftToRight
                }
                leftPadding: 10.0
                Container {
                    layoutProperties: StackLayoutProperties {
                        spaceQuota: 1
                    }
                    minHeight: 2.0
                    background: Color.create("#ffe9e9e9")
                }
            }
        }
    }
    
    onImgsrcChanged: {
        productImage.imageSource = "asset:///data/products/smartphones/images/" + imgsrc;
    }
    
    property string titleBarText: ""
    property string imgsrc: ""
    property string osorig: ""
    property string osmax: ""
    property string syncsw: ""
    property string sysreq: ""
}