import bb.cascades 1.0
import bb.system 1.0

NavigationPane {
    id: navigationPane
    Menu.definition: MenuDefinition {
        actions: [
            ActionItem {
                title: "About"
                imageSource: "asset:///data/images/icons/ic_info.png"
                onTriggered: {
                    aboutSheet.open();
                }
            },
            ActionItem {
                ActionBar.placement: ActionBarPlacement.InOverflow
                id: emailAction
                title: "Send Feedback"
                imageSource: "asset:///data/images/icons/ic_feedback.png"
                onTriggered: {
                    helper.invokeEmail("Devices Feedback");
                }
            }
        ]
    }
    ListPage {
        id: listPage
        contentSource: "asset:///data/listView.xml"
        timeSource: "asset://data/bb-time.xml"
        navigationPane: navigationPane
        titleBarText: qsTr("Products")
    }
    attachedObjects: [
        AboutSheet {
            id: aboutSheet
        }
    ]
    onPopTransitionEnded: {
        page.destroy();
    }
}
