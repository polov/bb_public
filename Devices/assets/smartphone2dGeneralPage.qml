import bb.cascades 1.0

Page {

    titleBar: TitleBar {
        kind: TitleBarKind.Default
        title: "Product Info"
        //title: "asset:///data/images/" + imgsrc
    }
    ScrollView {
        scrollViewProperties.overScrollEffectMode: OverScrollEffectMode.None
        Container {
            Container {
                id: header
                layout: StackLayout {
                    orientation: LayoutOrientation.LeftToRight
                }
                background: Color.create("#fff0eff4")
                Container {
                    leftPadding: 30.0
                    rightPadding: 30.0
                    topPadding: 35.0
                    bottomPadding: 35.0
                    ImageView {
                        id: productImage
                        gestureHandlers: TapHandler {
                            onTapped: {
                                var picturePageContent = picturePage.createObject();
                                picturePageContent.imgsrc = imgsrc;
                                navigationPane.push(picturePageContent);
                            }
                        }
                    }
                }
                Container {
                    layout: StackLayout {
                        orientation: LayoutOrientation.TopToBottom
                    }
                    layoutProperties: StackLayoutProperties {
                        spaceQuota: 1
                    }
                    verticalAlignment: VerticalAlignment.Center
                    Label {
                        text: titleBarText
                        multiline: true
                        textStyle.fontSizeValue: 160.0
                        textStyle {
                            fontSize: FontSize.PercentageValue
                            color: Color.Black
                        }
                    }
                }
            }
            Container {
                background: Color.create("#fff0eff4")
                layout: StackLayout {
                    orientation: LayoutOrientation.LeftToRight
                }
                leftPadding: 10.0
                rightPadding: 10.0
                bottomPadding: 10.0
                topPadding: 10.0
                Label {
                    text: "OVERVIEW"
                    multiline: true
                    textStyle.color: Color.create("#ff96959A")
                    layoutProperties: StackLayoutProperties {
                        spaceQuota: 1
                    }
                }
            }

            // divider
            Container {
                layout: StackLayout {
                    orientation: LayoutOrientation.LeftToRight
                }
                Container {
                    layoutProperties: StackLayoutProperties {
                        spaceQuota: 1
                    }
                    minHeight: 2.0
                    background: Color.create("#ffe9e9e9")
                }
            }

            Container {
                leftPadding: 10.0
                rightPadding: 10.0
                topPadding: 5.0
                bottomPadding: 5.0
                Label {
                    topMargin: 3.0
                    bottomMargin: 3.0
                    text: "Introduced"
                    multiline: true
                }
                Label {
                    topMargin: 3.0
                    bottomMargin: 3.0
                    text: introduced
                    multiline: true
                    textStyle.color: Color.create("#ff96959A")
                }
            }

            // divider
/*            Container {
                layout: StackLayout {
                    orientation: LayoutOrientation.LeftToRight
                }
                leftPadding: 10.0
                Container {
                    layoutProperties: StackLayoutProperties {
                        spaceQuota: 1
                    }
                    minHeight: 2.0
                    background: Color.create("#ffe9e9e9")
                }
            }

            Container {
                leftPadding: 10.0
                rightPadding: 10.0
                topPadding: 5.0
                bottomPadding: 5.0
                Label {
                    topMargin: 3.0
                    bottomMargin: 3.0
                    text: "Discontinued"
                    multiline: true
                }
                Label {
                    topMargin: 3.0
                    bottomMargin: 3.0
                    text: discontinued
                    multiline: true
                    textStyle.color: Color.create("#ff96959A")
                }
            }
*/
            // divider
            Container {
                layout: StackLayout {
                    orientation: LayoutOrientation.LeftToRight
                }
                leftPadding: 10.0
                Container {
                    layoutProperties: StackLayoutProperties {
                        spaceQuota: 1
                    }
                    minHeight: 2.0
                    background: Color.create("#ffe9e9e9")
                }
            }

            Container {
                leftPadding: 10.0
                rightPadding: 10.0
                topPadding: 5.0
                bottomPadding: 5.0
                Label {
                    topMargin: 3.0
                    bottomMargin: 3.0
                    text: "Model Identifier"
                    multiline: true
                }
                Label {
                    topMargin: 3.0
                    bottomMargin: 3.0
                    text: identifier
                    multiline: true
                    textStyle.color: Color.create("#ff96959A")
                }
            }
/*
            // divider
            Container {
                layout: StackLayout {
                    orientation: LayoutOrientation.LeftToRight
                }
                leftPadding: 10.0
                Container {
                    layoutProperties: StackLayoutProperties {
                        spaceQuota: 1
                    }
                    minHeight: 2.0
                    background: Color.create("#ffe9e9e9")
                }
            }

            Container {
                leftPadding: 10.0
                rightPadding: 10.0
                topPadding: 5.0
                bottomPadding: 5.0
                Label {
                    topMargin: 3.0
                    bottomMargin: 3.0
                    text: "Support status"
                    multiline: true
                }
                Label {
                    topMargin: 3.0
                    bottomMargin: 3.0
                    text: support
                    multiline: true
                    textStyle.color: Color.create("#ff96959A")
                }
            }
*/
            // divider
            Container {
                layout: StackLayout {
                    orientation: LayoutOrientation.LeftToRight
                }
                Container {
                    layoutProperties: StackLayoutProperties {
                        spaceQuota: 1
                    }
                    minHeight: 2.0
                    background: Color.create("#ffe9e9e9")
                }
            }

            Container {
                leftPadding: 10.0
                rightPadding: 10.0
                topPadding: 5.0
                bottomPadding: 5.0
                Label {
                    topMargin: 3.0
                    bottomMargin: 3.0
                    text: "Input Methods"
                    multiline: true
                }
                Label {
                    topMargin: 3.0
                    bottomMargin: 3.0
                    text: inputmethods
                    multiline: true
                    textStyle.color: Color.create("#ff96959A")
                }
            }

            // divider
            Container {
                layout: StackLayout {
                    orientation: LayoutOrientation.LeftToRight
                }
                leftPadding: 10.0
                Container {
                    layoutProperties: StackLayoutProperties {
                        spaceQuota: 1
                    }
                    minHeight: 2.0
                    background: Color.create("#ffe9e9e9")
                }
            }

            Container {
                leftPadding: 10.0
                rightPadding: 10.0
                topPadding: 5.0
                bottomPadding: 5.0
                Label {
                    topMargin: 3.0
                    bottomMargin: 3.0
                    text: "Colors"
                    multiline: true
                }
                Label {
                    topMargin: 3.0
                    bottomMargin: 3.0
                    text: colors
                    multiline: true
                    textStyle.color: Color.create("#ff96959A")
                }
            }

            // divider
            Container {
                layout: StackLayout {
                    orientation: LayoutOrientation.LeftToRight
                }
                leftPadding: 10.0
                Container {
                    layoutProperties: StackLayoutProperties {
                        spaceQuota: 1
                    }
                    minHeight: 2.0
                    background: Color.create("#ffe9e9e9")
                }
            }

            Container {
                leftPadding: 10.0
                rightPadding: 10.0
                topPadding: 5.0
                bottomPadding: 5.0
                Label {
                    topMargin: 3.0
                    bottomMargin: 3.0
                    text: "Dimensions"
                    multiline: true
                }
                Label {
                    topMargin: 3.0
                    bottomMargin: 3.0
                    text: dimx + " H x " + dimy + " W x " + dimz + " D mm (opened)"
                    multiline: true
                    textStyle.color: Color.create("#ff96959A")
                }
                Label {
                    topMargin: 3.0
                    bottomMargin: 3.0
                    text: dimx2 + " H x " + dimy2 + " W x " + dimz2 + " D mm (closed)"
                    multiline: true
                    textStyle.color: Color.create("#ff96959A")
                }
            }

            // divider
            Container {
                layout: StackLayout {
                    orientation: LayoutOrientation.LeftToRight
                }
                leftPadding: 10.0
                Container {
                    layoutProperties: StackLayoutProperties {
                        spaceQuota: 1
                    }
                    minHeight: 2.0
                    background: Color.create("#ffe9e9e9")
                }
            }

            Container {
                leftPadding: 10.0
                rightPadding: 10.0
                topPadding: 5.0
                bottomPadding: 20.0
                Label {
                    topMargin: 3.0
                    bottomMargin: 3.0
                    text: "Weight"
                    multiline: true
                }
                Label {
                    topMargin: 3.0
                    bottomMargin: 3.0
                    text: weight + " g"
                    multiline: true
                    textStyle.color: Color.create("#ff96959A")
                }
            }

            // divider
            Container {
                layout: StackLayout {
                    orientation: LayoutOrientation.LeftToRight
                }
                Container {
                    layoutProperties: StackLayoutProperties {
                        spaceQuota: 1
                    }
                    minHeight: 2.0
                    background: Color.create("#ffe9e9e9")
                }
            }

            Container {
                background: Color.create("#fff0eff4")
                layout: StackLayout {
                    orientation: LayoutOrientation.LeftToRight
                }
                leftPadding: 10.0
                rightPadding: 10.0
                bottomPadding: 10.0
                topPadding: 10.0
                Label {
                    text: "DISPLAY AND STORAGE"
                    multiline: true
                    textStyle.color: Color.create("#ff96959A")
                    layoutProperties: StackLayoutProperties {
                        spaceQuota: 1
                    }
                }
            }

            // divider
            Container {
                layout: StackLayout {
                    orientation: LayoutOrientation.LeftToRight
                }
                Container {
                    layoutProperties: StackLayoutProperties {
                        spaceQuota: 1
                    }
                    minHeight: 2.0
                    background: Color.create("#ffe9e9e9")
                }
            }

            Container {
                leftPadding: 10.0
                rightPadding: 10.0
                topPadding: 5.0
                bottomPadding: 5.0
                Label {
                    topMargin: 3.0
                    bottomMargin: 3.0
                    text: "Display size"
                    multiline: true
                }
                Label {
                    topMargin: 3.0
                    bottomMargin: 3.0
                    text: dsize + "-inch (diagonal)"
                    multiline: true
                    textStyle.color: Color.create("#ff96959A")
                }
            }

            // divider
            Container {
                layout: StackLayout {
                    orientation: LayoutOrientation.LeftToRight
                }
                leftPadding: 10.0
                Container {
                    layoutProperties: StackLayoutProperties {
                        spaceQuota: 1
                    }
                    minHeight: 2.0
                    background: Color.create("#ffe9e9e9")
                }
            }

            Container {
                leftPadding: 10.0
                rightPadding: 10.0
                topPadding: 5.0
                bottomPadding: 5.0
                Label {
                    topMargin: 3.0
                    bottomMargin: 3.0
                    text: "Display type"
                    multiline: true
                }
                Label {
                    topMargin: 3.0
                    bottomMargin: 3.0
                    text: dtype
                    multiline: true
                    textStyle.color: Color.create("#ff96959A")
                }
            }

            // divider
            Container {
                layout: StackLayout {
                    orientation: LayoutOrientation.LeftToRight
                }
                leftPadding: 10.0
                Container {
                    layoutProperties: StackLayoutProperties {
                        spaceQuota: 1
                    }
                    minHeight: 2.0
                    background: Color.create("#ffe9e9e9")
                }
            }

            Container {
                leftPadding: 10.0
                rightPadding: 10.0
                topPadding: 5.0
                bottomPadding: 5.0
                Label {
                    topMargin: 3.0
                    bottomMargin: 3.0
                    text: "Display resolution"
                    multiline: true
                }
                Label {
                    topMargin: 3.0
                    bottomMargin: 3.0
                    text: dresolutionx + "-by-" + dresolutiony + "-pixel resolution at " + dresolutiond + " ppi"
                    multiline: true
                    textStyle.color: Color.create("#ff96959A")
                }
            }

            // divider
            Container {
                layout: StackLayout {
                    orientation: LayoutOrientation.LeftToRight
                }
                leftPadding: 10.0
                Container {
                    layoutProperties: StackLayoutProperties {
                        spaceQuota: 1
                    }
                    minHeight: 2.0
                    background: Color.create("#ffe9e9e9")
                }
            }

            Container {
                leftPadding: 10.0
                rightPadding: 10.0
                topPadding: 5.0
                bottomPadding: 5.0
                Label {
                    topMargin: 3.0
                    bottomMargin: 3.0
                    text: "Display modes"
                    multiline: true
                }
                Label {
                    topMargin: 3.0
                    bottomMargin: 3.0
                    text: dmodes
                    multiline: true
                    textStyle.color: Color.create("#ff96959A")
                }
            }

            // divider
            Container {
                layout: StackLayout {
                    orientation: LayoutOrientation.LeftToRight
                }
                leftPadding: 10.0
                Container {
                    layoutProperties: StackLayoutProperties {
                        spaceQuota: 1
                    }
                    minHeight: 2.0
                    background: Color.create("#ffe9e9e9")
                }
            }
//---
            Container {
                leftPadding: 10.0
                rightPadding: 10.0
                topPadding: 5.0
                bottomPadding: 5.0
                Label {
                    topMargin: 3.0
                    bottomMargin: 3.0
                    text: "Secondary Display size"
                    multiline: true
                }
                Label {
                    topMargin: 3.0
                    bottomMargin: 3.0
                    text: dsize2 + "-inch (diagonal)"
                    multiline: true
                    textStyle.color: Color.create("#ff96959A")
                }
            }
            
            // divider
            Container {
                layout: StackLayout {
                    orientation: LayoutOrientation.LeftToRight
                }
                leftPadding: 10.0
                Container {
                    layoutProperties: StackLayoutProperties {
                        spaceQuota: 1
                    }
                    minHeight: 2.0
                    background: Color.create("#ffe9e9e9")
                }
            }
            
            Container {
                leftPadding: 10.0
                rightPadding: 10.0
                topPadding: 5.0
                bottomPadding: 5.0
                Label {
                    topMargin: 3.0
                    bottomMargin: 3.0
                    text: "Secondary Display type"
                    multiline: true
                }
                Label {
                    topMargin: 3.0
                    bottomMargin: 3.0
                    text: dtype2
                    multiline: true
                    textStyle.color: Color.create("#ff96959A")
                }
            }
            
            // divider
            Container {
                layout: StackLayout {
                    orientation: LayoutOrientation.LeftToRight
                }
                leftPadding: 10.0
                Container {
                    layoutProperties: StackLayoutProperties {
                        spaceQuota: 1
                    }
                    minHeight: 2.0
                    background: Color.create("#ffe9e9e9")
                }
            }
            
            Container {
                leftPadding: 10.0
                rightPadding: 10.0
                topPadding: 5.0
                bottomPadding: 5.0
                Label {
                    topMargin: 3.0
                    bottomMargin: 3.0
                    text: "Secondary Display resolution"
                    multiline: true
                }
                Label {
                    topMargin: 3.0
                    bottomMargin: 3.0
                    text: dresolutionx2 + "-by-" + dresolutiony2 + "-pixel resolution at " + dresolutiond2 + " ppi"
                    multiline: true
                    textStyle.color: Color.create("#ff96959A")
                }
            }
            
            // divider
            Container {
                layout: StackLayout {
                    orientation: LayoutOrientation.LeftToRight
                }
                leftPadding: 10.0
                Container {
                    layoutProperties: StackLayoutProperties {
                        spaceQuota: 1
                    }
                    minHeight: 2.0
                    background: Color.create("#ffe9e9e9")
                }
            }
            
            Container {
                leftPadding: 10.0
                rightPadding: 10.0
                topPadding: 5.0
                bottomPadding: 5.0
                Label {
                    topMargin: 3.0
                    bottomMargin: 3.0
                    text: "Secondary Display modes"
                    multiline: true
                }
                Label {
                    topMargin: 3.0
                    bottomMargin: 3.0
                    text: dmodes2
                    multiline: true
                    textStyle.color: Color.create("#ff96959A")
                }
            }
            
            // divider
            Container {
                layout: StackLayout {
                    orientation: LayoutOrientation.LeftToRight
                }
                leftPadding: 10.0
                Container {
                    layoutProperties: StackLayoutProperties {
                        spaceQuota: 1
                    }
                    minHeight: 2.0
                    background: Color.create("#ffe9e9e9")
                }
            }
//--
            Container {
                leftPadding: 10.0
                rightPadding: 10.0
                topPadding: 5.0
                bottomPadding: 5.0
                Label {
                    topMargin: 3.0
                    bottomMargin: 3.0
                    text: "Storage capacity"
                    multiline: true
                }
                Label {
                    topMargin: 3.0
                    bottomMargin: 3.0
                    text: capacity
                    multiline: true
                    textStyle.color: Color.create("#ff96959A")
                }
            }

            // divider
            Container {
                layout: StackLayout {
                    orientation: LayoutOrientation.LeftToRight
                }
                leftPadding: 10.0
                Container {
                    layoutProperties: StackLayoutProperties {
                        spaceQuota: 1
                    }
                    minHeight: 2.0
                    background: Color.create("#ffe9e9e9")
                }
            }

            Container {
                leftPadding: 10.0
                rightPadding: 10.0
                topPadding: 5.0
                bottomPadding: 5.0
                Label {
                    topMargin: 3.0
                    bottomMargin: 3.0
                    text: "Memory card slot"
                    multiline: true
                }
                Label {
                    topMargin: 3.0
                    bottomMargin: 3.0
                    text: slot
                    multiline: true
                    textStyle.color: Color.create("#ff96959A")
                }
            }

            // divider
            Container {
                layout: StackLayout {
                    orientation: LayoutOrientation.LeftToRight
                }
                leftPadding: 10.0
                Container {
                    layoutProperties: StackLayoutProperties {
                        spaceQuota: 1
                    }
                    minHeight: 2.0
                    background: Color.create("#ffe9e9e9")
                }
            }

            Container {
                leftPadding: 10.0
                rightPadding: 10.0
                topPadding: 5.0
                bottomPadding: 20.0
                Label {
                    topMargin: 3.0
                    bottomMargin: 3.0
                    text: "Memory card capacity"
                    multiline: true
                }
                Label {
                    topMargin: 3.0
                    bottomMargin: 3.0
                    text: slotcapacity
                    multiline: true
                    textStyle.color: Color.create("#ff96959A")
                }
            }

            // divider
            Container {
                layout: StackLayout {
                    orientation: LayoutOrientation.LeftToRight
                }
                Container {
                    layoutProperties: StackLayoutProperties {
                        spaceQuota: 1
                    }
                    minHeight: 2.0
                    background: Color.create("#ffe9e9e9")
                }
            }

            Container {
                background: Color.create("#fff0eff4")
                layout: StackLayout {
                    orientation: LayoutOrientation.LeftToRight
                }
                leftPadding: 10.0
                rightPadding: 10.0
                bottomPadding: 10.0
                topPadding: 10.0
                Label {
                    text: "CHIPSET AND MEMORY"
                    multiline: true
                    textStyle.color: Color.create("#ff96959A")
                    layoutProperties: StackLayoutProperties {
                        spaceQuota: 1
                    }
                }
            }

            // divider
            Container {
                layout: StackLayout {
                    orientation: LayoutOrientation.LeftToRight
                }
                Container {
                    layoutProperties: StackLayoutProperties {
                        spaceQuota: 1
                    }
                    minHeight: 2.0
                    background: Color.create("#ffe9e9e9")
                }
            }

            Container {
                leftPadding: 10.0
                rightPadding: 10.0
                topPadding: 5.0
                bottomPadding: 5.0
                Label {
                    topMargin: 3.0
                    bottomMargin: 3.0
                    text: "Chipset"
                    multiline: true
                }
                Label {
                    topMargin: 3.0
                    bottomMargin: 3.0
                    text: proc
                    multiline: true
                    textStyle.color: Color.create("#ff96959A")
                }
            }

            // divider
            Container {
                layout: StackLayout {
                    orientation: LayoutOrientation.LeftToRight
                }
                leftPadding: 10.0
                Container {
                    layoutProperties: StackLayoutProperties {
                        spaceQuota: 1
                    }
                    minHeight: 2.0
                    background: Color.create("#ffe9e9e9")
                }
            }

            Container {
                leftPadding: 10.0
                rightPadding: 10.0
                topPadding: 5.0
                bottomPadding: 5.0
                Label {
                    topMargin: 3.0
                    bottomMargin: 3.0
                    text: "Processor Cores"
                    multiline: true
                }
                Label {
                    topMargin: 3.0
                    bottomMargin: 3.0
                    text: cores
                    multiline: true
                    textStyle.color: Color.create("#ff96959A")
                }
            }

            // divider
            Container {
                layout: StackLayout {
                    orientation: LayoutOrientation.LeftToRight
                }
                leftPadding: 10.0
                Container {
                    layoutProperties: StackLayoutProperties {
                        spaceQuota: 1
                    }
                    minHeight: 2.0
                    background: Color.create("#ffe9e9e9")
                }
            }

            Container {
                leftPadding: 10.0
                rightPadding: 10.0
                topPadding: 5.0
                bottomPadding: 5.0
                Label {
                    topMargin: 3.0
                    bottomMargin: 3.0
                    text: "Processor Frequency"
                    multiline: true
                }
                Label {
                    topMargin: 3.0
                    bottomMargin: 3.0
                    text: speed
                    multiline: true
                    textStyle.color: Color.create("#ff96959A")
                }
            }

            // divider
            Container {
                layout: StackLayout {
                    orientation: LayoutOrientation.LeftToRight
                }
                leftPadding: 10.0
                Container {
                    layoutProperties: StackLayoutProperties {
                        spaceQuota: 1
                    }
                    minHeight: 2.0
                    background: Color.create("#ffe9e9e9")
                }
            }

            Container {
                leftPadding: 10.0
                rightPadding: 10.0
                topPadding: 5.0
                bottomPadding: 5.0
                Label {
                    topMargin: 3.0
                    bottomMargin: 3.0
                    text: "Architecture"
                    multiline: true
                }
                Label {
                    topMargin: 3.0
                    bottomMargin: 3.0
                    text: architecture
                    multiline: true
                    textStyle.color: Color.create("#ff96959A")
                }
            }

            // divider
            Container {
                layout: StackLayout {
                    orientation: LayoutOrientation.LeftToRight
                }
                leftPadding: 10.0
                Container {
                    layoutProperties: StackLayoutProperties {
                        spaceQuota: 1
                    }
                    minHeight: 2.0
                    background: Color.create("#ffe9e9e9")
                }
            }

            Container {
                leftPadding: 10.0
                rightPadding: 10.0
                topPadding: 5.0
                bottomPadding: 5.0
                Label {
                    topMargin: 3.0
                    bottomMargin: 3.0
                    text: "Graphics"
                    multiline: true
                }
                Label {
                    topMargin: 3.0
                    bottomMargin: 3.0
                    text: graphics
                    multiline: true
                    textStyle.color: Color.create("#ff96959A")
                }
            }

            // divider
            Container {
                layout: StackLayout {
                    orientation: LayoutOrientation.LeftToRight
                }
                leftPadding: 10.0
                Container {
                    layoutProperties: StackLayoutProperties {
                        spaceQuota: 1
                    }
                    minHeight: 2.0
                    background: Color.create("#ffe9e9e9")
                }
            }

            Container {
                leftPadding: 10.0
                rightPadding: 10.0
                topPadding: 5.0
                bottomPadding: 20.0
                Label {
                    topMargin: 3.0
                    bottomMargin: 3.0
                    text: "Memory"
                    multiline: true
                }
                Label {
                    topMargin: 3.0
                    bottomMargin: 3.0
                    text: memory
                    multiline: true
                    textStyle.color: Color.create("#ff96959A")
                }
            }

            // divider
            Container {
                layout: StackLayout {
                    orientation: LayoutOrientation.LeftToRight
                }
                Container {
                    layoutProperties: StackLayoutProperties {
                        spaceQuota: 1
                    }
                    minHeight: 2.0
                    background: Color.create("#ffe9e9e9")
                }
            }

            Container {
                background: Color.create("#fff0eff4")
                layout: StackLayout {
                    orientation: LayoutOrientation.LeftToRight
                }
                leftPadding: 10.0
                rightPadding: 10.0
                bottomPadding: 10.0
                topPadding: 10.0
                Label {
                    text: "BATTERY"
                    multiline: true
                    textStyle.color: Color.create("#ff96959A")
                    layoutProperties: StackLayoutProperties {
                        spaceQuota: 1
                    }
                }
            }

            // divider
            Container {
                layout: StackLayout {
                    orientation: LayoutOrientation.LeftToRight
                }
                Container {
                    layoutProperties: StackLayoutProperties {
                        spaceQuota: 1
                    }
                    minHeight: 2.0
                    background: Color.create("#ffe9e9e9")
                }
            }

            Container {
                leftPadding: 10.0
                rightPadding: 10.0
                topPadding: 5.0
                bottomPadding: 5.0
                Label {
                    topMargin: 3.0
                    bottomMargin: 3.0
                    text: "System Battery"
                    multiline: true
                }
                Label {
                    topMargin: 3.0
                    bottomMargin: 3.0
                    text: battery
                    multiline: true
                    textStyle.color: Color.create("#ff96959A")
                }
            }

            // divider
            Container {
                layout: StackLayout {
                    orientation: LayoutOrientation.LeftToRight
                }
                leftPadding: 10.0
                Container {
                    layoutProperties: StackLayoutProperties {
                        spaceQuota: 1
                    }
                    minHeight: 2.0
                    background: Color.create("#ffe9e9e9")
                }
            }

            Container {
                leftPadding: 10.0
                rightPadding: 10.0
                topPadding: 5.0
                bottomPadding: 20.0
                Label {
                    topMargin: 3.0
                    bottomMargin: 3.0
                    text: "Battery Usage"
                    multiline: true
                }
                Label {
                    topMargin: 3.0
                    bottomMargin: 3.0
                    text: batteryusage
                    multiline: true
                    textStyle.color: Color.create("#ff96959A")
                }
            }
        }
    }
    actions: [
        ActionItem {
            title: "Software"
            ActionBar.placement: ActionBarPlacement.OnBar
            imageSource: "asset:///data/images/icons/ic_sw.png"
            onTriggered: {
                tmpDataModel.source = "asset:///data/products/smartphones/sw/sw-" + fileBase;
                var item = tmpDataModel.data([ 0 ]);
                var swPageContent = swPage.createObject();
                swPageContent.titleBarText = item.title;
                swPageContent.imgsrc = imgsrc;
                swPageContent.osorig = item.osorig;
                swPageContent.osmax = item.osmax;
                swPageContent.syncsw = item.syncsw;
                swPageContent.sysreq = item.sysreq;
                navigationPane.push(swPageContent);
                console.log("software");
            }
        },
        ActionItem {
            title: "Connections"
            ActionBar.placement: ActionBarPlacement.OnBar
            imageSource: "asset:///data/images/icons/ic_conn.png"
            onTriggered: {
                tmpDataModel.source = "asset:///data/products/smartphones/conn/conn-" + fileBase;
                var item = tmpDataModel.data([ 0 ]);
                var connPageContent = connPage.createObject();
                connPageContent.titleBarText = item.title;
                connPageContent.imgsrc = imgsrc;
                connPageContent.networks = item.networks;
                connPageContent.ports = item.ports;
                connPageContent.wifi = item.wifi;
                connPageContent.bt = item.bt;
                connPageContent.nfc = item.nfc;
                connPageContent.sim = item.sim;
                connPageContent.loc = item.loc;
                connPageContent.sensors = item.sensors;
                connPageContent.alerts = item.alerts;
                navigationPane.push(connPageContent);
                console.log("connections");
            }
        },
        ActionItem {
            title: "Media"
            ActionBar.placement: ActionBarPlacement.InOverflow
            imageSource: "asset:///data/images/icons/ic_media.png"
            onTriggered: {
                tmpDataModel.source = "asset:///data/products/smartphones/media/media-" + fileBase;
                var item = tmpDataModel.data([ 0 ]);
                var mediaPageContent = mediaPage.createObject();
                mediaPageContent.titleBarText = item.title;
                mediaPageContent.imgsrc = imgsrc;
                mediaPageContent.cameraf = item.cameraf;
                mediaPageContent.camerar = item.camerar;
                mediaPageContent.formats = item.formats;
                mediaPageContent.encdec = item.encdec;
                mediaPageContent.image = item.image;
                mediaPageContent.sharing = item.sharing;
                navigationPane.push(mediaPageContent);
                console.log("media");
            }
        },
        ActionItem {
            ActionBar.placement: ActionBarPlacement.InOverflow
            id: pictureAction
            title: "Product Image"
            imageSource: "asset:///data/images/icons/ic_picture.png"
            onTriggered: {
                var picturePageContent = picturePage.createObject();
                picturePageContent.imgsrc = imgsrc;
                navigationPane.push(picturePageContent);
            }
        }
    ]
    attachedObjects: [
        ComponentDefinition {
            id: swPage
            source: "smartphoneSwPage.qml"
        },
        ComponentDefinition {
            id: connPage
            source: "smartphoneConnPage.qml"
        },
        ComponentDefinition {
            id: mediaPage
            source: "smartphoneMediaPage.qml"
        },
        ComponentDefinition {
            id: picturePage
            source: "smartphonePicturePage.qml"
        },
        XmlDataModel {
            id: tmpDataModel
            source: tmpContentSource
        }
    ]
    
    onImgsrcChanged: {
        productImage.imageSource = "asset:///data/products/smartphones/images/" + imgsrc;
    }

    property NavigationPane navigationPane
    property string fileBase: ""
    property string tmpContentSource: ""

    property string titleBarText: ""
    property string imgsrc: ""
    property string introduced: ""
    property string discontinued: ""
    property string identifier: ""
    property string number: ""
    property string price: ""
    property string support: ""
    property string inputmethods: ""
    property string colors: ""
    
    property string dimx: ""
    property string dimy: ""
    property string dimz: ""
    property string dimxi: ""
    property string dimyi: ""
    property string dimzi: ""

    property string dimx2: ""
    property string dimy2: ""
    property string dimz2: ""
    property string dimxi2: ""
    property string dimyi2: ""
    property string dimzi2: ""
    
    property string weight: ""
    property string weightoz: ""
    property string dsize: ""
    property string dtype: ""
    property string dresolutionx: ""
    property string dresolutiony: ""
    property string dresolutiond: ""
    property string dmodes: ""

    property string dsize2: ""
    property string dtype2: ""
    property string dresolutionx2: ""
    property string dresolutiony2: ""
    property string dresolutiond2: ""
    property string dmodes2: ""

    property string capacity: ""
    property string slot: ""
    property string slotcapacity: ""
    property string proc: ""
    property string speed: ""
    property string cores: ""
    property string architecture: ""
    property string graphics: ""
    property string memory: ""
    property string battery: ""
    property string batteryusage: ""
}
