import bb.cascades 1.0

Page {
    property string imgsrc: ""
    
    content: Container {
        layout: DockLayout {
        }
        background: Color.White
        Container {
            horizontalAlignment: HorizontalAlignment.Center
            verticalAlignment: VerticalAlignment.Center
            ImageView {
                id: productImage
            }            
        }
    }
    
    onImgsrcChanged: {
        productImage.imageSource = "asset:///data/products/tablets/images/big/" + imgsrc;
    }
}
