import bb.cascades 1.0

Page {
    property string contentSource: ""
    property string tmpContentSource: ""
    property string timeSource: ""
    property string titleBarText: ""
    property NavigationPane navigationPane
    titleBar: TitleBar {
        kind: TitleBarKind.Default
        title: titleBarText
    }
    Container {
        background: Color.White
        ListView {
            scrollRole: ScrollRole.Main
            dataModel: XmlDataModel {
                source: contentSource
            }
            listItemComponents: [
                ListItemComponent {
                    type: "item"
                    Container {
                        id: listItemComponentContainer
                        topMargin: 0.0
                        bottomMargin: 0.0
                        leftMargin: 0.0
                        rightMargin: 0.0
                        background: listItemComponentContainer.ListItem.active ? Color.create("#fff0eff4") : Color.Transparent // : Color.White
                        Container {
                            topPadding: 20.0
                            bottomPadding: 20.0
                            layout: StackLayout {
                                orientation: LayoutOrientation.LeftToRight
                            }
                            Container {
                                leftPadding: 10.0
                                ImageView {
                                    imageSource: "asset:///data/products/" + ListItemData.image
                                }
                            }
                            Container {
                                leftPadding: 15.0
                                verticalAlignment: VerticalAlignment.Center
                                Label {
                                    text: ListItemData.title
                                    multiline: true
                                    textStyle.fontSizeValue: 120.0
                                    textStyle {
                                        fontSize: FontSize.PercentageValue
                                        fontWeight: FontWeight.W400
                                        color: Color.Black
                                    }
                                }
                            }
                        }
                        // divider
                        Container {
                            layout: StackLayout {
                                orientation: LayoutOrientation.LeftToRight
                            }
                            Container {
                                layoutProperties: StackLayoutProperties {
                                    spaceQuota: 1
                                }
                                minHeight: 2.0
                                background: Color.create("#ffe9e9e9")
                            }
                        }
                    }
                }
            ]
            onTriggered: {
                var chosenItem = dataModel.data(indexPath);
                if (chosenItem.type == "smartphone") {
                    tmpDataModel.source = "asset:///data/products/smartphones/" + chosenItem.filename;
                    var item = tmpDataModel.data([ 0 ]);
                    var smartphonePageContent = smartphoneGeneralPage.createObject();
                    smartphonePageContent.fileBase = chosenItem.filename;
                    smartphonePageContent.navigationPane = navigationPane;
                    smartphonePageContent.titleBarText = item.title;
                    smartphonePageContent.imgsrc = item.image;
                    smartphonePageContent.introduced = item.introduced;
                    smartphonePageContent.discontinued = item.discontinued;
                    smartphonePageContent.identifier = item.identifier;
                    smartphonePageContent.number = item.number;
                    smartphonePageContent.price = item.price;
                    smartphonePageContent.support = item.support;
                    smartphonePageContent.inputmethods = item.inputmethods;
                    smartphonePageContent.colors = item.colors;
                    smartphonePageContent.dimx = item.dimx;
                    smartphonePageContent.dimy = item.dimy;
                    smartphonePageContent.dimz = item.dimz;
                    smartphonePageContent.dimxi = item.dimxi;
                    smartphonePageContent.dimyi = item.dimyi;
                    smartphonePageContent.dimzi = item.dimzi;
                    smartphonePageContent.weight = item.weight;
                    smartphonePageContent.weightoz = item.weightoz;
                    smartphonePageContent.dsize = item.dsize;
                    smartphonePageContent.dtype = item.dtype;
                    smartphonePageContent.dresolutionx = item.dresolutionx;
                    smartphonePageContent.dresolutiony = item.dresolutiony;
                    smartphonePageContent.dresolutiond = item.dresolutiond;
                    smartphonePageContent.dmodes = item.dmodes;
                    smartphonePageContent.capacity = item.capacity;
                    smartphonePageContent.slot = item.slot;
                    smartphonePageContent.slotcapacity = item.slotcapacity;
                    smartphonePageContent.proc = item.proc;
                    smartphonePageContent.speed = item.speed;
                    smartphonePageContent.cores = item.cores;
                    smartphonePageContent.architecture = item.architecture
                    smartphonePageContent.graphics = item.graphics;
                    smartphonePageContent.memory = item.memory;
                    smartphonePageContent.battery = item.battery;
                    smartphonePageContent.batteryusage = item.batteryusage;
                    navigationPane.push(smartphonePageContent);
                    console.log("showing smartphone");
                } else if (chosenItem.type == "smartphone2d") {
                    tmpDataModel.source = "asset:///data/products/smartphones/" + chosenItem.filename;
                    var item = tmpDataModel.data([ 0 ]);
                    var smartphonePageContent = smartphone2dGeneralPage.createObject();
                    smartphonePageContent.fileBase = chosenItem.filename;
                    smartphonePageContent.navigationPane = navigationPane;
                    smartphonePageContent.titleBarText = item.title;
                    smartphonePageContent.imgsrc = item.image;
                    smartphonePageContent.introduced = item.introduced;
                    smartphonePageContent.discontinued = item.discontinued;
                    smartphonePageContent.identifier = item.identifier;
                    smartphonePageContent.number = item.number;
                    smartphonePageContent.price = item.price;
                    smartphonePageContent.support = item.support;
                    smartphonePageContent.inputmethods = item.inputmethods;
                    smartphonePageContent.colors = item.colors;
                    smartphonePageContent.dimx = item.dimx;
                    smartphonePageContent.dimy = item.dimy;
                    smartphonePageContent.dimz = item.dimz;
                    smartphonePageContent.dimxi = item.dimxi;
                    smartphonePageContent.dimyi = item.dimyi;
                    smartphonePageContent.dimzi = item.dimzi;
                    smartphonePageContent.dimx2 = item.dimx2;
                    smartphonePageContent.dimy2 = item.dimy2;
                    smartphonePageContent.dimz2 = item.dimz2;
                    smartphonePageContent.dimxi2 = item.dimxi2;
                    smartphonePageContent.dimyi2 = item.dimyi2;
                    smartphonePageContent.dimzi2 = item.dimzi2;
                    smartphonePageContent.weight = item.weight;
                    smartphonePageContent.weightoz = item.weightoz;
                    smartphonePageContent.dsize = item.dsize;
                    smartphonePageContent.dtype = item.dtype;
                    smartphonePageContent.dresolutionx = item.dresolutionx;
                    smartphonePageContent.dresolutiony = item.dresolutiony;
                    smartphonePageContent.dresolutiond = item.dresolutiond;
                    smartphonePageContent.dmodes = item.dmodes;
                    smartphonePageContent.dsize2 = item.dsize2;
                    smartphonePageContent.dtype2 = item.dtype2;
                    smartphonePageContent.dresolutionx2 = item.dresolutionx2;
                    smartphonePageContent.dresolutiony2 = item.dresolutiony2;
                    smartphonePageContent.dresolutiond2 = item.dresolutiond2;
                    smartphonePageContent.dmodes2 = item.dmodes2;
                    smartphonePageContent.capacity = item.capacity;
                    smartphonePageContent.slot = item.slot;
                    smartphonePageContent.slotcapacity = item.slotcapacity;
                    smartphonePageContent.proc = item.proc;
                    smartphonePageContent.speed = item.speed;
                    smartphonePageContent.cores = item.cores;
                    smartphonePageContent.architecture = item.architecture
                    smartphonePageContent.graphics = item.graphics;
                    smartphonePageContent.memory = item.memory;
                    smartphonePageContent.battery = item.battery;
                    smartphonePageContent.batteryusage = item.batteryusage;
                    navigationPane.push(smartphonePageContent);
                    console.log("showing smartphone with 2 displays");
                } else if (chosenItem.type == "tablet") {
                    tmpDataModel.source = "asset:///data/products/tablets/" + chosenItem.filename;
                    var item = tmpDataModel.data([ 0 ]);
                    var tabletPageContent = tabletGeneralPage.createObject();
                    tabletPageContent.fileBase = chosenItem.filename;
                    tabletPageContent.navigationPane = navigationPane;
                    tabletPageContent.titleBarText = item.title;
                    tabletPageContent.imgsrc = item.image;
                    tabletPageContent.introduced = item.introduced;
                    tabletPageContent.discontinued = item.discontinued;
                    tabletPageContent.identifier = item.identifier;
                    tabletPageContent.number = item.number;
                    tabletPageContent.price = item.price;
                    tabletPageContent.support = item.support;
                    tabletPageContent.inputmethods = item.inputmethods;
                    tabletPageContent.colors = item.colors;
                    tabletPageContent.dimx = item.dimx;
                    tabletPageContent.dimy = item.dimy;
                    tabletPageContent.dimz = item.dimz;
                    tabletPageContent.dimxi = item.dimxi;
                    tabletPageContent.dimyi = item.dimyi;
                    tabletPageContent.dimzi = item.dimzi;
                    tabletPageContent.weight = item.weight;
                    tabletPageContent.weightoz = item.weightoz;
                    tabletPageContent.dsize = item.dsize;
                    tabletPageContent.dtype = item.dtype;
                    tabletPageContent.dresolutionx = item.dresolutionx;
                    tabletPageContent.dresolutiony = item.dresolutiony;
                    tabletPageContent.dresolutiond = item.dresolutiond;
                    tabletPageContent.dmodes = item.dmodes;
                    tabletPageContent.capacity = item.capacity;
                    tabletPageContent.slot = item.slot;
                    tabletPageContent.slotcapacity = item.slotcapacity;
                    tabletPageContent.proc = item.proc;
                    tabletPageContent.speed = item.speed;
                    tabletPageContent.cores = item.cores;
                    tabletPageContent.architecture = item.architecture
                    tabletPageContent.graphics = item.graphics;
                    tabletPageContent.memory = item.memory;
                    tabletPageContent.battery = item.battery;
                    tabletPageContent.batteryusage = item.batteryusage;
                    navigationPane.push(tabletPageContent);
                    console.log("showing tablet");
                } else if (chosenItem.type == "audiodevice") {
                    tmpDataModel.source = "asset:///data/products/devices/" + chosenItem.filename;
                    var item = tmpDataModel.data([ 0 ]);
                    var audioDevicePageContent = audioDeviceGeneralPage.createObject();
                    audioDevicePageContent.fileBase = chosenItem.filename;
                    audioDevicePageContent.navigationPane = navigationPane;
                    audioDevicePageContent.titleBarText = item.title;
                    audioDevicePageContent.imgsrc = item.image;
                    audioDevicePageContent.introduced = item.introduced;
                    audioDevicePageContent.type = item.type;
                    audioDevicePageContent.bt = item.bt;
                    audioDevicePageContent.wlan = item.wlan;
                    audioDevicePageContent.nfc = item.nfc;
                    audioDevicePageContent.audio = item.audio;
                    audioDevicePageContent.video = item.video;
                    audioDevicePageContent.usb = item.usb;
                    audioDevicePageContent.batteryusage = item.batteryusage;
                    navigationPane.push(audioDevicePageContent);
                    console.log("showing audio device");                    
                } else if (chosenItem.type == "videodevice") {
                    tmpDataModel.source = "asset:///data/products/devices/" + chosenItem.filename;
                    var item = tmpDataModel.data([ 0 ]);
                    var videoDevicePageContent = videoDeviceGeneralPage.createObject();
                    videoDevicePageContent.fileBase = chosenItem.filename;
                    videoDevicePageContent.navigationPane = navigationPane;
                    videoDevicePageContent.titleBarText = item.title;
                    videoDevicePageContent.imgsrc = item.image;
                    videoDevicePageContent.introduced = item.introduced;
                    videoDevicePageContent.type = item.type;
                    videoDevicePageContent.bt = item.bt;
                    videoDevicePageContent.wlan = item.wlan;
                    videoDevicePageContent.nfc = item.nfc;
                    videoDevicePageContent.audio = item.audio;
                    videoDevicePageContent.video = item.video;
                    videoDevicePageContent.usb = item.usb;
                    videoDevicePageContent.batteryusage = item.batteryusage;
                    navigationPane.push(videoDevicePageContent);
                } else if (chosenItem.type == "list") {
                    var listPageContent = listPage.createObject();
                    listPageContent.contentSource = "asset:///data/" + chosenItem.filename;
                    listPageContent.navigationPane = navigationPane;
                    listPageContent.titleBarText = chosenItem.title;
                    navigationPane.push(listPageContent);
                    console.log("showing list");
                } else {
                    //do nothing
                }
            }
        }
    }
    actions: [
        ActionItem {
            title: qsTr("Search");
            ActionBar.placement: ActionBarPlacement.OnBar

            imageSource: "asset:///data/images/icons/ic_search.png"
            onTriggered: {
            	var searchPageContent = searchPage.createObject();
                navigationPane.push(searchPageContent);
                searchPageContent.readyForFocus = true;                
            }           
        }/*,
        ActionItem {
            title: "čas.osa"
            ActionBar.placement: ActionBarPlacement.InOverflow
            enabled: { (timeSource != "") ? true : false }
            onTriggered: {
                var timePageContent = timePage.createObject();
                navigationPane.push(timePageContent);
            }
        }*/
    ]
    attachedObjects: [
        ComponentDefinition {
            id: listPage
            source: "ListPage.qml"
        },
        ComponentDefinition {
            id: audioDeviceGeneralPage
            source: "audioDeviceGeneralPage.qml"
        },
        ComponentDefinition {
            id: searchPage
            source: "search.qml"
        },
        ComponentDefinition {
            id: smartphoneGeneralPage
            source: "smartphoneGeneralPage.qml"
        },
        ComponentDefinition {
            id: smartphone2dGeneralPage
            source: "smartphone2dGeneralPage.qml"
        },
        ComponentDefinition {
            id: tabletGeneralPage
            source: "tabletGeneralPage.qml"
        },
        ComponentDefinition {
            id: videoDeviceGeneralPage
            source: "videoDeviceGeneralPage.qml"
        },
        ComponentDefinition {
            id: timePage
            source: "timePage.qml"
        },
        XmlDataModel {
            id: tmpDataModel
            source: tmpContentSource
        }
    ]
}
