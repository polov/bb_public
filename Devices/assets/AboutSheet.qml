import bb.cascades 1.3

Sheet {
    id: aboutSheetContent
    Page {
        content: ScrollView {
            Container {
                leftPadding: 10.0
                rightPadding: 10.0
                bottomPadding: 10.0
                //logo
                Container {
                    layout: StackLayout {
                        orientation: LayoutOrientation.LeftToRight
                    }
                    Container {
                        topPadding: 40.0
                        layoutProperties: StackLayoutProperties {
                            spaceQuota: 1
                        }
                        ImageView {
                            horizontalAlignment: HorizontalAlignment.Center
                            imageSource: "asset:///data/images/about_logo.png"
                        }
                    }
                }
                //title
                Container {
                    layout: StackLayout {
                        orientation: LayoutOrientation.LeftToRight
                    }
                    Container {
                        topPadding: 40.0
                        layoutProperties: StackLayoutProperties {
                            spaceQuota: 1
                        }
                        Label {
                            horizontalAlignment: HorizontalAlignment.Center
                            text: "Devices"
                            textStyle.fontSizeValue: 140.0
                            textStyle {
                                fontSize: FontSize.PercentageValue
                                fontWeight: FontWeight.W500
                            }
                        }
                    }
                }
                //version
                Container {
                    layout: StackLayout {
                        orientation: LayoutOrientation.LeftToRight
                    }
                    Container {
                        topPadding: 10.0
                        layoutProperties: StackLayoutProperties {
                            spaceQuota: 1
                        }
                        Label {
                            horizontalAlignment: HorizontalAlignment.Center
                            text: "version 1.2.0"
                        }
                    }
                }
                
                Container {
                    layout: StackLayout {
                        orientation: LayoutOrientation.LeftToRight
                    }
                    Container {
                        topPadding: 40.0
                        layoutProperties: StackLayoutProperties {
                            spaceQuota: 1
                        }
                        Label {
                            horizontalAlignment: HorizontalAlignment.Center
                            text: "build by" + " <a mimetype=\"application/x-bb-appworld\" href=\"appworld://vendorpage/63768\">Martin Polovinčák</a>"
                            multiline: true
                            textFormat: TextFormat.Html
                        }
                    }
                }

                Container {
                    layout: StackLayout {
                        orientation: LayoutOrientation.LeftToRight
                    }
                    Container {
                        topPadding: 40.0
                        layoutProperties: StackLayoutProperties {
                            spaceQuota: 1
                        }
                        Label {
                            horizontalAlignment: HorizontalAlignment.Center
                            text: "<a href=\"http://www.polovincak.eu/privacy_policy.html\">" + "Privacy policy" + "</a>"
                            multiline: true
                            textFormat: TextFormat.Html
                        }
                    }
                }
                
                Container {
                    layout: StackLayout {
                        orientation: LayoutOrientation.LeftToRight
                    }
                    Container {
                        topPadding: 40.0
                        layoutProperties: StackLayoutProperties {
                            spaceQuota: 1
                        }
                        Label {
                            horizontalAlignment: HorizontalAlignment.Center
                            text: "PLEASE READ THIS SOFTWARE LICENSE AGREEMENT CAREFULLY BEFORE USING Devices. BY USING Devices, YOU ARE AGREEING TO BE BOUND BY THE TERMS OF THIS LICENSE. IF YOU DO NOT AGREE TO THE TERMS OF THIS LICENSE, DO NOT USE Devices."
                            multiline: true
                        }
                    }
                }
                Container {
                    layout: StackLayout {
                        orientation: LayoutOrientation.LeftToRight
                    }
                    Container {
                        topPadding: 40.0
                        layoutProperties: StackLayoutProperties {
                            spaceQuota: 1
                        }
                        Label {
                            horizontalAlignment: HorizontalAlignment.Center
                            text: "Use this software, Devices, at your own risk. The author of this software, Martin Polovinčák, makes no claims as to the fitness of this software. It is provided \"as is\" without express or implied warranty. Prohibited uses include modifying the Software. Any modified or merged portion of the Software is subject to this agreement, reverse-engineering, disassemble, decompile, or make any attempt to discover the source code to the Software, create derivative works based on the Software, remove, obscure, or alter any copyright notice or other proprietary rights related to the Software or Package including manuals is forbidden. You may not rent, lease, lend, redistribute or sublicense the Software without written consent from the author. Any feedback you provide regarding Devices shall be deemed to be non-confidential. The author shall be free to use such information on an unrestricted basis."
                            multiline: true
                        }
                    }
                }          
                Container {
                    layout: StackLayout {
                        orientation: LayoutOrientation.LeftToRight
                    }
                    Container {
                        topPadding: 40.0
                        layoutProperties: StackLayoutProperties {
                            spaceQuota: 1
                        }
                        Label {
                            horizontalAlignment: HorizontalAlignment.Center
                            text: "THE AUTHOR DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE, INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS, IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, INDIRECT OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE."
                            multiline: true
                        }
                    }
                }
                Container {
                    layout: StackLayout {
                        orientation: LayoutOrientation.LeftToRight
                    }
                    Container {
                        topPadding: 40.0
                        layoutProperties: StackLayoutProperties {
                            spaceQuota: 1
                        }
                        Label {
                            horizontalAlignment: HorizontalAlignment.Left
                            text: "All company and/or product names may be trade names, trademarks and/or registered trademarks of the respective owners with which they are associated."
                            multiline: true
                        }
                    }
                }
            }
        }
        actions: [
            ActionItem {
                title: qsTr("Done")
                ActionBar.placement: ActionBarPlacement.OnBar
                imageSource: "asset:///data/images/icons/ic_done.png"
                onTriggered: {
                aboutSheetContent.close();
                }
            }
        ]
    }
}
