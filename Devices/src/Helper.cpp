/*
 * Helper.cpp
 *
 *  Created on: Jan 3, 2014
 *      Author: martin
 */

#include "Helper.hpp"

#include <QDebug>
#include <bb/system/InvokeReply>
#include <bb/system/InvokeRequest>
#include <bb/cascades/GroupDataModel>

#include <QFile>
#include <QDir>

#include <bb/data/XmlDataAccess>

Helper::Helper() :
		QObject(), m_invokeManager(new bb::system::InvokeManager(this)), _model(
				NULL), _fmodel(NULL) {
}

Helper::~Helper() {
}

void Helper::invokeEmail(QString subject) {
	qDebug() << "email invocation, subject:" << subject;
	bb::system::InvokeRequest request;
	request.setTarget("sys.pim.uib.email.hybridcomposer");
	request.setAction("bb.action.SENDEMAIL");
	request.setUri(
			QUrl(
					"mailto:support@polovincak.eu?subject="
							+ subject.replace(" ", "%20")));
	//InvokeTargetReply *reply = invokeManager.invoke(request);
	m_invokeManager->invoke(request);
}

bb::cascades::GroupDataModel* Helper::getFilteredModel(
		const QString searchString) {

	if (_model == NULL) {

		bb::data::XmlDataAccess xda;
		QVariant list = xda.load(
				QDir::currentPath() + "/app/native/assets/data/search.xml",
				"/root/item");
		if (xda.hasError()) {
			qDebug() << xda.error().errorMessage() << xda.error().errorType();
		}
		_model = new bb::cascades::GroupDataModel(QStringList() << "title");
		_model->insertList(list.value<QVariantList>());

		qDebug() << "Got model, size:" << _model->size();
	}

	if (_model->size() > 0) {
		if (_fmodel == NULL) {
			_fmodel = new bb::cascades::GroupDataModel(
					QStringList() << "title");
		}
		QList<QVariantMap> list = _model->toListOfMaps();
		QVariantList filteredItems;
		foreach(QVariantMap map, list)
		{
			if (map["title"].toString().toLower().contains(
					searchString.toLower())) {
				filteredItems << map;
			}
		}
		_fmodel->clear();
		_fmodel->insertList(filteredItems);
	}

	return _fmodel;
}
