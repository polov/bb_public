#ifndef ApplicationUI_HPP_
#define ApplicationUI_HPP_

#include <QObject>

#include "Helper.hpp"

namespace bb {
namespace cascades {
class Application;
class LocaleHandler;
class NavigationPane;
}
namespace system {
class InvokeManager;
}
}

class QTranslator;

/*!
 * @brief Application object
 *
 *
 */

class ApplicationUI: public QObject {
Q_OBJECT
public:
	ApplicationUI(bb::cascades::Application *app);
	virtual ~ApplicationUI() {
	}
private slots:
	void onSystemLanguageChanged();
public slots:
	void onInvoked(const bb::system::InvokeRequest& invokeRequest);
private:
	QTranslator* m_pTranslator;
	bb::cascades::LocaleHandler* m_pLocaleHandler;

	Helper *_helper;
	bb::system::InvokeManager *_invokeManager;
	bb::cascades::NavigationPane *_navigationPane;
};

#endif /* ApplicationUI_HPP_ */
