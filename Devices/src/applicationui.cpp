#include "applicationui.hpp"

#include <bb/cascades/Application>
#include <bb/cascades/QmlDocument>
#include <bb/cascades/LocaleHandler>
#include <bb/cascades/AbstractPane>
#include <bb/cascades/NavigationPane>
#include <bb/system/InvokeManager>

using namespace bb::cascades;
using namespace bb::system;

ApplicationUI::ApplicationUI(bb::cascades::Application *app) :
		QObject(app) {
	// prepare the localization
	m_pTranslator = new QTranslator(this);
	m_pLocaleHandler = new LocaleHandler(this);
	if (!QObject::connect(m_pLocaleHandler, SIGNAL(systemLanguageChanged()),
			this, SLOT(onSystemLanguageChanged()))) {
		// This is an abnormal situation! Something went wrong!
		// Add own code to recover here
		qWarning() << "Recovering from a failed connect()";
	}

	_invokeManager = new InvokeManager(this);
	connect(_invokeManager, SIGNAL(invoked(const bb::system::InvokeRequest&)),
			this, SLOT(onInvoked(const bb::system::InvokeRequest&)));

	// initial load
	onSystemLanguageChanged();

	// Create scene document from main.qml asset, the parent is set
	// to ensure the document gets destroyed properly at shut down.
	QmlDocument *qml = QmlDocument::create("asset:///main.qml").parent(this);

	_helper = new Helper();
	qml->setContextProperty("helper", _helper);

	// Create root object for the UI
	AbstractPane *root = qml->createRootObject<AbstractPane>();

	// Set created root object as the application scene
	app->setScene(root);

	//_navigationPane = root->findChild<NavigationPane*>("mynavigation");
	_navigationPane = static_cast<NavigationPane *>(root);

}

void ApplicationUI::onSystemLanguageChanged() {
	QCoreApplication::instance()->removeTranslator(m_pTranslator);
	// Initiate, load and install the application translation files.
	QString locale_string = QLocale().name();
	QString file_name = QString("BB_Tracker_%1").arg(locale_string);
	if (m_pTranslator->load(file_name, "app/native/qm")) {
		QCoreApplication::instance()->installTranslator(m_pTranslator);
	}
}

void ApplicationUI::onInvoked(const bb::system::InvokeRequest& invokeRequest) {

	if (invokeRequest.action() == "bb.action.SEARCH.EXTENDED") {
		QmlDocument *qml = QmlDocument::create("asset:///search.qml").parent(this).property("navigationPane", _navigationPane);

		qml->setContextProperty("helper", _helper);

		Page *searchPage = qml->createRootObject<Page>();
		searchPage->setProperty("searchString", QString(invokeRequest.data()));

		if(_navigationPane!=NULL) {
			_navigationPane->push(searchPage);
		} else {
			qDebug() << "There is no navigation pane!";
		}
	} else {
		// some other invocation..
	}
}
