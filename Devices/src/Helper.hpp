/*
 * Helper.hpp
 *
 *  Created on: Jan 3, 2014
 *      Author: martin
 */

#ifndef HELPER_HPP_
#define HELPER_HPP_

#include <QObject>
#include <bb/system/InvokeManager>

namespace bb {
namespace cascades {
class GroupDataModel;
}
}

class Helper: public QObject {
Q_OBJECT

public:
	Helper();
	virtual ~Helper();
	Q_INVOKABLE
	void invokeEmail(QString subject);
	Q_INVOKABLE
	bb::cascades::GroupDataModel* getFilteredModel(const QString searchString);
private:
	bb::system::InvokeManager* m_invokeManager;
	bb::cascades::GroupDataModel* _model;
	bb::cascades::GroupDataModel* _fmodel;
};

#endif /* HELPER_HPP_ */
