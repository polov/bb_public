/*
 * HandBellBpsEventHandler.cpp
 *
 *  Created on: 28. 12. 2014
 *      Author: martin
 */

#include "HandBellBpsEventHandler.hpp"
#include <QDebug>
#include <bps/sensor.h>

HandBellBpsEventHandler::HandBellBpsEventHandler() :
        left(true), right(true), x_acc(0.0), y_acc(0.0), z_acc(0.0), x_acc_new(0.0), y_acc_new(0.0), z_acc_new(
                0.0)
{
    subscribe(sensor_get_domain());
    bps_initialize();
    //sensor_set_rate(SENSOR_TYPE_ACCELEROMETER, 1000000);

    sensor_set_skip_duplicates(SENSOR_TYPE_ACCELEROMETER, true);
    sensor_request_events(SENSOR_TYPE_ACCELEROMETER);
    qDebug() << "event handler initialized";
}

HandBellBpsEventHandler::~HandBellBpsEventHandler()
{
    bps_shutdown();
    qDebug() << "event handler destroyed";
}

void HandBellBpsEventHandler::event(bps_event_t *event)
{
    int domain = bps_event_get_domain(event);
    if (domain == sensor_get_domain()) {
        int code = bps_event_get_code(event);
        switch (code) {
            case SENSOR_TYPE_ACCELEROMETER:
                sensor_event_get_xyz(event, &x_acc_new, &y_acc_new, &z_acc_new);
                //qDebug() << "SENSOR_TYPE_ACCELEROMETER bps event" << x_acc_new << y_acc_new
                //        << z_acc_new;

                if (x_acc_new > 0.1) {
                    if (left) {
                        //qDebug() << "left";
                        //qDebug() << x_acc_new;
                        left = false;
                        right = true;
                        emit soundRequested("-");
                    }
                } else if (x_acc_new < -0.1) {
                    if (right) {
                        //qDebug() << "right";
                        //qDebug() << x_acc_new;
                        left = true;
                        right = false;
                        emit soundRequested("+");
                    }
                }
                break;
        }
    }
}
