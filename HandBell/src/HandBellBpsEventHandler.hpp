/*
 * HandBellBpsEventHandler.hpp
 *
 *  Created on: 28. 12. 2014
 *      Author: martin
 */

#ifndef HANDBELLBPSEVENTHANDLER_HPP_
#define HANDBELLBPSEVENTHANDLER_HPP_

#include <bps/bps.h>
#include <bb/AbstractBpsEventHandler>

class HandBellBpsEventHandler: public QObject, public bb::AbstractBpsEventHandler
{
    Q_OBJECT
public:
    HandBellBpsEventHandler();
    virtual ~HandBellBpsEventHandler();

    virtual void event(bps_event_t *event);

signals:
    void soundRequested(QString);

private:
    bool left, right;
    float x_acc, y_acc, z_acc;
    float x_acc_new, y_acc_new, z_acc_new;
};

#endif /* HANDBELLBPSEVENTHANDLER_HPP_ */
