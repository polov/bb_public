/*
 * Copyright (c) 2011-2014 BlackBerry Limited.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import bb.cascades 1.3
import com.polov 1.0
import bb.multimedia 1.0

Page {
    id: page
    Container {
        layout: DockLayout {
        }
        ImageView {
            verticalAlignment: VerticalAlignment.Center
            horizontalAlignment: HorizontalAlignment.Center
            imageSource: "asset:///small18.png"
        }
        Label {
            verticalAlignment: VerticalAlignment.Bottom
            horizontalAlignment: HorizontalAlignment.Center
            textFormat: TextFormat.Html
            text: "<div>Image made by <a href=\"http://www.danielbruce.se\" title=\"Daniel Bruce\">Daniel Bruce</a> from <a href=\"http://www.flaticon.com\" title=\"Flaticon\">www.flaticon.com</a> is licensed by <a href=\"http://creativecommons.org/licenses/by/3.0/\" title=\"Creative Commons BY 3.0\">CC BY 3.0</a></div>"
            textStyle.fontSize: FontSize.XXSmall
            multiline: true
        }
    }
    attachedObjects: [
        HandBellBpsEventHandler {
            id: handBellBpsEventHandler
        },
        MediaPlayer {
            id: mediaPlayer
            sourceUrl: "asset:///bell.wav"
        },
        SystemSound {
            id: systemSound
            sound: SystemSound.InputKeypress
        }
    ]
    onCreationCompleted: {
        Application.mainWindow.screenIdleMode = 1;
        handBellBpsEventHandler.soundRequested.connect(page.onSoundRequested);
    }
    function onSoundRequested(value) {
        //console.log(value);
        //systemSound.play();
        //console.log(mediaPlayer.position);
        if(mediaPlayer.position != 0) {
            mediaPlayer.stop();
        }
        mediaPlayer.play();
        
    }
}
